'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _CODEGEN --> _CODEGEN.CMD|
'' +--------------------------------------------------------------------------+
'' | Sphinx     A Compiler for the Propeller Chip    _CODEGEN.SPIN     v0.xxx |
'' +--------------------------------------------------------------------------+
'' |  Authors:                                                                |
'' |  (c) 2009,2010      Michael Park (mpark)       (original Sphinx compiler)|
'' |                     Radical Eye Software (rokicki?)  (fsrw objects)      |
'' |                     Lonesock & Rokicki               (fsrw objects)      |
'' |                     Andy Schenk (Ariba)              (sxkb_pc, sxtv_pc)  |
'' |                     Ray Rodrick (Cluso99)            (TriBlade/RamBlade) |
'' |                     Ray Rodrick (Cluso99)            (addn's to SphinxOS)|
'' |  (c) 2015,2016      Ray Rodrick (Cluso99)      (include in PropOS)       |
'' |  License            MIT License - See end of file for terms of use       |
'' +--------------------------------------------------------------------------+
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' This OS module uses an included module "__MODULE.spin" to perform most of
''  the housekeeping - see this for history, details and acknowledgements.
''
'' This module......
''
''      CODEGEN <filename> [-Vn]
''
''      Reads filename.spn/.tok on SD card, writes to filename.sob
'' +--------------------------------------------------------------------------+
''2009
''April
''  15    Adding file transfer to/from PC via serial
''  16    0.02
''        SendFile bugfix (while blksize == 1024)
''        Tries to run .bin before .eep now.
''June
''  4     Changing name from cl to sphinx. Converting to use sxfs, sxtv.
''  15    sxkb; adding c and cl commands
''  23    Corrected clock mode calculation.
''
''Juli 2009
'' changed for (Prop-)Terminal instead TV and PS2 by Andy Schenk
'' added GET <file> and PUT <file> for File up/download (not working yet)
'' +--------------------------------------------------------------------------+
'' RR20100117 Cluso99: allow specific pins for SD card (passed in basepin)
'' RR20100119 Cluso99: rename SphinxOS_xxx.spin; add isx_in.spin and isx.out.spin for standard I/O
'' RR20100203          v007 failed so retry taking 006 --> 006a
'' RR20100203          pass rendezvous at start
'' RR20100217          v006e
''                     v006n AuxIn/AuxOut 1-pin Kbd & TV working
'' RR20100222          v006q add OPTION to select the I/O drivers for StdIn/StdOut & AuxIn/AuxOut
'' RR20100222          v006s modify "sxfs2.spin" to only load 56 sectors to hub ram (was 63)
''                            this prevents overwriting the screen buffer & rendezvous area
''                     v006t add OPTIONs 2 & 3
''                     v006u RamBlade - kbd does not work at 6.5MHz, ok at 6MHz
'' RR20100224          v011  RamBlade (kbd not wkg @ 6.5MHz - to fix later), back to 6MHz
'' RR20100226          v012  RamBlade - only start StdI/O Driver
'' RR20100226          v015  RamBlade (use OPTION = 0 compiled with 6.5MHz)
''                                    (use OPTION = 1 at 6MHz only for now as Keyboard does not work at 6.5MHz)
'' RR20151228          v020  P8XBlade2 (use OPTION = 0 compiled with 12MHz PLL8)
''--------------------------------------------------------------------------------------------------------
'' Test version
'' RR20160219                rename Sphinx8->Sphinx9 & autorun codegen9 
'' RR20160220                "SphinxS" - works in OS_V101a/works_p8xblade2
''                                     - try   in OS_V101a/sphinx2(tryit)   (sxfs/sxfs2/sxspiq/sxfile/isxfs are diff)
''                                         - copy sxfs/sxfs2/sxspiq/sxfile/isxfs from /works_p8xblade2
''                           SphinxS & CodegenS working together as RUN sphinxs under v1.01a PropOS
'' RR20160220                Combine SphinxS & CodegenS into "COMPILE.SPIN"
'' RR20160221          v021  reverse serPins si/so (_HWDEF & _PCSIO)     (works good)
''                     v022  test hub relocation (works)      copy files to \OS_V101a\compile\
''--------------------------------------------------------------------------------------------------------
''                     v023  rename kwdefs -> _kwdefs, etc 
''                           get _kwdefs, _symbols, _stringx, _eval,   (from OS_V101a)
'' RR20160222                BUG in sxfs readbyte!!! aborts on EOF=-1 but does not abort on <-1 !!!
''                           tidy for comparison w _codegen
''       0223                use: _HWDEF (V101b); 
''       0224                use FAT driver
''       0225                compil3 ifdef fat32
''         26                c3.bin works; _c3.cmd fails pass2
''                           problem is in _methods.parse2 token.Advance aborts with wrong token.xxxx info
''                             perhaps it is due to nesting _tokenrd calls to fat32 driver ??? (yes!!!)
'' RR20160227                move fat access to _tokenrd
''                           try including methods (fixed most problems for fat32)
''         28                try including eval (SOB $C0<>$C1; 1x$00 byte missing)
''                           try fixing workspaces to aid debug (SOB1 byte$04 is +1; byte$1E $00 missing)
''                             SOB1 byte$04[4] is timestamp which will vary :)
''                             _SD_FAT32 WriteString: include null terminator
'' RR20160228                works; rename c5.spin
''                           remove fat16 drivers (#ifdef fat16 & fat32)
''--------------------------------------------------------------------------------------------------------
'' RR20160228                rename _CODEGEN
'' RR20160304 v060  release
'' RR20160307 v061  tidy debugging
                                    

PUB executeCommand(StringPointer) | err, l

  'get the parameters
  str.stringCopy(@tokenFilename, str.tokenizeString(0),constant(MAXFILENAMELENGTH+1)) ' fat filename
  str.stringCopy(@param, str.tokenizeString(0),4)                               ' optional -Vn parameter

  if (tokenFilename[0] == "?" and tokenFilename[1] == 0)                        ' help parameter...
    'Print.StrCR(string(" CODEGEN <filename> [-Vn]"))
    return -2

  if (param[0] == "-" or param[0] == "/") and (param[1] == "v" or param[1] == "V")
    verbosity := param[2] & $0F

  if verbosity => 1
    Print.StrCR(string("CODEGEN v0.61: Compile SPIN/PASM #2"))

  str.stringToUpperCase(@tokenFilename)
  l := strsize( @tokenFilename )
  if tokenFilename[l-4] == "." and tokenFilename[l-3] == "S" and tokenFilename[l-2] == "P" and tokenFilename[l-1] == "N"
    tokenFilename[l-4]~
  if strsize( @tokenFilename ) > 8
    abort string(" -- filename too long")

  str.stringCopy(   @outputFilename, @tokenFilename ,constant(MAXFILENAMELENGTH+1))
  str.stringCopy(   @binFilename,    @tokenFilename ,constant(MAXFILENAMELENGTH+1))
  str.stringAppend( @tokenFilename,  string(".TOK") ,constant(MAXFILENAMELENGTH+1))
  str.stringAppend( @outputFilename, string(".SOB") ,constant(MAXFILENAMELENGTH+1))
  str.stringAppend( @binFilename,    string(".BIN") ,constant(MAXFILENAMELENGTH+1))

'--------------------------------------------------------
' ALL INITIALISATION COMPLETE
'--------------------------------------------------------

  err := \ProcessCommand
  if err
    if err > 0
      Print.Str( err )
    else
      Print.Str( string("*Error ") )
      Print.Decimal( err )
    Print.CRLF
    if long[pSymbolTableSpace] <> $6666_6666
      Print.StrCR( string("**Stack corrupt**") )
      reboot                                            'presume corrupt so reboot
    Print.Decimal( token.LineNumber )
    Print.Char( "," )
    Print.Decimal( token.Column + 1 )
    Print.Char( " " )
    Print.Char( "'" )
    Print.Str( token.Text )
    Print.Char( "'" )
    Print.CRLF
    Print.StrCR( string("**Compile failed**") )
    return -1                                           'fail
  else 
    fat.CloseFile
    bt.Stop    
    checkstack
    Print.StrCR( string(" Compile success.") )
    return 0                                            'success

  
OBJ
  kw      : "spin_kwdefs"
  sym     : "spin_symbols"
  bt      : "spin_bintree"
  str     : "spin_stringx"
  token   : "spin_tokenrd"

CON
  MAXFILENAMELENGTH = 8 + 1 + 3 ' 8.3
  MAXSTRINGBUFFERLENGTH = 32

VAR
  byte tokenFilename[MAXFILENAMELENGTH+1]               ' input .tok file  
  byte outputFilename[MAXFILENAMELENGTH+1]              ' output .sob file  
  byte binFilename[MAXFILENAMELENGTH+1]                 ' output .bin file (just used to empty the .bin)
  byte sobFilename[MAXFILENAMELENGTH+1]                 ' used for child .sob file(s)  
  byte stringBuffer[MAXSTRINGBUFFERLENGTH+1]            ' temp string buffer
  byte param[4]                                         ' optional parameter(s)
  
DAT
verbosity     byte      1       ' set by /V option  default=1
timeFilename  byte      "timestmp.d8a",0

STACKSPACE    long      500<<2 ''======================575<<2   '200
TABLESPACE    long      1200<<2 ''======================1500<<2  '200
OBJECTSPACE   long      0       ' set by process
'OBJECTSPACE   long      4000/4  ' ============================set by process
{
        |<---STACKSPACE--->|<--------TABLESPACE---------->|<-----------OBJECTSPACE------------>|<-I/O RENDEZVOUS etc.
        +-----+------------+-----------+------------------+------+----------------+------------+--+
FFFFF9FF|stack|            |symboltable|                  |header|dat/PASM|methods|            |  |
        +-----+------------+-----------+------------------+------+----------------+------------+--+
        ^                  ^           |tempstorage|      ^                       ^ |stringtemp|  ^
        |                  |                              |                       | ^          ^  |
hub[$A]-+                  |                    pObjSpace-+              pObjWork-+ |          |  +--$8000
         pSymbolTableSpace-+                                            pStringWork-+  pObjTop-+
}
var
' These next 3 vars actually live in methods_spin now.
' word pObjSpace      ' points to area where the compiled object's header and bytecode will live (this pointer doesn't change)
' word pObjWork       ' points to next available byte (this pointer changes as code is compiled)
' word pObjTop        ' points just beyond available object workspace

  word pDatWork       ' like pObjWork but for the DAT segment
  word pSymbolTableSpace
  
pri ProcessCommand | i
{{--}}
  pSymbolTableSpace := word[ $0a ] + STACKSPACE         ' word[$0A] = hub[10] = stack pointer (after 2x FF FF F9 FF)
  methods_set_pObjSpace( pSymbolTableSpace + TABLESPACE )
  methods_set_pObjTop( _hub#_HUB_RESERVED )             ' set bottom of _OS
  OBJECTSPACE := methods_get_pObjTop - methods_get_pObjSpace
{{--}}
{{
'RR20160228 fixed to compare fat16 v fat32 (debug)
  methods_set_pObjTop(   _hub#_HUB_RESERVED                  )                  ' set start of _OS (bottom)
  methods_set_pObjSpace( _hub#_HUB_RESERVED    - OBJECTSPACE )                  ' set start of Object Space
  pSymbolTableSpace := ( methods_get_pObjSpace - TABLESPACE  )                  ' set start of Table Space
}}

{##############################################################################}
  if verbosity => 2
    Print.Str( string(" stack : $") )
    i := methods_get_pObjSpace - TABLESPACE - STACKSPACE
    Print.Hex(i, 8)
    Print.Char(" ")
    Print.Decimal( STACKSPACE )
    Print.CRLF
    i += STACKSPACE
    Print.Str( string(" table : $") )
    Print.Hex(i, 8)                     
    Print.Char(" ")
    Print.Decimal( TABLESPACE )
    Print.CRLF
    Print.Str( string(" object: $") )
    i += TABLESPACE
    Print.Hex(i, 8)
    Print.Char(" ")
    Print.Decimal( OBJECTSPACE )
    Print.CRLF
    Print.Str( string(" top   : $") )
    i += OBJECTSPACE
    Print.Hex(i, 8)
    Print.CRLF
  if OBJECTSPACE < 0
    abort string("*** insufficient space")
{##############################################################################}
    
  ReadTimestamp

  bvarSize~
  wvarSize~
  lvarSize~
  nPubs := 1 ' pub indexes start at 1; 0th entry in object header is not a pub
  nPris~     ' pri indexes have to be compensated later once final nPubs is known
  nObjs~
  pObjList~
  pObjListEnd~

  bt.Init( pSymbolTableSpace, methods_get_pObjSpace )
  sym.Init

  long[ bt.Alloc(4) ] := $6666_6666
  
'------------------------------------------------------------------------------
' Pass 1
'------------------------------------------------------------------------------
  if verbosity => 2
    Print.StrCR(string(" Pass 1"))
    Print.Str( string(" Open ") )
    Print.StrCR( @tokenFilename )
  
  token.Open( @tokenFilename )
  ParsePass1
  token.Close

  CheckStack
  
  varSize := (lvarSize + wvarSize + bvarSize + 3) & !3
  pDatWork := methods_get_pObjSpace + (nPubs + nPris + nObjs) << 2 ' each PUB/PRI/OBJ takes up 4 bytes in the object header
  methods_set_pObjWork( pDatWork + datSize )
  bytefill( methods_get_pObjSpace, 0, pDatWork - methods_get_pObjSpace ) ' zero the header
  
  FixupOffsets( bt.PeekW( sym.GetPSymbolTable ) )
  
'------------------------------------------------------------------------------
' Pass 2
'------------------------------------------------------------------------------
  if verbosity => 2
    Print.StrCR(string(" Pass 2"))
    Print.Str( string(" Open ") )
    Print.StrCR( @tokenFilename )

  token.Open( @tokenFilename )
  ParsePass2  

  if verbosity == 9
    Print.StrCR(string("===ParsePass 2 end"))                      
    Print.Dump(pSymbolTableSpace, 5)                                              
    Print.CRLF
    Print.Dump(pSymbolTableSpace + TABLESPACE, 5)                                 
    Print.CRLF

  token.Close

  methods_set_pObjWork( (methods_get_pObjWork + 3) & !3 ) ' Round up to multiple of 4

'------------------------------------------------------------------------------
' Write SOB File(s)
'------------------------------------------------------------------------------
  if verbosity => 2
    Print.StrCR(string(" Write SOB file(s)"))
  CheckStack

  WriteSobFile


pri CheckStack
  if long[pSymbolTableSpace] <> $6666_6666
    abort string("*Stack overflow")

pri ReadTimestamp | errorString, n  

  errorString := \fat.openFile(@timeFilename, "R" )
  if fat.partitionError == 0
    timestamp := fat.ReadLong + 1
    fat.closeFile
  else
    '-----------------------------------------------------------------
    ' Create the FAT destination file
    errorString := \fat.newFile(@timeFilename)
    checkError(@timeFilename, fat.partitionError, errorString)                   ' if error, display & abort
    '-----------------------------------------------------------------
    timestamp := 1

  fat.OpenFile(@timeFilename, "W" )
  fat.WriteLong( timestamp )    
  fat.CloseFile

  if verbosity => 2
    Print.Str( string(" timestamp: ") )
    Print.Decimal( timestamp )
    Print.CRLF
  
pri WriteSobFile | errorString, p

  if verbosity => 3
    Print.StrCR(string(" Write SOB File"))

  if nPubs == 1                 ' recall that nPubs starts at 1 because of the 0th entry in the object header.
    abort string("*No PUB routines defined")

'-----------------------------------------------------------------
  if verbosity => 2                                                                                                    
    Print.Str( string(" Delete ") )                     ' Delete the .bin file. If we forget to link,                   
    Print.StrCR( @binFilename )                         ' we'll get an error when we try to run the .bin.      
  '-----------------------------------------------------------------
  ' If the FAT Destination file Exists, Delete it
  errorString := \fat.deleteEntry(@binFilename)                                  ' delete file
  if (fat.partitionError <> $0D)                                                 ' if error other than not found,
    checkError(@binFilename, fat.partitionError, errorString)                    '   display & abort
  '-----------------------------------------------------------------
  
  objBinSize := methods_get_pObjWork - methods_get_pObjSpace                    ' This gets stored in .sob header
  word[methods_get_pObjSpace][0] := objBinSize                                  ' This gets stored in object header
  word[methods_get_pObjSpace][1] := nPubs + nPris + nObjs << 8

  ' Compute checksum and hash
  checksum~
  hash~
  p := methods_get_pObjSpace
  repeat objBinSize
    checksum += byte[p]
    hash := (hash <- 1) ^ byte[p++]
  
  if verbosity => 2
    Print.Str( string(" Write ") )
    Print.StrCR( @outputFilename )
  
  wordfill( @numExports, 0, 4 )
  ComputeSobInfo( bt.PeekW( sym.GetPSymbolTable ) )

  '-----------------------------------------------------------------
  errorString := \fat.OpenFile( @outputFilename, "W" )                           ' open xxx.sob file for writing
  if fat.partitionError == $0D                                                   ' if not found,
    errorString := \fat.NewFile( @outputFilename)                                '   create file, and
    errorString := \fat.OpenFile( @outputFilename, "W" )                         '   open xxx.sob file for writing
    if fat.partitionError <> 0                                                   ' if error,
      checkError(@outputFilename, fat.partitionError, errorString)               '   display & abort
  else
    checkError(@outputFilename, fat.partitionError, errorString)                 '   display & abort
  '-----------------------------------------------------------------
  fat.WriteData( @SobFileHeader, SIZEOFSOBHEADER )
  if verbosity == 9
    Print.Str(string(" SOB: "))
    Print_Bytes(@SobFileHeader, SIZEOFSOBHEADER )
  WriteExports( bt.PeekW( sym.GetPSymbolTable ) )
  WriteImports
  fat.WriteData( methods_get_pObjSpace, objBinSize )
  if verbosity == 9
    Print.Str(string(" Obj: "))
    Print_Bytes(methods_get_pObjSpace, objBinSize )                              
  fat.CloseFile

pri Print_Bytes(addr,len) | i                                               
  if len > 0
    repeat i from 0 to len-1
      Print.Hex(byte[addr][i],2)
      Print.Space
    Print.CRLF

pri Print_Hex(val,len)
  Print.Hex(val,len)
  Print.Space
  
pri FixupOffsets( p ) | s, t, v, size, headerSize, q
{{
  Goes through the symbol table and adjusts offsets of word VARs to put them after long VARs
  and puts byte VARs after word VARs.
  Also adjusts PRI method indexes so they come after PUBs, and OBJ indexes to put them after PRIs.
  And now also adjusts DAT labels upward by the size of the object header.
}}
  ifnot p
    return

  FixupOffsets( bt.PeekW( p ) )

  s := p + 4
  s += strsize(s) + 1
  if byte[s] == sym#kVAR_SYMBOL           ' var is followed by
    ++s 
    size := byte[s++]                     '  1-byte size (1, 2, 4) and 2-byte offset
    t~
    if size == 2                   
      t := lvarSize                       ' word vars come after long vars
    elseif size == 1               
      t := lvarSize + wvarSize            ' byte vars come after word vars
    bt.PokeW( s, bt.PeekW(s) + t )        ' adjust var symbol's offset
  elseif byte[s] == sym#kPRI_SYMBOL
    ++s
    byte[s] += nPubs                      ' the PUBs are 1..nPubs-1, the PRIs are nPubs..nPubs+nPris-1
  elseif byte[s] == sym#kOBJ_SYMBOL
    ++s
    byte[s][2] += nPubs + nPris              ' the OBJs are nPubs+nPris..
  elseif byte[s] == sym#kDAT_SYMBOL
    headerSize := (nPubs + nPris + nObjs) << 2
    q := s                                              ' start at global label
    repeat                                              '  and follow the linked list of local labels
      bt.PokeW( q+2, bt.PeekW( q+2 ) + headerSize )     ' adjust label's dp
      ifnot q := bt.PeekW( q+6 )
        quit
      q += strsize(q) + 1                               ' next in the list

  FixupOffsets( bt.PeekW( p + 2 ) )

con
SIZEOFSOBHEADER = 26

dat
SobFileHeader long
              byte      "SOB1"
timestamp     long      0
hash          long      0
numExports    word      0                                                                        
exportSize    word      0
numImports    word      0
importSize    word      0
objBinSize    word      0
checksum      byte      0
{padding}     byte      0      
varSize       word      0  ' total size in bytes of vars (rounded up to multiple of 4)


pri ComputeSobInfo( p ) | pName, type
{{
  Traverses the symbol table and computes exportSize, numExports, importSize, numImports.
  Initialize those variables to 0 before entering this routine.
}}
  ifnot p
    return
   
  pName := p + 4
  type := byte[pName + strsize(pName) + 1]
  if type == sym#kINT_CON_SYMBOL or type == sym#kFLOAT_CON_SYMBOL
    ++numExports
    exportSize += strsize(pName) + 6 ' name + null + type + 4-byte value
  elseif type == sym#kPUB_SYMBOL
    ++numExports
    exportSize += strsize(pName) + 4 ' name + null + type + index + #args
  elseif type == sym#kOBJ_SYMBOL
    pName := bt.PeekW( pName + strsize(pName) + 2 )
    ++numImports
    importSize += strsize(pName) - 4 + 4 ' name (excluding ".SOB") + null + 2-byte count + reserved
        
  ComputeSobInfo( bt.PeekW( p ) )
  ComputeSobInfo( bt.PeekW( p + 2 ) )

pri WriteExports( p ) | pName, pData, type
{{
  Traverses the symbol table and writes exports to output file.
}}

  if verbosity == 9
    Print.StrCR(string(" Exports:"))

  ifnot p
    return
   
  pName := p + 4
  pData := pName + strsize(pName) + 1  
  type := byte[pData]


  if type == sym#kINT_CON_SYMBOL or type == sym#kFLOAT_CON_SYMBOL ' name + null + type + 4-byte value
    fat.WriteString0( pName )
    fat.WriteByte( type )
    fat.WriteLong( bt.PeekL( pData+1 ) )
    if verbosity == 9
      Print.Str( pName )                                                        
      Print.Space                                                               
      Print_Hex( 0, 2)                                    'null terminator      
      Print_Hex( type, 2)                                                       
      Print_Hex( bt.PeekL( pData+1 ), 8 )                                       
      Print.CRLF                                                                
  elseif type == sym#kPUB_SYMBOL ' name + null + type + index + #args
    fat.WriteString0( pName )
    fat.WriteByte( type )
    fat.WriteData( pData+1, 2 ) ' index + #args
    if verbosity == 9
      Print.Str( pName )                                                        
      Print.Space                                                               
      Print_Hex( 0, 2)                                    'null terminator      
      Print_Hex( type, 2)                                                       
      Print_Bytes( pData+1, 2 )                                                 
      Print.CRLF                                                                
        
  WriteExports( bt.PeekW( p ) )
  WriteExports( bt.PeekW( p + 2 ) )

pri WriteImports | pName, pData, type
{{
  Traverses the OBJ list and writes imports to output file.

  We use a list so that the imports are written in the order they appear in the source.
  This is important for OBJ symbols because we want the SOB imports to line up properly.
}}

  if verbosity == 9
    Print.StrCR(string(" Imports:"))

  pName := pObjList
  repeat while pName
  
    pData := pName + strsize(pName) + 1                 ' pName is name of symbol (e.g. "DEBUG")
    ' pData points to symbol's data: 1-byte type + 2-byte pointer to SOB + 1-byte index + 2-byte count + 2-byte link
    pName := bt.PeekW( pData + 1 )                      ' pName is name of .sob file (e.g. "TV_TEXT.SOB")
    fat.WriteData( pName, strsize(pName)-4 )             ' Write name but drop .SOB suffix.
    fat.WriteByte( 0 )                                   ' Write null terminator
    fat.WriteWord( bt.PeekW( pData + 4 ) )               ' Write count
    fat.WriteByte( 0 )                                   ' Write reserved byte
    if verbosity == 9
      Print_Bytes( pName, strsize(pName)-4 )                                     
      Print_Hex( 0, 2 )                                   'null                  
      Print.Hex( bt.PeekW( pData + 4 ), 4 )                                      
      Print_Hex( 0, 2 )                                   'null                  
      Print.CRLF                                                                 

    pName := bt.PeekW( pData + 6 )                      ' link to next

pri ParsePass1
  eval_set_pass( 1 )
  InitDat
  ParseCon1

  repeat while token.Type <> kw#kEOF
    ifnot token.IsBlockDesignator
      abort string("*Syntax error 1a")
    if token.Column
      abort string("*Block designator not in column 1")
    if token.AdvanceIf( kw#kCON )
      ParseCon1
    elseif token.AdvanceIf( kw#kDAT )
      ParseDat1
    elseif token.AdvanceIf( kw#kOBJ )
      ParseObj1
    elseif token.AdvanceIf( kw#kPRI )
      ParseMethod1( sym#kPRI_SYMBOL )
    elseif token.AdvanceIf( kw#kPUB )
      ParseMethod1( sym#kPUB_SYMBOL )
    elseif token.AdvanceIf( kw#kVAR )
      ParseVar1
    else
      abort string("*Syntax error 1b")


pri ParsePass2

  eval_set_pass( 2 )
  InitDat
  ParseCon2

  repeat while token.Type <> kw#kEOF
    CheckStack
    ifnot token.IsBlockDesignator
      abort string("*Syntax error 2a")
    if token.AdvanceIf( kw#kCON )
      ParseCon2
    elseif token.AdvanceIf( kw#kDAT )
      ParseDat2
    elseif token.AdvanceIf( kw#kOBJ )
      SkipBlock
    elseif token.AdvanceIf( kw#kPRI )
      methods_Parse2( sym#kPRI_SYMBOL )
    elseif token.AdvanceIf( kw#kPUB )
      methods_Parse2( sym#kPUB_SYMBOL )
    elseif token.AdvanceIf( kw#kVAR )
      SkipBlock
    else
      abort string("*Syntax error 2b")
        
pri SkipBlock
  repeat until token.IsBlockDesignator or token.Type == kw#kEOF
    token.Advance
          
pri ParseCon1 | o, p, inc, v
  o~ 
  token.AdvanceIf( kw#kEOL )
  repeat
    inc := 1
    if token.AdvanceIf( kw#kOCTOTHORP )
      o := eval_EvaluateExpression( 0 )
    elseif token.IsId                                   ' Id +?
      sym.AddToTable( token.Text )
      byte[ p := bt.Alloc(1) ] := sym#kINT_CON_SYMBOL   ' p points to symbol type byte
                                                        ' in case we have to patch the type retroactively
      token.Advance
      if token.AdvanceIf( kw#kEQUAL )                   ' Id =
        v := eval_TryToEvaluateExpression( 0 )
        if eval_Succeeded
          bt.PokeL( bt.Alloc(4), v )
        else                    ' it is undefined
          byte[p] := sym#kUNDEFINED_CON_SYMBOL
          bt.Alloc(4)           ' but we should allocate space for the eventual value.
      elseif token.AdvanceIf( kw#kLBRACKET )            ' Id [
        inc := eval_EvaluateExpression( 0 )
        token.Eat( kw#kRBRACKET )
        bt.PokeL( bt.Alloc(4), o )
        o += inc
      else                                              ' Plain Id
        bt.PokeL( bt.Alloc(4), o )
        o += inc
    else
      quit
    if token.Type == kw#kCOMMA or token.Type == kw#kEOL
      token.Advance
    else
      abort string("*Syntax error 1c")            

pri ParseCon2 | o, p, inc

  o~ 
  token.AdvanceIf( kw#kEOL )
  repeat
    inc := 1
    if token.AdvanceIf( kw#kOCTOTHORP )
      o := eval_EvaluateExpression( 0 )
    elseif token.IsId                                   ' Id +?
      p := sym.SymbolLookup( token.Text )
      token.Advance
      if token.AdvanceIf( kw#kEQUAL )                   ' Id =
        bt.PokeL( p+1, eval_EvaluateExpression( 0 ) )
        byte[p] := sym#kINT_CON_SYMBOL
      elseif token.AdvanceIf( kw#kLBRACKET )            ' Id [
        inc := eval_EvaluateExpression( 0 )
        token.Eat( kw#kRBRACKET )
        o += inc
      else                                              ' Plain Id
        o += inc
    else
      quit
    if token.Type == kw#kCOMMA or token.Type == kw#kEOL
      token.Advance
    else
      abort string("*Syntax error 2c")             
  
var
  word dp             ' dp is the address in DAT space of current source line.
                      ' dp = pObjWork - pObjSpace
  word ooo            ' dp + ooo = cogx4; cogx4 / 4 = cog address of current source line.
  word pGlobalLabel   ' points to most-recently defined global label; used as the head of a linked list of local labels
  byte alignment      ' 1, 2, or 4
{
     a DAT line consists of (in rough pseudo-bnf) one of the following:
     1) [label | :label] ORG|RES [ <expr> ]
     2) [label | :label] <size> <expr>* (with commas between <expr> -- yeah, yeah, I said it was rough)
     3) [label | :label] [ <cond> ] <pasm op> <dst> , <src> <effect>*

  As we process DAT statements and emit bytes, pDatWork increases, always pointing where the next byte will go.
  dp = pDatWork - pObjSpace (i.e., dp 0 is the beginning of DAT space)
  cogx4 = dp + ooo, where ooo is updated each time an ORG directive is encountered:
  ooo = org_expression * 4 - dp
  Example:
  Let us say pObjSpace is 1000. The object header starts there. Say it takes up 100 bytes, so pDatWork is 1100.
  In terms of DAT space, dp = 1100 - 1000 = 100.
  Say there's an ORG 100, so ooo = 100 * 4 - 100 = 300.
  A label defined at this point would have dp = 100, cogx4 = 100 + 300 = 400 (divide this by 4 to get cog address 100).
  Suppose we assemble a PASM instruction and emit the four bytes.
  Now pDatWork = 1104, dp = 104, and cogx4 = 104 + 300 = 404 (cog address 101).
}
pri InitDat
  if eval_get_pass == 1
    dp~
    ooo~
  else
    ooo := methods_get_pObjSpace - pDatWork '= -dp
    
  pGlobalLabel~
  alignment := 1
  
pri ParseDat1 | pLabelData, p, v, size, count, dpInc, temp, cogx4
  eval_EnteringDat
  cogx4~
  repeat
    pLabelData~
    if token.AdvanceIf( kw#kEOL )
      next

    if token.IsId                                        ' label?
      if byte[token.Text] == ":"                         ' local label?
        ' Traverse the list of locals to check for duplicates
        p := pGlobalLabel
        repeat while p := bt.PeekW( p + 6 )             ' p points to string part of local label
          if strcomp( p, token.Text )
            abort string("*Duplicate local label")
          p += strsize(p) + 1                           ' Increment p past string part to data part
        
        p := bt.Alloc( strsize( token.Text ) + 9 )      ' p points to 
        str.stringCopyUnsafe( p, token.Text )           '  local label's text followed by
        pLabelData := p + strsize( token.Text ) + 1     '   8 bytes of label data (to be filled in later)
        byte[ pLabelData ] := sym#kDAT_SYMBOL           ' type
        bt.PokeW( pLabelData+6, bt.PeekW(pGlobalLabel+6) ) ' pointer to local labels
        ifnot pGlobalLabel
          abort string("*No preceding global label")
        bt.PokeW( pGlobalLabel+6, p )                   ' Insert this local label at head of list
        
      else                                              ' plain old label
        sym.AddToTable( token.Text )
        pLabelData := bt.Alloc( 8 )                     ' local label data
        byte[ pLabelData ] := sym#kDAT_SYMBOL           ' type
        bt.PokeW( pLabelData+6, 0 )                     ' pointer to local labels
         
        pGlobalLabel := pLabelData
      
      token.Advance
       
    dpInc~
    if token.AdvanceIf( kw#kORG )                       ' Case 1a: ORG [ <expr> ]
      alignment := 4
      PadToAlignment
      temp~                                             ' default ORG is 0
      if token.Type <> kw#kEOL
        temp := eval_EvaluateExpression( pGlobalLabel ) << 2
      ooo := temp - dp
      cogx4 := dp + ooo
    elseif token.AdvanceIf( kw#kRES )                   ' Case 1b: RES [ <expr> ]
      alignment := 4
      PadToAlignment
      temp := 4                                         ' default RES is 1 long
      if token.Type <> kw#kEOL
        temp := eval_EvaluateExpression( pGlobalLabel ) << 2
      cogx4 := dp + ooo
      ooo += temp
    elseif token.AdvanceIf( kw#kFIT )
      temp := 496
      if token.Type <> kw#kEOL
        temp := eval_EvaluateExpression( pGlobalLabel )
      if cogx4 > temp << 2
        abort string("*Origin exceeds FIT limit")
    elseif token.IsSize                                 ' Case 2. BYTE/WORD/LONG <expr>*
      alignment := |< (token.Type - kw#kBYTE) ' 1/2/4
      PadToAlignment
      cogx4 := dp + ooo
      token.Advance
      if token.Type <> kw#kEOL
        repeat
          size := alignment
          if token.IsSize
            size := |< (token.Type - kw#kBYTE) ' 1/2/4 -- just size
            token.Advance
          v := eval_TryToEvaluateExpression( pGlobalLabel )
          count := 1
          if token.AdvanceIf( kw#kLBRACKET )
            count := eval_EvaluateExpression( pGlobalLabel )
            token.Eat( kw#kRBRACKET )
          dpInc += count * size
        while token.AdvanceIf( kw#kCOMMA )
    elseif token.IsCond or token.IsPasm                 ' Case 3. [ <cond> ] <pasm op>
      alignment := 4
      PadToAlignment
      cogx4 := dp + ooo
      dpInc := 4
      repeat 
        token.Advance
      until token.Type == kw#kEOL
    elseif token.Type <> kw#kEOL
      abort string("*Syntax error 3")
      
    if pLabelData               ' Was a label defined on the current source line?
      byte[pLabelData+1] := alignment                   ' data size
      bt.PokeW( pLabelData+2, dp )                      ' Address in DAT space
      bt.PokeW( pLabelData+4, cogx4 )                   ' Cog address x 4
                                                        ' The local label linked list pointer was filled in previously
    dp += dpInc
    cogx4 += dpInc
    
  until token.IsBlockDesignator or token.Type == kw#kEOF

  datSize := dp
  
  eval_LeavingDat
  
pri ParseDat2 | pLabelData, p, v, size, count, temp, cond, pasmOp, ds, effect, oooInc

  eval_EnteringDat
  oooInc~
  repeat
    pLabelData~
    if token.AdvanceIf( kw#kEOL )
      next

    if token.IsId                                       ' label?
      if byte[token.Text] == ":"                        ' local label?
        ' Traverse the list of locals to find this one.
        p := pGlobalLabel
        repeat while p := bt.PeekW( p + 6 )             ' p points to string part of local label
          if strcomp( p, token.Text )
            quit
          p += strsize(p) + 1                           ' Increment p past string part to data part
        pLabelData := p + strsize(p) + 1
      else                                              ' plain old label
        pLabelData := sym.SymbolLookup( token.Text )
        pGlobalLabel := pLabelData

      token.Advance

    dp := pDatWork - methods_get_pObjSpace
    
    if token.AdvanceIf( kw#kORG )                       ' Case 1: ORG <expr>
      temp~                                             ' default ORG is 0
      alignment := 4
      PadToAlignment
      if token.Type <> kw#kEOL
        temp := eval_EvaluateExpression( pGlobalLabel ) << 2
      ooo := temp - dp
    elseif token.AdvanceIf( kw#kRES )                   ' Case 1b: RES [ <expr> ]
      alignment := 4
      PadToAlignment
      oooInc := 4                                       ' default RES is 1 long
      if token.Type <> kw#kEOL
        oooInc := eval_EvaluateExpression( pGlobalLabel ) << 2
    elseif token.AdvanceIf( kw#kFIT )                   ' ignore FIT in pass 2
      if token.Type <> kw#kEOL
        eval_EvaluateExpression( pGlobalLabel )
     
    elseif token.IsSize                                 ' Case 2. BYTE/WORD/LONG <expr>*
      alignment := |< (token.Type - kw#kBYTE) ' 1/2/4
      PadToAlignment
      token.Advance
      if token.Type == kw#kEOL
        next
      repeat
        size := alignment
        if token.IsSize
          size := |< (token.Type - kw#kBYTE) ' 1/2/4 -- just size
          token.Advance
        v := eval_EvaluateExpression( pGlobalLabel )
        count := 1
        if token.AdvanceIf( kw#kLBRACKET )
          count := eval_EvaluateExpression( pGlobalLabel )
          token.Eat( kw#kRBRACKET )
        repeat count
          temp := v
          repeat size
            EmitDat( temp )
            temp >>= 8
      while token.AdvanceIf( kw#kCOMMA )
    elseif token.IsCond or token.IsPasm
      alignment := 4
      PadToAlignment
      cond~~
      if token.IsCond
        cond := token.GetCond
        token.Advance
      ifnot token.IsPasm
        abort string("Expected PASM instruction")
      ds := %11
      case token.Type
        kw#kAND: pasmOp := $60bc0000                    ' Special handling for double-duty PASM mnemonics
        kw#kOR: pasmOp := $68bc0000
        kw#kWAITCNT: pasmOp := $f8bc0000
        kw#kWAITPEQ: pasmOp := $f03c0000
        kw#kWAITPNE: pasmOp := $f43c0000
        kw#kWAITVID: pasmOp := $fc3c0000
        kw#kCOGID: pasmOp := $0cfc0001
          ds := %10
        kw#kCOGINIT: pasmOp := $0c7c0002
          ds := %10
        kw#kCOGSTOP: pasmOp := $0cfc0003
          ds := %10
        kw#kCLKSET: pasmOp := $0c7c0000
          ds := %10
        other:
          pasmOp := token.GetPasmOp
          ds := token.GetPasmDS

      if token.Type == kw#kCALL                         ' special handling for CALL instruction
        token.Advance
        token.Eat( kw#kOCTOTHORP )
        ifnot token.IsId
          abort string("Expected return label")
        str.stringCopy( @stringBuffer, token.Text,constant(MAXSTRINGBUFFERLENGTH+1))
        str.stringAppend( @stringBuffer, string("_RET") ,constant(MAXSTRINGBUFFERLENGTH+1))
        ifnot p := sym.SymbolLookup( @stringBuffer )
          abort string("Return label not found")
        if byte[p] <> sym#kDAT_SYMBOL
          abort string("Non-DAT return label")
        temp := bt.PeekW( p+4 )
        if temp & 3
          abort string("Return label is not long")
        pasmOp |= ((temp >> 2 & $1ff) << 9) | (eval_EvaluateExpression( pGlobalLabel ) & $1ff) ' combine destination and source

      else                                              ' regular PASM instruction
        token.Advance
        if ds & %10                                     ' destination operand
          pasmOp |= (eval_EvaluateExpression( pGlobalLabel ) & $1ff) << 9
        if ds == %11
          token.Eat( kw#kCOMMA )
        if ds & %01
          if token.AdvanceIf( kw#kOCTOTHORP )           ' immediate?
            pasmOp |= |< 22                             ' set I
          pasmOp |= eval_EvaluateExpression( pGlobalLabel ) & $1ff           ' source operand
        
      effect~
      if token.Type <> kw#kEOL
        repeat
          ifnot token.IsEffect
            abort string("Expected PASM effect")
          if token.GetEffect == 8                       ' NR?
            effect &= %110                              ' clear R
          else
            effect |= token.GetEffect
          token.Advance
        while token.AdvanceIf( kw#kCOMMA )

      if pasmOp == 0 and (cond <> -1 or effect )
        abort string("Conditions and effects not allowed on NOP")
        
      pasmOp |= (effect << 23)
      if cond <> -1
        pasmOp := pasmOp & %111111_1111_0000_111111111_111111111 | (cond << 18)
        
      repeat 4
        EmitDat( pasmOp )
        pasmOp >>= 8
    
    if pLabelData               ' Was a label defined on the current source line?
      if byte[pLabelData+1] <> alignment                { data size
}       or bt.PeekW( pLabelData+2 ) <> dp               { Address in DAT space
}         or bt.PeekW( pLabelData+4 ) <> dp + ~~ooo     ' Cog address x 4
{{
        if verbosity => 3
          Print.Decimal(byte[pLabelData+1])
          Print.Char(" ")
          Print.Decimal(alignment)
          Print.CRLF
          Print.Decimal(bt.PeekW( pLabelData+2 ))
          Print.Char(" ")
          Print.Decimal(dp)
          Print.CRLF
          Print.Decimal(bt.PeekW( pLabelData+4 ))
          Print.Char(" ")
          Print.Decimal(dp+~~ooo)
          Print.CRLF
}}
        abort string("Phase error")
    ooo += oooInc~
    
  until token.IsBlockDesignator or token.Type == kw#kEOF
  eval_LeavingDat

pri PadToAlignment
  repeat (alignment - dp) & (alignment - 1)             ' number of bytes needed to pad to new alignment
    ++dp
    if eval_get_pass == 2
      EmitDat( 0 )              ' padding

pri EmitDat( b )
  byte[pDatWork++] := b
{{ #ifdef dbgcg
  Print.Hex(b,2)
  Print.Space
  Print.ASCII(b)
  Print.StrCR(string(" <EmitDat"))
}} '' #endif 'dbgcg

var
  word pObjList       ' OBJ symbols are linked together in a list
  word pObjListEnd    ' Pointer to the last OBJ; append new OBJs at the end
  
pri ParseObj1 | i, count, pObj, pSob, pObjName
{{
  <objId> { [ <const expr> ] } : <sobId>
}}
  token.AdvanceIf( kw#kEOL )
  repeat while token.IsId
    pObjName := bt.Alloc( 0 ) + 4 ' Hacky way to get pointer to the name field of the symbol we're about to add.
    sym.AddToTable( token.Text )
    pObj := bt.Alloc( 8 )       ' type (1), ptr to sym table (2), index(1), count (2), ptr to next (2)
    byte[pObj] := sym#kOBJ_SYMBOL

    token.Advance

    count := 1
    if token.AdvanceIf( kw#kLBRACKET )
      count := eval_EvaluateExpression( 0 )
      token.Eat( kw#kRBRACKET )

    token.Eat( kw#kCOLON )

    byte[pObj][3] := nObjs
    nObjs += count
    bt.PokeW( pObj+4, count )

    ' Link this OBJ into the list of OBJs
    if pObjListEnd                                      ' this points to last OBJ name field
      pObjListEnd += strsize(pObjListEnd)+1             ' now this points to OBJ data
      bt.PokeW( pObjListEnd+6, pObjName )               ' make a link to new OBJ name field

    pObjListEnd := pObjName
    bt.PokeW( pObj+6, 0 )

    ifnot pObjList
      pObjList := pObjName
      
    i~
    repeat
      ifnot token.IsIntLiteral
        abort string("*Syntax error 4")
      sobFilename[i++] := token.Value
      if i > MAXFILENAMELENGTH-4 ' have to leave room for ".sob" suffix
        abort string(" SOB filename too long")
      token.Advance
    while token.AdvanceIf( kw#kCOMMA )

    sobFilename[i]~
    str.stringToUpperCase( @sobFilename )
    str.stringAppend( @sobFilename, string(".SOB"),constant(MAXFILENAMELENGTH+1))

    ifnot pSob := sym.SymbolLookup( @sobFilename )
      sym.AddToTable( @sobFilename )
      byte[ pSob := bt.Alloc(3) ] := sym#kSOB_SYMBOL ' type (1), ptr to sym table (2)
      bt.PokeW( pSob+1, 0 )
      ReadSobFile( pSob+1 )
    bt.PokeW( pObj+1, pSob - strsize(@sobFilename) - 1 )

    token.Eat( kw#kEOL )

con
SOBFILEFORMATVERSION = "S" + "O" << 8 + "B" << 16 + "1" << 24 ' "SOB1"

pri ReadSobFile( pSobTable ) | p, t

  if verbosity => 3
    Print.Str( string("Reading ") )
    Print.StrCR( @sobFilename )

  if fat.OpenFile( @sobFilename, "R" ) <> 0
    Print.Str( @sobFilename )
    abort string(" -- couldn't open")
  if fat.Readlong <> SOBFILEFORMATVERSION                ' SOB file format version
    abort string("*Unrecognized SOB file format")
  fat.Readlong                                           ' timestamp
  fat.ReadLong                                           ' hash
  numExports := fat.ReadWord                             ' number of exports
  exportSize := fat.ReadWord                             ' size of exports segment  
  fat.ReadWord                                           ' number of imports
  importSize := fat.ReadWord                             ' size of imports segment
  fat.ReadWord                                           ' size of bytecode segment   
  fat.ReadByte                                           ' checksum
  fat.ReadByte                                           ' padding
  fat.ReadWord                                           ' size of sob's VAR space

  repeat numExports
'   fat.ReadString( @stringBuffer, MAXSTRINGBUFFERLENGTH )
'   str.stringToUpperCase( @stringBuffer )
    ReadStringToUpperCase( @stringBuffer, MAXSTRINGBUFFERLENGTH )
    bt.AddToTable( @stringBuffer, pSobTable )
    case t := fat.ReadByte
      sym#kINT_CON_SYMBOL, sym#kFLOAT_CON_SYMBOL:
        p := bt.Alloc( 5 )
        byte[p++] := t
        bt.PokeL( p, fat.ReadLong )
      sym#kPUB_SYMBOL:
        p := bt.Alloc( 3 )
        byte[p++] := t
        byte[p++] := fat.ReadByte                        ' method index
        byte[p++] := fat.ReadByte                        ' #params
  fat.CloseFile
   
pri ReadStringToUpperCase( p, MAXLENGTH ) | ch
  repeat MAXLENGTH + 1
    fat.ReadData(@ch, 1)
    if "a" =< ch and ch =< "z"
      ch -= constant("a" - "A")
    ifnot byte[p++] := ch
      return
  abort string("*String too long")

pri ParseMethod1( type ) | nargs
' type = sym#kPRI_SYMBOL or sym#kPUB_SYMBOL
  ifnot token.IsId
    abort string("*Expected ID")
  sym.AddToTable( token.Text )
  token.Advance
  nargs~
  if token.AdvanceIf( kw#kLPAREN )
    repeat
      ++nargs
      ifnot token.IsId
        abort string("*Expected ID")
      token.Advance
    while token.AdvanceIf( kw#kCOMMA )
    token.Eat( kw#kRPAREN )
    if nargs > 255
      abort string("*Too many parameters")
  byte[ bt.Alloc(1) ] := type
  if type == sym#kPUB_SYMBOL
    byte[ bt.Alloc(1) ] := nPubs++
  else
    byte[ bt.Alloc(1) ] := nPris++
  byte[ bt.Alloc(1) ] := nargs

  SkipBlock

var
  word bvarSize       ' size in bytes of byte var area
  word wvarSize       ' size in bytes of word var area
  word lvarSize       ' size in bytes of long var area
  byte nPubs, nPris
  word nObjs
  word datSize        ' size in bytes of DAT area
  
pri ParseVar1 | s, t, dim                
  token.AdvanceIf( kw#kEOL )
  repeat while token.IsSize
    if token.Type == kw#kBYTE
      s~
    elseif token.Type == kw#kWORD
      s := 1
    elseif token.Type == kw#kLONG
      s := 2        
    token.Advance
    repeat
      ifnot token.IsId
        abort string("*Expected ID")
      sym.AddToTable( token.Text )
      token.Advance
      byte[ bt.Alloc(1) ] := sym#kVAR_SYMBOL
      byte[ bt.Alloc(1) ] := |< s
      if token.AdvanceIf( kw#kLBRACKET )
        dim := eval_EvaluateExpression( 0 )
        token.Eat( kw#kRBRACKET )
      else
        dim := 1
      bt.PokeW( bt.Alloc(2), bvarSize[s] )
      bvarSize[s] += dim << s
    while token.AdvanceIf( kw#kCOMMA )
    token.Eat( kw#kEOL )

''============================= METHODS BEGIN =================================

var
  long  pLocalSymbols
  byte  repeatDepth   ' nested REPEAT depth. 0 => at top level
  word  pStringWork
  word  pObjSpace
  word  pObjWork
  word  pObjTop
  
pub methods_get_pObjSpace
  return pObjSpace

pub methods_set_pObjSpace( p )
  pObjSpace := p

pub methods_get_pObjWork
  return pObjWork

pub methods_set_pObjWork( p )
  pObjWork := p

pub methods_get_pObjTop
  return pObjTop

pub methods_set_pObjTop( p )
  pObjTop := p

pub methods_Parse2( type ) | pSym, pHeaderEntry, dim, localOffset, addedResult, n
{{
  Second pass processing of PRI and PUB methods_
}}
  pLocalSymbols := bt.Alloc( 2 )                        ' Local symbol table space will be freed at the end of this method
  bt.PokeW( pLocalSymbols, 0 )

  pStringWork := pObjTop                                ' temp string storage starts at high end of memory

  pSym := sym.SymbolLookup( token.Text )
  
  pHeaderEntry := pObjSpace + byte[pSym][1] << 2        ' method index * 4
  
' n := token.Advance            ' past method name     
  n := \token.Advance           ' past method name      ' need to trap abort for fat32 !!! ???still???

  localOffset := 4 ' LOC+0 is RESULT, LOC+4, +8, +12... are params, beyond that are locals
  ' parse parameter names
  if token.AdvanceIf( kw#kLPAREN )
    repeat
      ifnot token.IsId
        abort string("Expected ID")
      if sym.SymbolLookup( token.Text )
        abort string("Expected unique parameter name")
      bt.AddToTable( token.Text, pLocalSymbols )
      bt.PokeW( bt.Alloc(2), localOffset )
      localOffset += 4
      token.Advance
    while token.AdvanceIf( kw#kCOMMA )
    token.Eat( kw#kRPAREN )

  ' parse return value
  addedResult~
  if token.AdvanceIf( kw#kCOLON )
    ifnot token.IsId or token.Type == kw#kRESULT
      abort string("Expected ID")
    if sym.SymbolLookup( token.Text )
      abort string("Expected unique result name")
    bt.AddToTable( token.Text, pLocalSymbols )
    bt.PokeW( bt.Alloc(2), 0 )
    addedResult := token.Type == kw#kRESULT
    token.Advance

  ifnot addedResult
    bt.AddToTable( string("RESULT"), pLocalSymbols )
    bt.PokeW( bt.Alloc(2), 0 )
    
  ' parse local variables
  if token.AdvanceIf( kw#kBAR )
    ifnot token.IsId
      abort string("Expected ID")
    repeat
      if sym.SymbolLookup( token.Text )
        abort string("Expected unique variable name")
      bt.AddToTable( token.Text, pLocalSymbols )
      bt.PokeW( bt.Alloc(2), localOffset )
      token.Advance
      if token.AdvanceIf( kw#kLBRACKET )
        dim := eval_EvaluateExpression( 0 )
        token.Eat( kw#kRBRACKET )
      else
        dim := 1
      localOffset += dim << 2      
    while token.AdvanceIf( kw#kCOMMA )   
  token.Eat( kw#kEOL )

  word[pHeaderEntry][0] := pObjWork - pObjSpace
  ' Now store size of locals in header.
  word[pHeaderEntry][1] := localOffset - (byte[pSym][2] + 1) << 2

  repeatDepth~
  pFloatingReferences~
  
  CompileStatements( -1 )

  Emit( $32 ) ' RETURN

  StringCleanup
  ResolveFloatingReferences

  bt.Alloc( pLocalSymbols - bt.Alloc(0) )               ' hack to free local symbol table memory
                                                        ' should be fine as long as no permanent allocations were made

pri CompileStatements( col ) | p, op, nArgs
  repeat until token.IsBlockDesignator or token.Type == kw#kEOF or token.Column =< col
    if token.Type == kw#kIF or token.Type == kw#kIFNOT
      CompileIf
    elseif token.Type == kw#kREPEAT 
      CompileRepeat
    elseif token.Type == kw#kCASE   
      CompileCase
    elseif token.AdvanceIf( kw#kRETURN )
      if token.Type == kw#kEOL
        Emit( $32 )             ' RETURN
      else
        CompileExpression( 13, true, false )
        Emit( $33 )             ' RETVAL
      token.Eat( kw#kEOL )
    elseif token.AdvanceIf( kw#kABORT )
      if token.Type == kw#kEOL
        Emit( $30 )             ' ABORT
      else
        CompileExpression( 13, true, false )
        Emit( $31 )             ' ABOVAL
      token.Eat( kw#kEOL )
    elseif token.AdvanceIf( kw#kNEXT )
      ifnot repeatDepth
        abort string("No enclosing REPEAT")
      Emit( $04 )                                       ' GOTO
      MakeFloatingReference( pObjWork, nextDest )
      Emit( $80 )               ' placeholder
      Emit( $00 )
      token.Eat( kw#kEOL )
    elseif token.AdvanceIf( kw#kQUIT )
      ifnot repeatDepth
        abort string("No enclosing REPEAT")
      Emit( $04 )                                       ' GOTO
      MakeFloatingReference( pObjWork, quitDest )
      Emit( $80 )               ' placeholder
      Emit( $00 )
      token.Eat( kw#kEOL )
    elseif token.IsStmtFn
      op := token.GetSpinOpcode
      nArgs := token.GetNumArgs
      token.Advance
      CompileArgs( nArgs )
      Emit( op )
      token.Eat( kw#kEOL )
    elseifnot token.AdvanceIf( kw#kEOL )
      CompileExpression( 13, false, false )
      token.Eat( kw#kEOL )
   
pri CompileIf | col, jumpOp, pIfJump, pElseJump
{{
  Current token is IF, IFNOT, ELSEIF, or ELSEIFNOT.
}}
  col := token.Column
  if token.Type == kw#kIF or token.Type == kw#kELSEIF
    jumpOp := $0a               ' JPF
  else
    jumpOp := $0b               ' JPT
  token.Advance
  CompileExpression( 13, true, false )
  pIfJump := MakeForwardJump( jumpOp )
  token.Eat( kw#kEOL )
  CompileStatements( col )
  if token.Column == col
    if token.AdvanceIf( kw#kELSE )
      pElseJump := MakeForwardJump( $04 )               ' GOTO   
      ResolveForwardJump( pIfJump )
      token.Eat( kw#kEOL )
      CompileStatements( col )
      ResolveForwardJump( pElseJump )
      return
    if token.Type == kw#kELSEIF or token.Type == kw#kELSEIFNOT
      pElseJump := MakeForwardJump( $04 )               ' GOTO
      ResolveForwardJump( pIfJump )
      CompileIf
      ResolveForwardJump( pElseJump )
      return
  ResolveForwardJump( pIfJump )

pri MakeForwardJump( jumpOp )
{{
  Emits jump instruction with 2-byte jump offset to be filled in by a subsequent call to ResolveJump.
  Returns a pointer to the jump instruction; pass this pointer to ResolveJump.
}}
  result := pObjWork
  Emit( jumpOp )
  Emit( $00 )
  Emit( $00 )

pri ResolveForwardJump( pJump ) | d, p
{{
  Resolves the jump instruction at pJump. If that instruction is part of a linked list
  of jumps (as in a bunch of QUIT jumps out of a loop) then this method resolves all
  the linked jumps.
}}
  repeat while pJump
    d := pObjWork - pJump - 3
    p := bt.PeekW( pJump + 1 )  
    if d =< 16383
      byte[pJump][1] := $80 + (d >> 8)
      byte[pJump][2] := d
    else
      abort string("Jump out of range(1)")
    pJump := p
    
pri MarkBackwardJumpDestination
  return pObjWork
  
pri ResolveBackwardJump( jumpOp, pDest ) | d
{{
  Emits a jump instruction to the location pointed to by pDest.
}}
  Emit( jumpOp ) 
  d := pDest - pObjWork - 1     ' d < 0
  if d => -64
    Emit( d + 128 )
  elseif --d => -16384          ' decrementing d to compensate for emitting an additional byte
    Emit( (d >> 8) + 256 )
    Emit( d )
  else
    abort string("Jump out of range(2)")  

var
  word nextDest       ' destination for NEXT
  word quitDest       ' destination for QUIT
dat
uniqueDest    word      $8000
  
pri CompileRepeat | col, pJump, saveNextDest, saveQuitDest, pQuitJump, jumpOp, topDest, fromAllowed, p, q {
}, pFromVarStart      { points to start of code for REPEAT FROM variable
}, pFromVarEnd        { points to end   "
}, pFromVarOp         { points to the PUSH op that has to change to USING
}, pFromToExprStart   { points to start of code for FROM..TO.. expressions
}, pFromToExprEnd     { points to end   "
}, pStepExprStart     { points to start of STEP expression
}, pStepExprEnd       { points to end   "
}
{{
  Current token is REPEAT
}}
  saveNextDest := nextDest
  saveQuitDest := quitDest
  ++repeatDepth
  nextDest := uniqueDest++
  quitDest := uniqueDest++
  
  col := token.Column
  token.Advance
  if token.AdvanceIf( kw#kEOL ) ' Uncounted REPEAT or REPEAT ... WHILE/UNTIL -- can't tell until the end of the loop
    topDest := MarkBackwardJumpDestination
    CompileStatements( col )

    jumpOp := $04               ' GOTO (used if this is an uncounted REPEAT)    
    if token.Column == col
      if token.AdvanceIf( kw#kWHILE )
        jumpOp := $0b           ' JPT
      elseif token.AdvanceIf( kw#kUNTIL )
        jumpOp := $0a           ' JPF
      if jumpOp == $04
        Retarget( nextDest, topDest )                   ' for plain uncounted REPEAT, NEXT jumps to the top of the loop.
      else      
        Retarget( nextDest, pObjWork )                  ' for REPEAT ... WHILE/UNTIL <expr>, NEXT jumps to the expression.
        CompileExpression( 13, true, false )
        token.Eat( kw#kEOL )
        
    ResolveBackwardJump( jumpOp, topDest ) ' back to top of loop
    Retarget( quitDest, pObjWork )
    
  elseif token.Type == kw#kWHILE or token.Type == kw#kUNTIL
    jumpOp := $0a               ' JPF
    if token.Type == kw#kUNTIL
      jumpOp := $0b             ' JPT
    token.Advance
    nextDest := topDest := MarkBackwardJumpDestination
    CompileExpression( 13, true, false )
    token.Eat( kw#kEOL )
    pJump := MakeForwardJump( jumpOp )
    CompileStatements( col )
    ResolveBackwardJump( $04, topDest )                 ' GOTO top of loop
    ResolveForwardJump( pJump )
    Retarget( quitDest, pObjWork )
          
  else                                                  ' REPEAT expr or REPEAT var FROM... 
    pFromVarStart := pObjWork
    fromAllowed := CompileExpression( 13, true, true )
    if token.AdvanceIf( kw#kFROM )                      ' REPEAT var FROM...
      ifnot fromAllowed
        abort string("FROM not allowed")
      {
        REPEAT var FROM f TO t STEP s
        If we just generated the bytecode in order, we'd get
                                vPUSHffffttttssss
        What we want is
                                ffffvPOP
                                the body of the loop
                                ssssffffttttvUSING+RPTINCJ
        (vPUSH is the code for pushing var; we'll modify the PUSH op to POP and then to USING.)
        So:
        Generate vPUSH, copy it to temp storage, and reset pObjWork to overwrite it.
        Generate ffff and note pObjWork.
        Generate tttt, copy fffftttt to temp storage.
        Generate ssss, copy to temp storage.
        Reset pObjWork to overwrite ttttssss.
        Modify vPUSH to vPOP, emit it after ffff.
        Generate the code for the body of the loop.
        Emit ssss.
        Emit fffftttt.
        Modify vPOP to vUSING, emit it.
        Emit RPTINCJ or RPTADDJ
      }
      pFromVarOp := fromAllowed
      pFromVarEnd := pObjWork
      p := SaveBytes( pFromVarStart, pFromVarEnd )
      pObjWork := pFromVarStart                         ' overwrite var code
      pFromVarEnd -= pFromVarStart - p
      pFromVarOp -= pFromVarStart - p
      pFromVarStart := p

      pFromToExprStart := pObjWork
      CompileExpression( 13, true, false )              ' FROM expression
      token.Eat( kw#kTO )
      q := pObjWork             ' we're going to reset pObjWork to this point to overwrite TO and STEP expressions.
      CompileExpression( 13, true, false )              ' TO expression
      pFromToExprEnd := pObjWork
      p := bt.Alloc( pFromToExprEnd - pFromToExprStart )
      bytemove( p, pFromToExprStart, pFromToExprEnd - pFromToExprStart )
      pFromToExprEnd -= pFromToExprStart - p
      pFromToExprStart := p
      pStepExprStart~
      if token.AdvanceIf( kw#kSTEP )
        pStepExprStart := pObjWork
        CompileExpression( 13, true, false )            ' STEP expression
        pStepExprEnd := pObjWork
        p := SaveBytes( pStepExprStart, pStepExprEnd )
        pStepExprEnd -= pStepExprStart - p
        pStepExprStart := p
      token.Eat( kw#kEOL )

      pObjWork := q

      ' change PUSH to POP
      if byte[pFromVarOp] | 1 == $3f                    ' reg?
        byte[pFromVarOp][1] += $20                      ' modify following byte: $9r => $br
      else
        ++byte[pFromVarOp]                              ' PUSH => POP
      
      repeat p from pFromVarStart to pFromVarEnd-1
        Emit( byte[p] )

      topDest := MarkBackwardJumpDestination

      CompileStatements( col )

      Retarget( nextDest, pObjWork )

      jumpOp := $02             ' RPTINCJ for no STEP
      if pStepExprStart                                 ' emit STEP expression if present
        repeat p from pStepExprStart to pStepExprEnd - 1
          Emit( byte[p] )
        jumpOp := $06           ' RPTADDJ for STEP
          
      repeat p from pFromToExprStart to pFromToExprEnd - 1 ' emit FROM and TO expressions
        Emit( byte[p] )

      ' change POP to USING
      if byte[pFromVarOp] | 1 == $3f                    ' reg?
        byte[pFromVarOp][1] += $20                      ' modify following byte: $br => $dr
      else
        ++byte[pFromVarOp]                              ' POP => USING
      
      repeat p from pFromVarStart to pFromVarEnd-1      ' emit USING VAR expression
        Emit( byte[p] )
      ResolveBackwardJump( jumpOp, topDest )
      
      Retarget( quitDest, pObjWork )
      
    else                                                ' REPEAT expr
      token.Eat( kw#kEOL )
      pQuitJump := MakeForwardJump( $08 ) ' LOOPJPF
      topDest := MarkBackwardJumpDestination

      CompileStatements( col )
                                                                                                                                 
      Retarget( nextDest, pObjWork )
      ResolveBackwardJump( $09, topDest ) ' LOOPRPT
      ResolveForwardJump( pQuitJump )
      Retarget( quitDest, pObjWork )

  nextDest := saveNextDest
  quitDest := saveQuitDest
  --repeatDepth

pri SaveBytes( p, q ) : r | l
  r := bt.Alloc( l := q - p )
  bytemove( r, p, l )

pri CompileCase | col, matchCol, otherEncountered, p0, p1, p2, pMatch, pCase
{{
  Current token is CASE.
}}
  otherEncountered~
  p0~
  p1~
  col := token.Column
  token.Advance
  Emit( $39 )                   ' PUSH#k2
  MakeFloatingReference( pObjWork, pObjWork ) ' Eventually this will be the offset (within the obj) to the end of the CASE construct.
  pCase := pObjWork
  Emit( 0 )                     ' placeholder
  Emit( 0 )                     '   "
  CompileExpression( 13, true, false )
  token.Eat( kw#kEOL )
  matchCol := token.Column
  if matchCol =< col
    abort string("No cases encountered")
  repeat while token.Column > col
    if token.AdvanceIf( kw#kOTHER )
      otherEncountered~~
      token.Eat( kw#kCOLON )
      CompileStatements( matchCol )
      Emit( $0c )               ' GOTO []
      Swaperoo( p0, p1, pObjWork )
      quit
    else
      pMatch := pObjWork
      repeat
        CompileExpression( 13, true, false )
        if token.AdvanceIf( kw#kDOTDOT )
          CompileExpression( 13, true, false )
          Emit( $0e )           ' CASER
          MakeFloatingReference( pObjWork, pMatch )
          Emit( $80 )
          Emit( 0 )
        else
          Emit( $0d )           ' CASE
          MakeFloatingReference( pObjWork, pMatch )
          Emit( $80 )
          Emit( 0 )
      while token.AdvanceIf( kw#kCOMMA )
      token.Eat( kw#kCOLON )
      Retarget( pMatch, pObjWork )
      Swaperoo( p0, p1, pObjWork )
      p0 += pObjWork - p1
      CompileStatements( matchCol )
      Emit( $0c )               ' GOTO []
      p1 := pObjWork

  ifnot otherEncountered
    Emit( $0c )                 ' GOTO []
    Swaperoo( p0, p1, pObjWork )

  Retarget( pCase, pObjWork )
  
var
  word  pFloatingReferences
{
  Floating reference data structure: 2-byte source address, 2-byte destination address, 2-byte pointer to next floating reference.
  Floating refs are stored in temporary bt.Alloc-ed memory which is reclaimed after each method is compiled.
}
pri MakeFloatingReference( src, dst ) | p
  p := bt.Alloc( 6 )
  bt.PokeW( p  , src )
  bt.PokeW( p+2, dst )
  bt.PokeW( p+4, pFloatingReferences )
  pFloatingReferences := p

pri Retarget( dst0, dst1 ) | p
{{
  Retarget any floating refs with destination dst0 to dst1.
}}
  p := pFloatingReferences
  repeat while p
    if bt.PeekW( p+2 ) == dst0
      bt.PokeW( p+2, dst1 )
    p := bt.PeekW( p+4 )
    
pri ResolveFloatingReferences | p, src, dst, b, d
  p := pFloatingReferences
  repeat while p
    dst := bt.PeekW( p+2 )
    src := bt.PeekW( p )
    b := byte[src-1]
    if b == $39                                         ' PUSH#k2 at beginning of CASE
      byte[src] := (dst - pObjSpace) >> 8
      byte[src+1] := dst - pObjSpace
    elseif b == $0d or b == $0e or b == $04 or b == $0a or b == $0b or b == $87 ' CASE or CASER or GOTO or JPF or JPT or PUSH#.B (for STRING)
      d := dst - src - 2        ' relative offset
      if b == $87               ' except for PUSH#.B
        d := dst - pObjSpace
      byte[src] := (d >> 8) | $80                       ' SHOULDDO: range check
      byte[src+1] := d
    p := bt.PeekW( p+4 )
    
pri Swaperoo( p0, p1, p2 ) | pFR, src, dst
{{
  Exchanges bytes [p0..p1) with [p1..p2)
  E.g. abcdeXYZ => XYZabcde
  Updates any floating references that have endpoints in [p0..p2)
}}
  if p0 == p1
    return

  pFR := pFloatingReferences
  repeat while pFR
    bt.PokeW( pFR  , AdjustEndpoint( bt.PeekW( pFR   ), p0, p1, p2 ) )
    bt.PokeW( pFR+2, AdjustEndpoint( bt.PeekW( pFR+2 ), p0, p1, p2 ) )
    pFR := bt.PeekW( pFR+4 )
    
  Reverse( p0, p1 )
  Reverse( p1, p2 )
  Reverse( p0, p2 )

pri AdjustEndpoint( p, p0, p1, p2 )
{{
  Addresses [p0..p1) will be swapped with [p1..p2).
  If p is in the affected range, return p's new value; otherwise, just return p
}}
  if p0 =< p and p < p1         
    return p + p2 - p1          ' [p0..p1) will be shifted p2-p1 bytes        
  if p1 =< p and p < p2
    return p - p1 + p0          ' p used to be p-p1 from p1, will be p-p1 from p0
  return p
  
pri Reverse( i, j ) | t
{{
  Reverses bytes [i..j)
  E.g. abcde => edcba
}}
  repeat while i < --j
    t := byte[i]
    byte[i++] := byte[j]
    byte[j] := t

pri CompileString | p, size
{{
  Current token is the ( after STRING.

  Copy the string byte-by-byte to pObjWork, then copy the string up to
  temp string storage in high memory. The reason for this nutty two-step is
  the string length is unknown to begin with, so we don't know how much
  temp string storage to allocate, but we can't leave the string at pObjWork
  because it will doubtlessly be overwritten by subsequent calls to Emit.   
}}  
  token.Eat( kw#kLPAREN )
  p := pObjWork
  repeat
    byte[p++] := eval_EvaluateExpression( 0 )
  while token.AdvanceIf( kw#kCOMMA )
  byte[p++]~
  size := p - pObjWork
  if (pStringWork -= size) < pObjWork
    abort string("Out of string space")
  bytemove( pStringWork, pObjWork, size )
  Emit( $87 )                   ' PUSH#.B
  MakeFloatingReference( pObjWork, pStringWork )
  Emit( 0 )                     ' placeholder: this will eventually be the address of the string.
  Emit( 0 )
  
  token.Eat( kw#kRPAREN )

pri StringCleanup | size, p
{{
  Call this at the end of compiling a method to move the strings down to abut the rest of the method code
  and adjust floating references.
}}
  p := pFloatingReferences
  repeat while p
    bt.PokeW( p+2, AdjustEndpoint( bt.PeekW( p+2 ), pObjWork, pStringWork, pObjTop ) )
    p := bt.PeekW( p+4 )
    
  size := pObjTop - pStringWork                         ' we're going to move this many bytes
  bytemove( pObjWork, pStringWork, size )               '
  pObjWork += size

con
#0, opPUSH, opPOP, opUSING, opPEA, opNONE = -1
#0, MEMSPACE, DATSPACE, VARSPACE, LOCALSPACE, REGSPACE, SPRSPACE 
 
pri CompileExpression( rbp, push, fromAllowed ) | prec, opcode, opcode2, lbp
  prec := token.GetPrecedence
  
  if token.AdvanceIf( kw#kAT )
    CompileVariable( push, opPEA, 0, false )
  elseif token.AdvanceIf( kw#kMINUS )
    if push
      if token.IsIntLiteral or token.IsFloatLiteral
        EmitPushInt( -token.Value )
        token.Advance
      else
        CompileExpression( prec, push, false )
        Emit( $e6 ) ' NEG
    else  ' in-place negate requires variable operand
      CompileVariable( push, opUSING, $46, false ) ' NEG
  elseif token.AdvanceIf( kw#kPLUS )
    if push
      if token.IsIntLiteral or token.IsFloatLiteral
        EmitPushInt( token.Value )
        token.Advance
      else  ' in-place negate requires variable operand
        CompileVariable( push, opPUSH, 0, false ) ' just push
    else
      abort string("Syntax error after +")
  elseif token.IsPostfixOp ' At this point, a postfix op is a prefix op
    opcode := token.GetSpinOpcode
    token.Advance
    CompileVariable( push, opUSING, opcode, false )
  elseif token.AdvanceIf( kw#kATAT )                    ' special handling for @@
    if push
      CompileExpression( prec, push, false )
      Emit( $97 )               'PUSH#.B OBJ+0[]
      Emit( 0 )
    else
      abort string("@@ must push")
  elseif token.IsUnaryOp
    opcode := token.GetSpinOpcode
    opcode2 := token.GetSpinOpcode2
    ifnot opcode2
      opcode2 := opcode - $a0
    token.Advance
    if push
      CompileExpression( prec, push, false )
      Emit( opcode )
    else ' in-place unary op requires variable operand
      CompileVariable( push, opUSING, opcode2, false )
  elseif token.AdvanceIf( kw#kLPAREN )
    CompileExpression( 13, push, false )
    token.Eat( kw#kRPAREN )
  elseif token.IsIntLiteral or token.IsFloatLiteral
    if push
      EmitPushInt( token.Value )
      token.Advance
    else
      abort string("Unexpected literal")
  elseif token.IsId
    result := CompileId( push, fromAllowed, false )
  elseif token.AdvanceIf( kw#kSTRING )
    CompileString
  elseif token.AdvanceIf( kw#kCONSTANT )
    token.Eat( kw#kLPAREN )
    EmitPushInt( eval_EvaluateExpression( 0 ) )
    token.Eat( kw#kRPAREN )
  elseif token.AdvanceIf( kw#kBACKSLASH )
    CompileId( push, false, true )
  elseif token.IsIntrinsic
    CompileIntrinsic( push )
  else
    result := CompileVariable( push, opNONE, 0, fromAllowed )

  repeat
    lbp := token.GetPrecedence
    if rbp =< lbp
      quit
    opcode := token.GetSpinOpcode
    token.Advance
    CompileExpression( lbp, push, false )
    Emit( opcode )
    result~

pri Emit( b )
  if pObjWork => pStringWork
    abort string("Out of obj space")
  byte[pObjWork++] := b
{{ #ifdef dbgcg
  Print.Hex(b,2)
  Print.Space
  Print.ASCII(b)
  Print.StrCR(string(" <EmitObj"))
}} '' #endif 'dbgcg

pri EmitPushInt( v ) | mask, i, negate
{{
  Emits the code the push integer v.
}}
  if( v == -1 )
    Emit( $34 )                 ' PUSH#-1
  elseif( v == 0 )
    Emit( $35 )                 ' PUSH#0
  elseif( v == 1 )
    Emit( $36 )                 ' PUSH#-1
  elseif Kp( v )
    ' nothing to do; Kp emits if necessary
  else
    negate~
    mask~~
    repeat i from 1 to 4
      mask <<= 8
      if (v & mask) == 0
        quit
      if (v | !mask) == -1
        negate~~
        !v
        quit
        
    ' here i indicates how many bytes of v to emit (1..4)
    v <<= (4-i)<<3              ' left-justify the i bytes we're interested in         
    Emit( $37 + i )             ' PUSH#k1/PUSH#k2/PUSH#k3/PUSH#k4
    repeat i
      Emit( v >> 24 )           ' pushing them out MSByte first
      v <<= 8
    if negate
      Emit( $e7 )               ' BIT_NOT

pri Kp( v ) : f | b, m
{{
  If v is one of the special bit patterns below, Kp emits the appropriate code and returns true;
  otherwise just returns false.

  Pattern        Emits  
  2^(b+1)    =>  %000bbbbb
  2^(b+1)-1  =>  %001bbbbb
  !(2^(b+1)) =>  %010bbbbb
  -(2^(b+1)) =>  %011bbbbb
}}   
  m := 2
  repeat b from 0 to 30
    f~~
    if v == m
      quit
    if v == m-1
      b |= $20
      quit
    if v == !m
      b |= $40
      quit
    if v == -m
      b |= $60
      quit
    m <<= 1
    f~
  if f
    Emit( $37 )                 ' PUSH#kp
    Emit( b )

pri CompileId( push, fromAllowed, abortFlag ) | p, type
  p := sym.SymbolLookup( token.Text )
  ifnot p                                               ' If it's not in the main symbol table...
    if abortFlag
      abort string("Expected method call")
    return CompileVariable( push, opNONE, 0, fromAllowed ) ' ...it might be a local variable.

  type := byte[p]
  return CompileIdHelper( p, type, 0, push, fromAllowed, abortFlag, 0, 0 )

pri CompileIdHelper( p, type, objIndex, push, fromAllowed, abortFlag, i0, i1 ) | v, methodIndex, nArgs, pObj, expectCon 
{{
  returns non-zero if FROM may follow
}}
  if abortFlag
    ifnot type == sym#kPUB_SYMBOL or type == sym#kPRI_SYMBOL or type == sym#kOBJ_SYMBOL
      abort string("Expected method call")
  if type == sym#kINT_CON_SYMBOL or type == sym#kFLOAT_CON_SYMBOL or type == sym#kBUILTIN_INT_SYMBOL or type == sym#kBUILTIN_FLOAT_SYMBOL
    if push
      v := bt.PeekL( p+1 )
      EmitPushInt( v )
    else
      abort string("CON symbol not allowed here")
    token.Advance
  elseif type == sym#kPUB_SYMBOL or type == sym#kPRI_SYMBOL
    Emit( ($01 & not push) | ($02 & abortFlag) )
    ' push  abort  Emit
    ' true  false  $00  ' FRAME call w/ return value 
    ' false false  $01  ' FRAME call w/o return value
    ' true  true   $02
    ' false true   $03 
    token.Advance
    methodIndex := byte[p][1]
    nArgs := byte[p][2]
    CompileArgs( nArgs )
    if objIndex
      if i0 == i1
        Emit( $06 )             ' CALLOBJ
      else
        Swaperoo( i0, i1, pObjWork )
        Emit( $07 )             ' CALLOBJ[]
      Emit( objIndex )
      Emit( methodIndex )       ' method #
    else
      Emit( $05 )               ' CALL
      Emit( methodIndex )       ' method #
  elseif type == sym#kOBJ_SYMBOL
    token.Advance
    objIndex := byte[p][3]
    p := bt.PeekW( p+1 )
    pObj := bt.PeekW( p + strsize(p) + 2 )
    if token.AdvanceIf( kw#kLBRACKET )
      i0 := pObjWork
      CompileExpression( 13, true, false )
      i1 := pObjWork
      token.Eat( kw#kRBRACKET )
      token.Eat( kw#kDOT )
      expectCon~
    elseif token.AdvanceIf( kw#kOCTOTHORP )
      expectCon~~
    elseif token.AdvanceIf( kw#kDOT )
      expectCon~
    else
      abort string("Expected . or #")
    ifnot p := bt.FindInTable( token.Text, pObj )
      abort string("Symbol unknown in sub-object")
    type := byte[p]
    if expectCon and ( type == sym#kINT_CON_SYMBOL or type == sym#kFLOAT_CON_SYMBOL ) {
}       or not expectCon and ( type == sym#kPUB_SYMBOL or type == sym#kPRI_SYMBOL )
      CompileIdHelper( p, type, objIndex, push, false, abortFlag, i0, i1 )
    elseif expectCon
      abort string("Expected CON symbol")
    else
      abort string("Expected PUB symbol")   
  else
    result := CompileVariable( push, opNONE, 0, fromAllowed )

pri CompileArgs( n ) | t
  ifnot n
    return
  t := kw#kLPAREN
  repeat n
    token.Eat( t )              ' 1st time, '('; subsequently, ','
    t := kw#kCOMMA
    CompileExpression( 13, true, false )
  token.Eat( kw#kRPAREN )

pri CompileVariable( push, memop, spinOpcode, fromAllowed ) | space, offset, size, subscripted, dim, p, t, p0, p1
{{
  Current token is anything that could be a variable: BYTE/WORD/LONG, a special register, SPR, an id.
  Returns pointer to PUSH instruction if FROM is permitted to follow, false otherwise.
}}
  p0~
  p1~
  subscripted~
  if token.IsSize                                       '========== BYTE/WORD/LONG
    space := MEMSPACE
    size := |< (token.Type - kw#kBYTE)                  ' size = 1, 2, or 4
    token.Advance
    if token.AdvanceIf( kw#kLBRACKET )
      p0 := pObjWork
      CompileExpression( 13, true, false )
      p1 := pObjWork      
      token.Eat( kw#kRBRACKET )
      if subscripted := token.AdvanceIf( kw#kLBRACKET )
        CompileExpression( 13, true, false )
        p1 := pObjWork      
        token.Eat( kw#kRBRACKET )
  elseif token.IsReg                                    '========== register (PAR, INA, etc.)
    if memop == opPEA
      abort string("Can't take address of register")
    space := REGSPACE
    offset := token.GetReg
    size := 2                   ' repurpose size for registers: 2 for plain reg
    token.Advance
    if token.AdvanceIf( kw#kLBRACKET )
      size~                     ' 0 for reg[]
      CompileExpression( 13, true, false )
      if token.AdvanceIf( kw#kDOTDOT )
        ++size                  ' 1 for reg[..]
        CompileExpression( 13, true, false )
      token.Eat( kw#kRBRACKET )
  elseif token.AdvanceIf( kw#kSPR )                     '========== SPR
    if memop == opPEA
      abort string("Can't take address of register")
    space := SPRSPACE
    token.Eat( kw#kLBRACKET )
    CompileExpression( 13, true, false )
    token.Eat( kw#kRBRACKET )
  elseif token.IsId or token.Type == kw#kRESULT         '========== id or RESULT
    if p := sym.SymbolLookup( token.Text )
      t := byte[p++]
      if t == sym#kDAT_SYMBOL or t == sym#kVAR_SYMBOL
        if t == sym#kDAT_SYMBOL
          space := DATSPACE
        else
          space := VARSPACE
        size := byte[p++]
        offset := bt.PeekW( p )
      else
        abort string("Expected variable")
    elseif p := bt.FindInTable( token.Text, bt.PeekW( pLocalSymbols ) )
      space := LOCALSPACE
      size := 4
      offset := bt.PeekW( p )
    else
      abort string("Expected variable")
    token.Advance
    if token.AdvanceIf( kw#kDOT )
      ifnot token.IsSize
        abort string("Syntax error")
      size := |< (token.Type - kw#kBYTE)
      token.Advance    
    if subscripted := token.AdvanceIf( kw#kLBRACKET )
      p0 := pObjWork
      CompileExpression( 13, true, false )
      p1 := pObjWork
      token.Eat( kw#kRBRACKET )
  else                                                  '========== 
    abort string("Syntax error")                      

  ' Now look for assignment or postfix op following variable
  
  if token.AdvanceIf( kw#kCOLONEQUAL )                  ' Straight assignment
    if memop <> opNONE
      abort string("Assignment not allowed here")
    CompileExpression( 13, true, false )
    Swaperoo( p0, p1, pObjWork )
    if( push )
      EmitVariable( space, size, offset, subscripted, opUSING )
      Emit( $80 )
    else
      EmitVariable( space, size, offset, subscripted, opPOP )
    return false
  elseif token.IsAssignmentOp                           ' Assignment op (+=, AND=)
    spinOpcode := token.GetSpinOpcode - $a0
    token.Advance
    CompileExpression( 13, true, false )
    Swaperoo( p0, p1, pObjWork )
    memop := opUSING
  elseif token.IsPostfixOp                              ' Postfix (++)
    if memop <> opNONE
      abort string("Postfix op not allowed here")
    spinOpcode := token.GetSpinOpcode2
    token.Advance
    memop := opUSING

  if $20 =< spinOpcode and spinOpcode < $40           ' If pre/post inc/dec, adjust opcode based on size:
    spinOpcode += (size >> 1) << 1 + 2                ' size 1, 2, 4 => +2, +4, +6
  if push
    spinOpcode |= $80
  if memop == opNONE
    memop := opPUSH
  if memop == opPUSH and fromAllowed
    result := pObjWork 
    
  EmitVariable( space, size, offset, subscripted, memop )
  if memop == opUSING
    Emit( spinOpcode )

pri EmitVariable( space, size, offset, subscripted, memop ) | op
  if space == REGSPACE
    Emit( $3d + size )          ' size means something different for reg: 0 => reg[], 1 => reg[..], 0 => reg
    Emit( $90 + offset + (memop << 5) ) ' PUSH/POP/USING => $9r/$br/$dr
    return
    
  if space == SPRSPACE
    Emit( $24 + memop )         ' PUSH/POP/USING => $24/$25/$26
    return
    
  if size == 4 and offset < 32 and not subscripted
    op := %0100_0000 + offset + memop                   ' offset is a multiple of 4, given that size = 4
    if space == VARSPACE
      Emit( op )
      return 
    elseif space == LOCALSPACE
      Emit( op + %0010_0000 )
      return
  op := $80 + ((size>>1) << 5) + (space<<2) + memop
  if subscripted
    op += |< 4
  Emit( op )
  if( space <> MEMSPACE )
    if offset < $80
      Emit( offset )
    else
      Emit( (offset >> 8) | $80 ) ' high byte with MSB set
      Emit( offset )              ' low byte

pri CompileIntrinsic( push ) | t
  t := token.Type
  token.Advance
  if t & $10_00_0 ' LOOKUP|DOWN[Z] end with $1x_xx_x
    CompileLookX( t, push )
    return
  if t & $20_00_0               ' LOCKCLR/NEW/SET end with $2x_xx_x
    CompileLock( t, push )
    return
  if t & $100                   ' STRCOMP/STRSIZE end with $1x_x
    AssertPush( push )
    CompileArgs( t & 3 )        ' could be 1 or 2
    Emit( t >> 4 )
    return
    
  case t
    kw#kCHIPVER:
      AssertPush( push )
      Emit( $34 )               ' PUSH#-1
      Emit( $80 )               ' PUSH.B Mem[]
    kw#kCLKFREQ:
      AssertPush( push )
      Emit( $35 )               ' PUSH#0
      Emit( $c0 )               ' PUSH.L Mem[]
    kw#kCLKMODE:
      AssertPush( push )
      EmitPushInt( 4 )
      Emit( $80 )               ' PUSH.B Mem[]
    kw#kCOGID:
      AssertPush( push )
      Emit( $3f )               ' REGPUSH cogid
      Emit( $89 )
    kw#kCOGINIT:
      AssertNotPush( push )
      CompileCoginewt( false, true )
    kw#kCOGNEW:
      CompileCoginewt( push, false )
    kw#kREBOOT:
      AssertNotPush( push )
      EmitPushInt( $80 )
      EmitPushInt( $00 )
      Emit( $20 )               ' CLKSET

pri AssertPush( push )
  ifnot push
    abort string("Unexpected (must push)")

pri AssertNotPush( push )
  if push
    abort string("Unexpected (can't push)")

pri CompileLookX( t, push ) | op, opr, dest
{{
  Current token is ( after LOOKUP/LOOKUPZ/LOOKDOWN/LOOKDOWNZ.
  t = token.Type
}}
  AssertPush( push )
  op := (t >> 4) & $ff
  opr := (t >> 12) & $ff
  EmitPushInt( t & 1 )
  Emit( $39 )                   ' PUSH#k2
  dest := uniqueDest++
  MakeFloatingReference( pObjWork, dest )
  Emit( 0 )                     ' placeholder
  Emit( 0 )

  token.Eat( kw#kLPAREN )
  CompileExpression( 13, true, false )
  token.Eat( kw#kCOLON )
  repeat
    CompileExpression( 13, true, false )
    if token.AdvanceIf( kw#kDOTDOT )
      CompileExpression( 13, true, false )
      Emit( opr )
    else
      Emit( op )
  while token.AdvanceIf( kw#kCOMMA )
  Emit( $0f )                   ' LOOKEND
  Retarget( dest, pObjWork )
  
  token.Eat( kw#kRPAREN )

pri CompileLock( t, push ) | op
{{
  Current token is ( after LOCKCLR/NEW/SET.
  t = token.Type
}}
  op := (t >> (4 + 8 & push)) & $ff ' fn op is bits 19..12; sub op is 11..4
  CompileArgs( t & 1 )          ' only two possibilities for these LOCK* guys.
  Emit( op )

pri CompileCoginewt( push, init ) | p0, p1, mark, pSym, index, nArgs
{{
  Compile either COGINIT (init=true) or COGNEW (init=false).
  Current token is the ( after COGINIT/NEW.
  COGINIT( <exprA>, <exprB>, <exprC> )
  COGNEW( <exprB, <exprC> )
}}
  token.Eat( kw#kLPAREN )

  mark~
  if init
    p0 := pObjWork
    CompileExpression( 13, true, false )                ' exprA
    p1 := pObjWork
    token.Eat( kw#kCOMMA )

  ' see if the current token (start of exprB) is a method name
  if (pSym := sym.SymbolLookup( token.Text )) and (byte[pSym] == sym#kPRI_SYMBOL or byte[pSym] == sym#kPUB_SYMBOL)
                                ' Short-circuit evaluation would be nice here, but this works (it's just a little wasteful).
    mark~~
    index := byte[pSym][1]
    nArgs := byte[pSym][2]
    token.Advance                                       ' exprB is a method call, which
    CompileArgs( nArgs )                                '  we handle here ourselves
    Emit( $39 )                 ' PUSH#k2
    Emit( nArgs )
    Emit( index )
  else
    ifnot init
      Emit( $34 )               ' PUSH#-1
    CompileExpression( 13, true, false )                ' exprB is a plain old expression

  token.Eat( kw#kCOMMA )
  CompileExpression( 13, true, false )                  ' exprC
  if mark
    Emit( $15)                  ' MARK
  if init and mark
    Swaperoo( p0, p1, pObjWork )
    Emit( $3f )                 ' REGPUSH    $8f? 
    Emit( $8f )
    Emit( $37 )                 ' PUSH#kp    -4 ($fffffffc)
    Emit( $61 )
    Emit( $d1 )                 ' POP.L      Mem[][] 

  if push
    Emit( $28 )                 ' COGIFUN
  else
    Emit( $2c )                 ' COGISUB

  token.Eat( kw#kRPAREN )      

''============================= METHODS END ===================================

''============================= EVAL BEGIN ====================================

dat
pass                    byte    0
insideDAT               byte    0
ignoreUndefined         byte    0
evalSuccess             byte    0
e_pGlobalLabel          word    0

pub eval_set_Pass( p )
  pass := p

pub eval_get_Pass
  return pass

pub eval_EnteringDat
  insideDAT~~
  
pub eval_LeavingDat
  insideDAT~

pub eval_Succeeded
  return evalSuccess
    
pub eval_TryToEvaluateExpression( _e_pGlobalLabel )
{{
}}
  e_pGlobalLabel := _e_pGlobalLabel
  ignoreUndefined~~
  evalSuccess~~
  return Evaluate( 13 )

pub eval_EvaluateExpression( _e_pGlobalLabel )       
  e_pGlobalLabel := _e_pGlobalLabel
  ignoreUndefined~
  evalSuccess~~
  return Evaluate( 13 )
    
pri Evaluate( rbp ) | prec, lbp, t
  prec := token.GetPrecedence
  
  if token.AdvanceIf( kw#kAT )
    result := EvaluateSymbol( true )
  elseif (token.IsUnaryOp and not token.IsPostfixOp) or token.Type == kw#kMINUS
    t := token.Type
    token.Advance
    result := EvaluateUnary( t, prec )
  elseif token.AdvanceIf( kw#kLPAREN )
    result := Evaluate( 13 )
    token.Eat( kw#kRPAREN )
  elseif token.IsIntLiteral or token.IsFloatLiteral
    result := token.Value
    token.Advance
  elseif token.IsReg
    result := $1f0 + token.GetReg
    token.Advance
  else
    result := EvaluateSymbol( false )

  repeat
    lbp := token.GetPrecedence

    if rbp =< lbp
      quit

    t := token.Type
    token.Advance
    result := EvaluateBinary( t, result, Evaluate( lbp ) )

pri EvaluateSymbol( takeAddress ) | p, t, pObj, cogx4, pLocalLabels
{{
  CON symbols evaluate as their defined values, as do OBJ#constants.
  DAT symbols evaluate differently depending on context.
}}
  if byte[token.Text] == ":"                            ' local label?
    ifnot e_pGlobalLabel
      abort string("Unknown local label(1)")
      
    p := e_pGlobalLabel                               ' Search local label list for current token.
    repeat
      ifnot p := bt.PeekW( p + 6 )                    ' p points to string part of local label
        if ignoreUndefined
          evalSuccess~
          token.Advance
          return -1           ' dummy value
        abort string("Unknown local label(2)")
      if strcomp( p, token.Text )
        quit
      p += strsize(p) + 1                             ' Increment p past string part to data part
                                                      ' On exit, we've found the local label.
    p += strsize(p) + 1                               ' Increment p past string part to data part

  else
    p := sym.SymbolLookup( token.Text )
    ifnot p
      if ignoreUndefined
        evalSuccess~
        token.Advance
        return -1
      abort string("Unknown symbol")
      
  t := byte[p]
  
  if t == sym#kINT_CON_SYMBOL or t == sym#kFLOAT_CON_SYMBOL or t == sym#kBUILTIN_INT_SYMBOL or t == sym#kBUILTIN_FLOAT_SYMBOL
  ' TODO: should handle floats differently
    if takeAddress
      abort string("Can't take address of CON symbol")
    token.Advance
    return bt.PeekL( p+1 )
  if t == sym#kUNDEFINED_CON_SYMBOL
    if ignoreUndefined
      evalSuccess~
      token.Advance
      return -1                 ' dummy value
    abort string("Undefined CON symbol")
  if t == sym#kOBJ_SYMBOL
    if takeAddress
      abort string("Can't take address of OBJ symbol")
    token.Advance
    p := bt.PeekW( p+1 )
    pObj := bt.PeekW( p + strsize(p) + 2 )
    token.Eat( kw#kOCTOTHORP )
    ifnot p := bt.FindInTable( token.Text, pObj )
      abort string("CON symbol unknown in sub-object")
    't := byte[p] -- at this point, t should be either kINT_CON_SYMBOL or kFLOAT_CON_SYMBOL
    ' TODO: should handle floats differently
    token.Advance
    return bt.PeekL( p+1 )

  if t == sym#kDAT_SYMBOL
    if pass == 1
      if ignoreUndefined
        evalSuccess~
        token.Advance
        return -1               ' dummy value
      if takeAddress
        abort string("Can't take address of DAT symbol in pass 1")
    if not insideDAT or takeAddress
      token.Advance
      return bt.PeekW( p+2 )
      
    cogx4 := bt.PeekW( p+4 )
    if cogx4 & 3
      abort string("Address is not long")
    token.Advance
    return cogx4 >> 2
     
  abort string("Invalid symbol in constant expression")

pri EvaluateUnary( t, prec )
{{

}}
  result := Evaluate( prec )
  case t & $ff
    $ed: -result
    $e7: !result
    $ff: not result
    $f3: |<result
    $f1: >|result
    $f8: ^^result
    $e9: ||result
    other: abort string("Syntax error in constant expression(1)")    

pri EvaluateBinary( t, a, b )
  case t & $ff
    $ec: return a + b
    $ed: return a - b
    $f4: return a * b
    $f6: return a / b
    $f5: return a ** b
    $f7: return a // b
    $e0: return a -> b
    $e1: return a <- b
    $e2: return a >> b
    $e3: return a << b
    $ee: return a ~> b
    $ef: return a >< b
    $e8: return a & b
    $ea: return a | b
    $eb: return a ^ b
    $e4: return a #> b
    $e5: return a <# b
    $f9: return a < b
    $fa: return a > b
    $fc: return a == b
    $fb: return a <> b
    $fd: return a =< b
    $fe: return a => b
    $f0: return a and b
    $f2: return a or b
    other: abort string("Syntax error in constant expression(2)")

''============================= EVAL END ======================================

DAT
{{
Copyright (c) 2009 Michael Park
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    |
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}
        