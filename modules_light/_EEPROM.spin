'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _EEPROM  --> _EEPROM.CMD |
'' +--------------------------------------------------------------------------+
'' |  Authors:       (c)2012,2013 "Cluso99" (Ray Rodrick)                     |
'' |  Modifications:                                                          |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' This OS module uses an included module "__MODULE.SPIN" to perform most of
''  the housekeeping - see this for history, details and acknowledgements.
''
'' This module......
''      Copies an SD File "<filename>.EEP" to the EEPROM (lower or upper 32KB of 64KB)
''
'' RR20130724   start
'' RR20160304   rename Basic_I2C_Driver -> _I2C.spin
'' RR20160726   add [-RL] & [-RU] options to read EEPROM and write to file
''                rename _program.spin --> _eeprom.spin
''

'include "__MODULE.spin"         ' include the common code for OS modules
''#include "__MODULE.spin"  

CON
  PAGESIZE = 32                                         'eeprom pagesize
  MAXPGMSIZE = 16*1024                                  'max program size to be placed into eeprom !!!

OBJ
     i2c   : "I2C"                                      ' eeprom routines (formerly Basic_I2C_Driver.spin)
     str   : "StringE"

VAR
  byte  filename0[13]           ' FAT source filename (string)
  byte  fname0[13]              ' filename w/o dot
  byte  param[5]                ' options: -WL, -WU
  long  e_offset                ' eeprom offset
  
     long   hardware                                    ' hw def & pins
     long   DO, CLK, DI, CS, SI, SO

     byte   Mounted
     long   ioControl[2]                                '\\
     long   i0Control_sector                            '// sdspiFemto passes ioControl[3] ==> sd sector no 
     long   fbase                                       ' SD first sector
     long   fsize                                       ' SD filesize

'--------------- buffer for eeprom 16KB!!! --------------------------
     long   ndx                                         ' index of chars in pgmbuff 0..16K-1
     byte   pgmbuff[MAXPGMSIZE]                         ' buffer for program to be copied to eeprom (16KB max!!!)
 
DAT
'_ModuleStr   byte      "=== COPY ===",0        'module's name (string)

PRI executeCommand(stringPointer)  
  
  'get the parameters
  str.stringCopy(@filename0, str.tokenizeString(0),13)  ' source filename
  str.stringCopy(@param, str.tokenizeString(0),5)       ' options

  if strsize(@filename0) == 0                           ' if no parameters, display usage
    {Print.StrCR(string(" EEPROM Read/Write from/to Lower/Upper 32KB to/from an SD file..."))
    Print.StrCR(string("    EEPROM <filename> -{R/W}{L/U}"))
    }return -2

'------------------------------------------------------------------------------
' validateParams                                        ' validate the params
  param[1] := param[1] & $DF                            ' simple convert to uppercase
  param[2] := param[2] & $DF                            ' simple convert to uppercase

  if     ((param[0] == "-") and (param[1] == "W") and (param[2] == "L"))
    e_offset := $0000
  elseif ((param[0] == "-") and (param[1] == "W") and (param[2] == "U"))
    e_offset := $8000
  elseif ((param[0] == "-") and (param[1] == "R") and (param[2] == "L"))
    e_offset := $0000
  elseif ((param[0] == "-") and (param[1] == "R") and (param[2] == "U"))
    e_offset := $8000
  else
    checkError(string(" parameter error"), $F0, @param)          ' if error, display & abort

  if param[1] == "W"
    WriteEeprom
  else
    ReadEeprom

  return 0

Pri WriteEeprom | errorString, r 
' Write Lower/Upper 32KB to EEPROM
'------------------------------------------------------------------------------
' ensure file exists and is 32KB
  errorString := \fat.openfile(@filename0, "R")             ' open source file
  checkError(@filename0, fat.partitionError, errorString)   ' if error, display & abort
  fsize := fat.fileSize                                     ' get size
  if fsize <> 32768
    Print.Str(string("t) File size "))
    Print.Str(str.integerToDecimal(fsize,8))
    Print.StrCR(string(" - must be 32KB"))
    Print.CRLF
    abort -2
'------------------------------------------------------------------------------
  Print.Str(string(" Program "))
  Print.Str(@filename0)
  Print.Str(string(" to EEPROM "))
  if e_offset == 0
    Print.StrCR(string("Lower 32KB"))
  else
    Print.StrCR(string("Upper 32KB"))
'------------------------------------------------------------------------------
  r := \Program32KB(@filename0)                         ' get file & program to eeprom
  if r == 0
    Print.Str(string(" EEPROM programmed successfully"))
    Print.CRLF
  else
    Print.Str(string("*** Programming Failed *** code "))
    Print.Str(str.integerToDecimal(r,3))
    Print.CRLF
    abort -1
'------------------------------------------------------------------------------
' Close up
  fat.closeFile
  Print.StrCR(string(" EEPROM written."))
'------------------------------------------------------------------------------

Pri ReadEeprom | errorString, r  
' Read Lower/Upper 32KB from EEPROM
'------------------------------------------------------------------------------
' Ensure FAT destination file does NOT exist
  errorString := \fat.newFile(@filename0)
  checkError(@filename0, fat.partitionError, errorString) ' if error, display & abort
'------------------------------------------------------------------------------
' Create the FAT destination file
  errorString := \fat.openFile(@filename0, "W")           ' open destination file for write
  checkError(@filename0, fat.partitionError, errorString) ' if error, display & abort
'------------------------------------------------------------------------------
  Print.Str(string(" Read EEPROM "))
  if e_offset == 0
    Print.Str(string("Lower 32KB"))
  else
    Print.Str(string("Upper 32KB"))
  Print.Str(string(" and copy to "))
  Print.StrCR(@filename0)

'------------------------------------------------------------------------------
' Read first 16KB from EEPROM and write to file
  Print.StrCR(string(" Reading first 16KB block..."))
  r := LoadEeprom(e_offset + $0000)                         ' read first 16KB from eeprom
  DisplayBuffer(e_offset + $0000)
  'write first 16KB to file
  errorString := \fat.writeData(@pgmbuff, MAXPGMSIZE)
  checkError(@filename0, fat.partitionError, errorString)   ' if error, display & abort

  bytefill(@pgmbuff, $00, MAXPGMSIZE)                       ' clear the buffer
' Read second 16KB from EEPROM and write to file
  Print.StrCR(string(" Reading second 16KB block..."))
  r := LoadEeprom(e_offset + $4000)                         ' read second 16KB from eeprom
  DisplayBuffer(e_offset + $4000)
  'write second 16KB to file
  errorString := \fat.writeData(@pgmbuff, MAXPGMSIZE)
  checkError(@filename0, fat.partitionError, errorString)   ' if error, display & abort

'------------------------------------------------------------------------------
' Close up
  fat.closeFile
  Print.StrCR(string(" File written successfully."))
'------------------------------------------------------------------------------

Pri Program32KB(st) | r
' Program the EEPROM from file

  r := FindFile(st)                                     ' eeprom file present?
  ' program eeprom lower 16KB
  r := LoadFile16KB                                     ' load lower 16KB of file
  DisplayBuffer(e_offset + $0000)
  Print.Str(string(" Programming first 16KB block..."))
  Print.Str(str.integerToHexadecimal(ndx,8))
  Print.CRLF
  r := ProgramEeprom(e_offset + $0000)                  ' program lower eeprom from buffer
  Print.Str(string(" Programmed first 16KB block"))
  Print.CRLF
  bytefill(@pgmbuff, $00, MAXPGMSIZE)                   ' clear the buffer
  r := LoadEeprom(e_offset + $0000)                     ' readback lower eeprom
  DisplayBuffer(e_offset + $0000)
  ' verify eeprom_lo
  r := FindFile(st)                                     ' eeprom file 
  Print.Str(string(" Verifying first 16KB block..."))
  Print.CRLF
  r := VerifyFile16KB(e_offset + $0000)                 ' verify lower 16KB of file
  Print.Str(string(" Verified first 16KB block"))
  Print.CRLF

  ' program eeprom upper 16KB (file already points to start of second 16KB)
  r := LoadFile16KB                                     ' load upper 16KB of file
  DisplayBuffer(e_offset + $4000)
  Print.Str(string(" Programming second 16KB block..."))
  Print.Hex(ndx,8)
  Print.CRLF
  r := ProgramEeprom(e_offset + $4000)                  ' program upper eeprom from buffer
  Print.Str(string(" Programmed second 16KB block"))
  Print.CRLF
  'now we need to reposition the file at the upper 16KB block
  r := FindFile(st)                                     ' eeprom file 
  r := LoadFile16KB                                     ' load(skip) lower 16KB of file
  bytefill(@pgmbuff, $00, MAXPGMSIZE)                   ' clear the buffer
  Print.Str(string(" Verifying second 16KB block..."))
  Print.CRLF
  r := LoadEeprom(e_offset + $4000)                     ' readback second 16KB from eeprom
  DisplayBuffer(e_offset + $4000)
  ' verify eeprom_hi
  r := VerifyFile16KB(e_offset + $4000)                 ' verify 16KB of file
  Print.Str(string(" Verified second 16KB block"))
  Print.CRLF


PRI FindFile (st) | errorString, r     
  Print.Str(string(" Locating "))
  Print.StrCR(st)
  errorString := \fat.openfile(st, "R")                 ' open source file
  checkError(st, fat.partitionError, errorString)       ' if error, display & abort

Pri LoadFile16KB | errorString, r
' Load a 16KB block from file
{
  ndx~
  repeat while ndx < MAXPGMSIZE
    r := fat.readByte                                   ' read a byte at a time from the file 
    if r < 0
      Print.Str(string("t) File error"))
      Print.CRLF
      abort -3
    else
      pgmbuff[ndx++] := r                               ' save bytes to buffer
  return 0
}
  errorString := \fat.readData(@pgmbuff, MAXPGMSIZE)
  checkError(@filename0, fat.partitionError, errorString)   ' if error, display & abort


Pri VerifyFile16KB (offset) | r
' Verify 16KB block of file
  ndx~
  repeat while ndx < MAXPGMSIZE
    r := fat.readByte                                   ' read a byte at a time from the file
    if r < 0
      Print.Str(string("t) File error"))
      Print.CRLF
      abort -3
    else
      if r <> pgmbuff[ndx]                              ' compare bytes
        Print.Str(string("t) Verify Error at $"))
        Print.Hex(ndx+offset,4)
        Print.CRLF
        abort -4
      ndx++
  return 0

Pri ProgramEeprom (offset) | i, start_time
'program 16KB to eeprom
  repeat i from 0 to constant(MAXPGMSIZE/PAGESIZE)-1
    if i2c.WritePage(i2c#BootPin, i2c#EEPROM, offset+(i*PAGESIZE), @pgmbuff[i*PAGESIZE], PAGESIZE)
      abort -1                                          'error during write
    start_time := cnt                                   'prepare to check for a timeout
    repeat while i2c.WriteWait(i2c#BootPin, i2c#EEPROM, offset+(i*PAGESIZE))
      if cnt - start_time > clkfreq / 10
        abort -2                                        'waited more than 100ms

Pri LoadEeprom (offset) | i
'readback 16KB only
  repeat i from 0 to constant(MAXPGMSIZE/PAGESIZE)-1
    if i2c.ReadPage(i2c#BootPin, i2c#EEPROM, offset+(i*PAGESIZE), @pgmbuff[i*PAGESIZE], PAGESIZE)
      abort -1                                          'error during read

PRI DisplayBuffer (offset) | r, c, lc[16], w, adx
'display first 16KB
  w := 16
  if byte[_hub#_OS_Columns] < 70
    w := 8
  adx~
  ndx~
' repeat while ndx < (MAXPGMSIZE / 64)                  '<==== only show 0.25KB
  repeat while ndx < (MAXPGMSIZE / 128)                 '<==== only show 0.125KB
    Print.Hex((adx + offset),4)
    Print.Str(string(":"))
    c := 0
    repeat while c < w
      r := pgmbuff[ndx++]                               ' get next char
      Print.Char(" ")
      Print.Hex(r,2)
      lc[c] := r
      c++
    c := 0
    Print.Char(" ")
    repeat while c < w
      r := lc[c]
      Print.ASCII(r)
      c++
    adx += w
    Print.CRLF

dat                                                   
{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}} 