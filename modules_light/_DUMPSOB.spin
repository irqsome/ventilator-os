'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _DUMPSOB --> _DUMPSOB.CMD|
'' +--------------------------------------------------------------------------+
'' | A Compiler Utility for the Propeller Chip       _DUMPSOB.SPIN     v0.xxx |
'' +--------------------------------------------------------------------------+
'' |  Authors:                                                                |
'' |  (c) 2009,2010      Michael Park (mpark)       (original Sphinx compiler)|
'' |  (c) 2015,2016      Ray Rodrick (Cluso99)      (include in PropOS)       |
'' |  License            MIT License - See end of file for terms of use       |
'' +--------------------------------------------------------------------------+
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' This OS module uses an included module "__MODULE.spin" to perform most of
''  the housekeeping - see this for history, details and acknowledgements.
''
'' This module......
''
''      DUMPSOB <filename>              ' Displays filename.SOB (Compiler file)
'' +--------------------------------------------------------------------------+
'' RR20160305 v060  (Thanks to Dave Hein's idea for sobdump in Spinix)
''                   not working properly yet!!
'' RR20160307 v061  working

'''#include "__MODULE.spin"         ' include the common code for OS modules 

OBJ
  sym   : "spin_symbols"
  str   : "StringE"

CON
  MAXFILENAMELENGTH = 8 + 1 + 3 ' 8.3
  MAXTOKENLENGTH = 32
   
  SIZEOFSOBHEADER = 26

DAT
SobFileHeader long
Sob1          byte      "SOB1"
timestamp     long      0
hash          long      0
numExports    word      0                                                                        
exportSize    word      0
numImports    word      0
importSize    word      0
objBinSize    word      0
checksum      byte      0
padding       byte      0      
varSize       word      0  ' total size in bytes of vars (rounded up to multiple of 4)

'Exports
pName         byte      0[MAXTOKENLENGTH]
type          byte      0
val           long      0
ndx           byte      0
args          byte      0
'Imports
iName         byte      0[MAXTOKENLENGTH]
icount        word      0
iresv         byte      0
'Obj/Var
misc          byte      0
justincase    long      0

filename                byte    0[MAXFILENAMELENGTH+1]                          ' input  .SOB file 

PRI executeCommand(StringPointer) | l, errorString, n, i, j, n1,n2,n3,n4

  'get the parameters
  str.stringCopy(@filename, str.tokenizeString(0),constant(MAXFILENAMELENGTH+1)) ' fat filename

  if strsize(@filename) == 0                                                    ' if no parameters, display usage
    {Print.StrCR(string(" Display SOB File..."))
    Print.StrCR(string("    DumpSOB <filename>"))
    }return -2

  Print.StrCR(string("DumpSOB v0.61: Display SOB File"))

  str.stringToUpperCase(@filename)
  l := strsize( @filename )
  if filename[l-4] == "." and filename[l-3] == "S" and filename[l-2] == "O" and filename[l-1] == "B"
    filename[l-4]~
  if strsize( @filename ) > 8
    abort string(" -- filename too long")

  str.stringAppend( @filename, string(".SOB") ,constant(MAXFILENAMELENGTH+1))

'--------------------------------------------------------
' ALL INITIALISATION COMPLETE
'--------------------------------------------------------

'-----------------------------------------------------------------
' Open the FAT source file
  errorString := \fat.OpenFile(@filename, "R")                                  ' open source file
  checkError(@filename, fat.PartitionError, errorString)                        ' if error, display & abort
'-----------------------------------------------------------------
' Read Header
  fat.ReadData( @SobFileHeader, SIZEOFSOBHEADER )
  Print.Space
  repeat i from 0 to 3
    Print.ASCII(Sob1[i])
  Print.Str(string(" ts="))
  Print.Decimal(timestamp)
  Print.Str(string(" hash="))
  Print.Hex(hash, 8)
  Print.Str(string(" Exports="))
  Print.Decimal(numExports)
  Print.Char(",")
  Print.Decimal(exportSize)
  Print.Str(string(" Imports="))
  Print.Decimal(numImports)
  Print.Char(",")
  Print.Decimal(importSize)
  Print.Str(string(" ObjSize="))
  Print.Hex(objBinSize, 4)
  Print.Str(string(" csum="))
  Print.Hex(checksum, 2)
  Print.Space
  Print.Hex(padding, 2)
  Print.Str(string(" VarSize="))
  Print.Hex(varSize, 4)
  Print.CRLF
'-----------------------------------------------------------------
' Read Exports
  Print.Str(string(" Exports: "))
  Print.Decimal(numExports)
  Print.CRLF
  if numExports > 0
    repeat i from 1 to numExports
      ReadString(pname, MAXTOKENLENGTH)                                         ' includes null terminator
      fat.ReadData(@type, 1)
      Print.Spaces(2)
      Print.Str(pname)
      Print.Space
      Print.Hex(type, 2)
      Print.Space
      if type == sym#kINT_CON_SYMBOL or type == sym#kFLOAT_CON_SYMBOL           ' name + null + type + 4-byte value
        fat.ReadData(@val, 4)
        Print.Hex(val, 8)
      elseif type == sym#kPUB_SYMBOL                                            ' name + null + type + index + #args
        fat.ReadData(@ndx, 1)
        fat.ReadData(@args, 1)
        Print.Decimal(ndx)
        Print.Char(",")
        Print.Decimal(args)
      else
        Print.Char("?")
      Print.CRLF
'-----------------------------------------------------------------
' Read Imports
  Print.Str(string(" Imports: "))
  Print.Decimal(numImports)
  Print.CRLF
  if numImports > 0
    repeat i from 1 to numImports 
      ReadString(iname, MAXTOKENLENGTH)                                         ' fname + count + resv
      fat.ReadData(@icount, 1)
      Print.Spaces(2)
      Print.Str(iname)
      Print.Str(string(".SOB "))
      Print.Decimal(icount)
      Print.Space
      Print.Hex(iresv, 2)
      Print.CRLF
'-----------------------------------------------------------------
' Obj
  Print.Str(string(" Obj: "))
  Print.Hex(objBinSize, 4)
  Print.CRLF
  if objBinSize > 0
    j~
    repeat i from 1 to objBinSize
      n := fat.ReadData(@misc, 1)
      if n < 1
        quit
      if j == 0
        Print.Spaces(2)
      Print.Hex(misc, 2)
      Print.Space
      j++
      if j => 16
        j~
        Print.CRLF
    if j > 0    
      Print.CRLF
'-----------------------------------------------------------------
' Var
  Print.Str(string(" Var: "))
  Print.Hex(varSize, 4)
  Print.CRLF
  if varSize > 0
    j~
    repeat i from 1 to varSize
      n := fat.ReadData(@misc, 1)
      if n < 1
        quit
      if j == 0
        Print.Spaces(2)
      Print.Hex(misc, 2)
      Print.Space
      j++
      if j => 16
        j~
        Print.CRLF
    if j > 0    
      Print.CRLF
'-----------------------------------------------------------------
  Print.StrCR(string(" --------"))
  waitcnt(cnt + clkfreq/10_000)                         'tiny delay to let terminal keep up!
  fat.closeFile
  return 0

pri ReadString( p, MAXLENGTH ) | ch, n
  n~
  repeat MAXLENGTH + 1
    fat.ReadData(@ch, 1)
    if "a" =< ch and ch =< "z"
      ch -= constant("a" - "A")
    ifnot byte[p++] := ch
      return n
    n++  
  abort string("*String too long")



{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    |
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}