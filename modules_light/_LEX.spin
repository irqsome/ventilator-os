'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _LEX     --> _LEX.CMD    |
'' +--------------------------------------------------------------------------+
'' | Sphinx     A Compiler for the Propeller Chip    _LEX.SPIN         v0.xxx |
'' +--------------------------------------------------------------------------+
'' |  Authors:                                                                |
'' |  (c) 2009,2010      Michael Park (mpark)       (original Sphinx compiler)|
'' |  (c) 2015,2016      Ray Rodrick (Cluso99)      (include in PropOS)       |
'' |  License            MIT License - See end of file for terms of use       |
'' +--------------------------------------------------------------------------+
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' This OS module uses an included module "__MODULE.spin" to perform most of
''  the housekeeping - see this for history, details and acknowledgements.
''
'' This module......
''
''      LEX <filename> [-Vn]
''
''      Reads filename.spn on SD card, writes tokens to filename.tok
'' +--------------------------------------------------------------------------+
'' RR20151228 v020  Convert to run under PropOS
'' RR20151231       rename _LEX.SPIN
'' RR20160101       stringConcatenate -> stringAppend
'' RR20160104       tidy
'' RR20160125 v036  get working (see fixes in tokenizr.spn) "saved20160129"
'' RR20160212 v040  tokenizr -> _tokeniz; keywords -> _keyword; bintree -> _bintree; etc
'' RR20160213       tweek file handling
'' RR20160219
'' RR20160228 v050  tweek to c5
'' RR20160304 v060  release
'' RR20160307 v061  tidy debugging


'''#include "__MODULE.spin"         ' include the common code for OS modules 
'#include "__MODULE.spin" 

OBJ
  token : "spin_tokeniz"
  key   : "spin_keyword"
  bt    : "spin_bintree"
  str   : "StringE"
   
CON
  MAXFILENAMELENGTH = 8 + 1 + 3 ' 8.3
  MAXSTRINGBUFFERLENGTH = 32

VAR
  byte spinFilename[MAXFILENAMELENGTH+1]                ' input  .SPN file 
  byte outputFilename[MAXFILENAMELENGTH+1]              ' output .TOK file  
  byte param[4]                                         ' optional parameter(s)
  byte param2[4]
  byte param3[4]
  byte stringBuffer[MAXSTRINGBUFFERLENGTH+1]            ' temp string buffer
  long pTokenText
  
dat
verbosity     byte      1       ' set by -V option      (default=1)
type          long      0       '\ keep together
lineNum       word      0       '|
column        word      0       '/
  

PRI executeCommand(StringPointer) | l, err

  'get the parameters
  str.stringCopy(@spinFilename, str.tokenizeString(0),constant(MAXFILENAMELENGTH+1)) ' fat filename
  str.stringCopy(@param, str.tokenizeString(0),4)                               ' optional -Vn parameter

  if (spinFilename[0] == "?" and spinFilename[1] == 0)                          ' help parameter...
    'Print.StrCR(string(" LEX <filename> [-Vn]"))
    return -2

  if (param[0] == "-" or param[0] == "/") and (param[1] == "v" or param[1] == "V")
    verbosity := param[2] & $0F

  if verbosity => 1
    Print.StrCR(string("LEX v0.61: Compile SPIN/PASM #1"))

  str.stringToUpperCase(@spinFilename)
  l := strsize( @spinFilename )
  if spinFilename[l-4] == "." and spinFilename[l-3] == "S" and spinFilename[l-2] == "P" and spinFilename[l-1] == "N"
    spinFilename[l-4]~
  if strsize( @spinFilename ) > 8
    abort string(" -- filename too long")

  str.stringCopy( @outputFilename, @spinFilename ,constant(MAXFILENAMELENGTH+1))
  str.stringAppend( @spinFilename, string(".SPN") ,constant(MAXFILENAMELENGTH+1))
  str.stringAppend( @outputFilename, string(".TOK"),constant(MAXFILENAMELENGTH+1) )

'--------------------------------------------------------
' ALL INITIALISATION COMPLETE
'--------------------------------------------------------

  err := \Process
  if err
    if err > 0
      Print.Str( err )
    else
      Print.Str( string("*Error ") )
      Print.Decimal( err )
    Print.CRLF
    Print.Decimal( token.LineNumber )
    Print.Char( "," )
    Print.Decimal( token.Column + 1 )
    Print.Char( " " )
    Print.Char( "'" )
    Print.Str( pTokenText )
    Print.Char( "'" )
    Print.CRLF
    return(-1)                                          ' error

  bt.Stop
  return(0)                                             ' success
  

pri Process | v, errorString

  bt.Init( 0, 0 )
  key.Init
  pTokenText := token.GetPText

  if verbosity => 2
    Print.Str( string(" Reading ") )
    Print.StrCR( @spinFilename )
  token.Open( @spinFilename )

  if verbosity => 2
    Print.Str( string(" Writing ") )
    Print.StrCR( @outputFilename )

'-----------------------------------------------------------------
' If the FAT Destination file Exists, Delete it
  errorString := \fat.deleteEntry(@outputFilename)                              ' delete file
  if (fat.partitionError <> $0D)                                                ' if error other than not found,
    checkError(@outputFilename, fat.partitionError, errorString)                '   display & abort
'-----------------------------------------------------------------
' Create the FAT destination file 
  errorString := \fat.newFile(@outputFilename)
  checkError(@outputFilename, fat.partitionError, errorString)                  ' if error, display & abort
'-----------------------------------------------------------------
' Open the FAT destination file for Writing
  errorString := \fat.openFile(@outputFilename, "W")                            ' open destination file for write
  checkError(@outputFilename, fat.partitionError, errorString)                  ' if error, display & abort
'-----------------------------------------------------------------

  repeat
    type := token.Type
    lineNum := token.LineNumber
    column := token.Column

    if verbosity => 9
      Print.Str(str.integerToHexadecimal(type, 8))
      Print.Space
      Print.Str(str.integerToDecimal(lineNum,10))
      Print.Char(",")
      Print.Str(str.integerToDecimal(column,10))
      Print.Char(":")
      Print.Space    
      if type == key#kINTLITERAL or type == key#kFLOATLITERAL
        v := token.Value
        Print.Str(str.integerToHexadecimal(v, 8))
      else
        Print.Str(pTokenText)
      Print.CRLF
      Debug(@type, 8)                                    

    errorString := \fat.WriteData( @type, 8 )                                   ' long + word + word
    checkError(@outputFilename, fat.partitionError, errorString)                ' if error, display & abort
    
    if type == key#kINTLITERAL or type == key#kFLOATLITERAL
      v := token.Value
      errorString := \fat.WriteData( @v, 4 )
      checkError(@outputFilename, fat.partitionError, errorString)              ' if error, display & abort
    else
      errorString := \fat.WriteData( pTokenText, strsize(pTokenText) + 1 )
      checkError(@outputFilename, fat.partitionError, errorString)              ' if error, display & abort
          
    if token.Type == key#kEOF
      quit
    token.Advance
  token.Close
  fat.CloseFile

pri Debug(s,len) | i
    repeat i from 0 to len
      Print.Hex(byte[s+i], 2)
      Print.Space
    Print.Spaces(3)
    repeat i from 0 to len
      Print.ASCII(byte[s+i])
    Print.CRLF    

{{
Copyright (c) 2009 Michael Park
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    |
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}
                   