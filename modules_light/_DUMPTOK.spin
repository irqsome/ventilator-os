'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _DUMPTOK --> _DUMPTOK.CMD|
'' +--------------------------------------------------------------------------+
'' | A Compiler Utility for the Propeller Chip       _DUMPTOK.SPIN     v0.xxx |
'' +--------------------------------------------------------------------------+
'' |  Authors:                                                                |
'' |  (c) 2009,2010      Michael Park (mpark)       (original Sphinx compiler)|
'' |  (c) 2015,2016      Ray Rodrick (Cluso99)      (include in PropOS)       |
'' |  License            MIT License - See end of file for terms of use       |
'' +--------------------------------------------------------------------------+
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' This OS module uses an included module "__MODULE.spin" to perform most of
''  the housekeeping - see this for history, details and acknowledgements.
''
'' This module......
''
''      DUMPTOK <filename>              ' Displays filename.TOK (Compiler File)
'' +--------------------------------------------------------------------------+
'' RR20160305 v060  (Thanks to Dave Hein's idea for tokdump in Spinix)
'' RR20160307 v061  works


'#include "__MODULE.spin"         ' include the common code for OS modules 

OBJ
  key   : "spin_keyword"
  str   : "StringE"
   
CON
  MAXFILENAMELENGTH = 8 + 1 + 3 ' 8.3
  MAXTOKENLENGTH = 32
   
dat
tokenType               long    0
tokenValue              long    0
tokenLineNumber         word    0
tokenCol                word    0
tokenText               byte    0[MAXTOKENLENGTH+1]
filename                byte    0[MAXFILENAMELENGTH+1]                          ' input  .TOK file 

PRI executeCommand(StringPointer) | l, errorString, n, i, n1,n2,n3,n4

  'get the parameters
  str.stringCopy(@filename, str.tokenizeString(0),constant(MAXFILENAMELENGTH+1)) ' fat filename

  if strsize(@filename) == 0                                                    ' if no parameters, display usage
    {Print.StrCR(string(" Display TOK File..."))
    Print.StrCR(string("    DUMPTOK <filename>"))
    }return -2

  Print.StrCR(string("DumpTOK v0.61: Display TOK File"))

  str.stringToUpperCase(@filename)
  l := strsize( @filename )
  if filename[l-4] == "." and filename[l-3] == "T" and filename[l-2] == "O" and filename[l-1] == "K"
    filename[l-4]~
  if strsize( @filename ) > 8
    abort string(" -- filename too long")

  str.stringAppend( @filename, string(".TOK"),constant(MAXFILENAMELENGTH+1) )

'--------------------------------------------------------
' ALL INITIALISATION COMPLETE
'--------------------------------------------------------

'-----------------------------------------------------------------
' Open the FAT source file
  errorString := \fat.OpenFile(@filename, "R")                                  ' open source file
  checkError(@filename, fat.PartitionError, errorString)                        ' if error, display & abort
'-----------------------------------------------------------------

  byte[@tokenText]~                                     'clear tokentext

  repeat
    n1 := fat.ReadData(  @tokenType,       4 )
    if n1 == 0
      quit
    n2 := fat.ReadData(  @tokenLineNumber, 2 )
    n3 := fat.ReadData(  @tokenCol,        2 )
    if tokenType == key#kINTLITERAL or tokenType == key#kFLOATLITERAL
      n4 := fat.ReadData(@tokenValue,      4 )
    else
      n4 := ReadString( @tokenText, MAXTOKENLENGTH )
  
    Print.Space
    Print.Hex(tokenType,8)
    Print.Space
    if tokenLineNumber < 10
      Print.Space
    if tokenLineNumber < 100
      Print.Space
    Print.Decimal(tokenLineNumber)
    Print.Char(",")
    Print.Decimal(tokenCol)
    if tokenCol < 10
      Print.Space
    if tokenCol < 100
      Print.Space
    Print.Space
    if tokenType == key#kINTLITERAL or tokenType == key#kFLOATLITERAL
      Print.Hex(tokenValue,8)
      Print.Char("=")
      Print.Decimal(tokenValue)
    else
      if n4 > 0
        Print.Str(@tokenText)
    Print.CRLF
    waitcnt(cnt + clkfreq/10_000)                         'tiny delay to let terminal keep up!

'-----------------------------------------------------------------
  Print.StrCR(string(" --------"))
  fat.closeFile
  return 0

pri ReadString( p, MAXLENGTH ) | ch, n
  n~
  repeat MAXLENGTH + 1
    fat.ReadData(@ch, 1)
    if "a" =< ch and ch =< "z"
      ch -= constant("a" - "A")
    ifnot byte[p++] := ch
      return n
    n++  
  abort string("*String too long")

{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    |
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}