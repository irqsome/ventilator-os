'' +--------------------------------------------------------------------------+
'' | VentilatorOS                       - OS boot program:   _OS.BIN          |
'' +--------------------------------------------------------------------------+
'' |  Authors:       (c)2011 by Kwabena W. Agyman (Kye) & J. Moxham (Dr_Acula)|
'' |                 (c)2012-2016 "Cluso99" Ray Rodrick                       |
'' |                 (c)2009-2010 "mpark"   Michael Park (Sphinx compiler)    |
'' |                 (c)2019      "Wuerfel_21" Ada Gottensträter              |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+
'' This OS boot program sets up Upper HUB Memory according to the hardware,
''   and then calls the command module (_CMD.CMD)
''  
'' This program may be programmed into the EEPROM for booting. IMHO it is
''   better to program the generic bootloader "_BOOT_OS.spin" which compiles
''   to "Bootxxxx.EEP/eeprom".
''
'' The command module issues the prompt "SD:>" and waits for a command,
''   then executes the command internally, or externally or by calling
''   an external module (_xxxxxxx.CMD) or a binary file (xxxxxxxx.BIN)
''
'' All external command modules will return to _CMD.CMD after execution.
''
'' Some OS modules are derived from: 
''      KyeDOS v3 (c) 2011 by Kwabena W. Agyman (Kye) & J. Moxham (Dr_Acula)
''      Sphinx    (c) 2009-10 by Michael Park and others
'' Some concepts and code is from Sphinx (Michael Park) & Spinix (Dave Hein).
'' 
'' Special thanks to the following authors for their ideas and MIT code:
''   Michael Park, Dave Hein, Kye, James Moxham, Mike Green, Tomas Rokicki,
''   lonesock, hippy, Jeff Ledger, Roger Williams, Michelli Scales, Andy Schenk.
''   My apologies to any others I may have missed.
'' And of course to Chip Gracey for the fabulous Prop chip and the Spin Interpreter.
'' +--------------------------------------------------------------------------+
'' Modifications by Cluso99...
'' RR20120306   000   Initial module _DOS.CMD
'' RR20120323   004   v0.40 "OS.BIN" 
'' RR20120325   005   v0.50 revised hub layout
'' RR20120619   006   v0.60 use separate _SD_PASM.spin & _SD_FAT.spin
'' RR20130708               fix __module.spin (mountpartition for 2nd file access)
'' RR20130719         v0.70 fix _HWDEF.spin for RamBlade SD pins; add info option, improve SD errors
''                          no longer permits running _os.cmd again - delay reqd (was removed)
'' RR20130727               fixed re-executing _os.cmd & _os.bin (calling from RamBlades PropCmd does not work)
'' RR20130728               try outputting SD clocks (PropCMD cannot boot _OS.BIN)
'' RR20130730         v0.72 try resetting card (blockmode etc) using sd_reset.spin (mb_small_spi.spin from kye)
'' RR20130731         v0.73 (use "PropEEP.BIN" to reprogram the eeprom)
'' RR20130802         v0.74 fix _cmd to clear hub on load/run .bin files
'' RR20130802         v0.75 temp fix to use "_RUN.CMD"
'' RR20130903         v0.77 add _getpc & _putpc (incl getcpm -T/B options not wkg)
'' RR20131022         v0.78 togglelf now external command LF
'' RR20151210         v0.79 add P8XBlade2 detection
'' RR20151215         v0.80 add hubdump
'' RR20151216         v0.81 mod _SD_PASM.spin
'' RR20151219         v0.83 mod _SD_PASM.spin, mod hubdump.spin
'' RR20151220         v0.84 add CpuBlade7
'' RR20160104         v0.85 tidy
'' RR20160106         v0.87 mod TYPE <filename> [-Hn], CLEAR and ECHO now external commands
''                          fixed issue with RUN <filename>
'' RR20160121         v0.88 add DNLOAD (no parameters - supports PropTool F7, Ctl-F10, Ctl-F11 without reset)
''                    v0.89 default to 5MHz PLL16X SD=0..3 if not RamBlade/RamBlade3/TriBlade#2/CpuBlade7/P8XBlade2
''                    v0.90 tidy & release
'' RR20160129         v0.92 tidy/ship with BOOTP8X2.EEP (_BOOT_OS for P8XBlade2)
''                          _HUBDEF3.spin -> _HUBDEF.spin
'' RR20160129         v0.93
'' RR20160130         v0.94 fix RUN module
'' RR20160212         v0.95
'' RR20160212         v0.96 (_HUBDEF modified ready for stay-resident SD pasm code)
'' RR20160213         v0.97a another attempt at getting codegen.spin (sphinx) to work
'' RR20160219         v0.98  mod _stdin.spin & _stdout.spin to always use _hub#_StdIn & _hub#_StdOut rendezvous locations
''                           use _StdOut for Print.Str etc; _cmd.cmd->_cmd_.cmd (prevents old _OS eeprom boot)
'' RR20160219 v1.00  Release v1.00
'' RR20160229 v1.02
'' RR20160304 v1.10  Release v1.10 (includes compiler LEX/CODEGEN/LINK)
'' RR20160307 v1.11/v1/12    not released (now includes DUMPFIL/HUB/TOK/SOB) and updated?? lex/codegen/link
'' RR20160726         v1.13  _program: add read from eeprom and write to file
'' RR20161008 v1.14  Release v1.14
        


CON
  _clkfreq = 40_000_000         '\ just a default to get going (DO NOT CHANGE)
  _clkmode = xtal1 + pll8x      '/  (to modify, change in _HWDEF.spin)

OBJ
  _hw   : "_HWDEF"                      ' hardware pin definitions etc
  _hub  : "_HUBDEF"                     ' fixed hub definitions ($7FFF... down)
  _c    : "_CHARDEF"                    ' ascii char definitions

  fat   : "_SD_FAT.spin"                
  taf   : "_SD_FAT.spin"                
  str   : "StringE"                     ' (renamed ASCII0_STREngine)
  strb  : "StringB"
  std   : "_PCSIO.spin"                 ' pc i/o driver (loaded into separate cog)
#ifdef STDOUT_USE_TV 
  tv    : "_TVOUT.spin"                 ' TV driver (loaded into separate cog)
#endif
#ifdef STDOUT_USE_VGA
  vga  : "_VGAOUT.spin"                 ' VGA driver (loaded into separate cog)
#endif
#ifdef STDIN_USE_KEYBOARD 
  key   : "_germanKeyboard.spin"        ' Keyboard driver (loaded into separate cog)
#endif

  stdin : "_STDIN.spin"                 ' use std input driver
  print : "_STDOUT.spin"                ' use std output driver
  osver : "_OSVER.spin"
  builddesc : "_BUILD.spin"

VAR

 long _SDpins,_SIOpins,_SIObaud,_clkmode_,_clkfreq_

PUB Start | char, cog,z,z0,z1,z2,z3,z4,i,j,cogs[8], errorString 

{------------------------------------------------------------------------------}
'Determine what hardware we are running on
' preset for defined configuration in _HWDEF
  _SIOpins := _hw#_SIOpins
  _SIObaud := _hw#_SIObaud
  _SDpins  := _hw#_SDpins
  _clkfreq_:= _hw#_clkfreq_
  _clkmode_:= _hw#_clkmode_

' now check for RamBlade/RamBlade3/TriBlade#2/P8XBlade2/CpuBlade7
  {z := ina                                             'read the input pins (to check pullups)
  if     (z & _hw#_tb2PU) == _HW#_tb2PU                 'TriBlade#2 ?
    _SDpins  := _hw#_tb2
    _clkfreq_:= _hw#_tb2freq
    _clkmode_:= _hw#_tb2pll
  elseif (z & _hw#_rb1PU) == _hw#_rb1PU                 'RamBlade ?
    _SIOpins := _hw#_sioRB1                             ' serial pins differ on RB1
    _SDpins  := _hw#_rb1
    _clkfreq_:= _hw#_rb1freq
    _clkmode_:= _hw#_rb1pll
  elseif (z & _hw#_rb3PU) == _hw#_rb3PU                 'RamBlade3 ?
    _SDpins  := _hw#_rb3
    _clkfreq_:= _hw#_rb3freq
    _clkmode_:= _hw#_rb3pll
  elseif (z & _hw#_px2PU) == _hw#_px2PU                 'P8XBlade2 ?
    _SDpins  := _hw#_px2
    _clkfreq_:= _hw#_px2freq
    _clkmode_:= _hw#_px2pll
  elseif (z & _hw#_cb7PU) == _hw#_cb7PU                 'CpuBlade7 ?
    _SDpins  := _hw#_cb7
    _clkfreq_:= _hw#_cb7freq
    _clkmode_:= _hw#_cb7pll }

  clkset(_clkmode_, _clkfreq_)                          ' set new clkmode & ckfreq
  waitcnt(cnt + clkfreq/1000)

{------------------------------------------------------------------------------}
'Set hub parameters...
  long[_hub#_HubFree]     := _hub#_HUB_RESERVED         ' set top of hub free
  byte[_hub#_OS_Cogs]     := 0                          ' set resident cogs (not to be stopped)
  byte[_hub#_OS_Clkmode]  := byte[$0004]                ' save the current clockmode
  long[_hub#_OS_Clkfreq]  := long[$0000]                ' save the current clockfreq
' long[_hub#_OS_Seconds]  := 126_230_400 * 4            ' Yr2016 = 4yrs*4
  long[_hub#_OS_DateTime] := 0                          '?????
  long[_hub#_SDpins]      := _SDpins & $1F1F1F1F
  byte[_hub#_RAMpin]      := _hw#_RAMpin_CS        
  long[_hub#_SIOpins]     := $0F << 24 | _SIOpins       ' no cog running yet
  long[_hub#_SIObaud]     := _SIObaud                              
  long[_hub#_Hardware]    := $0ADA_0001                 ' $0001_000x = cluso's hardware

  str.copy(string("cmd_"),_hub#_pBuffer_A)              ' parameters passed between modules

{------------------------------------------------------------------------------}

#ifdef STDINOUT_USE_SERIAL
' Start serial...
'                 (inRENDEZVOUS, outRENDEZVOUS, serPins,  baud    )
  cog := std.start(_hub#_StdIn, _hub#_StdOut, _SIOpins, _SIObaud) ' start driver, returns cog+1
'Update hub parameters...
  long[_hub#_SIOpins]     := (cog-1) << 24 | _SIOpins
  byte[_hub#_OS_COGS]     := (1 << (cog -1))            ' set resident cog
  '' Serial terminal size is an assumption
  byte[_hub#_OS_Rows]     := 25 | 0<<7                  ' Screen rows & <lf> off (_hub#_OS_LFmask)
  byte[_hub#_OS_Columns]  := 80                         ' Screen columns
#endif

#ifdef STDIN_USE_KEYBOARD
' Start Keyboard
  cog := key.start(_hub#_StdIn,_hub#_StdOut,_hw#_KBDpin_DAT,_hw#_KBDpin_CLK, %0_001_110, %01_01000) ' start driver, returns cog+1
  byte[_hub#_KeyCog]      := cog
  byte[_hub#_OS_COGS]     |= (1 << (cog -1))            ' set resident cog
#endif

#ifdef STDOUT_USE_TV
' Start TV
  cog := tv.start(_hub#_StdIn,_hub#_StdOut,_hub#_screenBuffer,0,_hw#_TVpins,1) ' start driver, returns cog+1                                                                                                                                                        
  byte[_hub#_OS_Rows]     := 24 | 0<<7                  ' Screen rows & <lf> off (_hub#_OS_LFmask)
  byte[_hub#_OS_Columns]  := 40                         ' Screen columns
  byte[_hub#_TVcog]       := cog
  byte[_hub#_OS_COGS]     |= (1 << (cog -1))            ' set resident cog
#elseifdef STDOUT_USE_VGA
' Start VGA                                                                            
  cog := vga.start(_hub#_StdIn,_hub#_StdOut,_hub#_screenBuffer,_hw#_VGAbasepin/8,_hw#_VGApinmask,1) ' start driver, returns cog+1  
  byte[_hub#_OS_Rows]     := 29 | 0<<7                  ' Screen rows & <lf> off (_hub#_OS_LFmask)
  byte[_hub#_OS_Columns]  := 32                         ' Screen columns
  byte[_hub#_TVcog]       := cog
  byte[_hub#_OS_COGS]     |= (1 << (cog -1))            ' set resident cog
#endif

{------------------------------------------------------------------------------}
  waitcnt(cnt + clkfreq/10)                             ' small delay
  Print.Char(_c#_clearScreenCharacter)                  ' clear screen
  Print.Char(constant(_c#color_white+_c#colors_reverse)) ' reverse video on
  strb.builtString(true)
  strb.buildStringStr(string("*** "))
  strb.buildStringStr(osver.distro_name)
  char := constant((" ")+("v"<<16))
  strb.buildStringStr(@char)
  strb.buildStringStr(1+str.integerToDecimal(osver#major,str.decimalLength(osver#major)))
  strb.buildString(".")
  strb.buildStringStr(1+str.integerToDecimal(osver#minor,str.decimalLength(osver#minor)))
  if osver#revision
    strb.buildString(".")
    strb.buildStringStr(1+str.integerToDecimal(osver#revision,str.decimalLength(osver#revision)))
  strb.buildStringStr(string(" ***"))
  char := strb.builtString(true)
  print.spaces(((byte[_hub#_OS_Columns] - strsize(char))~>1)#>0)
  Print.Str(char)
  Print.char(11) 'clear to end of line
  Print.crlf
  Print.Char(_c#color_white) ' reverse video off
  'Print.StrCR(string("----------------------------------------"))
  Print.StrCR(string("(see 'VER' for acknowledgements)"))

  Print.str(string("Build: "))
  Print.strCR(builddesc.get)

{------------------------------------------------------------------------------}
' Hardware Info...
  Print.Str(string("Hardware: "))
  char := osver.hw_vendor(long[_hub#_hardware])
  ifnot char
    strb.buildString("?")
    strb.buildStringStr(str.integerToHexadecimal(long[_hub#_hardware]>>16,4))
    char := strb.builtString(true)
  Print.Str(char)
  Print.space
  char := osver.hw_name(long[_hub#_hardware])
  ifnot char
    strb.buildString("?")
    strb.buildStringStr(str.integerToHexadecimal(long[_hub#_hardware]&$ffff,4))
    char := strb.builtString(true)
  Print.Str(char)
  Print.CRLF

  Print.Str(string("CPU Speed: "))
  Print.decimal(long[$0]/1_000_000)
  Print.char(".")
  Print.str(1 + str.integerToDecimal(long[$0]//1_000_000,6))
  Print.Str(string("MHz (ClockMode $"))
  Print.Str(str.integerToHexadecimal(byte[$4],2))
  Print.StrCR(string(")"))
  

{------------------------------------------------------------------------------}
' Serial Info...
  z  := long[_hub#_sioPins]
  z0 :=  z        & $1F
  z1 := (z >>  8) & $1F
  z2 := (z >> 16) & $1F
  z3 := (z >> 24) & $0F                               ' =$0F if invalid
  z4 :=  long[_hub#_sioBaud]
  Print.Str(string(" Serial on pins "))
  Print.Str(1 + str.integerToDecimal(z0, 2))
  Print.Char(",")
  Print.Str(1 + str.integerToDecimal(z1, 2))
  Print.Str(string(", Mode "))
  Print.Str(1 + str.integerToDecimal(z2, 2))
  Print.Str(string(", Baud "))
  Print.Str(1 + str.integerToDecimal(z4, 8))
  Print.Str(string(", Cog "))
  if z3 > 7
    Print.Char("?")
  else
    Print.Str(1 + str.integerToDecimal(z3, 1))
  Print.CRLF

  Print.Str(string(" Screen size: "))
  z := byte[_hub#_OS_Columns]
  Print.Str(1 + str.integerToDecimal(z, 3))
  Print.Str(string(" x "))
  z := byte[_hub#_OS_Rows]
  Print.StrCR(1 + str.integerToDecimal(z, 3))

  Print.Str(string(" <LF> is "))
  if byte[_hub#_OS_Rows] & _hub#_LF_MASK                                              
    Print.StrCR(string("ON"))
  else
    Print.StrCR(string("OFF"))

{------------------------------------------------------------------------------}
'SD Driver Info...
  z  := long[_hub#_sdPins]
  z0 :=  z        & $1F
  z1 := (z >>  8) & $1F
  z2 := (z >> 16) & $1F
  z3 := (z >> 24) & $1F
  Print.Str(string(" SD card on pins "))
  Print.Str(1 + str.integerToDecimal(z0, 2))
  Print.Char(",")
  Print.Str(1 + str.integerToDecimal(z1, 2))
  Print.Char(",")
  Print.Str(1 + str.integerToDecimal(z2, 2))
  Print.Char(",")
  Print.Str(1 + str.integerToDecimal(z3, 2))
  Print.CRLF

{------------------------------------------------------------------------------}
' Start SD Driver...
'                          (DO, CLK, DI, CS, _SD_WP, _SD_CD, _RTC_DAT, _RTC_CLK, -1)
  cog := fat.FATEngineStart(z0, z1,  z2, z3, -1,     -1,     -1,       -1,       -1)
  ifnot (cog)
    reboot
'Update hub parameters...
  'byte[_hub#_OS_COGS]    |= (1 << (cog -1))            ' set resident cog
  '' SD DRIVER IS NOT RESIDENT WE DON'T CARE
  {Print.Str(string(" SD Driver Cog = "))
  Print.Char((cog -1) | $30)
  Print.CRLF}

{------------------------------------------------------------------------------}
' Cog(s) Info...
  Print.Str(string("Resident Cogs : "))
  repeat z from 0 to 7
    if (byte[_hub#_OS_Cogs] >> z) & $01 == 1
      Print.Str(1 + str.integerToDecimal(z, 1))
      Print.Str(string(" "))
  Print.Str(string(" This Cog: "))
  Print.StrCR(1 + str.integerToDecimal(cogid, 1))

  Print.Str(string("Cogs available: "))
  j~
  repeat i from 0 to 7
    cogs[i] := cognew(@entry,0) +1                    ' try to start cog(s)
    if cogs[i] 
      j++
  repeat i from 0 to 7
    if cogs[i]
      Print.Str(1 + str.integerToDecimal(cogs[i]-1, 1)) ' print avail cog#
      Print.Char(" ")
      cogstop(cogs[i] -1)                             ' stop the cog(s)
  Print.Str(string("(="))
  Print.Str(1 + str.integerToDecimal(j,1))
  Print.StrCR(string(")"))

  ' Old code for switching to serial
  ' Now handled by booting different OS images from VBIOS
  Print.StrCR(string("Entering shell..."))
  bootCard(string("_CMD_.CMD"))
  {
  Print.StrCr(string("Push space/enter/S for shell"))
  Print.StrCr(string("or X for serial transfer"))

  repeat
    case char:=stdin.char
      " ","s","S",13: ' regular boot
        bootCard(string("_CMD_.CMD"))
      "x","X": ' ' switch to serial
        switchToSerial
        bootCard(string("_CMD_.CMD"))
  }

PRI switchToSerial | cog


' Kill Keyboard

  cog := byte[_hub#_KeyCog]
  if cog--
    cogstop(cog)
    byte[_hub#_KeyCog]      := 0
    byte[_hub#_OS_COGS]     &= !(|<cog)               ' unset resident cog

' Kill TV
  cog := byte[_hub#_TVCog]
  if cog--
    cogstop(cog)
    byte[_hub#_TVCog]      := 0
    byte[_hub#_OS_COGS]     &= !(|<cog)               ' unset resident cog

  ' Start serial...
'                 (inRENDEZVOUS, outRENDEZVOUS, serPins,  baud    )
  cog := std.start(_hub#_StdIn, _hub#_StdOut, _SIOpins, _SIObaud) ' start driver, returns cog+1
'Update hub parameters...
  long[_hub#_SIOpins]     := (cog-1) << 24 | _SIOpins
  byte[_hub#_OS_COGS]     |= (1 << (cog -1))            ' set resident cog

PRI bootCard(filen) | i,errorString

{------------------------------------------------------------------------------}
' Boot the SD card...
  errorString := \fat.mountPartition(0)                 ' mount the sd card
  i := checkError(string("(SD card failure)"), fat.partitionError, errorString) ' if error, display
  if i
    repeat                      '<====================  ' non-recoverable error, loop indefinately 
      Print.Char("!")
      waitcnt(clkfreq*2 + cnt)

{------------------------------------------------------------------------------}
'Load/Run "_CMD_.CMD"...
  errorString := \fat.bootPartition(filen) ' load/run
  checkError(filen, fat.partitionError, errorString) ' if error, display
  
{------------------------------------------------------------------------------}
'Only here on error...
  Print.StrCR(string("ERROR: Rebooting..."))
  waitcnt(clkfreq * 2 + cnt)                            ' delay 
  reboot

{------------------------------------------------------------------------------}


PRI checkError(errorMessage, errorNumber, errorString)
  if errorNumber == 0
    return 0
  else
    Print.Str(string("* Error on file "))
    Print.Str(errorMessage)
    Print.Str(String(" - "))
    Print.Str(str.integerToHexadecimal(errorNumber,2))
    Print.Char(":")
    Print.StrCR(errorString)

    return errorNumber


DAT
'' Just a simple pasm program to test if a cog is available
              org       0
entry         jmp       #entry                  ' loops here until cogstop forced!

dat                                                   
{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}