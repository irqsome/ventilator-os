Sphinx linker.
Links <filename>.spn/.sob files on SD card, writes binary to <filename>.bin
use -V0 to -V9 to set verbosity.
Usage:
LINK <filename> [-Vn]
