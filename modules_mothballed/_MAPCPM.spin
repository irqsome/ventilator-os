'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _MAPCPM --> _MAPCPM.CMD  |
'' +--------------------------------------------------------------------------+
'' |  Authors:       (c)2012,2013 "Cluso99" (Ray Rodrick)                     |
'' |  Modifications:                                                          |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' This OS module uses an included module "__MODULE.spin" to perform most of
''  the housekeeping - see this for history, details and acknowledgements.
''
'' This module......
''                Creates and displays the CPM drive DIR & FAT tables
''
'' RR20130712   build cpm fat & dir table for a FAT CPM DISK
'' RR20130713   works
'' RR20130715   add -d option to display the file DIR entry
''
''
''-------------------------------------------------------------------------------------------------
''      CPM2.2 File System
''      ------------------
''      'ZiCog 8MB Hard Disk Parameter Blocks: 17 bytes CP/M + 2 bytes sector size used by SIMH hdsk_dpb
''      dpb_0                           '8MByte Default SIMH Altair HDSK params
''      :spt_low      BYTE      $20     'sectors per track (low byte)                   \ 32x 128byte sectors
''      :spt_high     BYTE      $00     'sectors per track (high byte)                  /
''      :bsh          BYTE      $05     'data allocation Block SHift factor               5=4KB  
''      :blm          BYTE      $1F     'data allocation block mask                       $1F+1=32*128=4KB
''      :exm          BYTE      $01     'EXtent Mask
''      :dsm_low      BYTE      $F9     'maximum data block number (low_byte)           \ $07F9+1= 2048-6 (8MB/4KB=2K -offset)
''      :dsm_high     BYTE      $07     'maximum data block number (high_byte)          /
''      :drm_low      BYTE      $FF     'total number of directory entries (low byte)   \ $03FF+1= 1024 entries of 32 bytes
''      :drm_high     BYTE      $03     'total number of directory entries (high byte)  /          
''      :al0          BYTE      $FF     'determine reserved directory blocks            \ $00FF+1= 256 blocks
''      :al1          BYTE      $00     'determine reserved directory blocks            /
''      :cks_low      BYTE      $00     'size of directory ChecK vector (low byte)      \ $0000 for fixed disk
''      :cks_high     BYTE      $00     'size of directory ChecK vector (high byte)     /
''      :off_low      BYTE      $06     'number of reserved tracks (offset) (low byte)  \ $0006 (6*32*128=6*4KB=24KB)
''      :off_high     BYTE      $00     'number of reserved tracks (offset) (high byte) /
''      :psh          BYTE      $00     'Physical record SHift factor, CP/M 3           \ $0000
''      :phm          BYTE      $00     'PHhysical record Mask, CP/M 3                  /
''      :ss_low       BYTE      $80     'Sector Size (low byte)                         \ $0080= 128 byte sectors
''      :ss_high      BYTE      $00     'Sector Size (high byte)                        /
''                                      'N.B. SS must be 128 for CP/M 2 can be varied for CP/M 3 hard disks.
''      
''      Directory Entries (32 bytes)
''      ----------------------------
''      UU  byte        'user number 0-15 or 0-31, $E5=unused/deleted
''      FN  byte[8]     'filename
''      TY  byte[3]     'filetype b7 of TY[0]=1=readonly, TY[1]=1=hidden
''      EX  byte        'extent counter (low) 0-31 (bits0-4) see S2 for (bits 5-12)
''                      ' where:   dir-entry-no = ((32*S2)|EX)/(exm+1) = ((S2<<5)|EX)>>1
''      S1  byte        'reserved =0
''      S2  byte        'extent counter (high) (bits 5-12)
''      RC  byte        'no of (128 byte) records used in this extent, $80=all
''                      'total records this extent
''                      'where:    recs = ((EX & exm)*128)+RC = ((EX & $01)*128)+RC
''      AL  word[8]     '8 allocation block locations (of 4KB for zicog 8MB HD)
''                      'where the block no is from the dir base (sector 192 for zicog 8MB HD)
''      'There is no allocation map (FAT) and is calculated at CPM boot from the dir entries.                             
{
DRAC_A  .DSK open 897 8388608
DIR at 24576
946 00...01 00 00 80 40 00 41 00 42 00 46 00 47 00 48 00 49 00 4A 00 .BDOS    PRN....@.A.B.F.G.H.I.J.
946 00...03 00 00 80 4B 00 4C 00 4D 00 4E 00 4F 00 50 00 51 00 52 00 .BDOS    PRN....K.L.M.N.O.P.Q.R.
946 00...05 00 00 80 53 00 54 00 55 00 56 00 57 00 58 00 59 00 5A 00 .BDOS    PRN....S.T.U.V.W.X.Y.Z.
947 00...07 00 00 80 5B 00 5C 00 5D 00 5E 00 5F 00 60 00 61 00 62 00 .BDOS    PRN....[.\.].^._.`.a.b.
947 00...09 00 00 80 63 00 64 00 65 00 66 00 67 00 68 00 69 00 80 00 .BDOS    PRN....c.d.e.f.g.h.i...
947 00...0A 00 00 40 81 00 82 00 00 00 00 00 00 00 00 00 00 00 00 00 .BDOS    PRN...@................
1008
}
''-------------------------------------------------------------------------------------------------
'The following are for CPM2.2 & "the" 8MB Hard Disk(s) we have setup in ZiCog
  sectorspertrack = $0020       'spt = 32
  blockshift      = 5           'bsh = 4KB
  blockmask       = $1F         'blm = 4KB (32*128=4096=4KB)
  extentmask      = $01         'exm = 1
  maxblock        = $07F9       'dsm = 2048 -offsettracks -1   (8MB/4KB=2K -off -1)
  direntries      = $03FF+1     'drm +1 
  reservblocks    = $00FF+1     'al0:al1 +1
'                 = $0000       'cks
  offsettracks    = $0006       'off = 6*32*128=6*4KB)
'                 = $00         'psh
'                 = $00         'phm
  sectorsize      = $80         'ss = 128
    
  dirdeleted      = $E5         'directory entry unused/deleted

  dirbase         = offsettracks * sectorspertrack '=192  'base of cpm directory entries (128 byte sectors)
  dirsize         = 32          'directory entry size = 32 bytes
  filebase        = direntries * dirsize/sectorsize + dirbase '=1024*32/128+192=448  'base of file data (end of dir entries)
  sectorsperblock = 1<< blockshift                                                ' 1<<5=32       32          "sectorsperblock"
  blocksize       = sectorsperblock * sectorsize                                  ' 32*128=4096=  4KB
  fatentries      = maxblock + (offsettracks*sectorspertrack/sectorsperblock) + 1 ' 2041+6+1=2048 2048 (x4KB) "No.of 4KB blocks"
  fatdirbase      = dirbase / sectorsperblock                                     ' 192/32=6      6
  fatfilebase     = filebase / sectorsperblock                                    ' 448/32=14     14   (=6+8)
  
  MAXEXTENTS      = 32          'maximum no of directory entries (extents) for a file (=1MB)

'''#include "__MODULE.spin"         ' include the common code for OS modules 
''#include "__MODULE.spin"         
 
VAR
  byte  databuf[512]
  long  records, allocblock, findrecord, foundaddr, total_records, free_blocks, free_dir_entry
  long  uu,ex,s1,s2,rc,al[8]
  long  dir_position[MAXEXTENTS]                'store the position of the entry (seek posn)
  byte  dir_sequence[MAXEXTENTS]                'store the directory sequence no
  byte  dir_sort[MAXEXTENTS]                    'used to sort the dir entries into sequence
  long  dir_index                               'index to dir_position & dir_sequence 

  byte  filename0[13]                           ' FAT16/32 filename                   (string)
  byte  diskname1[13]                           ' CPM drive stored as a FAT16/32 file (string)
  byte  filename1[13]                           ' CPM filename within diskname1       (string)
  byte  fname[13]                               ' CPM filename uppercase padded 8+3 w/o dot
  byte  param[10]                               ' optional parameter "-d" (any char)

  long  dirtable[direntries / 32]               ' CPM Dir Table 1024 entries mapped into 32 longs
  long  fattable[fatentries / 32]               ' CPM Fat Table 2048 entries mapped into 64 longs (2048*4KB=8MB)

DAT
'_ModuleStr   byte      "=== MAPCPM ===",0 'module's name (string)

PRI executeCommand(stringPointer) | sector[512], errorNumber, errorString, i, j, x

  'get the parameters
  str.stringCopy(@diskname1, str.tokenizeString(0),13)  ' cpm disk file A: stored as a fat file (ZICOG_A2.DSK)
  str.stringCopy(@filename1, str.tokenizeString(0),13)  ' cpm filename (optional)
  str.stringCopy(@param, str.tokenizeString(0),10)      ' optional -d parameter

  if strsize(@diskname1) == 0                           ' if no parameters, display usage
    {Print.StrCR(string(" Build the CPM DIR & FAT tables"))
    Print.StrCR(string("    MAPCPM <cpm_diskname> [<cpm_filename> [-d]]"))
    }return -2

  if strsize(@filename1) > 0                            ' if exists, prepare fname 8+3 for compare
    'convert filename1 to uppercase padded 8+3 w/o dot
    i := 0
    j := 0
    repeat while j < 11
      case filename1[i]
        "a".."z": fname[j++] := filename1[i++] - 32     ' copy ucase(a-z)
        "A".."Z": fname[j++] := filename1[i++]          ' copy A-Z
        0:   fname[j++] := " "
        ".": if j < 8
               fname[j++] := " "                        ' space fill 8 chars
             else
               i++                                      ' skip "."
        other: fname[j++] := filename1[i++]             ' copy other chars
    fname[11] := 0                                      ' terminate string


  buildCpmFatTable                                      ' build the DIR & FAT tables

  return 0


'' +--------------------------------------------------------------------------+
'' | Build CPM DIR & FAT Tables                                               |
'' +--------------------------------------------------------------------------+
PRI buildCpmFatTable | errorString, i, j, k, posn, found, x, y, z, u, v, w

'-----------------------------------------------------------------
' Open the CPM Disk (stored as a FAT file)
  errorString := \taf.openFile(@diskname1, "R")           ' open cpm diskname for R (FAT file representing CPM disk)
  checkError(@diskname1, taf.partitionError, errorString) ' if error, display & abort

' SEEK TO THE FIRST DIR ENTRY
  errorString := \taf.fileSeek(dirbase * sectorsize) ' seek to first DIR entry (byte) position
  checkError(@diskname1, taf.partitionError, errorString) ' if error, display & abort

' NOW MAP ALL/SPECIFIC FILENAME ENTRIES
  repeat i from 0 to direntries-1
    errorString := taf.readData(@databuf, dirsize)      ' read 32 byte dir entry
    checkError(@filename1, taf.partitionError, errorString) ' if error, display & abort

    if databuf[0] <> dirdeleted                         ' $E5 = no entry so skip
      '-------------------------------------------------
      ' map ONE/ALL files DIR entries
      if strsize(@filename1) > 0                        ' if exists, then we are only mapping a specific filename
        found := true                                   ' preset
        repeat j from 0 to 10                           ' validate filename entry
          if databuf[j+1] <> fname[j]
            found := false                              ' not found
      else
        found := true

      if found                                          ' display if ALL/SPECIFIC file found
        ' mark the dir entry as used
        y := i & $1F                                    ' extract lower 5 bits (=32 bits  to map)
        z := i >> 5                                     ' extract upper 4 bits (=32 longs to map)
        dirtable[z] |= (1 << y)                         ' map the bit (dir entry) as used
        ' scan and map the 8* extents
        repeat k from 0 to 14 step 2                    ' scan 8*word extents
          x := (databuf[17+k] << 8) | databuf[16+k]     ' AL 4KB block# (word, lsb first)
          if x == 0                                     ' if AL=0 skip this and subsequent AL's
            quit
          x += fatdirbase                               ' add the dir base offset (we don't use filebase!)
          y := x & $1F                                  ' extract lower 5 bits (=32 bits  to map)
          z := x >> 5                                   ' extract upper 8 bits (=64 longs to map)
          u := fattable[z]
          fattable[z] |= (1 << y)                       ' map the bit (4KB extent block) as used
        '-----------------------------------------------
        ' display the DIR entry ?
        if strsize(@param) > 0                          ' if exists, then display the DIR entry
          posn := taf.getCurrentPosition                ' save posn 
          Print.Str(str.integerToDecimal(posn,8))
          Print.Char(" ")
          Print.Hex(databuf[0],2)
          Print.Char(" ")
          repeat j from 1 to 11
            Print.ASCII(databuf[j])                     ' filename 8+3
          Print.Char(" ")
          repeat j from 12 to dirsize-1
            Print.Hex(databuf[j],2)
            Print.Char(" ")
          repeat j from 0 to dirsize-1
            Print.ASCII(databuf[j])
          Print.CRLF


'-----------------------------------------------------------------
  taf.closeFile
'-----------------------------------------------------------------
  ' display the dir map
  Print.CRLF
  Print.StrCR(string("DIR Map..."))
  repeat i from 0 to (direntries/32 -1)
    x := dirtable[i]                                    ' get a dir line (32 bits)
    Print.Str(str.integerToDecimal(i,3))
    Print.Char(" ")
    repeat j from 0 to 31
      ifnot ((x >> j) & $01)
        Print.Char("-")
      else
        Print.Char("$")
    ifnot (i & $01)
      Print.Char(" ")
    else
      Print.CRLF
  Print.CRLF

  ' display the fat map (we have ignored the filebase, so we should end early!)
  Print.StrCR(string("Fat Map..."))
  repeat i from 0 to (fatentries/32 -1)
    x := fattable[i]                                    ' get a fat line (32 bits)
    Print.Str(str.integerToDecimal(i,3))
    Print.Char(" ")
    repeat j from 0 to 31
      ifnot ((x >> j) & $01)
        if i == 0
          if j < fatdirbase
            Print.Char("r") 'reserved space (offset)
          elseif j < fatfilebase
            Print.Char("D")     'directory space
          else
            Print.Char(".")
        else
          Print.Char(".")
      else
        Print.Char("*")
    ifnot (i & $01)
      Print.Char(" ")
    else
      Print.CRLF
'-----------------------------------------------------------------


dat                                                   
{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}