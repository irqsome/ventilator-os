'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _GETCPM  --> _GETCPM.CMD |
'' +--------------------------------------------------------------------------+
'' |  Authors:       (c)2012,2013 "Cluso99" (Ray Rodrick)                     |
'' |  Modifications:                                                          |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' This OS module uses an included module "__MODULE.spin" to perform most of
''  the housekeeping - see this for history, details and acknowledgements.
''
'' This module......
''                Copy a CPM2.2 file to FAT16/32 filesystem
''                Restrictions: Max filesize 1MB, text or binary.
''                Does not copy readonly or hidden files.
''                Only copies files to/from user 0 (not checked)
''                Does not insert date/time stamp
'
'' RR20130709   get working (was CPMXFER_006a.spin)
'' RR20130711   working (OS_20130711a.zip) with debug info
''              remove debug info (OS_20130711b.zip)
'' RR20130713   ensure destination file does not exist
'' RR20130813   destination filename defaults to source filename
''              prep for -B/-T but not implemented!!
''
''
''-------------------------------------------------------------------------------------------------
''      CPM2.2 File System
''      ------------------
''      'ZiCog 8MB Hard Disk Parameter Blocks: 17 bytes CP/M + 2 bytes sector size used by SIMH hdsk_dpb
''      dpb_0                           '8MByte Default SIMH Altair HDSK params
''      :spt_low      BYTE      $20     'sectors per track (low byte)                   \ 32x 128byte sectors
''      :spt_high     BYTE      $00     'sectors per track (high byte)                  /
''      :bsh          BYTE      $05     'data allocation Block SHift factor               5=4KB  
''      :blm          BYTE      $1F     'data allocation block mask                       $1F=4KB
''      :exm          BYTE      $01     'EXtent Mask
''      :dsm_low      BYTE      $F9     'maximum data block number (low_byte)           \ $07F9+1= 2048-6 (8MB/4KB=2K -off)
''      :dsm_high     BYTE      $07     'maximum data block number (high_byte)          /
''      :drm_low      BYTE      $FF     'total number of directory entries (low byte)   \ $03FF+1= 1024 entries
''      :drm_high     BYTE      $03     'total number of directory entries (high byte)  /          (32 bytes)
''      :al0          BYTE      $FF     'determine reserved directory blocks            \ $00FF+1= 256 blocks
''      :al1          BYTE      $00     'determine reserved directory blocks            /
''      :cks_low      BYTE      $00     'size of directory ChecK vector (low byte)      \ $0000 for fixed disk
''      :cks_high     BYTE      $00     'size of directory ChecK vector (high byte)     /
''      :off_low      BYTE      $06     'number of reserved tracks (offset) (low byte)  \ $0006 
''      :off_high     BYTE      $00     'number of reserved tracks (offset) (high byte) /
''      :psh          BYTE      $00     'Physical record SHift factor, CP/M 3           \ $0000
''      :phm          BYTE      $00     'PHhysical record Mask, CP/M 3                  /
''      :ss_low       BYTE      $80     'Sector Size (low byte)                         \ $0080= 128 byte sectors
''      :ss_high      BYTE      $00     'Sector Size (high byte)                        /
''                                      'N.B. SS must be 128 for CP/M 2 can be varied for CP/M 3 hard disks.
''      
''      Directory Entries (32 bytes)
''      ----------------------------
''      UU  byte        'user number 0-15 or 0-31, $E5=unused/deleted
''      FN  byte[8]     'filename
''      TY  byte[3]     'filetype b7 of TY[0]=1=readonly, TY[1]=1=hidden
''      EX  byte        'extent counter (low) 0-31 (bits0-4) see S2 for (bits 5-12)
''                      ' where:   dir-entry-no = ((32*S2)|EX)/(exm+1) = ((S2<<5)|EX)>>1
''      S1  byte        'reserved =0
''      S2  byte        'extent counter (high) (bits 5-12)
''      RC  byte        'no of records used in this extent (128 bytes)
''                      'total records this extent
''                      'where:    recs = ((EX & exm)*128)+RC = ((EX & $01)*128)+RC
''      AL  word[8]     '8 allocation block locations (of 4KB for zicog 8MB HD)
''                      'where the block no is from the dir base (sector 192 for zicog 8MB HD)
''      'There is no allocation map (FAT) and is calculated at CPM boot from the dir entries.                             
{
DRAC_A  .DSK open 897 8388608
DIR at 24576
946 00...01 00 00 80 40 00 41 00 42 00 46 00 47 00 48 00 49 00 4A 00 .BDOS    PRN....@.A.B.F.G.H.I.J.
946 00...03 00 00 80 4B 00 4C 00 4D 00 4E 00 4F 00 50 00 51 00 52 00 .BDOS    PRN....K.L.M.N.O.P.Q.R.
946 00...05 00 00 80 53 00 54 00 55 00 56 00 57 00 58 00 59 00 5A 00 .BDOS    PRN....S.T.U.V.W.X.Y.Z.
947 00...07 00 00 80 5B 00 5C 00 5D 00 5E 00 5F 00 60 00 61 00 62 00 .BDOS    PRN....[.\.].^._.`.a.b.
947 00...09 00 00 80 63 00 64 00 65 00 66 00 67 00 68 00 69 00 80 00 .BDOS    PRN....c.d.e.f.g.h.i...
947 00...0A 00 00 40 81 00 82 00 00 00 00 00 00 00 00 00 00 00 00 00 .BDOS    PRN...@................
1008
}
''-------------------------------------------------------------------------------------------------
'The following are for CPM2.2 & "the" 8MB Hard Disk(s) we have setup in ZiCog
  sectorspertrack = $0020       'spt = 32
  blockshift      = 5           'bsh = 4KB
  blockmask       = $1F         'blm = 4KB
  extentmask      = $01         'exm = 1
  maxblock        = $07F9       'dsm = 2048 -offsettracks -1   (8MB/4KB=2K -off -1)
  direntries      = $03FF+1     'drm +1 
  reservblocks    = $00FF+1     'al0:al1 +1
'                 = $0000       'cks
  offsettracks    = $0006       'off
'                 = $00         'psh
'                 = $00         'phm
  sectorsize      = $80         'ss = 128
    
  dirbase         = offsettracks * sectorspertrack '=192  'base of cpm directory entries (128 byte sectors)
  dirsize         = 32          'directory entry size = 32 bytes
  filebase        = direntries * dirsize/sectorsize + dirbase '=1024*32/128+192=448  'base of file data (end of dir entries)
  dirdeleted      = $E5         'directory entry unused/deleted
  recsperblock    = 1<< blockshift '1<<5=32

  MAXEXTENTS      = 32          'maximum no of directory entries (extents) for a file (=1MB)

'''#include "__MODULE.spin"         ' include the common code for OS modules 
''#include "__MODULE.spin"         
 
VAR
  byte  databuf[512]
  long  records, allocblock, findrecord, foundaddr, total_records, free_blocks, free_dir_entry
  long  uu,ex,s1,s2,rc,al[8]
  long  dir_position[MAXEXTENTS]                'store the position of the entry (seek posn)
  byte  dir_sequence[MAXEXTENTS]                'store the directory sequence no
  byte  dir_sort[MAXEXTENTS]                    'used to sort the dir entries into sequence
  long  dir_index                               'index to dir_position & dir_sequence 

  byte  diskname0[13]                           ' CPM drive stored as a FAT16/32 file (string)
  byte  filename0[13]                           ' CPM filename within diskname0       (string)
  byte  filename1[13]                           ' FAT16/32 filename                   (string)
  byte  fname[13]                               ' CPM filename uppercase padded 8+3 w/o dot
  byte  param[13]                               ' options: -T, -B  (text/binary)
  long  binaryflag                              ' true/false for binary/text file transfer


DAT
'_ModuleStr   byte      "=== GETCPM ===",0      'module's name (string)

PRI executeCommand(stringPointer) | sector[512], errorNumber, errorString, i, j

  'get the parameters
  str.stringCopy(@diskname0, str.tokenizeString(0),13)  ' cpm disk file A: stored as a fat file (ZICOG_A2.DSK)
  str.stringCopy(@filename0, str.tokenizeString(0),13)  ' cpm source file in cpm filesystem
  str.stringCopy(@filename1, str.tokenizeString(0),13)  ' fat destination file
  str.stringCopy(@param,     str.tokenizeString(0),13)  ' options -T, -B

  if filename1[0] == "-" or filename1[0] == 0           ' filename1 missing?
    str.stringCopy(@param, @filename1,13)               ' copy options
    str.stringCopy(@filename1, @filename0,13)           ' filename1 defaults to filename0

  if param[0] == 0                                      ' options missing?
    str.stringCopy(@param, string("-B"),13)             ' default is -B

  if strsize(@diskname0) == 0                           ' if no parameters, display usage
    {Print.StrCR(string(" Copy a CPM disk\file to a FAT file..."))
    Print.StrCR(string("    GETCPM <cpm_diskname> <cpm_filename> <fat_filename> [{-T}{-B}]"))
    }return -2

'------------------------------------------------------------------------------
  'convert filename0 to uppercase padded 8+3 w/o dot
  i := 0
  j := 0
  repeat while j < 11
    case filename0[i]
      "a".."z": fname[j++] := filename0[i++] - 32       ' copy ucase(a-z)
      "A".."Z": fname[j++] := filename0[i++]            ' copy A-Z
      0:   fname[j++] := " "
      ".": if j < 8
             fname[j++] := " "                          ' space fill 8 chars
           else
             i++                                        ' skip "."
      other: fname[j++] := filename0[i++]               ' copy other chars
  fname[11] := 0                                        ' terminate string
    
'------------------------------------------------------------------------------
' validateParams                                        ' validate the params
  param[1] := param[1] & $DF                            ' simple convert to uppercase

  if ((param[0] == "-") and (param[1] == "B"))
    binaryflag := true
  elseif ((param[0] == "-") and (param[1] == "T"))
    binaryflag := false
  else
    checkError(string(" parameter error"), $F0, @param) ' if error, display & abort
    binaryflag := true                                  ' or: -B is default

'------------------------------------------------------------------------------

  errorNumber := \cpmToFat                              ' CPM-->FAT file transfer

  if errorNumber == 0                                 
    Print.Str(string(" File copied - Records= "))
    Print.Str(1+str.integerToDecimal(total_records, 7))
    Print.Str(string(" ("))
    total_records >>= 3                                 ' /8 converts to KB
    Print.Str(1+str.integerToDecimal(total_records, 4))
    Print.Str(string("KB)"))
    Print.CRLF
    return 0
  else
    Print.StrCR(string("t) *** File not copied ***"))
    abort -1


'' +--------------------------------------------------------------------------+
'' | Copy file from CPM2.2 to FAT16/32 file                                   |
'' +--------------------------------------------------------------------------+
PRI cpmToFat | errorString, errorNumber, i, j, k, posn, found

  Print.Str(string(" Copy File    CPM: "))
  Print.Str(@diskname0)
  Print.Char("\")
  Print.Str(@filename0)
  Print.Str(string(" ---> FAT: "))
  Print.Str(@filename1)
  Print.Str(string(" "))
  Print.StrCR(@param)

'-----------------------------------------------------------------
' Open the CPM Disk (stored as a FAT file)
  errorString := \fat.openFile(@diskname0, "R")                                 ' cpm diskname (a FAT file representing a CPM disk)
  checkError(@diskname0, fat.partitionError, errorString)                       ' if error, display & abort
  
{---
  Print.Str(string("     open sector="))
  Print.Str(str.integerToDecimal(fat.getSector, 10))
  Print.Str(string(" size="))
  Print.StrCR(str.integerToDecimal(fat.filesize, 10))

  Print.Str(string("     dirbase="))
  Print.Str(str.integerToDecimal(dirbase, 10))
  Print.Str(string(" direntries="))
  Print.Str(str.integerToDecimal(direntries, 10))
  Print.Str(string(" filebase="))
  Print.Str(str.integerToDecimal(filebase, 10))
  Print.Str(string(" sectorsize="))
  Print.Str(str.integerToDecimal(sectorsize, 10))
  Print.Str(string(" dirsize= "))
  Print.StrCR(str.integerToDecimal(dirsize, 10))
---}

'-----------------------------------------------------------------
' Locate CPM file within FAT cpm disk file...

' SEEK TO THE FIRST DIR ENTRY
  errorString := \fat.fileSeek(dirbase * sectorsize)                            ' seek to first DIR entry (byte) position
  checkError(@diskname0, fat.partitionError, errorString)                       ' if error, display & abort

' NOW DISPLAY (ALL/SPECIFIC) FILENAME ENTRIES
  dir_index := 0                                                                ' reset dir_position/seqence index
  repeat i from 0 to direntries-1
    posn := fat.getCurrentPosition                                              ' save posn in case we found the entry
    errorString := fat.readData(@databuf, dirsize)                              ' read 32 byte dir entry
    checkError(@filename0, fat.partitionError, errorString)                     ' if error, display & abort
    if databuf[0] <> dirdeleted                                                 ' $E5 = no entry so skip
      found:=true                                                               ' preset
      repeat j from 0 to 10                                                     ' validate filename entry
        if databuf[j+1] <> fname[j]
          found:=false                                                          ' not found
'     found:=true                                                               ' display all !!! <------------------
      if found                                                                  ' display if entry found
        'add the dir entry to the table
        dir_position[dir_index] := posn                                         ' save the position
        dir_sequence[dir_index] := ((databuf[14]<<5) | databuf[12])/(extentmask +1) '((S2<<5)|EX)/(exm+1)
        dir_index++
        'display
        foundaddr := posn                                                       ' save the posn for later
{---
        Print.Str(str.integerToDecimal(dir_position[dir_index-1],10))
        Print.Str(string(" seq# "))
        Print.Str(str.integerToDecimal(dir_sequence[dir_index-1],10))
'       fdx.dec(foundaddr)
        Print.Char(" ")
        Print.Str(str.integerToDecimal(fat.getSector,10))
        Print.Char(" ")
        Print.Str(str.integerToHexadecimal(databuf[0],2))
        Print.Str(string("..."))
        repeat j from 12 to dirsize-1
          Print.Str(str.integerToHexadecimal(databuf[j],2))
          Print.Char(" ")
        repeat j from 0 to dirsize-1
          Print.Char(ascii(databuf[j]))
        Print.CRLF
---}
        if dir_index => MAXEXTENTS
          checkError(@filename0, $FF, String("Too Many Extents"))               ' if error, display & abort
{---
  Print.StrCR(str.integerToDecimal(fat.getSector,10))                           ' verify we are at the end
  Print.Str(str.integerToDecimal(dir_index,4))
  Print.StrCR(string(" directory entries found"))
  Print.CRLF
---}

'-----------------------------------------------------------------
'NOW CHECK WE HAVE A CPM INPUT FILE
  if dir_index == 0                                                             ' no entries for the cpm file
    checkError(@filename0, $0D, String("Entry Not Found"))                      ' if error, display & abort
'-----------------------------------------------------------------
'NOW VALIDATE THE DIR ENTRIES SEQUENCE NUMBERS & SORT
  'now sort the entries by placing the entry# in the sort# as offset by the seq#
  repeat i from 0 to dir_index-1
    j := dir_sequence[i]                                                        ' get the seq#
    if j => dir_index                                                           ' check it is not too big
      checkError(@filename0, $FF, String("Extent Sequences Invalid"))           ' if error, display & abort
    dir_sort[j] := i                                                            ' store dir# in the sort# offset by seq#
{---
  repeat i from 0 to dir_index-1
    Print.Str(str.integerToDecimal(dir_sort[i],10))
    Print.Char(" ")
  Print.CRLF
---}

'-----------------------------------------------------------------
' Ensure FAT destination file does NOT exist
  errorString := \taf.newFile(@filename1)
  checkError(@filename1, taf.partitionError, errorString)                       ' if error, display & abort

'-----------------------------------------------------------------
' Create the FAT destination file
  errorString := \taf.openFile(@filename1, "W")                                 ' open for write
  checkError(@filename1, taf.partitionError, errorString)                       ' if error, display & abort

'-----------------------------------------------------------------
' Perform the copy...

'HERE IF SEQUENCE NUMBERS VALID. PROCESS EACH DIR ENTRY IN SEQUENCE (using sort#)      
  total_records := 0
  repeat i from 0 to dir_index-1
    process_one_entry(dir_position[dir_sort[i]])                                ' process this sector position

'-----------------------------------------------------------------
  fat.closeFile
  taf.closeFile
'-----------------------------------------------------------------

PRI process_one_entry(seek_position) | i, j, posn, errorString
'' Process the dir entry at seek_position

'-----------------------------------------------------------------
'SEEK TO THE DIR ENTRY
{---
  Print.StrCR(string("DirEntry----------------------------------------->"))
---}
  errorString := \fat.fileSeek(seek_position)                                   ' seek to DIR entry (byte) position
  checkError(@filename0, fat.partitionError, errorString)                       ' if error, display & abort
  posn := fat.getCurrentPosition
  errorString := \fat.readData(@databuf, dirsize)
  checkError(@filename0, fat.partitionError, errorString)                       ' if error, display & abort
{---
  Print.Str(str.integerToDecimal(posn,10))
  Print.Char(" ")
  Print.Str(str.integerToDecimal(fat.getSector,10))
  Print.Char(" ")
  Print.Str(str.integerToHexadecimal(databuf[0],2))
  Print.Str(string("..."))
  repeat j from 12 to dirsize-1
    Print.Str(str.integerToHexadecimal(databuf[j],2))
    Print.Char(" ")
  repeat j from 0 to dirsize-1
    Print.Char(ascii(databuf[j]))
  Print.CRLF
---}

'-----------------------------------------------------------------
'NOW STORE THE EXTENTS
  ex := databuf[12]
  s1 := databuf[13]
  s2 := databuf[14]
  rc := databuf[15]
{---
  Print.Str(string("ex="))
  Print.Str(str.integerToDecimal(ex,10))
  Print.Str(string(" s1="))
  Print.Str(str.integerToDecimal(s1,10))
  Print.Str(string(" s2="))
  Print.Str(str.integerToDecimal(s2,10))
  Print.Str(string(" rc="))
  Print.StrCR(str.integerToDecimal(rc,10))
---}
  repeat i from 0 to 7                                                          ' get 8 allocation block nos
    j := i << 1                                                                 ' 2*i
    al[i] := (databuf[17+j]<<8 | databuf[16+j]) << blockshift
{---
    Print.Str(string(" al["))
    Print.Str(str.integerToDecimal(i,10))
    Print.Str(string("]="))
    Print.Str(str.integerToDecimal(al[i],10))
    if i == 3
      Print.CRLF
  Print.CRLF
---}

'-----------------------------------------------------------------
'NOW SEEK TO READ EACH EXTENT & DISPLAY/OUTPUT
'calc correct rc value
  rc := ((ex & extentmask) * 128) + rc                                          ' 128 = $80 (256 will be all extents)
  total_records += rc                                                           ' accumulate total 4KB records
{---
  Print.Str(string("        Records= "))
  Print.Str(str.integerToDecimal(rc,10))
  Print.Str(string(" ("))
  i := rc >> 3                                                                  ' /8 converts to KB
  Print.Str(str.integerToDecimal(i,10))
  Print.Str(string("KB)"))
  Print.CRLF
---}

  i := 0
  repeat while (rc > 0) & (i < 8)                                               ' read up to 8 @ 4KB blocks (256 sectors)
    if rc > 31
      readcpmblock(al[i] + dirbase, 32)                                         ' read up to a 4KB block
      rc := rc - 32                                                             ' prep for next loop
    else
      readcpmblock(al[i] + dirbase, rc)
      rc := 0                                                                   ' all done
    i++


PRI readcpmblock(posn,recs) |  i, j, ctlz, lastrec, errorNumber, errorString
'' Read a block of data and display/write it
{---
  Print.StrCR(string("-------------------------------------------------"))
---}
'NOW SEEK TO FIRST EXTENT
  errorString := \fat.fileSeek(posn * sectorsize)
  checkError(@diskname0, fat.partitionError, errorString)                       ' if error, display & abort
  repeat i from 0 to recs - 1
{---
    Print.Str(str.integerToDecimal(fat.getCurrentPosition,10))
    Print.Char(";")
    Print.StrCR(str.integerToDecimal(fat.getSector,10))
---}
'   NOW READ SECTOR
    errorString := \fat.readData(@databuf, sectorsize)                          ' read sector/data from input file
    checkError(@filename0, fat.partitionError, errorString)                     ' if error, display & abort
'   NOW WRITE SECTOR
    errorString := \taf.writeData(@databuf, sectorsize)                         ' write sector/data to output file
    checkError(@filename1, taf.partitionError, errorString)                     ' if error, display & abort

{---
'   repeat j from 0 to sectorsize-1                                             ' display the sector data
'     Print.Char(ascii(databuf[j]))
'   Print.CRLF
    repeat j from 0 to sectorsize-1                                             ' display the sector data
      Print.Char(databuf[j])
  Print.CRLF
  Print.StrCR(string("-------------------------------------------------"))
---}

'do not scan for Ctl-Z (therefore works for text and binaries)
'   repeat j from 0 to 63                              
'     Print.Char(ascii(databuf[j]))
'   Print.Str(string(13,"               "))
'   repeat j from 64 to 127
'     Print.Char(ascii(databuf[j]))
'   Print.CRLF

{---
'should scan backwards for <>Ctl-Z !!!!!!!!
    lastrec := recs - 1
    if i <> lastrec                                                             'not last rec
      repeat j from 0 to 63                              
'       Print.Char(ascii(databuf[j]))
        taf.writeByte(databuf[j])
'     Print.Str(string(13,"               "))
      repeat j from 64 to 127
'       Print.Char(ascii(databuf[j]))
        taf.writeByte(databuf[j])
'     Print.CRLF
    else                                                                        'last rec
      ctlz := false                                                             'preset not found
      repeat j from 0 to 63                                                     'stops when Ctl-Z found
        if databuf[j] == $1A
          ctlz := true
        if !ctlz
'         Print.Char(ascii(databuf[j]))
          taf.writeByte(databuf[j])
'     Print.Str(string(13,"eof            "))
      repeat j from 64 to 127
        if databuf[j] == $1A
          ctlz := true
        if !ctlz
'         Print.Char(ascii(databuf[j]))
          taf.writeByte(databuf[j])
'     Print.CRLF
---}
{      
PRI ASCII(ch)
  result := ch & $7F                                    ' remove top bit
  if result < $20
    result := "."
  if result == $7F
    result := "."
}

dat                                                   
{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}