VentilatorOS
============

[![pipeline status](https://gitlab.com/irqsome/ventilator-os/badges/master/pipeline.svg)](https://gitlab.com/irqsome/ventilator-os/commits/master)

## What is it?
VentilatorOS is a simple kernel-less singletasking single-user operating system for the _Ventilator Computer_ (but it is easy to port to any hardware using the Parallax P8X32A chip).

It is based on [Cluso's Propeller OS][1].

## What can it do?
- Interact with the FAT16/FAT32 file system
- Boot BIN files (and preload their potential accompanying RAM files to SPI RAM)
- Transfer files over serial (TODO: Needs better PC client and faster transfers, current solution is comically clunky)
- User friendly shell with line editing, recalling of last command and useful shortcuts
- Built-in documentation for every command
- ~~Interact with CP/M disk images (CP/M emulator itself coming soon(TM))~~ (Mothballed for now)
- Compile small SPIN programs (untested, somewhat experimental, no file editor yet)

## How do I use it?
To compile VentilatorOS, you need these dependencies:
- .NET Framework (Windows) or Mono (Linux)
- Git
- Ruby
- Rake (usually comes bundled with Ruby)

After installing these, just open a terminal in the project root (on Windows, use the bash that comes with git for best results) and run `rake` to compile the OS.
(You can also use `rake -m` to compile faster, but with garbled output)
Afterwards, copy the contents of the "out" folder to the root of your SD card.
Now, just install [V-BIOS][2] to your EEPROM. Press the indicated buttons to boot your image of choice:
 - T for `OS.BIN` if you want to use a TV and keyboard
 - V for `OSVGA.BIN` if you want to use a VGA monitor and keyboard
 - X for `OSTERM.BIN` if you want to use a serial terminal (115200 baud)

After you get it running, use `HELP` to get some directions.

## How do I port it?
Simple:
1. Edit lib/_HWDEF.spin to reflect your hardware
2. Edit lib/_OSVER.spin and reserve a vendor ID and a hardware ID
3. Edit OS.spin to reflect to set the correct Vendor/Hardware ID
4. Edit OS.spin to load the I/O drivers you need (assuming they exist. If not, create some!)

## How do I modify it?

### Writing a module (=command)
1. Create a file in either modules/ or modules_light/ It's name must start with an underscore and must be 8 characters or less. Depending on which folder it is put in, either lib/__MODULE.spin or lib/__MODULE_light.spin is automatically prepended to provide I/O and more.
2. Write a `PRI executeCommand(stringPointer)` function that does what you want it to do.
3. Make sure you don't use any box drawing characters that cause the file to be saved as UTF-16
4. Create a corrosponding TXT file in help/ . Its contents will be displayed when `HELP <your command>` is ran or when `executeCommand` returns -2. Make sure the help file ends in a newline!

### Increasing the version number
1. Modify lib/_OSVER.spin
2. Commit that
3. Create an annotated(!!!) git tag (make sure to push this later)
4. `rake clean`

### Notes about modifying lib/_HUBDEF.spin
If you modify this file in a way that breaks compatibility with compiled modules, please increment the `ABI_VERSION` constant in the rakefile

## What's left TODO?
- More useful and faster serial file transfer
- More commands
- More optimization
- Prettier boot screen? (We need to get some rainbows up in there, like C65 BASIC, lol) (kindof unneccessary now that V-BIOS is a thing?)
- Keyboard driver for QWERTY keyboards (currently only QWERTZ is included)

## Who made it?
Use the `VER` command to find out! Or if you're lazy, just look at lib/CREDITS.TXT .

If you contribute, don't forget to add yourself to said file!



[1]: https://forums.parallax.com/discussion/138251/clusos-propeller-os-v1-14-now-with-spin-pasm-compiler-eeprom-read-write/p1
[2]: https://gitlab.com/irqsome/ventilator-software/-/tree/master/system/vbios
