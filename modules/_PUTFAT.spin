'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _PUTFAT  --> _PUTFAT.CMD |
'' +--------------------------------------------------------------------------+
'' |  Authors:       (c)2012,2013 "Cluso99" (Ray Rodrick)                     |
'' |  Modifications:                                                          |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' This OS module uses an included module "__MODULE.spin" to perform most of
''  the housekeeping - see this for history, details and acknowledgements.
''
'' This module......
''      PUT (copy) a FAT16/32 file from PC
'' RR20130202   get working but not all types of errors catered for!
'' RR20130903

'''#include "__MODULE.spin"         ' include the common code for OS modules 
'#include "__MODULE.spin" 


VAR
  byte  filename0[13]                           ' FAT destination filename  (string)

 
DAT
'_ModuleStr   byte      "=== PUTFAT ===",0        'module's name (string)

PRI executeCommand(stringPointer) | errorString, i, chksum, blksize, ch, filesize
  'get the parameters
  str.stringCopy(@filename0, str.tokenizeString(0),13)  ' destination filename

  if strsize(@filename0) == 0                           ' if no parameters, display usage
    {Print.StrCR(string(" PUT (copy) a FAT16/32 file from the PC..."))
    Print.StrCR(string("    PUTFAT <fat_destination_filename>"))
    }return -2

'-----------------------------------------------------------------
  'there should be an <lf> still in the input buffer, so check and clear it
  i := peekByte
  if i <> 0
    ch := getByte
    printByte(ch)

'-----------------------------------------------------------------
' Ensure FAT destination file does NOT exist
  errorString := \fat.newFile(@filename0)
  checkError(@filename0, fat.partitionError, errorString)   ' if error, display & abort

'-----------------------------------------------------------------
' Create the FAT destination file
  errorString := \fat.openFile(@filename0, "W")             ' open destination file for write
  checkError(@filename0, fat.partitionError, errorString)   ' if error, display & abort

'-----------------------------------------------------------------
  printByte("A")                                        ' ACK
  filesize := 0

  repeat
    ' wait for blksize BL & BH
    blksize := getByte                                  ' recv blocksize Lo
    blksize := blksize + (getByte << 8)                 '                Hi
    chksum~                                             ' =0
    if blksize > 0
      ' wait for data block
      repeat i from 1 to blksize   
        ch := getByte
        chksum += ch                                    ' accum checksum
        fat.writeByte(ch)                               ' write byte to file
    printByte(chksum & $FF)                             ' send checksum Lo
    printByte((chksum >> 8) & $FF)                      '               Hi
    filesize += blksize
  until blksize == 0       
  fat.closeFile

  ' all OK so wait for ack
  ch := getByte                                         ' wait for ACK
  if ch <> "A"
    printByte("T")
    abort -2
  printByte("E")                                        ' send END

  ' all done so complete with stats
  Print.Str(string("File written to SD card",13,10))
  Print.CRLF
  return 0

{------------------------------------------------------------------------------}
' These routines send/recv a binary byte without any changes...
PRI PrintByte(c)
    Print.Out(c)                                        ' send byte (unmolested)
    
PRI GetByte
    result := Input.Char                                ' wait for an input byte

PRI PeekByte
    result := Input.Peek                                ' returns 0 if no input byte
{------------------------------------------------------------------------------}


dat                                                   
{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}} 