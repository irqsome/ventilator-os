'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _DIR     --> _DIR.CMD    |
'' +--------------------------------------------------------------------------+
'' |  Authors:       (c)2012,2013 "Cluso99" (Ray Rodrick)                     |
'' |  Modifications:                                                          |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' This OS module uses an included module "__MODULE.spin" to perform most of
''  the housekeeping - see this for history, details and acknowledgements.
''
'' This module......
''      Displays a list of FAT16/32 files
''
'' RR20120317
'' RR20130717   fix wildcards, add sort, etc
'' RR20130802   single "?" for help, fix sort when only 1 entry
''
''
  sortsize        = 512         ' max files for sorting

  _STACK = 128

'''#include "__MODULE.spin"         ' include the common code for OS modules 
'#include "__MODULE.spin" 

VAR
  byte  databuf[512]
  byte  sort[12*sortsize]                       ' sort table - stores 8+3 fnames for sorting (must be LONG aligned)
  'byte  temp[12]                                ' temp swap are (must be LONG aligned)

  byte  filename1[16]                           ' filename 
  byte  fname1[16]                              ' filename uppercase padded 8+3 w/o dot
' byte  param[16]                               ' optional parameter "-s" (any char)

  byte  filename2[16]                           ' filename 
  byte  fname2[16]                              ' filename uppercase padded 8+3 w/o dota
  long  counter, size, line, lines              ' for terminal row/col size
  
DAT
_ModuleStr    byte      "=== DIR ===",0        'module's name (string)


PRI executeCommand(StringPointer)

  'get the parameters
  str.stringCopy(@filename1, str.tokenizeString(0),16)  ' fat filename (optional)
' str.stringCopy(@param, str.tokenizeString(0))         ' optional -s parameter

  {if (filename1[0] == "?" and filename1[1] == 0)       ' help parameter...
    Print.StrCR(string(" Display the file directory"))
    Print.StrCR(string("    DIR [mask]"))
    return 0}

  if strsize(@filename1) > 0                            ' if exists, prepare fname 8+3 for compare
    ExpandWildcards("?")                                ' expands filename to uppercase(fname) as 8+3 string
    str.stringCopy(@fname2, @fname1,16)                 ' save then compare-to fname
  str.stringCopy(@filename2, @filename1,16)

  ' determine column width 
  if byte[_hub#_OS_Columns] > 45
    size := byte[_hub#_OS_Columns] / 15                 ' x columns of 15
  else
    size := byte[_hub#_OS_Columns] / 13                 ' x columns of 13            
  lines := (byte[_hub#_OS_Rows] & _hub#_ROW_MASK) - 2 
  counter~
  line~

  buildFileTable                                        ' build the file list table

  return 0


'' +--------------------------------------------------------------------------+
'' | Build FAT FILE Table                                                     |
'' +--------------------------------------------------------------------------+
PRI buildFileTable | buffer, found, j, s,tmp,time 

'-----------------------------------------------------------------
  buffer := taf.listEntries("W")   
  checkError(string("DIR"), taf.partitionError, buffer)     ' if error, display & abort

' NOW MAP ALL/SPECIFIC FILENAME ENTRIES
  s~
  repeat while(buffer := taf.listEntries("N"))
    checkError(string("DIR"), taf.partitionError, buffer)    ' if error, display & abort
    str.stringCopy(@filename1, buffer,16)
    str.trimString(@filename1)
    ExpandWildcards(" ")                                ' expands filename to uppercase and 8+3 string
    '-------------------------------------------------
    ' map ONE/ALL files DIR entries
    found~~                                             ' preset
    if strsize(@filename2)                              ' if exists, then we are only mapping specific filename(s)
      repeat j from 0 to 10                             ' validate filename entry
        if (tmp := fname2[j]) <> "?"
          if (fname1[j] <> tmp)                              ' same or wildcard?
            found~                                       ' not found
            quit

    '-----------------------------------------------
    if found                                            ' display if ALL/SPECIFIC file(s) found
      ' add this file to the sort list
      bytemove(@sort[s*12], @fname1[0], 11)             ' copy fname to slot
      s++                                               ' next slot
      if s => sortsize
        checkError(string("DIR"), $F2, string("Sort table too small")) ' if error, display & abort

'-----------------------------------------------------------------
  taf.closeFile
'-----------------------------------------------------------------

'-----------------------------------------------------------------
' NOW SORT & DISPLAY THE FILE ENTRIES
  time := cnt
  sortFiles(s)
  time := cnt - time 
  print.str(string("Sort time: "))
  print.decimal(time)
  print.crlf
  displayFiles(s)


PRI ExpandWildcards(char) | i, j
'' Expand the string "filename" -> Uppercase"fname" using the passed wildcard "?" or " "
''  "fname" is expanded to a string of 11 characters for the filename 8+3 format

  i~
  j~
  repeat while filename1[j]                               
    if filename1[j] == "*"                              ' expand "*" with "?"
      if i < 8
        repeat
          fname1[i++] := char                           ' fill with "?" or " "
        until i == 8
      elseif i < 11
        repeat
          fname1[i++] := char                           ' fill with "?" or " "
        until i == 11
      else
        checkError(@filename1, $F0, String("Bad Wildcard Format"))              ' if error, display & abort
    elseif filename1[j] == "."
      repeat while i < 8
        fname1[i++] := char                             ' fill with "?" or " "
      if i > 8
        checkError(@filename1, $F1, String("Filename too long"))                ' if error, display & abort
    else
      if i < 11
        fname1[i++] := filename1[j]
      else
        checkError(@filename1, $F1, String("Filename too long"))                ' if error, display & abort
    j++
  repeat while i < 11
    fname1[i++] := char                                 ' fill with "?" or " "
  fname1[11] := 0                                       ' add in the string terminator                           
  repeat i from 0 to 10
    case fname1[i]
      "a".."z": fname1[i] -= 32                         ' convert to ucase(a-z)

{PRI sortFiles(s) | i, j, m, n, swapped
'' Sort the Files...

  if s < 2                                              ' no need to sort if < 2 entries
    return
    
  repeat                                                ' repeat until non swapped in loop
    swapped~
    repeat i from 0 to s-2                              ' repeat for all entries
      m := i*12                                         ' current entry index
      n := (i*12)+12                                    ' next    entry index
      repeat j from 0 to 10
        if sort[m+j] < sort[n+j]                        ' current < next ?
          quit
        elseif sort[m+j] > sort[n+j]                    ' current > next ?
          bytemove(@temp[0], @sort[m], 11)              '\ swap
          bytemove(@sort[m], @sort[n], 11)              '|
          bytemove(@sort[n], @temp[0], 11)              '/
          swapped~~
          quit
  while swapped}

{PRI sortFiles(s) | i, j, m, n, swapped
'' Sort the Files...

  if s < 2                                              ' no need to sort if < 2 entries
    return

  
    
  repeat                                                ' repeat until non swapped in loop
    swapped~
    repeat i from 0 to s-2                              ' repeat for all entries
      m := i*12                                         ' current entry index
      n := m+12                                         ' next    entry index
      'repeat j from 0 to 10
        j := compareNames(@sort+m,@sort+n)
        if j > 0                                    ' current > next ?
          longmove(@temp, @sort[m], 3)              '\ swap
          longmove(@sort[m], @sort[n], 3)              '|
          longmove(@sort[n], @temp, 3)              '/
          swapped~~
  while swapped}


PRI sortFiles(length) : i | ip,jp,i2p,zonep,k,realsize,subsize,sizze,temp[12/4]
'' Sort the Files...

  if length < 2                                              ' no need to sort if < 2 entries
    return

  '' Non-recursive in-place sorting algorithm
  '' As relayed to me by a friendly Discord user
  '' (ported to spin and optimized by me)
  subsize := 12
  length *= 12  
  repeat while subsize < length
    sizze := subsize<<1
    repeat i from 0 to length-12 step sizze
      jp := (ip:=@sort+i)+subsize
      realsize := sizze <# (length-i)
      i2p := ip
      repeat k from 0 to realsize-12 step 12
        if jp => (ip+realsize) or i2p => jp
          'pass
        elseif compareNames(i2p,jp) =< 0
          i2p += 12
        else 
          zonep := jp
          repeat           
            if (zonep+12) == (ip+realsize)
              longmove(@temp, i2p,3)
              longmove(i2p,jp,3)
              longmove(jp,jp+12,(zonep-jp)>>2)
              longmove(zonep,@temp,3)
              if jp == zonep
                i2p += 12
              else
                k -= 12
              quit
            elseif compareNames(zonep+12,i2p) > 0
              longmove(@temp, i2p,3)
              longmove(i2p,jp,3)
              longmove(jp,jp+12,(zonep-jp)>>2)
              longmove(zonep,@temp,3)
              if jp == zonep
                i2p += 12
              else
                k -= 12
              quit   
            zonep += 12
    subsize := sizze         

PRI compareNames(first,second)

  repeat 11
    
    if result := byte[first++] - byte[second++]
      return
  return 0

PRI displayFiles(s) | i, j, n
'' Display the Files...
  if s < 1
    Print.StrCR(string(" No Files"))
    return                                              
  repeat i from 0 to s-1
    n := i * 12
    repeat j from 0 to 7
      Print.ASCII(sort[n+j])                            ' filename 8
    Print.Char(".")
    repeat j from 8 to 10
      Print.ASCII(sort[n+j])                            ' filename +3
    if size > 3
      Print.Str(string("     "))                        ' col width=15
    else
      Print.Str(string(" "))                            ' col width=13
    counter++                                           ' add one to counter
    if counter => size                                  ' filled line?
      counter~
      Print.CRLF
      line++
    if line => lines                                    ' filled screen?
      Print.Str(string("more..."))
      Input.Char                                        ' wait for any key
      line~
      Print.CRLF
  if counter > 0
    Print.CRLF


dat                                                   
{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}} 