'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _TESTSD  --> _TESTSD.CMD |
'' +--------------------------------------------------------------------------+
'' |  Authors:       (c)2011 by Kwabena W. Agyman (Kye) & J. Moxham (Dr_Acula)|
'' |  Modifications: (c)2012 "Cluso99" (Ray Rodrick)                          |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+
'' This OS module uses an included module to perform most of the housekeeping.
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' Compile with PropTool + "PreSpin 0.3" by Andy Schenk www.insonix.ch/propeller                             
'' Some OS modules are derived from: 
''      KyeDOS v3 (c) 2011 by Kwabena W. Agyman (Kye) & J. Moxham (Dr_Acula)
'' See the included code for history and acknowledgments.
''
'' RR20120317   This module does......
'' RR20160104   writeShort -> writeWord



'''#include "__MODULE.spin"         ' include the common code for OS modules 
'#include "__MODULE.spin" 

DAT
'_ModuleStr   byte      "=== TESTSD ===",0        'module's name (string)
 
PRI executeCommand(stringPointer) | counter, buffer

  ifnot(str.stringCompareCI(string("testsd"), stringPointer))      '<-- module's name
    fat.newFile(stringPointer := string("P8X32A.ROM"))

    buffer := startTest(stringPointer, "W", string("Byte write test...   "))
    repeat counter from 32_768 to 65_535 step 1
      fat.writeByte(byte[counter])
    stopTest("W", 32_768, (cnt - buffer))

    buffer := startTest(stringPointer, "R", string("Byte read test...    "))
    repeat counter from 32_768 to 65_535 step 1
      if(fat.readByte <> byte[counter])
        fat.closeFile
        abort string("Byte read back failed")
        Print.CRLF
    stopTest("R", 32_768, (cnt - buffer))

    buffer := startTest(stringPointer, "W", string("Word write test...   "))
    repeat counter from 32_768 to 65_535 step 2
      fat.writeWord(word[counter])
    stopTest("W", 32_768, (cnt - buffer))

    buffer := startTest(stringPointer, "R", string("Word read test...    "))
    repeat counter from 32_768 to 65_535 step 2
      if(fat.readWord <> word[counter])
        fat.closeFile
        abort string("Word read back failed")
        Print.CRLF
    stopTest("R", 32_768, (cnt - buffer))

    buffer := startTest(stringPointer, "W", string("Long write test...   "))
    repeat counter from 32_768 to 65_535 step 4
      fat.writeLong(long[counter])
    stopTest("W", 32_768, (cnt - buffer))

    buffer := startTest(stringPointer, "R", string("Long read test...    "))
    repeat counter from 32_768 to 65_535 step 4
      if(fat.readLong <> long[counter])
        fat.closeFile
        abort string("Long read back failed")
        Print.CRLF
    stopTest("R", 32_768, (cnt - buffer))

    buffer := startTest(stringPointer, "W", string("Sector write test... "))
    repeat counter from 32_768 to 65_535 step 512
      fat.writeData(counter, 512)
    stopTest("W", 32_768, (cnt - buffer))

    buffer := startTest(stringPointer, "R", string("Sector read test...  "))
    repeat counter from 32_768 to 65_535 step 512
      fat.readData(counter, 512)
    stopTest("R", 32_768, (cnt - buffer))

    buffer := startTest(stringPointer, "W", string("Memory write test... "))
    repeat counter from 32_768 to 65_535 step 32_768
       fat.writeData(counter, 32_768)
    stopTest("W", 32_768, (cnt - buffer))

    buffer := startTest(stringPointer, "R", string("Memory read test...  "))
    repeat counter from 32_768 to 65_535 step 32_768
      fat.readData(counter, 32_768)
    stopTest("R", 32_768, (cnt - buffer))

    fat.newFile(fat.deleteEntry(stringPointer))
    buffer := startTest(stringPointer, "A", string("Block append test... "))
    fat.writeData(32_768, 32_768)
    stopTest("W", 32_768, (cnt - buffer))

    buffer := startTest(stringPointer, "R", string("Reverse seek test... "))
    repeat counter from 32_767 to 0 step 512
      fat.fileSeek(counter)
    stopTest("R", 32_768, (cnt - buffer))

    fat.deleteEntry(stringPointer)

    abort 0

PRI startTest(path, mode, message)

  fat.openFile(path, mode)
  Print.Str(message)
  return cnt

PRI stopTest(mode, count, cycles)

  if(mode == "W")
    Print.Str(string("Write "))
  else
    Print.Str(string("Read    "))

  Print.Str(1 + str.integerToDecimal((count >> 10), 3))
  Print.Str(string(" KBs at "))
  Print.Str(1 + str.integerToDecimal((multiplyDivide(count, cycles) >> 10), 3))
  Print.StrCR(string(" KBs per second"))
  fat.closeFile

PRI multiplyDivide(dividen, divisor) | productHigh, productLow

  productHigh := clkfreq ** dividen
  productLow := clkfreq * dividen
  if(productHigh => divisor)
    return posx

  repeat 32

    dividen := (productHigh < 0)
    productHigh := ((productHigh << 1) + (productLow >> 31))
    productLow <<= 1
    result <<= 1

    if((productHigh => divisor) or dividen)
      productHigh -= divisor
      result += 1

dat                                                   
{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}