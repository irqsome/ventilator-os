'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _GETFAT  --> _GETFAT.CMD |
'' +--------------------------------------------------------------------------+
'' |  Authors:       (c)2012,2013 "Cluso99" (Ray Rodrick)                     |
'' |  Modifications:                                                          |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' This OS module uses an included module "__MODULE.spin" to perform most of
''  the housekeeping - see this for history, details and acknowledgements.
''
'' This module......
''      GET (copy) a FAT16/32 file to PC 
'' RR20130202   working but not all types of errors catered for! (extra unknown lf char skipped at chksum)
'' RR20130903

'''#include "__MODULE.spin"         ' include the common code for OS modules 
'#include "__MODULE.spin" 

CON
 commsize       = 256 '1024     ' default block size for comms
 
VAR
  byte  filename0[13]                           ' FAT source filename       (string)

 
DAT
_ModuleStr    byte      "=== GETFAT ===",0        'module's name (string)

PRI executeCommand(stringPointer) | errorString, fsize, blksize, ch, chksum, csl, csh, c5, i
  'get the parameters
  str.stringCopy(@filename0, str.tokenizeString(0),13)  ' source filename

  if strsize(@filename0) == 0                           ' if no parameters, display usage
    {Print.StrCR(string(" GET (copy) a FAT16/32 file to the PC..."))
    Print.StrCR(string("    GETFAT <fat_source_filename>"))
    }return -2

'-----------------------------------------------------------------
  'there should be an <lf> still in the input buffer, so check and clear it
  i := peekByte
  if i <> 0
    ch := getByte
    printByte(ch)

'-----------------------------------------------------------------
' Open the FAT source file
  errorString := \fat.openfile(@filename0, "R")           ' open source file
  checkError(@filename0, fat.partitionError, errorString) ' if error, display & abort
  fsize := fat.fileSize                                   ' file size

'-----------------------------------------------------------------
  blksize := commsize                                 ' default blocksize
  if fsize>0   
    printbyte("A")                                      ' ACK
  else
    printbyte("T")                                      ' Terminate
    abort -2

'-----------------------------------------------------------------
  ' send blocks
  repeat
    if fsize =< blksize                                 ' if less than blocksize,
      blksize := fsize                                  '   adjust bocksize to bytes remaining
    fsize -= blksize                                    ' reduce remaining size (can be 0)
    ' send blocksize (BL,BH)
    printByte(blksize & $FF)                            ' send blocksize Lo (last block will be =0)
    printByte((blksize >> 8) & $FF)                     '                Hi
    chksum~
    if blksize > 0                                      ' bytes to be sent
      ' send bytes
      repeat blksize
        ch := (fat.readByte)                            ' read byte
        printByte(ch)                                   ' send byte
        chksum += ch                                    ' accum checksum
    ' wait for checksum CL & CH
    ch := getByte                                       ' recv checksum Lo
    ch := ch + (getByte << 8)                           '               Hi
    if (chksum & $FFFF) <> (ch & $FFFF)                 ' verify checksum (last block will be =0)
      printByte("T")                                    ' terminate
      printByte(ch & $FF)
      printByte((ch>>8) & $FF)
      printByte("<")
      printByte(">")
      printByte(chksum & $FF)
      printByte((chksum>>8) & $FF)
      fat.closeFile
      abort -1
  until blksize == 0                                  ' (fat.fileSize == fat.fileTell)
'-----------------------------------------------------------------
  fat.closeFile
  printByte("A")                                      ' ACK = success
'wait until we get the END from the pc
  repeat
    ch := getByte
    if ch <> "E"
      printByte(ch)
  until ch == "E"    
  ' all done so finish up
  return 0

{------------------------------------------------------------------------------}
' These routines send/recv a binary byte without any changes...
PRI PrintByte(c)
    Print.Out(c)                                        ' send byte
    
PRI GetByte
    result := Input.Char                                ' wait for an input byte

PRI PeekByte
    result := Input.Peek                                ' returns 0 if no input byte
{------------------------------------------------------------------------------}

{{
PRI executeCommand(stringPointer) | sector[512], errorString, bytes, n

  'get the parameters
  str.stringCopy(@filename0, str.tokenizeString(0))     ' source filename
  str.stringCopy(@filename1, str.tokenizeString(0))     ' destination filename

  if strsize(@filename0) == 0                           ' if no parameters, display usage
    Print.StrCR(string(" Copy a file..."))
    Print.StrCR(string("    COPY <source_filename> <destination_filename>"))
    return 0

  Print.Str(string("i) Copy File "))
  Print.Str(@filename0)
  Print.Str(string(" ---> "))
  Print.StrCR(@filename1)

'-----------------------------------------------------------------
' Open the FAT source file
  errorString := \fat.openfile(@filename0, "R")         ' open source file
  checkError(@filename0, fat.partitionError, errorString) ' if error, display & abort

'-----------------------------------------------------------------
' Ensure FAT destination file does NOT exist
  errorString := \taf.newFile(@filename1)
  checkError(@filename1, taf.partitionError, errorString) ' if error, display & abort

'-----------------------------------------------------------------
' Create the FAT destination file
  errorString := \taf.openFile(@filename1, "W")         ' open destination file for write
  checkError(@filename1, taf.partitionError, errorString) ' if error, display & abort

'-----------------------------------------------------------------
' Perform the copy...
  n := 0
  repeat
    bytes := \fat.readData(@sector, 512)                ' if good, returns the byte count
    checkError(@filename0, fat.partitionError, bytes)   ' if error, display & abort
    errorString := \taf.writeData(@sector, bytes)
    checkError(@filename1, taf.partitionError, errorString) ' if error, display & abort
    n += bytes
  while(bytes == 512)

'-----------------------------------------------------------------
' Close up
  fat.closeFile
  taf.closeFile
  Print.Str(string("i) File copied, "))
  Print.Str(1+str.integerToDecimal(n,10))
  Print.StrCR(string(" bytes"))
  return 0
}}
    

dat                                                   
{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}} 