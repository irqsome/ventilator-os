'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _LS      --> _LS.CMD     |
'' +--------------------------------------------------------------------------+
'' |  Authors:       (c)2011 by Kwabena W. Agyman (Kye) & J. Moxham (Dr_Acula)|
'' |  Modifications: (c)2012 "Cluso99" (Ray Rodrick)                          |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+
'' This OS module uses an included module to perform most of the housekeeping.
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' Compile with PropTool + "PreSpin 0.3" by Andy Schenk www.insonix.ch/propeller                             
'' Some OS modules are derived from: 
''      KyeDOS v3 (c) 2011 by Kwabena W. Agyman (Kye) & J. Moxham (Dr_Acula)
'' See the included code for history and acknowledgments.
''
'' RR20120317   This module does......

'''#include "__MODULE.spin"         ' include the common code for OS modules 
'#include "__MODULE.spin" 

'' Note: A better format display would be nicer!

DAT
'_ModuleStr   byte      "=== LS ===",0        'module's name (string)
 
PRI executeCommand(stringPointer) : counter | buffer, flag
'' LIST command...

    stringPointer := str.tokenizeString(0)
    flag := (str.findCharacter(stringPointer, "A") or str.findCharacter(stringPointer, "a"))
    fat.listEntries("W")

    repeat while(buffer := fat.listEntries("N"))
      Print.Str(buffer)
      ifnot(flag)

        if(++counter // 8)
          Print.Char(_c#_horizontalTabCharacter)
        else
          Print.CRLF

      else

        if(fat.listIsReadOnly)
          Print.Str(string(" [ R"))
        else
          Print.Str(string(" [ _"))

        if(fat.listIsHidden)
          Print.Char("H")
        else
          Print.Char("_")

        if(fat.listIsSystem)
          Print.Char("S")
        else
          Print.Char("_")

        if(fat.listIsDirectory)
          Print.Char("D")
        else
          Print.Char("_")

        if(fat.listIsArchive)
          Print.Char("A")
        else
          Print.Char("_")

        Print.Str(string(" ] [ "))
        Print.Str(1 + str.integerToDecimal(fat.listSize, 10))

        Print.Str(string(" ] [ Accessed "))
        Print.Str(1 + str.integerToDecimal(fat.listAccessMonth, 2))
        Print.Char("/")
        Print.Str(1 + str.integerToDecimal(fat.listAccessDay, 2))
        Print.Char("/")
        Print.Str(1 + str.integerToDecimal(fat.listAccessYear, 4))

        Print.Str(string(" ] [ Created "))
        Print.Str(1 + str.integerToDecimal(fat.listCreationHours, 2))
        Print.Char(":")
        Print.Str(1 + str.integerToDecimal(fat.listCreationMinutes, 2))
        Print.Char(":")
        Print.Str(1 + str.integerToDecimal(fat.listCreationSeconds, 2))

        Print.Char(" ")
        Print.Str(1 + str.integerToDecimal(fat.listCreationMonth, 2))
        Print.Char("/")
        Print.Str(1 + str.integerToDecimal(fat.listCreationDay, 2))
        Print.Char("/")
        Print.Str(1 + str.integerToDecimal(fat.listCreationYear, 4))

        Print.Str(string(" ] [ Modified "))
        Print.Str(1 + str.integerToDecimal(fat.listModificationHours, 2))
        Print.Char(":")
        Print.Str(1 + str.integerToDecimal(fat.listModificationMinutes, 2))
        Print.Char(":")
        Print.Str(1 + str.integerToDecimal(fat.listModificationSeconds, 2))

        Print.Char(" ")
        Print.Str(1 + str.integerToDecimal(fat.listModificationMonth, 2))
        Print.Char("/")
        Print.Str(1 + str.integerToDecimal(fat.listModificationDay, 2))
        Print.Char("/")
        Print.Str(1 + str.integerToDecimal(fat.listModificationYear, 4))

        Print.StrCR(string(" ]"))
    if((counter // 8) and counter) 
      Print.CRLF

    abort 0

dat                                                   
{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}