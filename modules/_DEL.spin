'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _DEL     --> _DEL.CMD    |
'' +--------------------------------------------------------------------------+
'' |  Authors:       (c)2012,2013 "Cluso99" (Ray Rodrick)                     |
'' |  Modifications:                                                          |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' This OS module uses an included module "__MODULE.spin" to perform most of
''  the housekeeping - see this for history, details and acknowledgements.
''
'' This module......
''      Deletes a FAT16/32 file
''
'' RR20130802
''

'''#include "__MODULE.spin"         ' include the common code for OS modules 
'#include "__MODULE.spin" 

VAR
  byte  filename0[13]                           ' FAT source filename       (string)

 
DAT
'_ModuleStr   byte      "=== DEL ===",0         'module's name (string)

PRI executeCommand(stringPointer) | sector[512], errorString, bytes, n

  'get the parameters
  str.stringCopy(@filename0, str.tokenizeString(0),13)  ' source filename

  if strsize(@filename0) == 0                           ' if no parameters, display usage
    {Print.StrCR(string(" Delete a file..."))
    Print.StrCR(string("    DEL <filename>"))
    }return -2

  Print.Str(string(" Deleting File "))
  Print.StrCR(@filename0)

'-----------------------------------------------------------------
' Delete the FAT source file
  errorString := \fat.deleteEntry(@filename0)           ' delete file
  checkError(@filename0, fat.partitionError, errorString) ' if error, display & abort

  return 0
    

dat                                                   
{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}} 