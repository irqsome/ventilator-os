'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _DUMPHUB --> _DUMPHUB.CMD|
'' +--------------------------------------------------------------------------+
'' |  Authors:       (c)2015,2016 "Cluso99" (Ray Rodrick)                     |
'' |  Modifications:                                                          |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' This OS module uses an included module "__MODULE.spin" to perform most of
''  the housekeeping - see this for history, details and acknowledgements.
''
'' RR20151127   This module does......
''              Displays Hub RAM
'' RR20151219   fix lines, add usage if no parameters
'' RR20160219   change parameter lines to bytes (16 bytes/line)
'' RR20160307   rename _hubdump->_dumphub


'''#include "__MODULE.spin"         ' include the common code for OS modules 
'#include "__MODULE.spin" 


VAR
  byte  tmp1[16],tmp2[16]
  long  hubaddr,lines
  

DAT
'_ModuleStr   byte      "==DUMPHUB=",0        'module's name (string)
 
PRI executeCommand(stringPointer) | i
'' HUBDUMP command...

  'get the parameters
  str.stringCopy(@tmp1, str.tokenizeString(0),16)       ' hub address
  str.stringCopy(@tmp2, str.tokenizeString(0),16)       ' lines/length

  if strsize(@tmp1) == 0                                ' if no parameters, display usage
    {Print.StrCR(string(" Dump Hub RAM..."))
    Print.StrCR(string("    DumpHUB <hubaddr> <bytes>"))
    }return -2

  hubaddr := $0000                                      ' set defaults
  lines   := 1
  if strsize(@tmp1) > 0                                 
    hubaddr := str.hexadecimalToInteger(@tmp1)  
  if strsize(@tmp2) > 0                                 
    lines := (str.hexadecimalToInteger(@tmp2) + 15) >> 4 ' convert bytes/16 to lines

  repeat lines
    Print.Hex(hubaddr, 4)
      Print.Str(string(": "))
    repeat i from 0 to 15
      Print.Hex(byte[hubaddr+i], 2)
      Print.Char(" ")
    Print.Spaces(5)
    repeat i from 0 to 15
      Print.ASCII(byte[hubaddr+i])
    Print.CRLF
    hubaddr+=16

  Print.CRLF
  return

dat                                                   
{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}