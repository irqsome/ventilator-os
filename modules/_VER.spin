'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _VER  --> _VER.CMD       |
'' +--------------------------------------------------------------------------+
'' |  Authors:       (c)2012,2013 "Cluso99" (Ray Rodrick)                     |
'' |  Modifications:                                                          |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' This OS module uses an included module "__MODULE.spin" to perform most of
''  the housekeeping - see this for history, details and acknowledgements.
''
'' This module......
'' RR20120317     Displays various system info.
'' RR20151215     add p8xblade & v0.8x
'' RR20151220     add cpublade7


'''#include "__MODULE.spin"         ' include the common code for OS modules 
'#include "__MODULE.spin" 

DAT
'_ModuleStr   byte      "=== VER ===",0        'module's name (string)


OBJ
 strb  : "StringB.spin"
 osver : "_OSVER.spin"
 builddesc: "_BUILD.spin"
 
PRI executeCommand(stringPointer) | char,z,z0,z1,z2,z3,z4,i,j,cogs[8]

'' ifnot(str.stringCompareCI(string("ver"), stringPointer))    '<-- module's name
    Print.CRLF
    'Print.StrCR(string("*** Cluso's Propeller Operating System v1.0x ***"))
    strb.buildStringStr(string("*** "))
    strb.buildStringStr(osver.distro_name)
    char := constant((" ")+("v"<<16))
    strb.buildStringStr(@char)
    strb.buildStringStr(1+str.integerToDecimal(osver#major,str.decimalLength(osver#major)))
    strb.buildString(".")
    strb.buildStringStr(1+str.integerToDecimal(osver#minor,str.decimalLength(osver#minor)))
    if osver#revision
      strb.buildString(".")
      strb.buildStringStr(1+str.integerToDecimal(osver#revision,str.decimalLength(osver#revision)))
    strb.buildStringStr(string(" ***"))
    Print.StrCR(strb.builtString(true))
    print.char(" ")
    char:= constant("/"+("\"<<8))
    repeat (byte[_hub#_OS_Columns]>>1)-1
      print.str(@char)
    print.crlf
    printCredits(4)
    {Print.StrCR(string(" Originally derived from KyeDOS...               "))
    Print.StrCR(string("     by Kwabena W. Agyeman & J. Moxham & Cluso99  "))
    Print.StrCR(string("     & acknowledgements to M. Park (Sphinx) etc   "))}
    Print.CRLF

    Print.str(string("Build: "))
    Print.strCR(builddesc.get)

    Print.Str(string("Hardware: "))
    char := osver.hw_vendor(long[_hub#_hardware])
    ifnot char
      strb.buildString("?")
      strb.buildStringStr(str.integerToHexadecimal(long[_hub#_hardware]>>16,4))
      char := strb.builtString(true)
    Print.Str(char)
    Print.space
    char := osver.hw_name(long[_hub#_hardware])
    ifnot char
      strb.buildString("?")
      strb.buildStringStr(str.integerToHexadecimal(long[_hub#_hardware]&$ffff,4))
      char := strb.builtString(true)
    Print.Str(char)
    Print.CRLF

    Print.Str(string("CPU Speed: "))
    Print.decimal(long[$0]/1_000_000)
    Print.char(".")
    Print.str(1 + str.integerToDecimal(long[$0]//1_000_000,6))
    Print.Str(string("MHz, ClockMode $"))
    Print.Str(str.integerToHexadecimal(byte[$4],2))
    Print.Str(string(", Cog "))
    Print.StrCR(1 + str.integerToDecimal(cogid, 1))

    Print.Str(string("SD card on pins "))
    z  := long[_hub#_sdPins]
    z0 :=  z        & $1F
    z1 := (z >>  8) & $1F
    z2 := (z >> 16) & $1F
    z3 := (z >> 24) & $1F
    Print.Str(1 + str.integerToDecimal(z0, 2))
    Print.Char(",")
    Print.Str(1 + str.integerToDecimal(z1, 2))
    Print.Char(",")
    Print.Str(1 + str.integerToDecimal(z2, 2))
    Print.Char(",")
    Print.Str(1 + str.integerToDecimal(z3, 2))
    Print.CRLF

    Print.Str(string("Serial on pins "))
    z  := long[_hub#_sioPins]
    z0 :=  z        & $1F
    z1 := (z >>  8) & $1F
    z2 := (z >> 16) & $1F
    z3 := (z >> 24) & $0F                               ' =$0F if invalid
    z4 :=  long[_hub#_sioBaud]
    Print.Str(1 + str.integerToDecimal(z0, 2))
    Print.Char(",")
    Print.Str(1 + str.integerToDecimal(z1, 2))
    Print.Str(string(", Mode "))
    Print.Str(1 + str.integerToDecimal(z2, 2))
    Print.Str(string(", Baud "))
    Print.Str(1 + str.integerToDecimal(z4, 8))
    Print.Str(string(", Cog "))
    if z3 > 7
      Print.Char("?")
    else
      Print.Str(1 + str.integerToDecimal(z3, 1))
    Print.CRLF

    Print.Str(string("Screen size: "))
    z := byte[_hub#_OS_Columns]
    Print.Str(1 + str.integerToDecimal(z, 3))
    Print.Str(string(" x "))
    z := byte[_hub#_OS_Rows]
    Print.StrCR(1 + str.integerToDecimal(z, 3))

    Print.Str(string("<LF> is "))
    if byte[_hub#_OS_Rows] & _hub#_LF_MASK                                              
      Print.StrCR(string("ON"))
    else
      Print.StrCR(string("OFF"))

    Print.Str(string("Resident Cogs : "))
    repeat z from 0 to 7
      if (byte[_hub#_OS_Cogs] >> z) & $01 == 1
        Print.Str(1 + str.integerToDecimal(z, 1))
        Print.Str(string(" "))
    Print.Str(string("    This Cog: "))
    Print.StrCR(1 + str.integerToDecimal(cogid, 1))

    Print.Str(string("Cogs available: "))
    j~
    repeat i from 0 to 7
      cogs[i] := cognew(@entry,0) +1                    ' try to start cog(s)
      if cogs[i] 
        j++
    repeat i from 0 to 7
      if cogs[i]
        Print.Str(1 + str.integerToDecimal(cogs[i]-1, 1)) ' print avail cog#
        Print.Char(" ")
        cogstop(cogs[i] -1)                             ' stop the cog(s)
    Print.Str(string("(="))
    Print.Str(1 + str.integerToDecimal(j,1))
    Print.StrCR(string(")"))

    Print.char(constant(_c#color_green+_c#colors_reverse))
    Print.Str(string("Link arms, don't make them."))
    Print.char(_c#color_white)
    Print.CRLF
    Print.CRLF
    return 0

DAT
'' Just a simple pasm program to test if a cog is available
              org       0
entry         jmp       #entry                  ' loops here until cogstop forced!

PRI printCredits(startvl) | chr,hleft,vleft,ptr,rows,cols
  ptr := @credtext
  vleft := rows := (((byte[_hub#_OS_Rows]&$7F) -1)-startvl) #> 0
  hleft := cols := byte[_hub#_OS_Columns] - 1
  repeat while chr:=byte[ptr++]
    if chr <> 10 'line feed?
      print.char(chr)
      --hleft
    else
      hleft~
    ifnot hleft 'LF or char limit?
      print.crlf
      hleft := cols
      ifnot --vleft
        waitMore
        vleft:=rows
  waitMore
  print.crlf

  
PRI waitMore
  print.str(@morestr)
  input.char
  repeat strsize(@morestr)
    print.char(_c#backspace)
  
DAT
morestr       byte "more...",0
credtext      file "CREDITS.TXT"  '' FILE MUST USE LF-ONLY LINE ENDINGS!!
              byte 0

dat                                                   
{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}