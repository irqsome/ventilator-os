'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _RUN  --> _RUN.CMD       |
'' +--------------------------------------------------------------------------+
'' |  Authors:       (c)2012,2013 "Cluso99" (Ray Rodrick)                     |
'' |  Modifications:                                                          |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' This OS module uses an included module "__MODULE.spin" to perform most of
''  the housekeeping - see this for history, details and acknowledgements.
''
'' This module......
''      Runs a "xxx.BIN" program. (because _cmd.cmd does not work!!)
''
'' RR20130802          Fix running ".BIN" files
'' RR20160104          stringConcatenate -> stringAppend
'' RR20160129   v0.93  tidy code
'' RR20160130   v0.94  use __MODULE and SD-MMC_FATEngine for boot/run program
''                     fixed RUN, RUN OS[.BIN], RUN _OS.CMD, RUN xxx.xxx, etc
'' RR20160302    1.02  fix runFile (remove prefix)


' need to fix SD FAT/PASM so we can use existing rather than load a new copy for run!

'''#include "__MODULE.spin"         ' include the common code for OS modules 
'#include "__MODULE.spin"

PRELOAD_BLOCK = 1024 

OBJ
  fat9   : "SD-MMC_FATEngine"                   ' use to boot/run <filename>
  ram    : "23lc1024"      

VAR
  byte  preloadBuffer[PRELOAD_BLOCK]
  byte  filename0[13]                           ' FAT source filename       (string)
  byte  filenameRAM[13]
  byte  buffer[40]
  
DAT
'_ModuleStr   byte      "=== RUN ====",0        'module's name (string)

PRI executeCommand(stringPointer) | sector[512], errorString, bytes, n,thestr

  'get the parameters
  'str.stringCopy(@filename0, str.tokenizeString(0))    ' source filename
  thestr := str.tokenizeString(0)

  if strsize(thestr) == 0                               ' if no parameters, display usage
    {Print.StrCR(string(" Run an 'xxx.BIN' file..."))
    Print.StrCR(string("    RUN <filename>"))
    }return -2

{------------------------------------------------------------------------------}
' Load/Run <filename> (append .BIN if required
  errorString := \runFilename(thestr, string(".BIN"))
  checkError(@filename0, fat.partitionError, errorString) ' if error, display & abort
  fat.closeFile
  return 0                                              ' file not found or error, so return to reload _cmd.cmd
  
PRI runFilename(stringptr1, stringptr2) | n,spleft,spblock,spptr,errorString, errorNumber
' if file exists, load/run, else return

    n := str.findCharacter(stringptr1, ".")             ' file extension used???
    if n == 0
      'No, so concatenate prefix+command+suffix to form filename
      str.stringCopy(  @buffer, stringptr1,40)
      str.stringAppend(@buffer, stringptr2,40)
    else
      'Yes, so just use command as filename
      str.stringCopy(  @buffer, stringptr1,40)

    if strsize(@buffer) > 12                            ' 8.3
      Print.StrCR(string("* filename too long"))
      abort string("***")

    str.stringCopy(@filename0, @buffer,13)              ' copy filename

{------------------------------------------------------------------------------}
' Check the file exists...
    errorString := \fat.openfile(@filename0, "R")       ' open source file
    errorNumber := \checkError(@filename0, fat.partitionError, errorString) ' if error, display & abort
    if errorNumber <> 0
      abort string("***")                               ' typically file not found
    fat.closeFile                                       ' close the file because we are not going to use it yet

      
    
' Check if RAM file exists (if RAM present)
    if (~byte[_hub#_RAMpin]) => 0
      bytemove(@filenameRAM,@filename0,13)
      bytemove(str.findCharacter(@filenameRAM, ".")+1,string("RAM"),3)
      errorString := \fat.openfile(@filenameRAM, "R")       ' open source file
      ifnot fat.partitionError 'exists
        spptr~  
        ram.start(byte[_hub#_SDpins+0]&$1F,byte[_hub#_SDpins+1]&$1F,byte[_hub#_SDpins+2]&$1F,byte[_hub#_RAMpin]&$1F)
        print.str(string("Preloading "))
        spleft := fat.fileSize
        print.decimal(spleft)
        print.str(string(" bytes to SPIRAM",13,"Please be patient...",13))
        repeat while spleft <> 0
          spblock := spleft <# PRELOAD_BLOCK
          fat.readData(@preloadBuffer,spblock)
          ram.write(@preloadBuffer,spptr,spblock)
          spptr += spblock
          spleft -= spblock
        
      fat.closeFile
      ram.stop
{------------------------------------------------------------------------------}
' Now we know the file exists, so stop fat and start fat9 to load/run the file...

    fat.FatEngineStop                                   ' stop the current fat driver

    Print.Str(string(" running file "))
    Print.StrCR(@filename0)
    bootFilename                                        ' start the new fat driver & load/run <filename>

PRI bootFilename | errorString, z, z0, z1, z2, z3
{------------------------------------------------------------------------------}
' Start new fat9 SD driver to load/run new program

  z  :=  long[_hub#_SDpins]
  z0 :=  z        & $1F
  z1 := (z >>  8) & $1F
  z2 := (z >> 16) & $1F
  z3 := (z >> 24) & $1F
 
' Print.StrCR(string("=restarting SD Driver"))                                  '===========debugging

'                           (DO, CLK, DI, CS, _SD_WP, _SD_CD, _RTC_DAT, _RTC_CLK, -1)
  ifnot( fat9.FATEngineStart(z0, z1,  z2, z3, -1,     -1,     -1,       -1,       -1))
    Print.StrCR(string("*SD driver failed! Rebooting..."))                      '===========debugging
    reboot

  fat9.mountPartition(0)                                ' mount the sd card

  ifnot(fat9.partitionMounted)
    Print.Str(string("*ERROR: Card Not Mounted, stop!"))
    repeat                                              ' LOOP HERE!!!
{------------------------------------------------------------------------------}
'Load/Run <filename0>...
  'waitcnt(clkfreq / 10 + cnt)                          ' delay
  errorString := \fat9.bootPartition(@filename0)        ' load/run
  checkError(@filename0, fat9.partitionError, errorString) ' if error, display
{------------------------------------------------------------------------------}
'Only here on error
  Print.StrCR(string("*ERROR: Rebooting..."))
  waitcnt(cnt + clkfreq*2)                              ' delay
  reboot
{------------------------------------------------------------------------------}


dat                                                   
{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}