'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _CMD  --> _CMD.CMD       |
'' +--------------------------------------------------------------------------+
'' |  Authors:       (c)2011 by Kwabena W. Agyman (Kye) & J. Moxham (Dr_Acula)|
'' |  Modifications: (c)2012,2013 "Cluso99" (Ray Rodrick)                     |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' This OS module uses an included module "__MODULE.spin" to perform most of
''  the housekeeping - see this for history, details and acknowledgements.
''
'' RR20120317   This module does......
''                Issues the prompt and waits for a command to be entered, then
''                  loads/runs the command or binary file, or
''                  runs an internal command, or displays "use help" & the prompt.
''
'' RR20130802   Fix running ".BIN" files (clear OS upper hub ram & stop cogs) - didnt work!!!
''              Replace internal commands reboot, del, era, mv
''              Remove internal commands mount, unmount
'' RR20131022   Remove internal command togglelf (now external command LF)
'' RR20160104   Don't use internal " xxx.bin", "clear", "echo"
'' RR20160129   v0.93 tidy code
'' RR20160130   v0.94 don't display error message for just <enter>
'' RR20160219 v1.00  tidy


'''#include "__MODULE.spin"         ' include the common code for OS modules 
'#include "__MODULE.spin" 

MAX_COMMAND = 63

VAR
  byte  buffer[72]
  long  footer[2]

DAT
'_ModuleStr   byte      "=== CMD ===",0        'module's name (string)

OBJ
 _abi : "_ABIVER.spin"
 
PRI executeCommand(stringPointer) | valid, i, b

    ' Is "last command" valid?
    valid~
    ifnot byte[constant(63+_hub#_pBuffer_B)] ' zero termination present?
      b := $DEADBEEF
      repeat i from _hub#_pBuffer_B to constant(62+_hub#_pBuffer_B)
        b += byte[i]                              
        b <-= 1
      if b == long[_hub#_pBuffer_C]
        valid~~

    ifnot valid
      bytefill(_hub#_pBuffer_B,0,64)

    shellLine(string("SD:>"))                          ' wait for a command
    processShortcuts
    
    'str.stringCopy(@commandLine, result) 'save for later in case this is a command with parameters eg "xmodem r filename.txt"
'================
    bytemove(_hub#_pBuffer_A, @commandLine,MAX_COMMAND+1) ' pass command line to next program
    byte[constant(_hub#_pBuffer_A+63)]~
'=================

    ' place into "last command" buffer
    bytemove(_hub#_pBuffer_B,_hub#_pBuffer_A,64)
    ' calculate checksum
    b := $DEADBEEF
    repeat i from _hub#_pBuffer_B to constant(62+_hub#_pBuffer_B)
      b += byte[i]
      b <-= 1
    long[_hub#_pBuffer_C] := b
    
    

    str.tokenizeString(@commandLine)
    if strsize(@commandLine) == 0 
      return 0
    if 0 < strsize(@commandLine) < 8
      ifnot \shellBuiltIns(@commandLine) ' try inbuilt commands
        \runFilename(string("_"), @commandLine, string(".CMD")) ' load/run file if found
      '! \runFilename(string(" "), @commandLine, string(".BIN")) ' load/run file if found
       
        Print.StrCR(string("Command not found, try HELP"))
        'else just re-load _cmd_.cmd
    
PRI runFilename(stringptr1, stringptr2, stringptr3) | r, errorString
' if file exists, load/run, else return

    'Concatenate prefix+command+suffix to form filename
    str.stringCopy(  @buffer, stringptr1,72)
    str.stringAppend(@buffer, stringptr2,72)
    str.stringAppend(@buffer, stringptr3,72)
    str.trimString(  @buffer)
    r := fat.openFile(@buffer, "R")                     ' if not found, will return to caller via abort

    'Check footer
    fat.fileSeek($8000) 'seek past binary
    fat.readData(@footer,constant(2*4))
    if footer <> constant("v"+("O"<<8)+("S"<<16)+("f"<<24))
      print.strCR(string("ERROR: Executable footer not found!"))
      abort
    if footer.byte[4] <> 1
      print.strCR(string("ERROR: Incompatible footer version!"))
    if footer.byte[5] <> _abi#ABI_VERSION
      print.strCR(string("ERROR: Incompatible ABI version!"))
      abort
    
                                                    
{{
    'if ".BIN" file and it exists, set to clear OS from hub & stop cogs
    if (str.stringCompareCI(stringptr3, string(".BIN"))) == 0
'     Print.StrCR(string("=clear hub"))                                         '===========debugging 
      long[_hub#_HubFree] := _hub#_HUB_RAMSIZE          ' set top of hub free (all hub to be loaded)
      byte[_hub#_OS_Cogs] := 0                          ' set resident cogs (all to be stopped)
      if taf.partitionMounted
        taf.unmountPartition
        Print.StrCR(string("=unmount taf"))
'     if fat.partitionMounted
'       fat.unmountPartition
'       Print.StrCR(string("=unmount fat"))                                     '===========debugging 
}}

    errorString := \fat.bootPartition(@buffer)          ' if found, load and run module, else returns via abort
'   checkError(@buffer, fat.partitionError, errorString) ' if error, display & abort
    if errorString
      Print.StrCR(string("error fat.bootPartition "))
      Print.StrCR(errorString)


    Print.StrCR(string("=why here?="))
    waitcnt(clkfreq/10 + cnt)
    reboot

PRI shellBuiltIns(stringPointer)

  ifnot(str.stringCompareCI(string("cd"), stringPointer))
    'return programChangeDirectory(stringPointer)
    print.strCR(string("ERROR: Working directory is not implemented yet!"))
    return -1
  ifnot(str.stringCompareCI(string("mkdir"), stringPointer))
    return programMakeDirectory(stringPointer)
  ifnot(str.stringCompareCI(string("mkfile"), stringPointer))
    return programMakeFile(stringPointer)
  ifnot(str.stringCompareCI(string("chmod"), stringPointer))
    return programChangeAttributes(stringPointer)

  return 0 ' no builtin found!

PRI shellLine(prompt) | char,i,j,c2

  print.char(constant(_c#color_green+_c#colors_dark))
  Print.Str(prompt)
  print.char(_c#color_white)
  print.char(_c#clear_to_end)
  i:=0
  bytefill(@commandLine,0,80)
  {repeat
    result := Input.Char        ' wait for a Char input
    Print.Char(result)          ' echo char to output
    str.buildString(result)     ' build an input string
  while((result <> _c#_carriageReturnCharacter) and (result <> _c#_lineFeedCharacter) and (result <> _c#_nullCharacter))}

  repeat
    char := Input.Char
    case char
      _c#left: ' Move left
        ifnot i
          print.char(_c#beep)
        else
          i--
          print.char(char)
      _c#right: ' Move right
        if i == constant(MAX_COMMAND-1) or not commandLine[i]
          print.char(_c#beep)
        else
          i++
          print.char(char)
      _c#up: ' Recall last command
        ifnot byte[_hub#_pBuffer_B]
          print.char(_c#beep)
          next
        repeat strsize(@commandLine)-i
          print.char($20) 'erase characters after cursor
        repeat strsize(@commandLine)
          print.char(3) 'move to beginning of line
        bytemove(@commandLine,_hub#_pBuffer_B,64)
        bytefill(@commandLine+64,0,constant(80-64))
        i := strsize(@commandLine)
        repeat j from 0 to constant(MAX_COMMAND-1)
          if c2 := commandLine[j]
            print.char(c2)
          else
            quit
      _c#backspace: ' Backspace
        ifnot i 'buffer empty?
          print.char(_c#beep)
          next
        if commandLine[i] 'Not at end of string?
          bytemove(@commandLine+i-1,@commandLine+i,MAX_COMMAND-(i)) 'copy string
          print.char(3)
          repeat j from i-1 to constant(MAX_COMMAND-1)
            if c2 := commandLine[j]
              print.char(c2)
            else
              quit
          print.char(" ")
          repeat (j+2) - i
            print.char(3) 'Move cursor back
          --i
        else
          commandLine[--i]~
          print.char(char)
      $09: ' Tab (TODO: autocomplete?)
      $0D,$0A: ' LF or CR
        print.crlf
        repeat until true ' work around homespun bug
        quit
      $00..$1F: 'ignore unprintable
      other: 'regular character
        if i == constant(MAX_COMMAND-1) 'buffer full?
          print.char(_c#beep)
          next
        print.char(char)
        if commandLine[i] 'Not at end of string? 
          bytemove(@commandLine+i+1,@commandLine+i,constant(MAX_COMMAND-1)-(i)) 'copy string
          repeat j from i+1 to constant(MAX_COMMAND-1)
            if c2 := commandLine[j]
              print.char(c2)
            else
              quit
          repeat j - (i+1)
            print.char(3) 'Move cursor back
        commandLine[i++] := char

 
PRI programChangeDirectory(stringPointer) | errorString
  '' TODO: This doesn't actually work
  stringPointer := str.tokenizeString(0)
  fat.changeDirectory(stringPointer)
  taf.changeDirectory(stringPointer)
  return 0


PRI programMakeDirectory(stringPointer) | errorString
    
    if errorString := \fat.newDirectory(str.tokenizeString(0))
      Print.Str(string("ERROR: "))
      Print.StrCR(errorString)
      return -1
    return 1

PRI programChangeAttributes(stringPointer) | errorString
  
  if errorString := \fat.changeAttributes(str.tokenizeString(0), str.tokenizeString(0))
    Print.Str(string("ERROR: "))
    Print.StrCR(errorString)
    return -1
  return 1

PRI programMakeFile(stringPointer) | errorString

  if errorString := \fat.newFile(str.tokenizeString(0))
    Print.Str(string("ERROR: "))
    Print.StrCR(errorString)
    return -1    
  return 1

PRI processShortcuts
  case commandLine
    "#":
      bytemove(@commandLine+4,@commandLine+1,58)
      bytemove(@commandLine,string("RUN "),4)
    other: 'nothing

dat                                                   
{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}