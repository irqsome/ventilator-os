'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _HELP    --> _HELP.CMD   |
'' +--------------------------------------------------------------------------+
'' |  Authors:       (c)2012,2013 "Cluso99" (Ray Rodrick)                     |
'' |  Modifications:                                                          |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' This OS module uses an included module "__MODULE.spin" to perform most of
''  the housekeeping - see this for history, details and acknowledgements.
''
'' This module......
''                Displays the help text "_HELP.TXT"
''
'' RR20130802   change to display _help.txt
''

'''#include "__MODULE.spin"         ' include the common code for OS modules 
'#include "__MODULE.spin" 

DAT
'_ModuleStr   byte      "=== HELP ===",0        'module's name (string)

OBJ
  strb : "StringB"
VAR
  long footerMagic,footerVersions,footerDate 'must stay contigous
PRI executeCommand(stringPointer) | t,errorString,extraFlag,usageError,cmdname, params,filename

  extraFlag~
  usageError := byte[_hub#_pBuffer_A] == "_" and byte[constant(_hub#_pBuffer_A+1)] == " " 'Launched by -2 status code?
  
  cmdname := str.tokenizeString(0)
  params := str.tokenizeString(0) 'get filename
  ifnot usageError
    extraFlag := byte[params] == "-" and str.ignoreCase(byte[params+1]) == "E"

  if byte[cmdname]
    strb.buildString("_")
    strb.buildStringStr(cmdname)
    strb.buildStringStr(string(".CMD"))
    filename := strb.builtString(true)
    errorString:=\fat.openFile(filename,"R")
    checkError(filename, fat.partitionError, errorString)
    fat.fileSeek($8000)
    fat.readData(@footerMagic,constant(3*4))
    if footerMagic <> constant("v"+("O"<<8)+("S"<<16)+("f"<<24))
      print.strCR(string("ERROR: Executable footer not found!"))
      return
    if footerVersions.byte <> 1
      print.strCR(string("ERROR: Incompatible footer version!"))
    if usageError
      print.strCR(string("Incorrect command usage!",13,10,"Read this command's HELP entry:",13,10))
    if extraFlag
      print.strCR(string("Executable metadata:"))
      print.str(string(" ABI version: "))
      print.decimal(footerVersions.byte[1])
      print.CRLF
      print.str(string(" Timestamp: "))
      printDate(footerDate)
      print.CRLF
      print.strCR(string(" Build: "))
      print.spaces(2)
      repeat while t:=fat.readByte
        print.char(t)
      print.CRLF
      printFootHelp(6)
    else
      repeat while t:=fat.readByte
      printFootHelp(3)
    
  else
    printQHelp(1)

  return 0

PRI printQHelp(startvl) | chr,hleft,vleft,ptr,rows,cols
  ptr := @quickhelp
  vleft := rows := (((byte[_hub#_OS_Rows]&$7F) -1)-startvl) #> 0
  hleft := cols := byte[_hub#_OS_Columns] - 1
  repeat while chr:=byte[ptr++]
    if chr <> 10 'line feed?
      print.char(chr)
      --hleft
    else
      hleft~
    ifnot hleft 'LF or char limit?
      print.crlf
      hleft := cols
      ifnot --vleft
        waitMore
        vleft:=rows
  waitMore
  print.crlf

PRI printFootHelp(startvl) | chr,hleft,vleft,ptr,rows,cols
  ptr := @quickhelp
  vleft := rows := (((byte[_hub#_OS_Rows]&$7F) -1)-startvl) #> 0
  hleft := cols := byte[_hub#_OS_Columns] - 1
  repeat while chr:=fat.readByte
    if chr <> 10 'line feed?
      print.char(chr)
      --hleft
    else
      hleft~
    ifnot hleft 'LF or char limit?
      print.crlf
      hleft := cols
      ifnot --vleft
        waitMore
        vleft:=rows
  'waitMore
  'print.crlf

PRI printDate(date) | m
  print.decimal((date>>17)&$1F) 'day
  print.str(string(". "))
  print.str(@monthtab+(((date>>20)&$3C)-4)) 'month
  print.str(string(". "))
  print.str(str.integerToDecimal(2000+(date>>26)&$3F,4)+1) 'year
  print.spaces(2)
  
  print.str(str.integerToDecimal((date>>12)&$1F,2)+1) 'hours
  print.char(":")
  print.str(str.integerToDecimal((date>>6)&$3F,2)+1) 'minutes
  print.char(":")
  print.str(str.integerToDecimal(date&$3F,2)+1) 'seconds

PRI waitMore
  print.str(@morestr)
  input.char
  repeat strsize(@morestr)
    print.char(_c#backspace)

DAT
morestr       byte "more...",0
quickhelp     file "QUICKHELP.txt"
              byte 0

monthtab      byte "Jan",0,"Feb",0,"Mar",0,"Apr",0,"May",0,"Jun",0,"Jul",0,"Aug",0,"Sep",0,"Oct",0,"Nov",0,"Dec",0

dat                                                   
{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}