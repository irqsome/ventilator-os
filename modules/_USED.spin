'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _USED  --> _USED.CMD     |
'' +--------------------------------------------------------------------------+
'' |  Authors:       (c)2011 by Kwabena W. Agyman (Kye) & J. Moxham (Dr_Acula)|
'' |  Modifications: (c)2012 "Cluso99" (Ray Rodrick)                          |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+
'' This OS module uses an included module to perform most of the housekeeping.
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' Compile with PropTool + "PreSpin 0.3" by Andy Schenk www.insonix.ch/propeller                             
'' Some OS modules are derived from: 
''      KyeDOS v3 (c) 2011 by Kwabena W. Agyman (Kye) & J. Moxham (Dr_Acula)
'' See the included code for history and acknowledgments.
''
'' RR20120317   This module does......

'''#include "__MODULE.spin"         ' include the common code for OS modules 
'#include "__MODULE.spin" 

DAT
'_ModuleStr   byte      "=== USED ===",0        'module's name (string)
 
PRI executeCommand(stringPointer)

    Print.Str(string("The ", _c#_quoteCharacter))
    Print.Str(str.trimString(fat.partitionFileSystemType))
    Print.Str(string(_c#_quoteCharacter, " ", _c#_quoteCharacter))
    Print.Str(str.trimString(fat.partitionVolumeLabel))
    Print.Str(string(_c#_quoteCharacter, " partition has "))
    Print.Decimal(fat.partitionCountOfClusters)
    Print.Str(string(" clusters and "))
    Print.Decimal(fat.partitionDataSectors)
    Print.StrCR(string(" total sectors."))
    Print.Str(string("Each cluster is "))
    Print.Decimal(fat.partitionSectorsPerCluster)
    Print.Str(string(" sectors and each sector is "))
    Print.Decimal(fat.partitionBytesPerSector)
    Print.StrCR(string(" bytes."))
    Print.Str(string("And there are... please wait... "))
    Print.Decimal(fat.partitionUsedSectorCount(byte[str.tokenizeString(0)]))
    Print.StrCR(string(" Used Sectors."))
    Print.Str(string("And there are... please wait... "))
    Print.Decimal(fat.partitionFreeSectorCount(byte[str.tokenizeString(0)]))
    Print.StrCR(string(" Free Sectors."))

    abort 0

dat                                                   
{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}} 