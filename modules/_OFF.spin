'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _CLEAR   --> _CLEAR.CMD  |
'' |  Authors:       (c)2016      "Cluso99" (Ray Rodrick)                     |
'' |  Modifications:                                                          |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' This OS module uses an included module "__MODULE.spin" to perform most of
''  the housekeeping - see this for history, details and acknowledgements.
''
'' RR20160104   This module does......
''              Clears the terminal screen 

'''''#include "__MODULE.spin"           ' include the common code for OS modules 
'''#include "__MODULE.spin"   


VAR
  

DAT
'_ModuleStr   byte      "=== OFF ==",0        'module's name (string)

topstr        byte      "It is now safe to turn off",_c#cr,_c#lf,0
bottomstr     byte      "your computer.",_c#cr,_c#lf,0
 
PRI executeCommand(stringPointer) | width,height

  width := byte[_hub#_OS_Columns]
  height := byte[_hub#_OS_Rows] & $7f

  'waitcnt(cnt + clkfreq/10)
  fat.unmountPartition
  taf.unmountPartition
  fat.FATEngineStop
  taf.FATEngineStop
  Print.Char(_c#color_red)
  Print.Char(_c#clear)
  gotoY((height>>1)-1)
  gotoX(((width-strsize(@topstr))#>0)>>1)
  Print.str(@topstr)
  gotoX(((width-strsize(@bottomstr))#>0)>>1)
  Print.str(@bottomstr)
  print.char(_c#home)

  repeat

PRI gotoX(x)
  print.char(_c#gotoX)
  print.out(x)

PRI gotoY(y)
  print.char(_c#gotoY)
  print.out(y)

dat                                                   
{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}