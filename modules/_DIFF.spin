'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _DIFF    --> _DIFF.CMD   |
'' +--------------------------------------------------------------------------+
'' |  Authors:       (c)2012,2013 "Cluso99" (Ray Rodrick)                     |
'' |  Modifications:                                                          |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' This OS module uses an included module "__MODULE.spin" to perform most of
''  the housekeeping - see this for history, details and acknowledgements.
''
'' This module......
''      Compares two files.
''
'' RR20120317
'' RR20130708   display "." for each correct byte
'' RR20130717   remove "." (slows down)

'''#include "__MODULE.spin"         ' include the common code for OS modules 
'#include "__MODULE.spin" 

VAR
  byte  filename1[13]                           ' FAT source filename 1     (string)
  byte  filename2[13]                           ' FAT source filename 2     (string)

 
DAT
'_ModuleStr   byte      "=== DIFF ===",0        'module's name (string)
 
PRI executeCommand(stringPointer) | byte0, byte1, errorString, same

  'get the parameters
  str.stringCopy(@filename1, str.tokenizeString(0),13)  ' source filename 1
  str.stringCopy(@filename2, str.tokenizeString(0),13)  ' source filename 2

  if strsize(@filename1) == 0                           ' if no parameters, display usage
    {Print.StrCR(string(" Compare two files..."))
    Print.StrCR(string("    DIFF <source_filename1> <source_filename2>"))
    }return -2

'-----------------------------------------------------------------
' Open the FAT source files
  errorString := \fat.openfile(@filename1, "R")         ' open source file 1
  checkError(@filename1, fat.partitionError, errorString) ' if error, display & abort
  errorString := \taf.openfile(@filename2, "R")         ' open source file 2
  checkError(@filename1, taf.partitionError, errorString) ' if error, display & abort
'-----------------------------------------------------------------

  Print.StrCR(string(" Comparing..."))
  same~~
  repeat until(fat.fileSize == fat.fileTell)
    byte0 := fat.readByte
    byte1 := taf.readByte

    if(byte0 <> byte1)
      Print.Str(string("Difference: $"))
      Print.Hex(byte0, 2)
      Print.Str(string(" <> $"))
      Print.Hex(byte1, 2)
      Print.Str(string(" at $"))
      Print.Hex(result, 8)
      Print.CRLF
      same~

    result += 1

  if (same)
    Print.StrCR(string(" Files identical."))
  else
    Print.CRLF
  fat.closeFile
  taf.closeFile

  return 0

dat                                                   
{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}  