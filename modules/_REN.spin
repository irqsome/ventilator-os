'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - OS module: _REN     --> _REN.CMD    |
'' +--------------------------------------------------------------------------+
'' |  Authors:       (c)2012,2013 "Cluso99" (Ray Rodrick)                     |
'' |  Modifications:                                                          |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+
'' Each module is called from, and returns to, the prop binary "_CMD.CMD"
'' This OS module uses an included module "__MODULE.spin" to perform most of
''  the housekeeping - see this for history, details and acknowledgements.
''
'' This module......
''      Renames a FAT16/32 file
''
'' RR20130802   first release

'''#include "__MODULE.spin"         ' include the common code for OS modules 
'#include "__MODULE.spin" 

VAR
  byte  filename0[13]                           ' FAT source filename       (string)
  byte  filename1[13]                           ' FAT destination filename  (string)

 
DAT
'_ModuleStr   byte      "=== REN ===",0         'module's name (string)

PRI executeCommand(stringPointer) | sector[512], errorString, bytes, n

  'get the parameters
  str.stringCopy(@filename0, str.tokenizeString(0),13)  ' source filename
  str.stringCopy(@filename1, str.tokenizeString(0),13)  ' destination filename

  if strsize(@filename0) == 0                           ' if no parameters, display usage
    {Print.StrCR(string(" Rename a file..."))
    Print.StrCR(string("    REN <source_filename> <destination_filename>"))
    }return -2

  Print.Str(string(" Renaming File "))
  Print.Str(@filename0)
  Print.Str(string(" -> "))
  Print.StrCR(@filename1)

'-----------------------------------------------------------------
' Rename the FAT source file
  errorString := \fat.moveEntry(@filename0,@filename1)  ' rename a file
  checkError(@filename0, fat.partitionError, errorString) ' if error, display & abort

  return 0
    

dat                                                   
{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}} 