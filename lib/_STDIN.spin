'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - Standard INPUT driver:  _STDIN.SPIN |
'' +--------------------------------------------------------------------------+
'' |  Authors:       (c)2010-2016 "Cluso99" (Ray Rodrick)                     |
'' |                 based on code used in SphinxOS by Michael Park           |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+
'' RR20100117 Cluso99: first version
'' RR20100203          pass rendezvous at start
'' RR20160219 v1.00    use pRENDEZVOUS = hub#_StdIn (remove pub start)
''                       rename: in->Char

'  This code allows the implementation of standard routines for input while
'  being decoupled from the actual underlying driver (hardware abstraction).
'  The underlying driver runs in a separate cog and may be changed on the fly.
'  Input characters are passed via a single long mailbox in hub (pRENDEZVOUS).

OBJ
  hub : "_HUBDEF"
CON
  pRENDEZVOUS = hub#_StdIn
  
{{
VAR
  long  pRENDEZVOUS                                     'stores a pointer to the hub mailbox

PUB start(rendezvous)
  pRENDEZVOUS := rendezvous                             'get rendezvous location
}}

PUB Char : chr
'' Wait for an input character

  repeat until chr := long[pRendezvous]                 'wait until the mailbox has a character
  long[pRendezvous]~                                    'clear the mailbox
  chr &= $FF                    '$100 -> $00            'extract lower 8 bits (byte)

PUB Peek
'' Returns a 0 if the buffer is empty,
'' else next character (+$100) but doesn't remove from the buffer.
'' The user routine must extract the 8 bits as the complete long is passed.

  return long[pRendezvous]                              'return ALL bits (long) in the mailbox

{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}