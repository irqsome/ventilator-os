
{==============================================================================}
{
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// General Purpose OS Command Module "_MODULE.spin" 
//
// Derived from SD3.01 FATEngine Demo
// Author:        1/2/2011      Kwabena W. Agyeman     
// Modifications: J. Moxham Dr_Acula to create KyeDOS v3.
// Modifications: Cluso99 (Ray Rodrick) break into modules
// Copyright:     (c) 2011 Kwabena W. Agyeman  (Kye)
//                (c) 2011 J. Moxham           (Dr_Acula)
//                (c) 2012,2013 Ray Rodrick    (Cluso99)
// Licence:       MIT (see end of file for details)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Some OS modules are derived from:
//      KyeDOS v3 (c) 2011 by Kwabena W. Agyman (Kye) & J. Moxham (Dr_Acula)
//      Sphinx    (c) 2009-10 by Michael Park and others
// Some concepts and code is from Sphinx (Michael Park) & Spinix (Dave Hein).
// 
// Special thanks to the following authors for their ideas and MIT code:
//   Michael Park, Dave Hein, Kye, James Moxham, Mike Green, Tomas Rokicki,
//   lonesock, hippy, Jeff Ledger, Roger Williams, Michelli Scales, Andy Schenk.
//   My apologies to any others I may have missed.
// And of course to Chip Gracey for the fabulous Prop chip and the Spin Interpreter.
//
// Special thanks to Bernd Winter for his encouragement, suggestions, testing and donation
//   for helping to get the _xxxCPM commands working (_GETCPM, _PUTCPM, _DIRCPM, etc)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Modifications by Cluso99...
// RR20120318                   Common code included in the OS modules
// RR20120323                   v0.40 change _HUBDEF
// RR20120325                   v0.50 change _HUBDEF
// RR20120603                   renamed _MODULE_.SPIN (single underline for homespun)
// RR20120619                   use separate _SD_PASM.spin & _SD_FAT.spin
// RR20130708                   add taf.mountpartition(0) to access a second file (copy & diff were not working)
// RR20130711                   add checkError routine for file errors
// RR20130719                   tidy
// RR20160101                   remove LineOfText3[80] for sphinx LINK
// RR20160104                   remove changes of 20160101
// RR20160106                   add printSpace, printSpaces(n)
// RR20160121                   clkfreq/clkmode value (do not change here)
// RR20160129                   tidy
// RR20160219 v1.00
// RR20160304                   tweek
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

CON
  _clkfreq = 40_000_000         '\ just a default to get going (DO NOT CHANGE)
  _clkmode = xtal1 + pll8x      '/  (to modify, change in _HWDEF.spin)

  _FREE = (_hub#_HUB_RAMSIZE - _hub#_HUB_RESERVED)/4

  __CMDSIZE = 80 'must be multiple of 4

OBJ
  _hw   : "_HWDEF"                      ' hardware pin definitions etc
  _hub  : "_HUBDEF"                     ' fixed hub definitions ($7FFF... down)
  _c    : "_CHARDEF"                    ' ascii char definitions

  fat   : "_SD_FAT"        
  taf   : "_SD_FAT"        
  str   : "StringE"                     ' (renamed ASCII0_STREngine)
  Input : "_STDIN"                      ' use std input  driver (i/o driver loaded by _OS.CMD)
  Print : "_STDOUT"                     ' use std output driver (i/o driver loaded by _OS.CMD)

VAR
   long __status
   byte CommandLine[__CMDSIZE] ' complete command line as typed in

   
PUB start | z,nextCmd, errorString
{------------------------------------------------------------------------------}
'StdIO is already running. _StdIn & _StdOut do not require starting.
{------------------------------------------------------------------------------}
'Start the SD driver (later some of this will also be resident)
  z  :=  long[_hub#_SDpins]
  {z0 := z        & $1F
  z1 := (z >>  8) & $1F
  z2 := (z >> 16) & $1F
  z3 := (z >> 24) & $1F}
'                          (DO   ,CLK       ,DI         ,CS         , _SD_WP, _SD_CD, _RTC_DAT, _RTC_CLK, -1)
  ifnot( fat.FATEngineStart(z&$1F,(z>>8)&$1F,(z>>16)&$1F,(z>>24)&$1F, -1, -1, -1, -1, -1))
    reboot

  fat.mountPartition(0)                                 ' mount the sd card
  taf.mountPartition(0)                                 ' mount the sd card

  errorString := string("*ERROR: Card Not Mounted, stop!")
  ifnot(fat.partitionMounted)
    Print.Str(errorString)
    repeat                                              ' LOOP HERE!!!
  ifnot(taf.partitionMounted)
    Print.Str(errorString)
    repeat                                              ' LOOP HERE!!!
{------------------------------------------------------------------------------}
'Execute the command
  str.stringCopy(@commandline, _hub#_pBuffer_A,__CMDSIZE) ' retrieve commandline
  \str.tokenizeString(@commandline)
  \__execWrapper                                        ' execute
  if __status == -2 'display help?
    nextCmd := string("_HELP.CMD")
    z :=  _hub#_pBuffer_A
    bytemove(z+2,z,constant(__CMDSIZE-3))
    byte[z++] := "_"
    byte[z] := " "
  else
    nextCmd := string("_CMD_.CMD")
{------------------------------------------------------------------------------}
'Load/Run "_CMD.CMD"...
  waitcnt(cnt + clkfreq / 10)                           ' delay
  errorString := \fat.bootPartition(nextCmd) ' load/run
  checkError(nextCmd, fat.partitionError, errorString) ' if error, display
{------------------------------------------------------------------------------}
'Only here on error
  Print.StrCR(string("*ERROR: Rebooting..."))
  waitcnt(cnt + clkfreq * 2)                            ' delay
  reboot
{------------------------------------------------------------------------------}

PRI __execWrapper
  __status := executeCommand(@commandline)

PRI checkError(errorMessage, errorNumber, errorString)
  if errorNumber == 0
    return 0
  else
    Print.Str(string("*ERROR: on file "))
    Print.Str(errorMessage)
    Print.Str(String(" - "))
    Print.Hex(errorNumber,2)
    Print.Char(":")
    Print.StrCR(errorString)
    'waitcnt(cnt + clkfreq * 2)                         ' delay
    abort errorNumber

{==============================================================================}
CON
    