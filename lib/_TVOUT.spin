'' +--------------------------------------------------------------------------+
'' | VentilatorOS...                                                          |
'' |                 TV Output Driver:                _TVOUT.SPIN             |
'' +--------------------------------------------------------------------------+
'' |  Authors:                                                                |
'' |  (c) 2004           Chip Gracey           - original code TV.spin        |
'' |  (c) 20XX           Jim Bagley            - modifications                |
'' |  (c) 2019           Ada Gottensträter (Wuerfel_21) - modifications       |
'' |  License            MIT License - See end of file for terms of use       |
'' +--------------------------------------------------------------------------+

OBJ

_hw : "_HWDEF"

PUB start(inRENDEZVOUS, outRENDEZVOUS, buffer, mode, tvpins,palette) | st1,st2
  setpalette(palette)
  _pins := tvpins
  if (tvpins & $8)
    st2 := pins1
  else
    st2 := pins0
  st1 := (tvpins>>1)<<3
  st2 >>= st1
  haxx_s |= st2&511
  st1 >>= 6
  haxx_d |= st1&511
  st1 <<= 3
  st2 &= 255
  st2 <<= st1
  _outpins := st2
  _outpins |= |< _hw#_AUDIO_1

  ltab[0] := fraction(fntsc,clkfreq,1)
  ltab[1] := fraction(fpal,clkfreq,1)

  _screen := buffer                  
  _mode := %10010 | (mode&1)                 
  return cognew(@entry,outRENDEZVOUS)+1

PRI setpalette(which) |i,palptr,tblptr,bgcolor,fgcolor
  tblptr := @entry + (colortable*4)
  palptr := @@word[@pallist][which]

  bgcolor := byte[palptr][16]


  repeat i from 0 to 15
    fgcolor := byte[palptr][i]
    long[tblptr][i*2+0] := (fgcolor<<24)+(bgcolor<<16)+(fgcolor<<8)+(bgcolor)
    long[tblptr][i*2+1]  := (fgcolor<<24)+(fgcolor<<16)+(bgcolor<<8)+(bgcolor)
    long[tblptr][i*2+32] := (bgcolor<<24)+(fgcolor<<16)+(bgcolor<<8)+(fgcolor)
    long[tblptr][i*2+33] := (bgcolor<<24)+(bgcolor<<16)+(fgcolor<<8)+(fgcolor)

PRI fraction(a, b, shift) : f

  if shift > 0                         'if shift, pre-shift a or b left
    a <<= shift                        'to maintain significant bits while 
  if shift < 0                         'insuring proper result
    b <<= -shift
 
  repeat 32                            'perform long division of a/b
    f <<= 1
    if a => b
      a -= b
      f++           
    a <<= 1

DAT

pallist
        word @test_pal
        word @default_pal

default_pal
        byte $07 'white
        byte $3D 'cyan
        byte $DD 'magenta
        byte $0D 'blue
        byte $8E 'yellow
        byte $5E 'green
        byte $BD 'red
        byte $06 'grey

        byte $05 'white
        byte $3C 'cyan
        byte $DC 'magenta
        byte $0C 'blue
        byte $8C 'yellow
        byte $5C 'green
        byte $BC 'red
        byte $03 'grey

        byte $02 'background (black)

test_pal
        byte $07[16]
        byte $0A
        
CON

  colortable = $180
  fntsc         = 3_579_545     'NTSC color frequency
  lntsc         = 3640          'NTSC color cycles per line * 16
  sntsc         = 624           'NTSC color cycles per sync * 16             

  fpal          = 4_433_618     'PAL60 color frequency
  lpal          = {4540}4508    'PAL60 color cycles per line * 16
  spal          = {848}773      'PAL60 color cycles per sync * 16             

  pins0                 = %11110000_01110000_00001111_00000111
  pins1                 = %11111111_11110111_01111111_01110111

DAT

'*******************************
'* Assembly language TV driver *
'*******************************

                        org
'
'
' Entry
'
entry
t1
haxx_s                  movs    vcfg,#0
t2                      
haxx_d                  movd    vcfg,#0
m1                      mov     dira,_outpins
m2                      movs    ctrb,#_hw#_AUDIO_1
y                       movi    ctrb,#%00110_000

pixels                  mov     x,#10                   'perform task sections initially
initl                   jmpret  taskret,taskptr
pixels2                 djnz    x,#initl
'
'
' Superfield
'
superfield              mov     taskptr,#tasks          'reset tasks

                        test    _mode,#%0001    wc      'if ntsc, set phaseflip
        if_nc           mov     phaseflip,phasemask

                        test    _mode,#%0010    wz      'get interlace into nz
                        mov     x,#0 wz
'
'
' Field
'
field                   mov     x,vinv                  'do invisible back porch lines
:black                  call    #hsync                  'do hsync
                        waitvid burst,sync_high2        'do black
                        jmpret  taskret,taskptr         'call task section (z undisturbed)
                        djnz    x,#:black               'another black line?

                        'wrlong visible,par            'set status to visible

                        mov     x,vb                    'do visible back porch lines
                        call    #blank_lines

                        call    #calc_addr
                        mov     cursor_ptr,t1
                        call    #toggle_cursor

                        mov     screen,_screen          'point to first tile (upper-leftmost)
                        mov     y,_vt                   'set vertical tiles
:line                   'mov    vx,#1                   'set vertical expand
:vert   if_z            xor     interlace,#1            'interlace skip?
        if_z            tjz     interlace,#:skip

                        call    #hsync                  'do hsync

                        mov     vscl,hb                 'do visible back porch pixels
                        xor     tile,colortable
                        waitvid tile,#0

                        mov     x,_ht                   'set horizontal tiles
                        mov     vscl,hx                 'set horizontal expand

:tile                   rdword  tile,screen             'read tile
                        or      tile,line               'set pointer bits into tile
                        rol     tile,#6                 'read tile pixels
                        rdlong  pixels,tile             '(2 instructions between reads)
                        add     tile,#4     ' JB MOD point to next scanline's pixels
                        add     screen,#2               'point to next tile
                        rdlong  pixels2,tile ' JB MOD get next scanline's pixels
                        or      pixels,pixels2 ' JB MOD or the two together
                        shr     tile,#10+6              'set tile colors
                        movs    :color,tile
                        mov     tile,phaseflip
:color                  xor     tile,colortable
                        waitvid tile,pixels             'pass colors and pixels to video
                        djnz    x,#:tile                'another tile?

                        sub     screen,hc2x             'repoint to first tile in same line

                        ' Update beeping
                        cmpsub  beep_left,#1 wc
              if_c      add     frqb,beep_frq
              if_nc     mov     frqb,bit31

                        mov     vscl,hf                 'do visible front porch pixels
                        mov     tile,phaseflip
                        xor     tile,colortable
                        waitvid tile,#0

:skip                   'djnz   vx,#:vert               'vertical expand?
                        ror     line,#1                 'set next line
                        add     line,lineadd    wc
                        rol     line,#1           
        if_nc           jmp     #:line
                        add     screen,hc2x             'point to first tile in next line
                        djnz    y,#:line                'another tile line?

        if_z            xor     interlace,#1    wz      'get interlace and field1 into z

                        test    _mode,#%0001    wc      'prepare visible front porch lines
                        mov     x,vf
        if_nz_and_c     add     x,#1
                        
                        call    #toggle_cursor
        
                        call    #blank_lines  'do visible front porch lines
                        test    _mode,#%0001    wc

        'if_nz         wrlong  invisible,par           'unless interlace and field1, set status to invisible

        if_z_eq_c       call    #hsync                  'if required, do short line
        if_z_eq_c       mov     vscl,hrest
        if_z_eq_c       waitvid burst,sync_high2
        if_z_eq_c       xor     phaseflip,phasemask

                        call    #vsync_high             'do high vsync pulses

                        movs    vsync1,#sync_low1       'do low vsync pulses
                        movs    vsync2,#sync_low2
                        call    #vsync_low

                        call    #vsync_high             'do high vsync pulses

        if_nz           mov     vscl,hhalf              'if odd frame, do half line
        if_nz           waitvid burst,sync_high2

        if_z            jmp     #field                  'if interlace and field1, display field2
                        jmp     #superfield             'else, new superfield

'
' Hide/unhide cursor
'
toggle_cursor           muxnz   :restorez,#1 'save flags
                        test tmflags,#$104 wz 'Test for scroll flag or "cursor off" flag
              if_z      rdword t2,cursor_ptr
              if_z      xor t2,bit15
              if_z      wrword t2,cursor_ptr
:restorez               or      zero,#0 nr,wz 'restore flags
toggle_cursor_ret       ret

'
'
' Blank lines
'
blank_lines             call    #hsync                  'do hsync


                        'mov     tile,phaseflip
                        xor     tile,colortable         'do background
                        waitvid tile,#0
                        jmpret taskret,taskptr ' RUN TASKS ON BLANK LINES FOR BETTER SPEED

                        djnz    x,#blank_lines

blank_lines_ret         ret
'
'
' Horizontal sync
'
hsync                   test    _mode,#%0001    wc      'if pal, toggle phaseflip
        if_c            xor     phaseflip,phasemask

                        mov     vscl,sync_scale1        'do hsync       
                        mov     tile,phaseflip
                        xor     tile,burst
                        waitvid tile,sync_normal
                        
                        mov     vscl,hvis               'setup in case blank line
                        mov     tile,phaseflip

hsync_ret               ret
'
'
' Vertical sync
'
vsync_high              movs    vsync1,#sync_high1      'vertical sync
                        movs    vsync2,#sync_high2

vsync_low               mov     x,vrep

vsyncx                  mov     vscl,sync_scale1
vsync1                  waitvid burst,sync_high1

                        mov     vscl,sync_scale2
vsync2                  waitvid burst,sync_high2

                        djnz    x,#vsyncx
vsync_low_ret
vsync_high_ret          ret
'
'
' Tasks - performed in sections during invisible back porch lines
'
tasks                   

                        jmpret  taskptr,taskret         '+1=140, break and return later

                        movs    :rd,#wtab               'load ntsc/pal metrics from word table
                        movd    :wr,#hvis
                        mov     t1,#wtabx - wtab
                        test    _mode,#%0001    wc
:rd                     mov     t2,0
                        add     :rd,#1
        if_nc           shl     t2,#16
                        shr     t2,#16
:wr                     mov     0,t2
                        add     :wr,d0
                        djnz    t1,#:rd                 '+54

        if_nc           movs    :ltab,#ltab             'load ntsc/pal metrics from long table
        if_c            movs    :ltab,#ltab+1
                        movd    :ltab,#fcolor
                        mov     t1,#(ltabx - ltab) >> 1
:ltab                   mov     0,0
                        add     :ltab,d0s1
                        djnz    t1,#:ltab               '+17

                        '' Update cursor blink
                        add     cursor_phs,cursor_frq wc
              if_c      xor     tmflags,#$004

                        {rdlong t1,#0                   'get CLKFREQ
                        shr     t1,#1                   'if CLKFREQ < 16MHz, cancel _broadcast
                        cmp     t1,m8           wc
        if_c            mov     _broadcast,#0
                        shr     t1,#1                   'if CLKFREQ < color frequency * 4, disable
                        cmp     t1,fcolor       wc
        if_c            jmp     #disabled}              '+11

                        jmpret  taskptr,taskret         '+1=83, break and return later

                        mov     t2,fcolor               'set ctra pll to fcolor * 16
                        'call   #divide                 'if ntsc, set vco to fcolor * 32 (114.5454 MHz)
                        test    _mode,#%0001    wc      'if pal, set vco to fcolor * 16 (70.9379 MHz)
        if_c            movi    ctra,#%00001_111        'select fcolor * 16 output (ntsc=/2, pal=/1)
        if_nc           movi    ctra,#%00001_110
        if_nc           shl     t2,#1
                        mov     frqa,t2                 '+147

                        jmpret  taskptr,taskret         '+1=148, break and return later

                        {mov    t1,_broadcast           'set ctrb pll to _broadcast
                        mov     t2,#0                   'if 0, turn off ctrb
                        tjz     t1,#:off
                        min     t1,m8                   'limit from 8MHz to 128MHz
                        max     t1,m128
                        mov     t2,#%00001_100          'adjust _broadcast to be within 4MHz-8MHz
:scale                  shr     t1,#1                   '(vco will be within 64MHz-128MHz)
                        cmp     m8,t1           wc
        if_c            add     t2,#%00000_001
        if_c            jmp     #:scale
:off                    movi    ctrb,t2
                        call    #divide
                        mov     frqb,t2                 '+165

                        jmpret  taskptr,taskret}        '+1=166, break and return later

                        mov     t1,#%10100_000          'set video configuration
                        test    _pins,#$01      wc      '(swap broadcast/baseband output bits?)
        if_c            or      t1,#%01000_000
                        'test   _mode,#%1000    wc      '(strip chroma from broadcast?)
        'if_nc          or      t1,#%00010_000
                        test    _mode,#%0100    wc      '(strip chroma from baseband?)
        if_nc           or      t1,#%00001_000
                        'and    _auralcog,#%111         '(set aural cog)
                        'or     t1,_auralcog
                        movi    vcfg,t1                 '+10

                        mov     hx,_hx                  'compute horizontal metrics
                        shl     hx,#8
                        or      hx,_hx
                        shl     hx,#4

                        mov     hc2x,_ht
                        shl     hc2x,#1

                        mov     t1,_ht
                        mov     t2,_hx
                        call    #multiply
                        mov     hf,hvis
                        sub     hf,t1
                        shr     hf,#1           wc
                        mov     hb,_ho
                        addx    hb,hf
                        sub     hf,_ho                  '+52

                        mov     t1,_vt                  'compute vertical metrics
                        mov     t2,#1
                        call    #multiply
                        shr     t1,#1           'JB MOD this divides the screen size by two, as tile size is now 16 not 32 pixels.
                        test    _mode,#%10000   wc      'consider tile size
                        'muxc   linerot,#1
                        'mov    lineadd,lineinc
        'if_c           shr     lineadd,#1
        if_c            shl     t1,#1
                        test    _mode,#%0010    wc      'consider interlace
        if_c            shr     t1,#1
                        mov     vf,vvis
                        sub     vf,t1
                        shr     vf,#1           wc
                        neg     vb,_vo
                        addx    vb,vf
                        add     vf,_vo                  '+53

                        'xor    _mode,#%0010            '+1, flip interlace bit for display

:colors                 jmpret  taskptr,taskret         '+1=117/160, break and return later

                        'save Z
                        muxnz   :restorez,#1
                        call    #update_terminal
                        call    #update_terminal
:restorez               movd    d0,#0 nr,wz

                        ' Update beeping
                        cmpsub  beep_left,#1 wc
              if_c      add     frqb,beep_frq
              if_nc     mov     frqb,bit31

                        jmp     #:colors                '+1, keep loading colors
'
'
' Divide t1/CLKFREQ to get frqa or frqb value into t2
'
{divide                 mov     m1,clockfreq            'get CLKFREQ

                        mov     m2,#32+1
:loop                   cmpsub  t1,m1           wc
                        rcl     t2,#1
                        shl     t1,#1
                        djnz    m2,#:loop

divide_ret              ret}                            '+140
'
'
' Multiply t1 * t2 * 16 (t1, t2 = bytes)
'
multiply                shl     t2,#4
multiply2               shl     t2,#8-1

                        mov     m1,#8
:loop                   shr     t1,#1           wc
        if_c            add     t1,t2
                        djnz    m1,#:loop
multiply2_ret
multiply_ret            ret                             '+37


update_terminal         test tmflags,#$100 wz 'scroll/clear screen?
              if_z      jmp #:dochar 'if not, get character

                        mov tm1,spacechar 'put empty char into tm1
                        add row,#1 'Increment row and check if last row
                        cmp row,_vt wz '...                                                                                                                                                                                                          
                        call #calc_addr
              if_e      sub row,#1 'If last row, adjust back (after calc_addr)
                        test tmflags,#$080 wc 'get clear flag into C
        if_z_and_c      mov row,#0 'if last row and clear, reset row to beginning
                        mov t2,t1
                        sub t2,_ht
                        sub t2,_ht
                        mov column,_ht
              if_z      andn tmflags,#$180 'If last row, unset scroll and clear flags 
:scrolloop   if_nz_and_nc rdword tm1,t1 'if last row or clear, don't read buffer (tm1 is still empty char)
                        add t1,#2
                        wrword tm1,t2
                        add t2,#2
                        djnz column,#:scrolloop
                        jmp update_terminal_ret


:dochar                 test tmflags,#$040 wc 'restore cursor?
              if_c      mov column,column2
              if_c      mov row,row2
                        andn tmflags,#$040
                        tjnz repchar,#:pokechar 'repeat character?
                        rdlong tm1,par wz
              if_z      jmp update_terminal_ret
                        wrlong  zero,par
                        sub     tm1,#$100
                        test    tmflags,#$003 wz 'GOTO flags active?
              if_nz     jmp #:check_goto
                        shl tm1,#0 wc 'Raw character? (bit 31 set)
              if_c      jmp #:pokechar
                        cmpsub  tm1,#$100 wc  'Color code?
              if_c      jmp #:setcolor
                        cmp     tm1,#16 wc,wz 'Is special character?
              if_a      jmp #:normalchar
              if_nz     testn   tm1,#1 wz 'Is home or clear?
              if_z      jmp #:homeclear 'clear is 0 or 16, home is 1
                        sub tm1,#2
                        tjnz tm1,#:not_gotoxy
                        '' GOTO XY
                        or tmflags,#$003 'set gotox and gotoy flags
                        jmp update_terminal_ret
                        
:not_gotoxy             djnz tm1,#:not_cleft
                        '' CURSOR LEFT
                        call #cursor_back
                        jmp update_terminal_ret
                        
:not_cleft              djnz tm1,#:not_cright
                        '' CURSOR RIGHT
                        add column,#1
                        cmp column,_ht wz
              if_z      sub column,#1
              if_z      add row,#1
              if_z      cmp row,_vt wc
        if_z_and_nc     sub row,#1
        if_z_and_c      mov column,#0
              
                        jmp update_terminal_ret

:not_cright             djnz tm1,#:not_cup
                        '' CURSOR UP
                        cmpsub row,#1
                        jmp update_terminal_ret

:not_cup                djnz tm1,#:not_cdown
                        '' CURSOR DOWN
                        add row,#1
                        cmp row,_vt wz,wc
              if_ae     sub row,#1
                        jmp update_terminal_ret

:not_cdown              djnz tm1,#:not_beep
                        '' BEEP
                        movd beep_left,#$04
                        jmp update_terminal_ret

:not_beep               djnz tm1,#:not_backspace
                        '' BACKSPACE
                        call #cursor_back
                        call #calc_addr
                        wrword spacechar,t1                        
                        jmp update_terminal_ret

:not_backspace          djnz tm1,#:not_tab
                        '' TAB
                        mov t1,column
                        and t1,#7 ' 8 spaces per tab
                        mov repchar,#8  '^^
                        sub repchar,t1
                        mov tm1,spacechar
                        jmp update_terminal_ret

:not_tab                djnz tm1,#:not_linefeed
:lf                     '' LINE FEED
                        add     row,#1
                        cmpsub  row,_vt    wc
                        muxc    tmflags,#$100 'Do scroll next update
                        jmp update_terminal_ret

:not_linefeed           djnz tm1,#:not_cltoend
                        '' CLEAR TO END
                        call #calc_addr
                        mov tm2,_ht
                        sub tm2,column
:cltoendloop            wrword spacechar,t1
                        add t1,#2
                        djnz tm2,#:cltoendloop
                        jmp update_terminal_ret

:not_cltoend            djnz tm1,#:not_clbelow
                        '' CLEAR BELOW
                        mov column2,column
                        mov row2,row
                        add row,#2
                        cmp row,_vt wc,wz
                        mov row,row2
                 if_be  or tmflags,#$1C0 'scroll,clear and restore flags
                        jmp update_terminal_ret

:not_clbelow            djnz tm1,#:not_newline
                        '' NEW LINE
                        mov column,#0
                        jmp #:lf

:not_newline            djnz tm1,#:not_gotox
                        '' GOTO X
                        or tmflags,#$001 'set gotox flag
                        jmp update_terminal_ret

:not_gotox              djnz tm1,#:not_gotoy
                        '' GOTO Y
                        or tmflags,#$002 'set gotox flag
:not_gotoy              jmp update_terminal_ret   
                        

:check_goto             test tmflags,#$001 wz ' goto x flag?
              if_z      jmp #:check_goto_y
                        mov t1,_ht
                        sub t1,#1
                        mov column,tm1
                        max column,t1
                        andn tmflags,#$001
                        jmp update_terminal_ret
:check_goto_y           'logicn tells us that the goto y flag must be set
                        mov t1,_vt
                        sub t1,#1
                        mov row,tm1
                        max row,t1
                        andn tmflags,#$002
                        jmp update_terminal_ret

:setcolor               and tm1,#31 'colors > 16 are reverse video
                        shl tm1,#11
                        mov termcolor,tm1
                        mov spacechar,#$22
                        shl spacechar,#4
                        add spacechar,termcolor
                        jmp update_terminal_ret
                                               
:homeclear              mov column,#0
                        mov row,#0
                        shr tm1,#0 wc 'is home or clear
              if_nc     or tmflags,#$180 'if clear, do clear+scroll
                        jmp update_terminal_ret
                        
              

:normalchar             'adjust character
                        shr     tm1,#1 wc
                        add     tm1,#$100
              if_c      or      tm1,d0
                        shl     tm1,#1
                        add     tm1,termcolor
                        
:pokechar               call    #calc_addr
                        wrword  tm1,t1

                        add     column,#1
                        cmpsub  column,_ht wc
              if_c      add     row,#1
                        cmpsub  row,_vt    wc
                        muxc    tmflags,#$100 'Do scroll next update
                        cmpsub repchar,#1                        

update_terminal_ret     ret

calc_addr               mov t1,_ht
                        mov t2,row
                        call #multiply2
                        add t1,column
                        shl t1,#1
                        add t1,_screen

calc_addr_ret           ret

cursor_back             sub column,#1 wc
              if_nc     jmp cursor_back_ret
                        cmpsub row,#1 wc
              if_c      add column,_ht
              if_nc     mov column,#0                        
cursor_back_ret         ret


'
'
' Initialized data
'
_pins                   long    1       '%pppmmmm       read-only
_mode                   long    1       '%tccip         read-only
_screen                 long    1       '@word          read-only
_ht                     long    40      '1+             read-only
_vt                     long    25      '1+             read-only
'_vx                    long    1       '1+             read-only
_ho                     long    0       '0+-            read-only
_vo                     long    0       '0+-            read-only

spacechar               long    $220
termcolor               long    0
zero                    long    0
m8                      long    8_000_000
'm128                   long    128_000_000
d0                      long    1 << 9 << 0
'd6                     long    1 << 9 << 6
d0s1                    long    1 << 9 << 0 + 1 << 1
'clockfreq              long    0 
'invisible              long    1
'visible                long    2
'pins0                  long    %11110000_01110000_00001111_00000111
'pins1                  long    %11111111_11110111_01111111_01110111
'
'
' NTSC/PAL metrics tables
'                               ntsc                    pal
'                               ----------------------------------------------
wtab                    word    lntsc - sntsc,          lpal - spal     'hvis
                        word    lntsc / 2 - sntsc,      lpal / 2 - spal 'hrest
                        word    lntsc / 2,              lpal / 2        'hhalf
                        word    243,                    243{286}         'vvis
                        word    10,                     10{18}           'vinv
                        word    6,                      5{5}            'vrep
                        word    $02_7A,                 $02_AA          'burst
                        word    4,                      5               'hx
wtabx
ltab                    long    fntsc                                   'fcolor
                        long    fpal
                        long    sntsc >> 4 << 12 + sntsc                'sync_scale1
                        long    spal >> 4 << 12 + spal
                        long    67 << 12 + lntsc / 2 - sntsc            'sync_scale2
                        long    79 << 12 + lpal / 2 - spal
                        long    %0101_00000000_01_10101010101010_0101   'sync_normal
                        long    %010101_00000000_01_101010101010_0101
ltabx

'
'
'
' Parameter buffer
'
                        fit     colortable              'fit underneath colortable ($180-$1BF)
                        long 0[colortable-$]
                        long $07_05_04_0A[64]
                        
                        fit $1C0

column                  long    0
row                     long    0
column2                 long    0
row2                    long    0
tm1                     long    0
tm2                     long    0
tmflags                 long    0
repchar                 long    0
cursor_phs              long    0
cursor_frq              long    1<<29
beep_left               long    0
beep_frq                long    117924613
bit15                   long    1 << 15
bit31                   long    1 << 31

interlace               long    0
x                       'alias
_outpins                long    0
taskptr                 long    tasks

phaseflip               long    $00000000
phasemask               long    $F0F0F0F0
line                    long    $00060000
lineadd                 long    $10000000       'JB MOD lineinc is now $20000000 so that it carries over in 8 loops instead of 16 for a half tile.


sync_high1              long    %0101010101010101010101_101010_0101
sync_high2              long    %01010101010101010101010101010101       'used for black
sync_low1               long    %1010101010101010101010101010_0101
sync_low2               long    %01_101010101010101010101010101010

'
' Uninitialized data
'
taskret                 res     1
                                                        'display
hf                      res     1
hb                      res     1
vf                      res     1
vb                      res     1
hx                      res     1
vx                      res     1
hc2x                    res     1
screen                  res     1
tile                    res     1
cursor_ptr              res     1

hvis                    res     1                       'loaded from word table
hrest                   res     1
hhalf                   res     1
vvis                    res     1
vinv                    res     1
vrep                    res     1
burst                   res     1
_hx                     res     1

fcolor                  res     1                       'loaded from long table
sync_scale1             res     1
sync_scale2             res     1
sync_normal             res     1


                        fit


{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}            