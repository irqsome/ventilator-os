'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System - Standard OUTPUT driver:_STDOUT.SPIN |
'' +--------------------------------------------------------------------------+
'' |  Authors:       (c)2010-2016 "Cluso99" (Ray Rodrick)                     |
'' |                 based on code used in SphinxOS by Michael Park           |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+
'' RR20100117 Cluso99: first version
'' RR20100203          pass rendezvous at start
'' RR20160219 v1.00    use pRENDEZVOUS = hub#_StdOut (remove pub start)
''                     rename: routines out->Char, str->String, dec->Decimal,
''                       hex->Hex; add: StringCR, ASCII, CRLF, Spaces;
''                       option to strip <lf>
'' RR20160222 v1.01    add Dump, etc & tidy

'  This code allows the implementation of standard routines for output while
'  being decoupled from the actual underlying driver (hardware abstraction).
'  The underlying driver runs in a separate cog and may be changed on the fly.
'  Output characters are passed via a single long mailbox in hub (pRENDEZVOUS).

OBJ
  _hub : "_HUBDEF"
  _c   : "_CHARDEF"
  
CON
  pRENDEZVOUS = _hub#_StdOut
  
PUB Out(chr)
'' Print a character (do not strip <lf>)

  repeat while long[pRENDEZVOUS]                        'wait for mailbox to be empty (=0)
  long[pRENDEZVOUS] := chr + $100    'add bit9=1 (allows $00 to be passed) & place in mailbox for driver

PUB Char(chr)
'' Print a character (optionally strip <lf>)

  if (chr == _c#_lineFeedCharacter) and (byte[_hub#_OS_Rows] & _hub#_LF_MASK == 0)
    return                                              'strip <lf>
  Out(chr)

PUB Str(stringptr)
'' Print a zero-terminated string

  repeat strsize(stringptr)
    Char(byte[stringptr++])

PUB StrCR(stringptr)
'' Print a zero-terminated string followed by CR+LF

  Str(stringptr)
  CRLF

PUB CRLF
'' Print CR+LF

  Char(_c#_carriageReturnCharacter)
  Char(_c#_lineFeedCharacter)
    
PUB Decimal(value) | _i
'' Print a decimal number

  if value < 0
    -value
    Char("-")

  _i := 1_000_000_000

  repeat 10
    if value => _i
      Char(value / _i + "0")
      value //= _i
      result~~
    elseif result or _i == 1
      Char("0")
    _i /= 10

PUB Hex(value, digits)
'' Print a hexadecimal number

  value <<= (8 - digits) << 2
  repeat digits
    Char(lookupz((value <-= 4) & $F : "0".."9", "A".."F"))

PUB Binary(value, digits)
'' Print a binary number

  value <<= 32 - digits
  repeat digits
    Char((value <-= 1) & 1 + "0")

PUB ASCII(chr)
' Print a visual ascii character $20..$7E, else print "."
  if chr < _c#_firstPrintableCharacter                  '$20
    Dot
  elseif chr > _c#_lastPrintableCharacter               '$7E
    Dot
  else
    Char(chr)

{##############################################################################}
PUB Space
' Print a <space>
  Char(" ")
PUB Spaces(n)
'' Print "n" spaces
  repeat n
    Space
PUB Dot
  Char(".")

PUB Dump(hubaddr,lines) | addr, i,width
  addr := hubaddr
  if byte[_hub#_OS_Columns] >= 70
    width := 16
  else
    width := 8
    lines<<=1
  repeat lines
    Hex(addr, 4)
    Str(string(": "))
    repeat i from 0 to width-1
      Hex(byte[addr+i], 2)
      Space
    'Spaces(5)
    repeat i from 0 to width-1
      ASCII(byte[addr+i])
    CRLF
    addr+=width
{##############################################################################}
{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}