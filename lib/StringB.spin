{{
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ASCII0 String Engine (String builder only)
//
// Author: Kwabena W. Agyeman
// Updated: 8/28/2010
// Designed For: P8X32A
// Version: 1.2
//
// Copyright (c) 2010 Kwabena W. Agyeman
// See end of file for terms of use.
//
// Update History:
//
// v1.0 - Original release - 4/10/2009.
// v1.1 - Made code faster - 8/18/2009.
// v1.2 - Updated library functions, fixed bugs, and made code more robust against whitespace and capitalization - 7/27/2010.
//
// For each included copy of this object only one spin interpreter should access it at a time.
//
// Nyamekye,
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}}
'' RR20160101   stringConcatenate -> stringAppend
'' RR20160129   rename ASCII0_STREngine.spin -> _StringE.spin (require 8.3 filename for later)

VAR
  byte characterToStringPointer, characterToString[255]

PUB buildString(character) '' 4 Stack longs

'' ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'' // Builds a string from individual characters. Use "builtString" to get the address of the string.
'' //
'' // If the backspace character is put into the string it is automatically evaluated by removing the previous character.
'' //
'' // If 254 characters are put into the string all characters excluding backspace that are put into the string are ignored.
'' //
'' // Character - The next character to include in the string. Null will be ignored.
'' ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  ifnot(characterToStringPointer)
    bytefill(@characterToString, 0, 255)

  if(characterToStringPointer and (character == 8))
    characterToString[--characterToStringPointer] := 0

  elseif(character and (characterToStringPointer <> 254))
    characterToString[characterToStringPointer++] := character

PUB buildStringStr(stri)
  repeat while result := byte[stri++]
    buildString(result) 

PUB builtString(resetString) '' 4 Stack Longs

'' ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'' // Returns the pointer to the string built from individual characters.
'' //
'' // Reset - If true the next call to "buildString" will begin building a new string and the old string will be destroyed.
'' ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  characterToStringPointer &= not(resetString)
  return @characterToString

PUB builderNumber '' 3 Stack Longs

'' ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'' // Returns the number of characters in the string builder buffer.
'' ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  return characterToStringPointer

PUB builderFull '' 3 Stack Longs

'' ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'' // Returns true if the string builder buffer is full and false if not.
'' ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  return (characterToStringPointer == 254)
{{

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                  TERMS OF USE: MIT License
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}}