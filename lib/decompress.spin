''  Simple decompression algorithm
''  As described in http://excamera.com/sphinx/article-compression.html

VAR
  word bitstream_ptr
  byte bitstream_bit

PUB decomp_all(dst,src) : left  |O,L,M,offset,rlen
  bitstream_bit~
  bitstream_ptr := src 

  O := getbits(4)
  L := getbits(4)
  M := getbits(2)
  left := getbits(16)
  repeat left
    if getbit 'back-reference?
      offset := (-getbits(O))-1
      rlen := getbits(L) + M
      repeat rlen
        byte[dst] := byte[dst+offset]
        dst++
    else 'literal
      byte[dst++] := getbits(8)

PUB getbits(n)
  result~
  repeat n
    result <<= 1
    result |= getbit

PUB getbit
  result := (byte[bitstream_ptr]>>bitstream_bit)&1
  if (++bitstream_bit => 8)
    bitstream_bit~
    bitstream_ptr++
{{

┌──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
│                                                   TERMS OF USE: MIT License                                                  │
├──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┤
│Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    │
│files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    │
│modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software│
│is furnished to do so, subject to the following conditions:                                                                   │
│                                                                                                                              │
│The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.│
│                                                                                                                              │
│THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          │
│WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         │
│COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   │
│ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         │
└──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
}}