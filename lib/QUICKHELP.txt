Quick Help
==========

To list all available commands, use
  DIR _*.CMD

To view the help for a certain command, use
  HELP <command>
For example,
  HELP DIR
  
To list all available BIN files, use
  DIR *.BIN
  
To run a bin file, use
  #<filename>
For example,
  #PROPPLAY
  
To shut down, use
  OFF

To view credits, hardware and license information, use
  VER
