{{
  Super-naive SDI RAM Driver.
}}
CON

addr_mask = $00FFFFFF
write_cmd = $02<<24
read_cmd = $03<<24

VAR

byte DOpin, CLKpin, DIpin, CSpin


pub init(DO,CLK,DI,CS)
  DOpin := DO
  CLKpin := CLK
  DIpin := DI
  CSpin := CS
  prepare
  dira[DOpin]~~
  dira[DIpin]~~
  outa[CSpin]~
  writeval($ffff,16) 'reset to SPI mode
  outa[CSpin]~~
  dira[DOpin]~
  outa[CSpin]~
  writeval(%00_00_11_11_11_00_11_11,16) 'set SDI (2-bit) mode (every bit is repeated twice since the ram is now in 1 bit mode)
  outa[CSpin]~~
  dira[DOpin]~~
  outa[CSpin]~
  writeval($0140,16) 'set sequential mode
  end

pub write(buffer,address,length)
  prepare
  dira[DOpin]~~
  dira[DIpin]~~
  outa[CSpin]~
  writeval((address&addr_mask)|write_cmd,32)

  repeat length
    writeval(byte[buffer++],8)

  end
pub read(buffer,address,length)
  prepare
  dira[DOpin]~~
  dira[DIpin]~~
  outa[CSpin]~
  writeval((address&addr_mask)|read_cmd,32)
  dira[DOpin]~
  dira[DIpin]~
  readval(8) ' Dummy byte (only required in SDI and SQI)

  repeat length
    byte[buffer++] := readval(8)

  end

pri readval(bits) | val
  val~ ' clear input
  bits >>= 1
  repeat Bits
    outa[CLKpin]~
    'val := (val << 1) | ina[DOpin]
    'val := (val << 1) | ina[DIpin]
    val := (val << 2) | ((ina>>DIpin)&%11)
    outa[CLKpin]~~ 
  return val

pri writeval(val,bits)
  val <<= (32 - bits) ' pre-align
  bits >>= 1
  repeat bits
    outa[CLKpin]~
    'outa[DOpin] := (val <-= 1) & 1
    'outa[DIpin] := (val <-= 1) & 1
    outa[DOpin..DIpin] := ((val <-= 2))     
    outa[CLKpin]~~ 

pri prepare
   outa[CLKpin]~~
   dira[CLKpin]~~
   outa[CSpin]~~
   dira[CSpin]~~
pri end
   outa[CSpin]~~
   dira[DIpin]~
   dira[DOpin]~
   dira[CLKpin]~
   dira[CSpin]~