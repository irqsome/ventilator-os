{{
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SD-MMC File Allocation Table Engine  (now _SD_FAT.spin & _SD_PASM.spin)
//
// Author: Kwabena W. Agyeman
// Updated: 8/31/2011
// Designed For: P8X32A
// Version: 2.0 - Special
//
// Copyright (c) 2011 Kwabena W. Agyeman
// See end of file for terms of use.
//
// Update History:
//
// v1.0 - Original release - 1/7/2010.
// v1.1 - Updated everything. Made code abort less and caught more errors - 3/10/2010.
// v1.2 - Added support for variable pin assignments and locks - 3/12/2010.
// v1.3 - Improved code and added new features - 5/27/2010.
// v1.4 - Added card detect and write protect pin support - 7/15/2010.
// v1.5 - Improved code and increased write speed - 8/29/2010.
// v1.6 - Implemented file system pathing in all methods - 12/17/2010.
// v1.7 - Broke methods up to be more readable and made file system methods more robust - 1/1/2011.
// v1.8 - Fixed a very minor error with deleting and updated documentation - 3/4/2011.
// v1.9 - Removed all hacks from the block driver and made it faster. The block driver now follows the SD/MMC card protocol
//        exactly. Also, updated file system methods to reflect changes in the block driver - 3/10/2011.
// v2.0 - Upgraded the flush data function to also flush out file meta data when called. Also, made sure that the unmount 
//        function waits for data to be written to disk before returning and added general code enhancements - 8/31/2011.
//
// v2.0 - Special - Changed the driver to have no RTC support. The clock IO error feature has been disabled in the driver also.             
//
// For each included copy of this object only one spin interpreter should access it at a time.
//
// Nyamekye,
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Modifications by Cluso99
// RR20120123   add getSector, getCurrentPosition
// RR20120304   ~line2417 (3 above label rebootCodeFillLoop change #64=64*0.5KB=32KB) _FillHub
// RR20120321   use _hubFree as top of hub ram to clear   
//              do not change the clockfreq or clkset in hub long[$0] and byte[$4] on rebooting
// RR20120323   revise hub layout -> change reboot code
// RR20120619   separate SD-MMC_FATEngine_006.spin into _SD_PASM.spin & _SD_FAT.spin
// RR20130802   problem with running binaries with my OS modifications to save upper hub !!!
//-------------------------------------------------------------------------------------------------
// RR20151216   combine readCSD & readCID
//              repsonce-->response & responce-->response
//              combine command/shutdown & command/response/shutdown SPI into subroutines
// RR20151219   make sectorCommandSPI for readblock & writeblock
//              reorganise routines
// RR20160104   ??? "init" returns cog+1
// RR20160104   mod "B" boot to load all hub RAM, stop all other cogs, execute from hub from cog 0, stop this cog.
// RR20160129   _HUBDEF.spin -> _HUBDEF.spin
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Simple SPI Circuit: (Uses 6 I/O Pins)
//
//                                   3.3V
//                                    |
//                                    R 10KOHM
//                                    |
// Data Out Pin Number          ------------- SD/SDHC/SDXC/MMC data out pin - pin 7 on the card.
//
//                                   3.3V
//                                    |
//                                    R 10KOHM
//                                    |
// Clock Pin Number             ------------- SD/SDHC/SDXC/MMC card clock pin - pin 5 on the card.
//
//                                   3.3V
//                                    |
//                                    R 10KOHM
//                                    |
// Data In Pin Number           ------------- SD/SDHC/SDXC/MMC data in pin - pin 2 on the card.
//
//                                   3.3V
//                                    |
//                                    R 10KOHM
//                                    |
// Chip Select Pin Number       ------------- SD/SDHC/SDXC/MMC chip select pin - pin 1 on the card.
//
//                                   3.3V
//                                    |
//                                    R 10KOHM
//                                    |
// Write Protect Pin Number     ------------- Write Protect Pin - Connected to ground when the card is writable.
//                                                                Floats when the card is not writable.
//                                   3.3V                         The card socket should provide this pin.
//                                    |
//                                    R 10KOHM
//                                    |
// Card Detect Pin Number       ------------- Card Detect Pin - Connected to ground when the card is inserted.
//                                                              Floats when the card is not inserted.
//                                   3.3V                       The card socket should provide this pin.
//                                    |
//                                    R 10KOHM
//                                    |
//                                    ------- SD/SDHC/SDXC data 1 pin - pin 8 on the card.
//
//                                   3.3V
//                                    |
//                                    R 10KOHM
//                                    |
//                                    ------- SD/SDHC/SDXC data 2 pin - pin 9 on the card.
//
//                                   3.3V
//                                    |
//                                    ------- SD/SDHC/SDXC/MMC VDD pin - pin 4 on the card.
//
//                                    ------- SD/SDHC/SDXC/MMC VSS1 pin - pin 3 on the card.
//                                    |
//                                   GND
//
//                                    ------- SD/SDHC/SDXC/MMC VSS2 pin - pin 6 on the card.
//                                    |
//                                   GND
//
// Advanced SPI Circuit: (Uses 4 I/O Pins)
//
//                                   3.3V
//                                    |
//                                    R 10KOHM
//                                    |
// Data Out Pin Number          ------------- SD/SDHC/SDXC/MMC data out pin - pin 7 on the card.
//
//                                   3.3V
//                                    |
//                                    R 10KOHM
//                                    |
//                                    ------- Write Protect Pin - Connected to ground when the card is writable.
//                                    |                           Floats when the card is not writable.
// Write Protect Pin Number     ---   R 10KOHM                    The card socket should provide this pin.
//                                |   |
// Clock Pin Number             ------------- SD/SDHC/SDXC/MMC card clock pin - pin 5 on the card.
//
//                                   3.3V
//                                    |
//                                    R 10KOHM
//                                    |
//                                    ------- Card Detect Pin - Connected to ground when the card is inserted.
//                                    |                         Floats when the card is not inserted.
// Card Detect Pin Number       ---   R 10KOHM                  The card socket should provide this pin.
//                                |   |
// Data In Pin Number           ------------- SD/SDHC/SDXC/MMC data in pin - pin 2 on the card.
//
//                                   3.3V
//                                    |
//                                    R 10KOHM
//                                    |
// Chip Select Pin Number       ------------- SD/SDHC/SDXC/MMC chip select pin - pin 1 on the card.
//
//                                   3.3V
//                                    |
//                                    R 10KOHM
//                                    |
//                                    ------- SD/SDHC/SDXC data 1 pin - pin 8 on the card.
//
//                                   3.3V
//                                    |
//                                    R 10KOHM
//                                    |
//                                    ------- SD/SDHC/SDXC data 2 pin - pin 9 on the card.
//
//                                   3.3V
//                                    |
//                                    ------- SD/SDHC/SDXC/MMC VDD pin - pin 4 on the card.
//
//                                    ------- SD/SDHC/SDXC/MMC VSS1 pin - pin 3 on the card.
//                                    |
//                                   GND
//
//                                    ------- SD/SDHC/SDXC/MMC VSS2 pin - pin 6 on the card.
//                                    |
//                                   GND
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}}

OBJ
  _hub  :       "_HUBDEF"

PUB init(ptrParams) | cardCogID
  return cognew(@initialization, ptrParams) ' ??? + 1
  

  
DAT

' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'                       SDC Driver
' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                        org     0

' ///////////////////// Initialization ////////////////////////////////////////////////////////////////////////////////////////
initialization          mov     counter,               #19                          ' copy parameters from hub
                        movd    :loop,                 #CIDPointer                  ' setup rdlong cog destination
                        mov     buffer,                par                          ' pointer to parameters
:loop                   rdlong  0-0,                   buffer                       ' save the parameters
                        add     buffer,                #4                           ' inc hub
                        add     :loop,                 fiveHundredAndTwelve         ' inc cog
                        djnz    counter,               #:loop                                  
                                                                                        
                        mov     ctra,                  clockCounterSetup            ' Setup counter modules.
                        mov     ctrb,                  dataInCounterSetup           '
                        mov     cardMounted,           #0                           ' Skip to instruction handle.
                        jmp     #instructionWait                                    '

' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'                       Command Center
' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

instructionFlush        call    #flushSPI                                           ' Clean up the SPI bus.
                        call    #shutdownSPI                                        '

instructionHalt         cmp     cardCommand,           #"B" wz                      ' Halt the chip if booting failure.
if_z                    mov     buffer,                #$02                         '
if_z                    clkset  buffer                                              '

instructionError        neg     buffer,                #1                           ' Assert error flag and unmount card.
                        wrbyte  buffer,                errorFlagAddress             '
                        mov     cardMounted,           #0                           '

                        mov     counter,               #16                          ' Setup to clear registers.
                        mov     SPIExtraBuffer,        CIDRegisterAddress           '
                        mov     SPIExtraCounter,       CSDRegisterAddress           '
:loop                   wrbyte  zero,                  SPIExtraBuffer               ' Clear registers.
                        add     SPIExtraBuffer,        #1                           '
                        wrbyte  zero,                  SPIExtraCounter              '
                        add     SPIExtraCounter,       #1                           '
                        djnz    counter,               #:loop                       '

' ///////////////////// Instruction Handle ////////////////////////////////////////////////////////////////////////////////////
instructionLoop         wrbyte  zero,                  commandFlagAddress           ' Clear command.

instructionWait         test    cardDetectPin,         ina wz                       ' Update the CD pin state.
                        muxnz   buffer,                #$FF                         '
                        wrbyte  buffer,                CDFlagAddress                '

                        test    writeProtectPin,       ina wc                       ' Update the WP pin state
                        muxc    buffer,                #$FF                         '
                        wrbyte  buffer,                WPFlagAddress                '

if_nz                   mov     cardMounted,           #0                           ' Check the command.
                        rdbyte  cardCommand,           commandFlagAddress           '
                        tjz     cardCommand,           #instructionWait             '

if_z                    cmp     cardCommand,           #"M" wz                      ' If mounting was requested do it. (& card detected)
if_z                    jmp     #mountCard                                          '

                        cmp     cardCommand,           #"O" wz                      ' If operation was requested do it.
if_z                    jmp     #instructionLoop                                    '

                        cmp     cardMounted,           #0 wz                        ' Check if the card is mounted.
if_z                    jmp     #instructionError                                   '

' ///////////////////// CID compare ///////////////////////////////////////////////////////////////////////////////////////////
                        mov     counter,               #16                          ' Setup to compare CIDs.
                        mov     SPIBuffer,             CIDRegisterAddress           '
                        rdlong  SPICounter,            par                          '
:loop                   rdbyte  SPIExtraBuffer,        SPIBuffer                    ' Compare CIDs.
                        add     SPIBuffer,             #1                           '
                        rdbyte  SPIExtraCounter,       SPICounter                   '
                        add     SPICounter,            #1                           '
                        cmp     SPIExtraBuffer,        SPIExtraCounter wz           '
if_nz                   jmp     #instructionError                                   '
                        djnz    counter,               #:loop                       '

                        cmp     cardCommand,           #"R" wz                      ' If reading was requested do it.
if_z                    jmp     #readBlock                                          '

                        cmp     cardCommand,           #"W" wz                      ' If writing was requested
if_z_and_nc             jmp     #writeBlock                                         '   & not write protected do it.
                        
                        cmp     cardCommand,           #"B" wz                      ' If rebooting was requested do it. 
if_nz                   jmp     #instructionError                                   '                   (& fall thru)
                                                                                                        
' -----------------------------------------------------------------------------------------------------------------------------        
' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'                       Reboot Chip
' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rebootChip              mov     counter,               #8                           ' Setup cog stop loop.
                        cogid   buffer                                              '
                        rdbyte  SPIExtraCounter,       ptrHubOScogs                 '\ don't stop these cogs  RR20120321
                        shl     SPIExtraCounter,       #24                          '|   and shift to b24-31
                        rdlong  saveClockFreq,         #$0                          '| save the clock freq
                        rdbyte  saveClockSet,          #$4                          '/ save the clock setting

' ///////////////////// Stop Cogs /////////////////////////////////////////////////////////////////////////////////////////////
:loop1                  sub     counter,               #1                           ' Stop all cogs but this one.
                        cmp     counter,               buffer wz                    '
                        rcl     SPIExtraCounter,       #1     wc                    ' Dont stop this cog?
''RR20120321 replace next line
''if_nz                 cogstop counter               
if_nz_and_nc            cogstop counter                                             ' Stop some cogs  RR20120321
                        tjnz    counter,               #:loop1                      '

' ///////////////////// Free Locks ////////////////////////////////////////////////////////////////////////////////////////////
                        mov     counter,               #8                           ' Free all locks. (07654321)
:loop2                  lockclr counter                                             '
                        lockret counter                                             '
                        djnz    counter,               #:loop2                      '

' ///////////////////// Setup Memory //////////////////////////////////////////////////////////////////////////////////////////
                        mov     counter,               #64                          ' Setup to grab all sector addresses.
                        rdlong  buffer,                sectorPntrAddress            '
:loop3                  rdlong  cardRebootSectors,     buffer                       ' Get all addresses of the 64 sectors.
                        add     buffer,                #4                           '
                        add     :loop3,                fiveHundredAndTwelve         '
                        djnz    counter,               #:loop3                      '

' ///////////////////// Fill Memory ///////////////////////////////////////////////////////////////////////////////////////////
                        mov     sectorCommandSPI,      #0                           ' Fill these two commands with NOPs.
                        mov     readBlockModify,       #0                           '

''RR20120321            mov     SPIExtraCounter,       #64                          ' Ready to fill all memory. Pointer at 0.
                        rdlong  SPIExtraCounter,       ptrHubFree                   '\  get top of hub clear limit  RR20120321
                        shr     SPIExtraCounter,       #9                           '/  /512 to get no of sectors
                        mov     buffer,                #0                           '

:loop4                  mov     SPIextraBuffer,        cardRebootSectors            ' Reuse read block code. Finish if 0 seen.
                        tjz     SPIextraBuffer,        #:rebootCodeClear            '
                        add     :loop4,                #1                           '
                        call    #readBlock                                          '
                        djnz    SPIExtraCounter,       #:loop4                      '

' ///////////////////// Clear Memory //////////////////////////////////////////////////////////////////////////////////////////
:rebootCodeClear        rdword  counter,               #$8                          ' Setup to clear the rest.
''RR20120321            mov     SPIExtraCounter,       fiveHundredAndTwelve         ' \ $8000
''RR20120321            shl     SPIExtraCounter,       #6                           ' /
                        rdlong  SPIExtraCounter,       ptrHubFree                   '>  get top of hub clear limit  RR20120321
:loop5                  wrbyte  zero,                  counter                      ' Clear the remaining memory.
                        add     counter,               #1                           '
                        cmp     counter,               SPIExtraCounter wz           '
if_nz                   jmp     #:loop5                                             '

' ///////////////////// Setup Stack Markers ///////////////////////////////////////////////////////////////////////////////////
                        rdword  buffer,                #$A                          ' Setup the stack markers.
                        sub     buffer,                #4                           '
                        wrlong  rebootStackMarker,     buffer                       '
                        sub     buffer,                #4                           '
                        wrlong  rebootStackMarker,     buffer                       '

' ///////////////////// Compute Checksum //////////////////////////////////////////////////////////////////////////////////////
                        mov     counter,               #0                           ' Setup to compute the checksum.
:loop6                  sub     SPIExtraCounter,       #1                           ' Compute the RAM checksum.
                        rdbyte  buffer,                SPIExtraCounter              '
                        add     counter,               buffer                       '
                        tjnz    SPIExtraCounter,       #:loop6                      '

' ///////////////////// Verify Checksum & Program Base ////////////////////////////////////////////////////////////////////////
                        and     counter,               #$FF                         ' Crash if checksum not 0.
                        tjnz    counter,               #instructionHalt             '
                        rdword  buffer,                #$6                          ' Crash if program base invalid.
                        cmp     buffer,                #$10 wz                      '
if_nz                   jmp     #instructionHalt                                    '

' ///////////////////// Boot Interpreter //////////////////////////////////////////////////////////////////////////////////////
                        jmp     #noClockChange                                      ' Dont change the clock  RR20120321
                        rdbyte  buffer,                #$4                          ' Switch clock mode for PLL stabilization.
                        and     buffer,                #$F8                         '
                        clkset  buffer                                              '

:delay                  djnz    twentyMilliseconds,    #:delay                      ' Allow PLL to stabilize.
                        rdbyte  buffer,                #$4                          ' Switch to new clock mode.
                        clkset  buffer                                              '
                        jmp     #bootSpin                                           '\ RR20120321
                                                                                    '|
noClockChange           wrlong  saveClockFreq,         #$0                          '| restore clock freq    RR20120321
                        wrbyte  saveClockSet,          #$4                          '/ restore clock setting
                        
bootSpin                coginit rebootInterpreter                                   ' Restart running new code.
                        cogid   buffer                                              ' Shutdown this cog.
                        cogstop buffer                                              '
' -----------------------------------------------------------------------------------------------------------------------------        

' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'                       Mount Card
' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

mountCard               mov     SPITiming,             #0                           ' Setup SPI parameters.
                        call    #flushSPI                                           '

' ///////////////////// Go Idle State /////////////////////////////////////////////////////////////////////////////////////////
                        mov     SPITimeout,            cnt                          ' Setup to try for 1 second.
:loop1                  movs    commandSPIIndex,       #($40 | 0)                   ' Send out command 0.
                        mov     SPIextraBuffer,        #0                           '
                        movs    commandSPICRC,         #$95                         '
                        call    #commandShutSPI                                     ' commandSPI & shutdownSPI                
                        cmp     SPIextraBuffer,        #1 wz                        ' Check time.
if_nz                   call    #timeoutSPI                                         '
if_nz                   jmp     #:loop1                                             '

' ///////////////////// Send Interface Condition //////////////////////////////////////////////////////////////////////////////
                        movs    commandSPIIndex,       #($40 | 8)                   ' Send out command 8.
                        mov     SPIextraBuffer,        #$1_AA                       '
                        movs    commandSPICRC,         #$87                         '
                        call    #commandLongShutSPI                                 ' commandSPI & longSPI & shutdownSPI
                        test    SPIextraBuffer,        #$4 wz                       ' If failure goto SD 1.X initialization.
if_nz                   jmp     #exitIdleState_SD                                   '

                        and     SPIresponse,           #$1_FF                       ' SD 2.0/3.0 initialization.
                        cmp     SPIresponse,           #$1_AA wz                    '
if_nz                   jmp     #instructionError                                   '

' ///////////////////// Send Operating Condition //////////////////////////////////////////////////////////////////////////////
exitIdleState_SD        movs    commandSPICRC,         #$FF                         ' Setup to try for 1 second.
                        mov     SPITimeout,            cnt                          '

:loop2                  movs    commandSPIIndex,       #($40 | 55)                  ' Send out command 55.
                        mov     SPIextraBuffer,        #0                           '
                        call    #commandShutSPI                                     ' commandSPI & shutdownSPI
                        test    SPIextraBuffer,        #$4 wz                       ' If failure goto MMC initialization.                                 '
if_nz                   jmp     #exitIdleState_MMC                                  '

                        movs    commandSPIIndex,       #($40 | 41)                  ' Send out command 41 with HCS bit set.
                        mov     SPIextraBuffer,        HCSBitMask                   '
                        call    #commandShutSPI                                     ' commandSPI & shutdownSPI                
                        cmp     SPIextraBuffer,        #0 wz                        ' Check time.
if_nz                   call    #timeoutSPI                                         '
if_nz                   jmp     #:loop2                                             '
                        rdlong  buffer,                sectorPntrAddress            ' It's an SDC card.
                        wrlong  itsAnSDCard,           buffer                       '
                        jmp     #readOCR                                            '

' ///////////////////// Send Operating Condition //////////////////////////////////////////////////////////////////////////////
exitIdleState_MMC       mov     SPITimeout,            cnt                          ' Setup to try for 1 second.
:loop3                  movs    commandSPIIndex,       #($40 | 1)                   ' Send out command 1.
                        mov     SPIextraBuffer,        HCSBitMask                   '
                        call    #commandShutSPI                                     ' commandSPI & shutdownSPI                
                        cmp     SPIextraBuffer,        #0 wz                        ' Check time.
if_nz                   call    #timeoutSPI                                         '
if_nz                   jmp     #:loop3                                             '
                        rdlong  buffer,                sectorPntrAddress            ' It's an MMC card.
                        wrlong  itsAnMMCard,           buffer                       '

' ///////////////////// Read OCR Register /////////////////////////////////////////////////////////////////////////////////////
readOCR                 movs    commandSPIIndex,       #($40 | 58)                  ' Ask the card for its OCR register.
                        mov     SPIextraBuffer,        #0                           '
                        call    #commandLongShutSPI                                 ' commandSPI & LongSPI & shutdownSPI                
                        tjnz    SPIextraBuffer,        #instructionError            ' If failure abort.
                        test    SPIresponse,           OCRCheckMask wz              ' If voltage not supported abort.
                        shl     SPIresponse,           #1 wc                        '
if_z_or_nc              jmp     #instructionError                                   '
                        shl     SPIresponse,           #1 wc                        ' SDHC/SDXC supported or not.
if_c                    mov     SPIShift,              #0                           '
if_nc                   mov     SPIShift,              #9                           '

' ///////////////////// Read CSD Register /////////////////////////////////////////////////////////////////////////////////////
                        movs    commandSPIIndex,       #($40 | 9)                   ' Ask the card for its CSD register.
                        mov     buffer2,               CSDRegisterAddress           '
                        call    #readCSDCID

' ///////////////////// Read CID Register /////////////////////////////////////////////////////////////////////////////////////
                        movs    commandSPIIndex,       #($40 | 10)                  ' Ask the card for its CID register.
                        mov     buffer2,               CIDRegisterAddress           '
                        call    #readCSDCID

' ///////////////////// Set Block Length //////////////////////////////////////////////////////////////////////////////////////
                        movs    commandSPIIndex,       #($40 | 16)                  ' Send out command 16.
                        mov     SPIextraBuffer,        fiveHundredAndTwelve         '
                        call    #commandShutSPI                                     ' commandSPI & shutdownSPI                
                        tjnz    SPIextraBuffer,        #instructionError            ' If failure abort.
                        neg     SPITiming,             #1                           ' Setup SPI parameters.

' ///////////////////// Setup Card Variables //////////////////////////////////////////////////////////////////////////////////
                        neg     cardMounted,           #1                           ' Return.
                        jmp     #instructionLoop                                    '

' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'                       Read CSD/CID Register                        
' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

readCSDCID              mov     SPIextraBuffer,        #0 
                        call    #commandSPI                                         '
                        tjnz    SPIextraBuffer,        #instructionFlush            ' If failure abort.
                        call    #responseSPI                                        '
                        cmp     SPIextraBuffer,        #$FE wz                      '
if_nz                   jmp     #instructionFlush                                   '

                        mov     counter,               #16                          ' Setup to read the CSD/CID register.
:loop                   call    #readSPI                                            ' Read the CSD/CID register in.
                        wrbyte  SPIBuffer,             buffer2                      '
                        add     buffer2,               #1                           '
                        djnz    counter,               #:loop                       '
                        call    #wordSPI                                            ' Shutdown SPI clock.
                        call    #shutdownSPI                                        '
readCSDCID_ret          ret
' -----------------------------------------------------------------------------------------------------------------------------        

' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'                       Write Block
' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

writeBlock              movs    commandSPIIndex,       #($40 | 24)                  ' write block command
                        call    #sectorCommandSPI                                   '
                        tjnz    SPIextraBuffer,        #instructionFlush            ' If failure abort.
                        call    #readSPI                                            ' Send dummy byte.

                        mov     phsb,                  #$FE                         ' Send start of data token.
                        call    #writeSPI                                           '
                        mov     counter,               fiveHundredAndTwelve         ' Setup loop.
                        rdlong  buffer,                blockPntrAddress             '
:loop1                  rdbyte  phsb,                  buffer                       ' Write data out from memory.
                        add     buffer,                #1                           '
                        call    #writeSPI                                           '
                        djnz    counter,               #:loop1                      '
                        call    #wordSPI                                            ' Write out the bogus 16 bit CRC.

                        call    #responseSPI                                        ' If failure abort.
                        and     SPIextraBuffer,        #$1F                         '
                        cmp     SPIextraBuffer,        #$5 wz                       '
if_nz                   jmp     #instructionFlush                                   '

                        wrbyte  zero,                  commandFlagAddress           ' Clear instruction.
                        mov     counter,               cnt                          ' Setup loop.
:loop2                  call    #readSPI                                            ' Wait until the card is not busy.
                        cmp     SPIBuffer,             #0 wz                        '
if_z                    mov     SPICounter,            cnt                          '
if_z                    sub     SPICounter,            counter                      '
if_z                    cmpsub  writeTimeout,          SPICounter wc, nr            '
if_z_and_c              jmp     #:loop2                                             '

if_z                    mov     cardMounted,           #0                           ' Unmount card on failure.
                        call    #shutdownSPI                                        ' Shutdown SPI clock.
                        jmp     #instructionWait                                    ' Return. (instruction already cleared)

' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'                       Read Block
' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

readBlock               movs    commandSPIIndex,       #($40 | 17)                  ' read block command
                        call    #sectorCommandSPI                                   '
                        tjnz    SPIextraBuffer,        #instructionFlush            ' If failure abort.

                        mov     counter,               cnt                          ' Setup loop.
:loop1                  call    #readSPI                                            ' Wait until the card sends the data.
                        cmp     SPIBuffer,             #$FF wz                      '
if_z                    mov     SPICounter,            cnt                          '
if_z                    sub     SPICounter,            counter                      '
if_z                    cmpsub  readTimeout,           SPICounter wc, nr            '
if_z_and_c              jmp     #:loop1                                             '

                        cmp     SPIBuffer,             #$FE wz                      ' If failure abort.
if_nz                   jmp     #instructionFlush                                   '

                        mov     counter,               fiveHundredAndTwelve         ' Setup loop.
readBlockModify         rdlong  buffer,                blockPntrAddress             '                   (reboot sets to nop)
:loop2                  call    #readSPI                                            ' Read data into memory.
                        wrbyte  SPIBuffer,             buffer                       '
                        add     buffer,                #1                           '
                        djnz    counter,               #:loop2                      '

                        call    #wordSPI                                            ' Shutdown SPI clock.
                        call    #shutdownSPI                                        '
readBlock_ret           jmp     #instructionLoop                                    ' Return. Becomes RET when rebooting.

' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'                       Sector Command SPI (write/read Block)
' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

sectorCommandSPI        rdlong  SPIextraBuffer,        sectorPntrAddress            ' sector address    (reboot sets to nop)
                        shl     SPIextraBuffer,        SPIShift                     ' sector << 0/9     (& fall thru)
                        
' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'                       Command SPI
' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

commandSPI              or      dira,                  dataInPin                    ' Untristate the I/O lines.
                        or      dira,                  clockPin                     '
                        or      dira,                  chipSelectPin                ' Activate the SPI bus.
                        call    #readSPI                                            '
commandSPIIndex         mov     phsb,                  #$FF                         ' Send out command. (overwritten by movs)
                        call    #writeSPI                                           '
                        movs    writeSPI,              #32                          ' Send out parameter.
                        mov     phsb,                  SPIextraBuffer               '
                        call    #writeSPI                                           '
                        movs    writeSPI,              #8                           '
commandSPICRC           mov     phsb,                  #$FF                         ' Send out CRC token. (overwritten by movs)
                        call    #writeSPI                                           '
                        call    #responseSPI                                        ' Read in response.
sectorCommandSPI_ret
commandSPI_ret          ret                                                         ' Return.

' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'                       response SPI
' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

responseSPI             mov     SPIextraBuffer,         #9                          ' Setup response poll counter.
:loop                   call    #readSPI                                            ' Poll for response.
                        cmpsub  SPIBuffer,              #$FF wc, nr                 '
if_c                    djnz    SPIextraBuffer,         #:loop                      '
                        mov     SPIextraBuffer,         SPIBuffer                   ' Move response into return value.
responseSPI_ret         ret                                                         ' Return.

' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'                       Result SPI
' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

longSPI                 add     readSPI,               #16                          ' Read in 32, 16, or 8 bits.
wordSPI                 add     readSPI,               #8                           '
byteSPI                 call    #readSPI                                            '
                        movs    readSPI,               #8                           '
                        mov     SPIresponse,           SPIBuffer                    ' Move long into return value.
byteSPI_ret                                                                         ' Return.
wordSPI_ret                                                                         '
longSPI_ret             ret                                                         '

' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'                       commandSPI/longSPI/shutdownSPI & commandSPI/shutdownSPI
' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

commandLongShutSPI      call    #commandSPI                                         '
                        call    #longSPI                                            ' read response long
                        jmp     #shutdownSPI
commandShutSPI          call    #commandSPI                                         '                   (& fall thru)                        

' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'                       Shutdown SPI
' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

shutdownSPI             call    #readSPI                                            ' Shutdown the SPI bus.
                        andn    dira,                  chipSelectPin                '
                        call    #readSPI                                            '
                        andn    dira,                  dataInPin                    ' Tristate the I/O lines.
                        andn    dira,                  clockPin                     '
commandLongShutSPI_ret
commandShutSPI_ret
shutdownSPI_ret         ret                                                         ' Return.

' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'                       Flush SPI
' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

flushSPI                or      dira,                  dataInPin                    ' Untristate the I/O lines.
                        or      dira,                  clockPin                     '
                        mov     SPIExtraCounter,       #74                          ' Send out more than 74 clocks. (74x8?)
:loop                   call    #readSPI                                            '
                        djnz    SPIExtraCounter,       #:loop                       '
flushSPI_ret            ret                                                         ' Return.

' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'                       Read SPI
' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

readSPI                 mov     SPICounter,            #8                           ' Setup counter to read in 1 - 32 bits.
                        mov     SPIBuffer,             #0 wc                        '
readSPIAgain            mov     phsa,                  #0                           ' Start clock low.
                        tjnz    SPITiming,             #readSPISpeed                '

' ///////////////////// Slow Reading //////////////////////////////////////////////////////////////////////////////////////////
                        movi    frqa,                  #%0000_0001_0                ' Start the clock - read 1 .. 32 bits.
:loop                   waitpne clockPin,              clockPin                     ' Get bit.
                        rcl     SPIBuffer,             #1                           '
                        waitpeq clockPin,              clockPin                     '
                        test    dataOutPin,            ina wc                       '
                        djnz    SPICounter,            #:loop                       ' Loop until done.
                        jmp     #readSPIFinish                                      '

' ///////////////////// Fast Reading //////////////////////////////////////////////////////////////////////////////////////////
readSPISpeed            movi    frqa,                  #%0010_0000_0                ' Start the clock - read 8 bits.
                        test    dataOutPin,            ina wc                       ' Read in data.
                        rcl     SPIBuffer,             #1                           '
                        test    dataOutPin,            ina wc                       '
                        rcl     SPIBuffer,             #1                           '
                        test    dataOutPin,            ina wc                       '
                        rcl     SPIBuffer,             #1                           '
                        test    dataOutPin,            ina wc                       '
                        rcl     SPIBuffer,             #1                           '
                        test    dataOutPin,            ina wc                       '
                        rcl     SPIBuffer,             #1                           '
                        test    dataOutPin,            ina wc                       '
                        rcl     SPIBuffer,             #1                           '
                        test    dataOutPin,            ina wc                       '
                        rcl     SPIBuffer,             #1                           '
                        test    dataOutPin,            ina wc                       '

' ///////////////////// Finish Up /////////////////////////////////////////////////////////////////////////////////////////////
readSPIFinish           mov     frqa,                  #0                           ' Stop the clock.
                        rcl     SPIBuffer,             #1                           '
                        cmpsub  SPICounter,            #8                           ' Read in any remaining bits.
                        tjnz    SPICounter,            #readSPIAgain                '
readSPI_ret             ret                                                         ' Return. Leaves the clock high.

' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'                       Write SPI
' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

writeSPI                mov     SPICounter,            #8                           ' Setup counter to write out 1 - 32 bits.
                        ror     phsb,                  SPICounter                   '
writeSPIAgain           mov     phsa,                  #0                           ' Start clock low.
                        tjnz    SPITiming,             #writeSPISpeed               '

' ///////////////////// Slow Writing /////////////////////////////////////////////////////////////////////////////////////////
                        movi    frqa,                  #%0000_0001_0                ' Start the clock - write 1 .. 32 bits.
:loop                   waitpeq clockPin,              clockPin                     ' Set bit.
                        waitpne clockPin,              clockPin                     '
                        shl     phsb,                  #1                           '
                        djnz    SPICounter,            #:loop                       ' Loop until done.
                        jmp     #writeSPIFinish                                     '

' ///////////////////// Fast Writing /////////////////////////////////////////////////////////////////////////////////////////
writeSPISpeed           movi    frqa,                  #%0100_0000_0                ' Write out data.
                        shl     phsb,                  #1                           '
                        shl     phsb,                  #1                           '
                        shl     phsb,                  #1                           '
                        shl     phsb,                  #1                           '
                        shl     phsb,                  #1                           '
                        shl     phsb,                  #1                           '
                        shl     phsb,                  #1                           '

' ///////////////////// Finish Up /////////////////////////////////////////////////////////////////////////////////////////////
writeSPIFinish          mov     frqa,                  #0                           ' Stop the clock.
                        cmpsub  SPICounter,            #8                           ' Write out any remaining bits.
                        shl     phsb,                  #1                           '
                        tjnz    SPICounter,            #writeSPIAgain               '
                        neg     phsb,                  #1                           '
writeSPI_ret            ret                                                         ' Return. Leaves the clock low.
' -----------------------------------------------------------------------------------------------------------------------------        

' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'                       Timeout SPI
' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

timeoutSPI              mov     SPICounter,            cnt                          ' Check if a second has passed.
                        sub     SPICounter,            SPITimeout                   '
                        rdlong  SPIBuffer,             #0                           ' Get clkfreq from hub $0000
                        cmpsub  SPIBuffer,             SPICounter wc, nr            '
if_nc                   jmp     #instructionError                                   '
timeoutSPI_ret          ret                                                         ' Return.
' -----------------------------------------------------------------------------------------------------------------------------        

' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'                       Data
' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
ptrHubFree              long    _hub#_HubFree                                       ' ptr to _HubFree location
ptrHubOScogs            long    _hub#_OS_Cogs                                       ' os config: cogs stay resident
saveClockFreq           long    0
saveClockSet            long    0

zero                                                                                '\ Constant byte 0
fiveHundredAndTwelve    long    $2_00                                               '/ Constant 512.
twentyMilliseconds      long    (((20 * (20_000_000 / 1_000)) / 4) / 1)             ' Constant 100,000.

itsAnSDCard             long    $00_43_44_53                                        ' Card type SD token.
itsAnMMCard             long    $00_43_4D_4D                                        ' Card type MMC token.

OCRCheckMask            long    %00_000000_11111111_00000000_00000000               ' Parameter check mask for OCR bits.
HCSBitMask              long    %01_000000_00000000_00000000_00000000               ' Parameter bit mask for HCS bit.

rebootInterpreter       long    (($00_01 << 18) | ($3C_01 << 4) | ($00_00 << 0))    ' Spin interpreter text boot information.
' ^ is the same as...   long       $0004 << 16  |   $F004 << 2  |  %0000
rebootStackMarker       long    $FF_F9_FF_FF                                        ' Spin interpreter stack boot information.


' ======================PARAMETER BLOCK (19 longs) passed to pasm driver at cognew=============================================

CIDPointer              long    0               ' Pointer to the SD/MMC CID register copy to compare.

' //////////////////////Configuration Settings/////////////////////////////////////////////////////////////////////////////////

readTimeout             long    0                                                   ' 100 millisecond timeout.
writeTimeout            long    0                                                   ' 500 millisecond timeout.
clockCounterSetup       long    0                                                   ' Clock control.
dataInCounterSetup      long    0                                                   ' Data in control.

' //////////////////////Pin Masks//////////////////////////////////////////////////////////////////////////////////////////////

dataOutPin              long    0
clockPin                long    0
dataInPin               long    0
chipSelectPin           long    0
writeProtectPin         long    0
cardDetectPin           long    0

' //////////////////////Addresses//////////////////////////////////////////////////////////////////////////////////////////////

blockPntrAddress        long    0
sectorPntrAddress       long    0
WPFlagAddress           long    0
CDFlagAddress           long    0
commandFlagAddress      long    0
errorFlagAddress        long    0
CSDRegisterAddress      long    0
CIDRegisterAddress      long    0
' ====================== end of PARAMETER BLOCK================================================================================

' //////////////////////Run Time Variables/////////////////////////////////////////////////////////////////////////////////////

buffer                  res     1
buffer2                 res     1
counter                 res     1

' //////////////////////Card Variables/////////////////////////////////////////////////////////////////////////////////////////

cardCommand             res     1
cardMounted             res     1

cardRebootSectors       res     64

' //////////////////////SPI Variables//////////////////////////////////////////////////////////////////////////////////////////

SPIShift                res     1
SPITiming               res     1
SPITimeout              res     1
SPIresponse             res     1
SPIBuffer               res     1
SPICounter              res     1
SPIExtraBuffer          res     1
SPIExtraCounter         res     1

' /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                        fit     496


{{

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                  TERMS OF USE: MIT License
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}}