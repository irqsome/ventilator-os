'' +--------------------------------------------------------------------------+
'' | ......OS   An Operating System for the Propeller Chip              v1.00 |
'' +--------------------------------------------------------------------------+
'' |                     HUB DEFINITIONS                  _HUBDEF.SPIN        |
'' +--------------------------------------------------------------------------+
'' |  License            MIT License                                          |
'' +--------------------------------------------------------------------------+
'' RR20120304   Proposal of hub layout
'' RR20120306   add _SDPins, _SIOPins, _SIOBaud
''              add _toggleLF, _hardware
'' RR20120310   add _restartCogs  (could combine as b24-31 in _HubFree???) 
'' RR20120321
'' RR20120323   revised hub layout
'' RR20120325   revised hub layout and Date/Time
'' RR20120619   add SDparam & SDcommand
'' RR20151210   add P8XBlade2 definition
'' RR20151220   add CpuBlade7 definition
'' RR20160129   _HUBDEF3 -> _HUBDEF
'' RR20160130   v0.95
'' +--------------------------------------------------------------------------+
'' RR20160208   v1.00  _SDPASMR (stay-resident cog SD PASM Driver for _OS v1.00)
'' RR20160212   v0.96  use v1.00 hub layout
'' RR20160219   v1.00
'' RR20160221          docn: _SIOpins: Byte 3=cog, 2=mode, 1=SI, 0=SO
'' RR20160229          lex/codegen/link fat32 vers

'' WARNING: If anything moves, all modules will require re-compiling !!!

CON

' OS Hub Definitions...         
' ------------------       bytes
  _HubFree      = $7FFC  '    4     ' \ stores total hub available (typ $7800)
                                    ' |  (hub is allocated to the OS above this value)
                                    ' /  (eg $7000 means $0000-$6FFF is available)

  _OS_DateTime  = $7FF8  '    4     'Year20xx  Month    Date    Hours   Minutes   Seconds
                                    ' (00-63)  (1-12)  (1-31)  (00-23)  (00-59)   (00-60)                                     
                                    '  000000___0000____00000___00000____000000____000000
                                    'To convert to FAT16/32 format, shift >>1 then add 16<<25
                                    '   --> Y(7)M(4)D(5)H(5)M(6)S*2(5bits) Base=1980
                                    '  (FYI seconds in 4yrs = 126,230,400)
                                    ' Note that the seconds field may be 60 due to leap seconds
                                                                                                   
  _OS_Rows      = $7FF7  '    1     ' \ combined <lf> and rows
    _LF_MASK      = $80             ' | b7  : 1= <lf> ON; 0= strip <lf>
    _ROW_MASK     = $7F             ' / b0-6: no of rows on screen (0-127)
  _OS_Columns   = $7FF6  '    1     '         no of cols on screen (0-255)
  _OS_Cogs      = $7FF5  '    1     ' stay resident cogs: 1= don't stop on reboot
  _OS_Clkmode   = $7FF4  '    1     ' clkmode: saved fm hub byte $0004

  _OS_Clkfreq   = $7FF0  '    4     ' clkfreq(Hz): saved fm hub long $0000

  
  _SDpins       = $7FEC  '    4     ' \ sd pins packed 4 bytes
                         '          ' / Byte 3=/CS, 2=DI, 1=CLK, 0=DO
  _SIOpins      = $7FE8  '    4     ' \ serial pins and mode settings (and cog#)
                         '          ' / Byte 3=cog, 2=mode, 1=SI, 0=SO
  _SIObaud      = $7FE4  '    4     ' serial baud (speed typ 115,200)

  _Hardware     = $7FE0  '    4     ' \ hardware: hi-word=company, lo-word=config
                                    ' |            $0001 = Cluso99  $0001 = RamBlade 1           
                                    ' |            $0001 = Cluso99  $0002 = TriBlade#2           
                                    ' |            $0001 = Cluso99  $0003 = RamBlade3
                                    ' |            $0001 = Cluso99  $0004 = P8XBlade2
                                    ' |            $0001 = Cluso99  $0007 = CpuBlade7
                                    ' /            $0ADA = IRQsome Software $0001 = Ventilator
                                    ' ^^^ may be more valuable for something else??

  _AuxIn        = $7FDC  '    4     ' auxilary input  rendezvous
  _AuxOut       = $7FD8  '    4     ' auxilary output rendezvous
  _StdIn        = $7FD4  '    4     ' standard input  rendezvous
  _StdOut       = $7FD0  '    4     ' standard output rendezvous

  _CIDRegister  = $7FC0  '   16     ' \ CID (card identification register)
  _CSDRegister  = $7FB0 '    16     ' | CSD (card specification data register)
  _CIDcopyAddr  = $7FAC  '    4     ' | address of CIDcopy
  _SDblockAddr  = $7FA8  '    4     ' | address of Block
  _SDsectorAddr = $7FA4  '    4     ' | address of Sector
  _SDflags      = $7FA3 '     1     ' | (WPflag)<<4 | (NoCard) 0==OK  (for later!!!
  _SDlockcogid  = $7FA2  '    1     ' | (LockID+1)<<4 | (CogID+1)
  _SDerror      = $7FA1  '    1     ' | error   (returned from pasm driver)
  _SDcommand    = $7FA0  '    1     ' / command (set by app, cleared by pasm driver)

  _OSreserved   = $7F90  '   16     ' undefined

  _pBuffer_C    = $7F80  '   16  '| ' \ user buffers... 
  _pBuffer_B    = $7F40  '   64  '| ' |   (maybe serial in and serial out buffers?)
  _pBuffer_A    = $7F00  '   64  '| ' /   (used to pass parameters between modules)
  _pBuffer      = $7F00  '  144  '^ ' / ...joins all 3 buffers A+B+C

  _screenBuffer = $7730  '  2000 - holds 40*25 screen - can be used by the stdout driver for any purpose
  _TVCog        = $772F ' 1 - TV cog + 1 (0 if TV isn't running)
  _KeyCog       = $772E ' 1 - Keyboard cog + 1 (0 if Keyboard isn't running)
  _ramPin       = $772D ' 123LC1024 CS pin (or 255 if not present)
  
  _moreStuff = $7720

{{
'<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
'required for "compile" using old sdspiq
  _SDSPI        = $7F00       - 3 * 4    '3 rendezvous variables \ fsrw routines
  _SXFS2        = _SDSPI      - 4 * 4    '4 rendezvous variables |
  _SXFS         = _SXFS2      - 4 * 4    '4 rendezvous variables |
  _SDbuffer     = _SXFS       - 512      'buffer                 /
'Currently ALL the following is position dependant (for older compatibility)
  SDSPIRENDEZVOUS   = _SDSPI             '3 rendezvous variables \ fsrw routines
  SXFS2RENDEZVOUS   = _SXFS2             '4 rendezvous variables |
  SXFSRENDEZVOUS    = _SXFS              '4 rendezvous variables |
  METADATABUFFER    = _SDbuffer          'buffer                 /

  _HUB_RESERVED = _SDbuffer         ' $7?00-$7FFF currently reserved by the OS
'<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
}}

{{------------------------------------------------------------------------------
'' RR20160208 removed until required...                         

  _pCogTables   = $7E00  '  256     ' \ 8*32B cog usage tables
                                    ' | TBD
                                    ' |   Current thinking is...
                                    ' |     1 byte for "type"
                                    ' |     1 long for sector address to retrieve filename
                                    ' |   However, could also be used as 2*16byte buffers
                                    ' /     with the head+tail(s) kept in _StdIn & _StdOut

  _pSectorBuf   = $7C00  '  512     ' SD card i/o buffer                 
  _pSpinVector  = $7800  ' 1024     ' vector table for Cluso's faster spin interpreter
                         
  _HUB_RESERVED = $7800             ' $7800-$7FFF currently reserved by the OS
  _HUB_RAMSIZE  = $8000             ' Total hub ram 

  _pSectorBuf   = $7C00  '  512     ' SD card i/o buffer                 
  _pSpinVector  = $7800  ' 1024     ' vector table for Cluso's faster spin interpreter
------------------------------------------------------------------------------}}                         

  _HUB_RESERVED = _moreStuff        ' $7F00-$7FFF currently reserved by the OS
  _HUB_RAMSIZE  = $8000             ' Total hub ram 
  
PUB dummy