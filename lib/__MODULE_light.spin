
{==============================================================================}
{
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// General Purpose OS Command Module "_MODULE_light.spin"
   ... like "__MODULE.spin", but with less memory usage 
}

CON
  _clkfreq = 40_000_000         '\ just a default to get going (DO NOT CHANGE)
  _clkmode = xtal1 + pll8x      '/  (to modify, change in _HWDEF.spin)

  _FREE = (_hub#_HUB_RAMSIZE - _hub#_HUB_RESERVED)/4

  __CMDSIZE = 64 'More isn't actually needed

OBJ
  _hw   : "_HWDEF"                      ' hardware pin definitions etc
  _hub  : "_HUBDEF"                     ' fixed hub definitions ($7FFF... down)
  _c    : "_CHARDEF"                    ' ascii char definitions

  fat   : "_SD_FAT2"        
  'taf   : "_SD_FAT2"        
  'str   : "StringE"                     ' (renamed ASCII0_STREngine)
  'Input : "_STDIN"                     ' use std input  driver (i/o driver loaded by _OS.CMD)
  Print : "_STDOUT"                     ' use std output driver (i/o driver loaded by _OS.CMD)

VAR
   long __status
   byte CommandLine[__CMDSIZE] ' complete command line as typed in

   
PUB start | z,nextCmd, errorString
{------------------------------------------------------------------------------}
'StdIO is already running. _StdIn & _StdOut do not require starting.
{------------------------------------------------------------------------------}
'Start the SD driver (later some of this will also be resident)
  z  :=  long[_hub#_SDpins]
  {z0 := z        & $1F
  z1 := (z >>  8) & $1F
  z2 := (z >> 16) & $1F
  z3 := (z >> 24) & $1F}

'                          (DO, CLK, DI, CS, _SD_WP, _SD_CD, _RTC_DAT, _RTC_CLK, -1)
  ifnot( fat.FATEngineStart(z&$1F,(z>>8)&$1F,(z>>16)&$1F,(z>>24)&$1F, -1, -1, -1, -1, -1))  
    reboot

  fat.mountPartition(0)                                 ' mount the sd card
  'taf.mountPartition(0)                                 ' mount the sd card

  ifnot(fat.partitionMounted)
    Print.Str(string("*ERROR: Card Not Mounted, stop!"))
    repeat                                              ' LOOP HERE!!!
  {ifnot(taf.partitionMounted)
    Print.Str(string("*ERROR: Card Not Mounted, stop!"))
    repeat}                                              ' LOOP HERE!!!
{------------------------------------------------------------------------------}
'Execute the command
  str.stringCopy(@commandline, _hub#_pBuffer_A,__CMDSIZE) ' retrieve commandline
  \str.tokenizeString(@commandline)
  \__execWrapper                                        ' execute
  if __status == -2 'display help?
    nextCmd := string("_HELP.CMD")
    z :=  _hub#_pBuffer_A
    bytemove(z+2,z,constant(__CMDSIZE-3))
    byte[z++] := "_"
    byte[z] := " "
  else
    nextCmd := string("_CMD_.CMD")
{------------------------------------------------------------------------------}
'Load/Run "_CMD.CMD"...
  waitcnt(cnt + clkfreq / 10)                           ' delay
  errorString := \fat.bootPartition(nextCmd) ' load/run
  checkError(nextCmd, fat.partitionError, errorString) ' if error, display
{------------------------------------------------------------------------------}
'Only here on error
  Print.StrCR(string("*ERROR: Rebooting..."))
  waitcnt(cnt + clkfreq * 2)                            ' delay
  reboot
{------------------------------------------------------------------------------}

PRI __execWrapper
  __status := executeCommand(@commandline)

PRI checkError(errorMessage, errorNumber, errorString)
  if errorNumber == 0
    return 0
  else
    Print.Str(string("*ERROR: on file "))
    Print.Str(errorMessage)
    Print.Str(String(" - "))
    Print.Hex(errorNumber,2)
    Print.Char(":")
    Print.StrCR(errorString)
    'waitcnt(cnt + clkfreq * 2)                         ' delay
    abort errorNumber

{==============================================================================}
CON
    