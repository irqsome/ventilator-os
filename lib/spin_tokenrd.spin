'' +--------------------------------------------------------------------------+
'' | Sphinx:  A Compiler for the Propeller Chip     LEX / CODEGEN / LINK.SPIN |
'' |                                  sub-module:   _TOKENRD.SPIN             |
'' +--------------------------------------------------------------------------+
'' |  Authors:                                                                |
'' |  (c) 2009,2010      Michael Park (mpark)       (original Sphinx compiler)|
'' |  (c) 2015,2016      Ray Rodrick (Cluso99)      (include in PropOS)       |
'' |  License            MIT License - See end of file for terms of use       |
'' +--------------------------------------------------------------------------+
'' mp2009
'' RR20151231      mod for PropOS
'' RR20160101      continued
'' RR20160105      fix endian reversal readWord & readLong
'' RR20160125      add fs.mountPartition(0), no file error handling!!!
'' RR20160127      rename _tokenrd.spin
'' RR20160212
'' RR20160213      use _SD_FAT2 (reduced support = smaller)
'' RR20160216      is this correct in Advance: fs.readData( @tokenValue, 4) ???
'' RR20160218      _SD_FAT readData returns 0 for EOF whereas isxfs returns -1 for EOF

'' RR20160222        BUG!!! isxfs/sxfs readbyte aborts on EOF=-1 but returns on <-1 !!!
''         23      combine _tokenrd (V101b & \Compile) - both filesystems
''         25      use c3.bat
''         26      continue debugging
'' RR20160228      works;


obj
  kw            : "spin_kwdefs"
  bt            : "spin_bintree"
{{ #ifdef dbgtok
  Print         : "_StdOut"
}} '' #endif
''#ifdef fat32
  fs            : "_SD_FAT2"
'' #endif
{{ #ifdef fat16
  sxfs          : "isxfs"              
}} '' #endif
   
dat
              long
{{ #ifdef fat16
filestuff     byte      0[sxfs#SIZEOFFILESTUFF]
}} '' #endif

pub Open( pFilename ) | errorString

''#ifdef fat32
  fs.mountPartition(0)                                                          ' mount the sd card
  errorString := \fs.openFile(pFilename, "R")
  ' checkError(pFilename, fs.partitionError, errorString)                       ' if error, display & abort
  if fs.partitionError <> 0
    abort errorString
'' #endif
{{ #ifdef fat16
  if \sxfs.Open( @filestuff, pFilename, "R" ) <> 0
    abort string("can't open file")
}} '' #endif
  
  Advance                                                                       ' prime the pump

pub Close

''#ifdef fat32
  fs.CloseFile
'' #endif
{{ #ifdef fat16
  sxfs.Close( @filestuff )
}} '' #endif
  
con
  MAXTOKENLENGTH = 32
   
dat
tokenType               long    0
tokenValue              long    0
tokenLineNumber         word    0
tokenCol                word    0
tokenText               byte    0[MAXTOKENLENGTH+1]

pub Text
  return @tokenText

pub Type
  return tokenType

pub Column
  return tokenCol

pub LineNumber
  return tokenLineNumber
  
pub Value
  return tokenValue

pub AdvanceIf( t )
{{
  If current token's type is t, advance and return true;
  otherwise return false without advancing.
  t should be one of the specific token types, not a generic type like kCOND.
}}
  if tokenType <> t
    return false

{{ #ifdef dbgtok
  Print.Str(string("AdvIf: "))
  Print.Hex(tokenType, 8)
  Print.CRLF
}} '' #endif

  Advance
  return true
  
pub Eat( t )
{{
  Like Advance, but asserts that the soon-to-be-previous token's type is t.
  t should be one of the specific token types, not a generic type like kCOND.
}}
  if tokenType <> t
    abort string("*Syntax error (unexpected)")
  Advance

pub Advance  | n1,n2,n3,n4, i

{{ #ifdef dbgtok
  Print.Str(string("Adv"))
}} '' #endif
  byte[@tokenText]~                                     'clear tokentext

''#ifdef fat32
  n1 := fs.ReadData( @tokenType,       4 )
  n2 := fs.ReadData( @tokenLineNumber, 2 )
  n3 := fs.ReadData( @tokenCol,        2 )
  if tokenType == kw#kINTLITERAL or tokenType == kw#kFLOATLITERAL
    n4 := fs.ReadData(@tokenValue, 4)
  else
    n4 := ReadString( @tokenText, MAXTOKENLENGTH )
'' #endif
{{ #ifdef fat16
  n1 := sxfs.Read( @filestuff, @tokenType,       4 )
  n2 := sxfs.Read( @filestuff, @tokenLineNumber, 2 )
  n3 := sxfs.Read( @filestuff, @tokenCol,        2 )
  if tokenType == kw#kINTLITERAL or tokenType == kw#kFLOATLITERAL
    n4 := sxfs.Read( @filestuff, @tokenValue, 4 )
  else
    n4 := ReadString( @tokenText, MAXTOKENLENGTH )
}} '' #endif

{{ #ifdef dbgtok
  Print.Str(string(":   "))
  Print.Hex(tokenType,8)
  Print.Space
  Print.Decimal(tokenLineNumber)
  Print.Char(",")
  Print.Decimal(tokenCol)
  Print.Space
  if tokenType == kw#kINTLITERAL or tokenType == kw#kFLOATLITERAL
    Print.Hex(tokenValue,8)
  else
    if n4 > 0
      Print.Str(@tokenText)
  Print.Space
  Print.Decimal(n1)
  Print.Space
  Print.Decimal(n2)
  Print.Space
  Print.Decimal(n3)
  Print.Space
  Print.Decimal(n4)
  Print.CRLF
  waitcnt(cnt + clkfreq/10_000)                         'tiny delay to let terminal keep up!
}} '' #endif

pri ReadString( p, MAXLENGTH ) | ch, n
  n~
  repeat MAXLENGTH + 1
''#ifdef fat32
    fs.ReadData(@ch, 1)
'' #endif
{{ #ifdef fat16
    sxfs.Read(@filestuff, @ch, 1)    
}} '' #endif
    if "a" =< ch and ch =< "z"
      ch -= constant("a" - "A")
    ifnot byte[p++] := ch
      return n
    n++  
  abort string("*String too long")

pub IsId
{{
  Returns non-zero (not necessarily -1) if token is a plain old identifier.
  Returns 0 otherwise.
}}
  return tokenType & kw#kID
 
pub IsIntLiteral
{{
  Returns non-zero (not necessarily -1) if token is an int literal.
  Returns 0 otherwise.
}}
  return tokenType & kw#kINTLITERAL
 
pub IsFloatLiteral
{{
  Returns non-zero (not necessarily -1) if token is an float literal.
  Returns 0 otherwise.
}}
  return tokenType & kw#kFLOATLITERAL

pub IsUnaryOp
{{
  Returns non-zero (not necessarily -1) if token is a unary operator.
  Returns 0 otherwise.
}}
  return tokenType & kw#kUNARY

pub IsBinaryOp
{{
  Returns non-zero (not necessarily -1) if token is a binary operator.
  Returns 0 otherwise.
}}
  return ( tokenType & constant(kw#kUNARY|kw#kBINARY) ) == kw#kBINARY
  
pub IsPostfixOp
{{
  Returns non-zero (not necessarily -1) if token is a postfix operator.
  Returns 0 otherwise.
}}
  return ( tokenType & constant(kw#kUNARY|kw#kPOSTFIX) ) == constant(kw#kUNARY|kw#kPOSTFIX)

pub IsAssignmentOp
{{
  Returns non-zero (not necessarily -1) if token is an assignment operator.
  Returns 0 otherwise.
}}
  return tokenType & kw#kASSIGNMENT

pub GetPrecedence
{{
  If token is an operator, returns its precedence.
  If token isn't, returns 13.
}}
  if tokenType & constant( kw#kBINARY | kw#kUNARY )
    return (tokenType >> 16) & $0f
  else
    return 13

pub GetSpinOpcode
{{
  If token is an operator or a statement function, returns its Spin opcode.
  If token isn't, returns nonsense.
}}
  return tokenType & $ff
        
pub GetSpinOpcode2
{{
  If token is a postfix operator, returns its alternate Spin opcode.
  If token isn't, returns nonsense.
}}
  return (tokenType >> 8) & $ff
        
pub IsPasm
{{
  Returns non-zero (not necessarily -1) if token is a PASM mnemonic.
  Returns 0 otherwise.
}}
  return tokenType & kw#kPASM

pub GetPasmOp
{{
  If token is a PASM mnemonic, return its PASM opcode.
}}
  return tokenType << 16

pub GetPasmDS
{{
  If token is a PASM mnemonic, return its DS information.
}}
  return (tokenType >> 16) & 3

pub IsCond
{{
  Returns non-zero (not necessarily -1) if token is a PASM condition code (e.g., IF_A).
  Returns 0 otherwise.
}}
  return tokenType & kw#kCOND

pub GetCond
{{
  Returns condition bits if token is a PASM condition code.
}}
  return tokenType & $0f
  
pub IsEffect
{{
  Returns non-zero (not necessarily -1) if token is a PASM effect (e.g., WC).
  Returns 0 otherwise.
}}
  return tokenType & kw#kEFFECT

pub GetEffect
{{
  Returns effect bits if token is a PASM effect.
}}
  return tokenType & $0f
  
pub IsReg
{{
  Returns non-zero (not necessarily -1) if token is a special register (e.g., PHSA).
  Returns 0 otherwise.
}}
  return kw#kPAR =< tokenType and tokenType =< kw#kVSCL

pub GetReg
{{
  Returns register# if token is a special register.
}}
  return tokenType - kw#kPAR
  
pub IsBlockDesignator
{{
  Returns true if token is a block designator (e.g., DAT).
}}
  return kw#kCON =< tokenType and tokenType =< kw#kVAR

pub IsSize
{{
  Returns true if token is BYTE, WORD, or LONG.
}}
  return kw#kBYTE =< tokenType and tokenType =< kw#kLONG

pub IsStmtFn
{{
  Returns non-zero (not necessarily -1) if token is a "statement function" (e.g. BYTEMOVE, WAITCNT).
  Returns 0 otherwise.
}}
  return tokenType & kw#kSTMTFN

pub GetNumArgs
{{
  Returns #args if token is a "statement function".
}}
  return (tokenType >> 8) & 3

pub IsIntrinsic
{{
  Returns non-zero (not necessarily -1) if token is an intrinsic (e.g., COGNEW, LOOKUP).
  Returns 0 otherwise.
}}
  return tokenType & kw#kINTRINSIC

{{
Copyright (c) 2009 Michael Park
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    |
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}