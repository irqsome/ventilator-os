'' +--------------------------------------------------------------------------+
'' |                     ASCII Character Definitions          _CHARDEF.SPIN   |
'' +--------------------------------------------------------------------------+
'' RR20160219   v1.00  previously in _OS.spin - "Cluso99" (Ray Rodrick)

CON  
  home                          = 1
  gotoXY                        = 3
  gotoX                         = 14
  gotoY                         = 15
  left                          = 3
  right                         = 4
  up                            = 5
  down                          = 6
  backspace                     = 8
  tab                           = 9
  LF                            = 10  '$0A
  CR                            = 13  '$0D
  clear                         = 16  '$10
  clear_to_end                  = 11
  clear_below                   = 12

  beep                          = 7
  
  _firstPrintableCharacter      = $20
  _quoteCharacter               = 34  '$22
  _lastPrintableCharacter       = $7E 'actually, $FF, but whatever
  '_backspaceKeycode             = 200
  colors_base                   = $100
  color_white                   = colors_base + 0
  color_cyan                    = colors_base + 1
  color_magenta                 = colors_base + 2
  color_blue                    = colors_base + 3
  color_yellow                  = colors_base + 4
  color_green                   = colors_base + 5
  color_red                     = colors_base + 6
  color_gray                    = colors_base + 7  
  colors_dark                   = $8
  colors_reverse                = $10

  ' crusty compatibility stuff
  _nullCharacter                = 0   'may also clear screen
  _homeCursorCharacter          = home
  _backspaceCharacter           = backspace
  _horizontalTabCharacter       = tab
  _lineFeedCharacter            = LF
  _carriageReturnCharacter      = CR
  _clearScreenCharacter         = clear
  
  

PUB dummy