'' +--------------------------------------------------------------------------+
'' | VentilatorOS...                                                          |
'' |                 VGA Output Driver:              _VGAOUT.SPIN             |
'' +--------------------------------------------------------------------------+
'' |  Authors:                                                                |
'' |  (c) 2004           Chip Gracey           - original code TV.spin        |
'' |  (c) 2020           Ada Gottensträter (Wuerfel_21) - modifications       |
'' |  License            MIT License - See end of file for terms of use       |
'' +--------------------------------------------------------------------------+

OBJ
_hw : "_HWDEF"

PUB start(inRENDEZVOUS, outRENDEZVOUS, buffer, pingroup, pinmask,palette)
  setpalette(palette)
  haxx_s |= pinmask&511
  haxx_d |= pingroup&511
  _outpins := pinmask<<(pingroup*8)
  _outpins |= |< _hw#_AUDIO_1   

  _screen := buffer                                                        
  return cognew(@entry,outRENDEZVOUS)+1

PRI setpalette(which) |i,palptr,tblptr,bgcolor,fgcolor
  tblptr := @entry + (colortable*4)
  palptr := @@word[@pallist][which]

  bgcolor := byte[palptr][16]


  repeat i from 0 to 15
    fgcolor := byte[palptr][i]
    long[tblptr][i*2+0] := (fgcolor<<24)+(bgcolor<<16)+(fgcolor<<8)+(bgcolor)
    long[tblptr][i*2+1]  := (fgcolor<<24)+(fgcolor<<16)+(bgcolor<<8)+(bgcolor)
    long[tblptr][i*2+32] := (bgcolor<<24)+(fgcolor<<16)+(bgcolor<<8)+(fgcolor)
    long[tblptr][i*2+33] := (bgcolor<<24)+(bgcolor<<16)+(fgcolor<<8)+(fgcolor)

DAT

pallist
        word @test_pal
        word @default_pal

default_pal
        byte %%333_3 'white
        byte %%133_3 'cyan
        byte %%313_3 'magenta
        byte %%113_3 'blue
        byte %%331_3 'yellow
        byte %%131_3 'green
        byte %%311_3 'red
        byte %%222_3 'grey

        byte %%222_3 'white
        byte %%022_3 'cyan
        byte %%202_3 'magenta
        byte %%002_3 'blue
        byte %%220_3 'yellow
        byte %%020_3 'green
        byte %%200_3 'red
        byte %%111_3 'grey

        byte %%000_3 'background (black)

test_pal
        byte $07[16]
        byte $0F
        
CON 

  colortable    = $180  'start of colortable inside cog

DAT

'********************************
'* Assembly language VGA driver *
'********************************

                        org
'
'
' Entry
'
entry
t1
haxx_s                  movs    vcfg,#0
t2                      
haxx_d                  movd    vcfg,#0
                        movi    vcfg,#%01_1_00_000      ' Set 4 color VGA
m1                      mov     dira,_outpins
m2                      movs    ctrb,#_hw#_AUDIO_1
y                       movi    ctrb,#%00110_000

                        mov     rate,_rate
                        
                        mov     t1,#%00001_011          'set ctra configuration
:max                    cmp     m8,rate         wc      'adjust rate to be within 4MHz-8MHz
        if_c            shr     rate,#1                 '(vco will be within 64MHz-128MHz)
        if_c            add     t1,#%00000_001
        if_c            jmp     #:max
:min                    cmp     rate,m4         wc
        if_c            shl     rate,#1
        if_c            sub     x,#%00000_001
        if_c            jmp     #:min
                        movi    ctra,t1                 '+22

                        rdlong t1,#0                    'divide _rate/CLKFREQ and set frqa
                        mov     hvbase,#32+1
:div                    cmpsub  rate,t1         wc
                        rcl     t2,#1
                        shl     rate,#1
                        djnz    hvbase,#:div            '(hvbase=0)
                        mov     frqa,t2                 '+136}

                        test    _mode,#%0001    wc      'make hvbase
                        muxnc   hvbase,vmask
                        test    _mode,#%0010    wc
                        muxnc   hvbase,hmask            '+4

                        mov     vscl,#1
                        
pixels                  mov     x,#100                  'perform task sections initially (wait for PLL to start)
initl                   jmpret  taskret,taskptr
pixels2                 djnz    x,#initl

                        ' hopefully start reliably...
                        cogid t1
                        nop
                        nop
                        nop
                        mov vscl,#65
'
'
' Superfield
'
superfield              mov     hv,hvbase               'set hv

                        mov     interlace,#0            'reset interlace

                        test    _mode,#%0100    wz      'get interlace into nz
                        'mov    x,#0 wz '????
'
'
' Field
'
field                   'wrlong  visible,par             'set status to visible

                        tjz     vb,#:nobl               'do any visible back porch lines
                        mov     x,vb
                        movd    bcolor,#colortable
                        call    #blank_line
:nobl

                        call    #toggle_cursor

                        mov     screen,_screen          'point to first tile (upper-leftmost)
                        mov     y,_vt                   'set vertical tiles
:line                   'mov     vx,#1                   'set vertical expand
:vert   if_nz           xor     interlace,#1            'interlace skip?
        if_nz           tjz     interlace,#:skip

                        tjz     hb,#:nobp               'do any visible back porch pixels
                        mov     vscl,hb
                        waitvid colortable,#0
:nobp
                        mov     x,_ht                   'set horizontal tiles
                        mov     vscl,hx                 'set horizontal expand

:tile                   rdword  tile,screen             'read tile
                        add     tile,line               'set pointer bits into tile
                        rol     tile,#6                 'read tile pixels
                        rdlong  pixels,tile             '(8 clocks between reads)
                        shr     tile,#10+6              'set tile colors
                        movd    :color,tile
                        add     screen,#2               'point to next tile
:color                  waitvid colortable,pixels       'pass colors and pixels to video 
                        djnz    x,#:tile                'another tile?

                        sub     screen,hc2x             'repoint to first tile in same line

                        ' Update beeping
                        cmpsub  beep_left,#1 wc
              if_c      add     frqb,beep_frq
              if_nc     mov     frqb,bit31

                        tjz     hf,#:nofp               'do any visible front porch pixels
                        mov     vscl,hf
                        waitvid colortable,#0
:nofp
                        mov     x,#1                    'do hsync
                        call    #blank_hsync            '(x=0)

:skip                   'djnz    vx,#:vert               'vertical expand?
                        ror     line,#1                 'set next line
                        add     line,lineadd    wc
                        rol     line,#1                             
        if_nc           jmp     #:line
                        add     screen,hc2x             'point to first tile in next line
                        djnz    y,#:line        wc      'another tile line? (c=0)

                        call    #toggle_cursor

                        tjz     vf,#:nofl               'do any visible front porch lines
                        mov     x,vf
                        movd    bcolor,#colortable
                        call    #blank_line
:nofl
        if_nz           xor     interlace,#1    wc,wz   'get interlace and field1 into nz (c=0/?)
        
        'if_z            wrlong  invisible,par           'unless interlace and field1, set status to invisible

                        mov     taskptr,#tasks          'reset tasks

                        'addx   x,_vf           wc      'do invisible front porch lines (x=0 before, c=0 after)
                        add    x,_vf           wc      'do invisible front porch lines (x=0 before, c=0 after)
                        call    #blank_line

                        mov     x,_vs                   'do vsync lines
                        call    #blank_vsync

                        mov     x,_vb                   'do invisible back porch lines, except last
                        call    #blank_vsync

        if_nz           jmp     #field                  'if interlace and field1, display field2
                        jmp     #superfield             'else, new superfield

'
' Hide/unhide cursor
'
toggle_cursor           muxnz   :restorez,#1 'save flags
                        test tmflags,#$104 wz 'Test for scroll flag or "cursor off" flag
              if_z      rdword t2,cursor_ptr
              if_z      xor t2,bit15
              if_z      wrword t2,cursor_ptr
:restorez               or      zero,#0 nr,wz 'restore flags
toggle_cursor_ret       ret



'
'
' Blank line(s)
'
blank_vsync             cmp     interlace,#2    wc      'vsync (c=1)

{
blank_line              mov     vscl,h1                 'blank line or vsync-interlace?
        if_nc           add     vscl,h2
        if_c_and_nz     xor     hv,#%01
        if_c            waitvid hv,#0
        if_c            mov     vscl,h2                 'blank line or vsync-normal?
        if_c_and_z      xor     hv,#%01
}

' Fake interlace / temporal AA hack!!!
blank_line              mov     vscl,h1                 'blank line or vsync-interlace?
        if_nc           add     vscl,h2 
        if_c            waitvid hv,#0
        if_c            mov     vscl,h2                 'blank line or vsync-normal?
        if_c            xor     hv,#%01

bcolor                  waitvid hv,#0

        if_nc           jmpret  taskret,taskptr         'call task section (z undisturbed)

blank_hsync             mov     vscl,_hf                'hsync, do invisible front porch pixels
                        waitvid hv,#0

                        mov     vscl,_hs                'do invisble sync pixels
                        xor     hv,#%10
                        waitvid hv,#0

                        call    #calc_addr
                        mov     cursor_ptr,t1 

                        mov     vscl,_hb                'do invisible back porch pixels
                        xor     hv,#%10
                        waitvid hv,#0

                        djnz    x,#blank_line   wc      '(c=0)

                        movd    bcolor,#hv
blank_hsync_ret
blank_line_ret
blank_vsync_ret         ret
'
'
' Tasks - performed in sections during invisible back porch lines
'
tasks

                        mov     hx,_hx                  'compute horizontal metrics
                        shl     hx,#8
                        or      hx,_hx
                        shl     hx,#4

                        mov     hc2x,_ht
                        shl     hc2x,#1

                        mov     h1,_hd
                        neg     h2,_hf
                        sub     h2,_hs
                        sub     h2,_hb
                        sub     h1,h2
                        shr     h1,#1           wc
                        addx    h2,h1

                        mov     t1,_ht
                        mov     t2,_hx
                        call    #multiply
                        mov     hf,_hd
                        sub     hf,t1
                        shr     hf,#1           wc
                        mov     hb,_ho
                        addx    hb,hf
                        sub     hf,_ho                  '+59
                        
                        mov     t1,_vt                  'compute vertical metrics
                        mov     t2,#1
                        call    #multiply
                        'shr    t1,#1 ' half height!
                        'test   _mode,#%1000    wc      'consider tile size
                        'muxc   linerot,#1
                        mov     lineadd,lineinc
        {if_c}          shr     lineadd,#1
        {if_c}          shl     t1,#1
                        
                        test    _mode,#%0100    wc      'consider interlace
        if_c            shr     t1,#1
                        mov     vf,_vd
                        sub     vf,t1
                        shr     vf,#1           wc
                        neg     vb,_vo
                        addx    vb,vf
                        add     vf,_vo                  '+53

                        '' Update cursor blink
                        add     cursor_phs,cursor_frq wc
              if_c      xor     tmflags,#$004

:colors                 jmpret  taskptr,taskret         '+1=114/160, break and return later

                        'save Z
                        muxnz   :restorez,#1
                        'call   #update_terminal
                        call    #update_terminal
:restorez               movd    d0,#0 nr,wz

                        ' Update beeping
                        cmpsub  beep_left,#1 wc
              if_c      add     frqb,beep_frq
              if_nc     mov     frqb,bit31

                        jmp     #:colors                '+1, keep loading colors
'
'
' Multiply t1 * t2 * 16 (t1, t2 = bytes)
'
multiply                shl     t2,#4
multiply2               shl     t2,#8-1

                        mov     tile,#8
:loop                   shr     t1,#1           wc
        if_c            add     t1,t2
                        djnz    tile,#:loop
                        
multiply2_ret
multiply_ret            ret                             '+37

update_terminal         test tmflags,#$100 wz 'scroll/clear screen?
              if_z      jmp #:dochar 'if not, get character

                        mov tm1,spacechar 'put empty char into tm1
                        add row,#1 'Increment row and check if last row
                        cmp row,_vt wz '...                                                                                                                                                                                                          
                        call #calc_addr
              if_e      sub row,#1 'If last row, adjust back (after calc_addr)
                        test tmflags,#$080 wc 'get clear flag into C
        if_z_and_c      mov row,#0 'if last row and clear, reset row to beginning
                        mov t2,t1
                        sub t2,_ht
                        sub t2,_ht
                        mov column,_ht
              if_z      andn tmflags,#$180 'If last row, unset scroll and clear flags 
:scrolloop   if_nz_and_nc rdword tm1,t1 'if last row or clear, don't read buffer (tm1 is still empty char)
                        add t1,#2
                        wrword tm1,t2
                        add t2,#2
                        djnz column,#:scrolloop
                        jmp update_terminal_ret


:dochar                 test tmflags,#$040 wc 'restore cursor?
              if_c      mov column,column2
              if_c      mov row,row2
                        andn tmflags,#$040
                        tjnz repchar,#:pokechar 'repeat character?
                        rdlong tm1,par wz
              if_z      jmp update_terminal_ret
                        wrlong  zero,par
                        sub     tm1,#$100
                        test    tmflags,#$003 wz 'GOTO flags active?
              if_nz     jmp #:check_goto
                        shl tm1,#0 wc 'Raw character? (bit 31 set)
              if_c      jmp #:pokechar
                        cmpsub  tm1,#$100 wc  'Color code?
              if_c      jmp #:setcolor
                        cmp     tm1,#16 wc,wz 'Is special character?
              if_a      jmp #:normalchar
              if_nz     testn   tm1,#1 wz 'Is home or clear?
              if_z      jmp #:homeclear 'clear is 0 or 16, home is 1
                        sub tm1,#2
                        tjnz tm1,#:not_gotoxy
                        '' GOTO XY
                        or tmflags,#$003 'set gotox and gotoy flags
                        jmp update_terminal_ret
                        
:not_gotoxy             djnz tm1,#:not_cleft
                        '' CURSOR LEFT
                        call #cursor_back
                        jmp update_terminal_ret
                        
:not_cleft              djnz tm1,#:not_cright
                        '' CURSOR RIGHT
                        add column,#1
                        cmp column,_ht wz
              if_z      sub column,#1
              if_z      add row,#1
              if_z      cmp row,_vt wc
        if_z_and_nc     sub row,#1
        if_z_and_c      mov column,#0
              
                        jmp update_terminal_ret

:not_cright             djnz tm1,#:not_cup
                        '' CURSOR UP
                        cmpsub row,#1
                        jmp update_terminal_ret

:not_cup                djnz tm1,#:not_cdown
                        '' CURSOR DOWN
                        add row,#1
                        cmp row,_vt wz,wc
              if_ae     sub row,#1
                        jmp update_terminal_ret

:not_cdown              djnz tm1,#:not_beep
                        '' BEEP
                        movd beep_left,#$04
                        jmp update_terminal_ret

:not_beep               djnz tm1,#:not_backspace
                        '' BACKSPACE
                        call #cursor_back
                        call #calc_addr
                        wrword spacechar,t1                        
                        jmp update_terminal_ret

:not_backspace          djnz tm1,#:not_tab
                        '' TAB
                        mov t1,column
                        and t1,#7 ' 8 spaces per tab
                        mov repchar,#8  '^^
                        sub repchar,t1
                        mov tm1,spacechar
                        jmp update_terminal_ret

:not_tab                djnz tm1,#:not_linefeed
:lf                     '' LINE FEED
                        add     row,#1
                        cmpsub  row,_vt    wc
                        muxc    tmflags,#$100 'Do scroll next update
                        jmp update_terminal_ret

:not_linefeed           djnz tm1,#:not_cltoend
                        '' CLEAR TO END
                        call #calc_addr
                        mov tm2,_ht
                        sub tm2,column
:cltoendloop            wrword spacechar,t1
                        add t1,#2
                        djnz tm2,#:cltoendloop
                        jmp update_terminal_ret

:not_cltoend            djnz tm1,#:not_clbelow
                        '' CLEAR BELOW
                        mov column2,column
                        mov row2,row
                        add row,#2
                        cmp row,_vt wc,wz
                        mov row,row2
                 if_be  or tmflags,#$1C0 'scroll,clear and restore flags
                        jmp update_terminal_ret

:not_clbelow            djnz tm1,#:not_newline
                        '' NEW LINE
                        mov column,#0
                        jmp #:lf

:not_newline            djnz tm1,#:not_gotox
                        '' GOTO X
                        or tmflags,#$001 'set gotox flag
                        jmp update_terminal_ret

:not_gotox              djnz tm1,#:not_gotoy
                        '' GOTO Y
                        or tmflags,#$002 'set gotox flag
:not_gotoy              jmp update_terminal_ret   
                        

:check_goto             test tmflags,#$001 wz ' goto x flag?
              if_z      jmp #:check_goto_y
                        mov t1,_ht
                        sub t1,#1
                        mov column,tm1
                        max column,t1
                        andn tmflags,#$001
                        jmp update_terminal_ret
:check_goto_y           'logicn tells us that the goto y flag must be set
                        mov t1,_vt
                        sub t1,#1
                        mov row,tm1
                        max row,t1
                        andn tmflags,#$002
                        jmp update_terminal_ret

:setcolor               and tm1,#31 'colors > 16 are reverse video
                        shl tm1,#11
                        mov termcolor,tm1
                        mov spacechar,#$22
                        shl spacechar,#4
                        add spacechar,termcolor
                        jmp update_terminal_ret
                                               
:homeclear              mov column,#0
                        mov row,#0
                        shr tm1,#0 wc 'is home or clear
              if_nc     or tmflags,#$180 'if clear, do clear+scroll
                        jmp update_terminal_ret
                        
              

:normalchar             'adjust character
                        shr     tm1,#1 wc
                        add     tm1,#$100
              if_c      or      tm1,d0
                        shl     tm1,#1
                        add     tm1,termcolor
                        
:pokechar               call    #calc_addr
                        wrword  tm1,t1

                        add     column,#1
                        cmpsub  column,_ht wc
              if_c      add     row,#1
                        cmpsub  row,_vt    wc
                        muxc    tmflags,#$100 'Do scroll next update
                        cmpsub repchar,#1                        

update_terminal_ret     ret

calc_addr               mov t1,_ht
                        mov t2,row
                        call #multiply2
                        add t1,column
                        shl t1,#1
                        add t1,_screen

calc_addr_ret           ret

cursor_back             sub column,#1 wc
              if_nc     jmp cursor_back_ret
                        cmpsub row,#1 wc
              if_c      add column,_ht
              if_nc     mov column,#0                        
cursor_back_ret         ret



'
'
' Initialized data
'

' Stuff                                                                                                                            
_mode                   long    %1100   '%tihv          read-only
_screen                 long    1       '@word          read-only 
_ht                     long    32      '1+             read-only
_vt                     long    29      '1+             read-only
_ho                     long    0       '0+-            read-only
_vo                     long    0       '0+-            read-only

' Video mode config               
_rate                   long    20_000_000 '500_000+    read-only
_hx                     long    1       '1+             read-only
_hd                     long    512     '1+             read-only
_hf                     long    10      '1+             read-only
_hs                     long    75      '1+             read-only
_hb                     long    43      '1+             read-only
_vd                     long    480     '1+             read-only
_vf                     long    10      '1+             read-only
_vs                     long    2       '1+             read-only
_vb                     long    33      '2+             read-only
{
_rate                   long    25_175_000 '500_000+    read-only
_hx                     long    1       '1+             read-only
_hd                     long    640     '1+             read-only
_hf                     long    16      '1+             read-only
_hs                     long    96      '1+             read-only
_hb                     long    48      '1+             read-only
_vd                     long    480     '1+             read-only
_vf                     long    10      '1+             read-only
_vs                     long    2       '1+             read-only
_vb                     long    33      '2+             read-only
}

                        fit     colortable              'fit underneath colortable ($180-$1BF)
                        long 0[colortable-$]
                        long $FF_AB_57_07[64]
                        
                        fit $1C0

column                  long    0
row                     long    0
column2                 long    0
row2                    long    0
tm1                     long    0
tm2                     long    0
tmflags                 long    0
repchar                 long    0
cursor_phs              long    0
cursor_frq              long    1<<29
beep_left               long    0
beep_frq                long    117924613/2
bit15                   long    1 << 15
bit31                   long    1 << 31
bottom_word             long    $FFFF

spacechar               long    $220
termcolor               long    0
zero                    long    0


pllmin                  long    500_000                 'pll lowest output frequency
pllmax                  long    128_000_000             'pll highest output frequency
m8                      long    8_000_000               '*16 = 128MHz (pll vco max)
m4                      long    4_000_000               '*16 = 64MHz (pll vco min)
d0                      long    1 << 9 << 0
'd6                     long    1 << 9 << 6
'invisible              long    1
'visible                long    2
line                    long    $00060000
lineinc                 long    $10000000 '<<1 ' Half height!
'linerot                long    0
vmask                   long    $01010101
hmask                   long    $02020202
'colormask              long    $FCFCFCFC

interlace               long    0
x                       'alias
_outpins                long    0
taskptr                 long    tasks


'
'
' Uninitialized data
'                                                              
taskret                 res     1 
                                                        'display

rate                    res     1                       
hf                      res     1
hb                      res     1
vf                      res     1
vb                      res     1
hx                      res     1
vx                      res     1
hc2x                    res     1
screen                  res     1
tile                    res     1
lineadd                 res     1 
hv                      res     1
hvbase                  res     1
h1                      res     1 
h2                      res     1
cursor_ptr              res     1

                        fit 


{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}