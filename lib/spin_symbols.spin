'' +--------------------------------------------------------------------------+
'' | Sphinx:  A Compiler for the Propeller Chip     LEX / CODEGEN / LINK.SPIN |
'' |                                  sub-module:   _SYMBOLS.SPIN             |
'' +--------------------------------------------------------------------------+
'' |  Authors:                                                                |
'' |  (c) 2009,2010      Michael Park (mpark)       (original Sphinx compiler)|
'' |  (c) 2015,2016      Ray Rodrick (Cluso99)      (include in PropOS)       |
'' |  License            MIT License - See end of file for terms of use       |
'' +--------------------------------------------------------------------------+
'' mp2009
'' RR20151231      mod for PropOS
'' RR20160127      rename _symbols.spin
'' RR20160212
'' RR20160228      works


obj
  bt: "spin_bintree"
  
dat
pSymbolTable            word    0
  
pub Init

  FixupTable( @table + 2 )
  pSymbolTable := @table
  bt.PokeW( pSymbolTable, @table + 2 )

pri FixupTable( p ) | q
  q := bt.PeekW( p )
  if q
    bt.PokeW( p, q += @table )
    FixupTable( q )  
  q := bt.PeekW( p + 2 )
  if q
    bt.PokeW( p + 2, q += @table )
    FixupTable( q )  

pub SymbolLookup( s )
  return bt.FindInTable( s, bt.PeekW(pSymbolTable) )

pub AddToTable( s )
  bt.AddToTable( s, pSymbolTable )

pub GetPSymbolTable
  return pSymbolTable
  
con #0
' In the symbol table, the symbol's name (null-terminated) is followed by one of the following type bytes
' which is in turn followed by additional data:
{0} kINT_CON_SYMBOL       ' followed by 4-byte constant
{1} kFLOAT_CON_SYMBOL     ' followed by 4-byte constant
{2} kPUB_SYMBOL           ' followed by 1-byte method index, 1-byte #params
{3} kPRI_SYMBOL           ' followed by 1-byte method index, 1-byte #params
{4} kOBJ_SYMBOL           ' followed by 2-byte pointer to child object's symbol table, 1-byte index, 2-byte count,
                          ' 2-byte pointer to next OBJ in linked list
{5} kVAR_SYMBOL           ' followed by 1-byte size (1, 2, 4), 2-byte var offset
{6} kDAT_SYMBOL           ' followed by 1-byte size (1, 2, 4), 2-byte dat offset, 2-byte cog address * 4, 2-byte pointer to local labels
{7} kSOB_SYMBOL           ' followed by 2-byte pointer to SOB's symbol table
{8} kUNDEFINED_CON_SYMBOL ' followed by 4-byte constant to be determined
{9} kBUILTIN_INT_SYMBOL   ' followed by 4-byte constant
{10}kBUILTIN_FLOAT_SYMBOL ' followed by 4-byte constant

dat
table   word 0
        byte $10,$00,$85,$00,$50,$4f,$53,$58,$00,$09,$ff,$ff,$ff,$7f,$1f,$00
        byte $58,$00,$50,$4c,$4c,$31,$58,$00,$09,$40,$00,$00,$00,$2b,$00,$48
        byte $00,$50,$49,$00,$0a,$db,$0f,$49,$40,$39,$00,$00,$00,$4e,$45,$47
        byte $58,$00,$09,$00,$00,$00,$80,$00,$00,$00,$00,$46,$41,$4c,$53,$45
        byte $00,$09,$00,$00,$00,$00,$00,$00,$00,$00,$50,$4c,$4c,$31,$36,$58
        byte $00,$09,$00,$04,$00,$00,$67,$00,$76,$00,$50,$4c,$4c,$34,$58,$00
        byte $09,$00,$01,$00,$00,$00,$00,$00,$00,$50,$4c,$4c,$32,$58,$00,$09
        byte $80,$00,$00,$00,$00,$00,$00,$00,$50,$4c,$4c,$38,$58,$00,$09,$00
        byte $02,$00,$00,$95,$00,$c3,$00,$58,$49,$4e,$50,$55,$54,$00,$09,$04
        byte $00,$00,$00,$a5,$00,$b5,$00,$52,$43,$53,$4c,$4f,$57,$00,$09,$02
        byte $00,$00,$00,$00,$00,$00,$00,$52,$43,$46,$41,$53,$54,$00,$09,$01
        byte $00,$00,$00,$00,$00,$00,$00,$54,$52,$55,$45,$00,$09,$ff,$ff,$ff
        byte $ff,$d2,$00,$e1,$00,$58,$54,$41,$4c,$32,$00,$09,$10,$00,$00,$00
        byte $00,$00,$00,$00,$58,$54,$41,$4c,$31,$00,$09,$08,$00,$00,$00,$00
        byte $00,$00,$00,$58,$54,$41,$4c,$33,$00,$09,$20,$00,$00,$00

{{
Copyright (c) 2009 Michael Park
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    |
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}
          