'' +--------------------------------------------------------------------------+
'' | Cluso's Propeller Operating System...                                    |
'' |                 PC Serial Input/Output Driver:   _PCSIO.SPIN             |
'' +--------------------------------------------------------------------------+
'' |  Authors:                                                                |
'' |  (c) 2008,2009      Carl Jacobs           - original code JDCogSerial    |
'' |  (c) 2009,2010      Andy Schenk (Ariba)   - modifications                |
'' |  (c) 2010           Ray Rodrick (Cluso99) - modifications                |
'' |  License            MIT License - See end of file for terms of use       |
'' +--------------------------------------------------------------------------+
'' RR20100117 Cluso99: mod to make standard input & output driver for SphinxOS
'' RR20100201          add comments
'' RR20100217          pass parameters(4)
'' RR20120321          returns cog+1 when cog started
''                       change order of serpins
'' RR20160219 v0.98    tidy only

'  This code allows the use of standard routines for input and output while
'  being decoupled from "this" underlying driver (hardware abstraction).
'  This underlying driver runs in a separate cog and may be changed on the fly.
'  Input  characters are passed via a single long mailbox in hub ( inRENDEZVOUS).
'  Output characters are passed via a single long mailbox in hub (outRENDEZVOUS).


CON
  RX1FIFO_SIZE = 160    '640 bytes
  TX1FIFO_SIZE = 160    '640 bytes

VAR
  long  cog                     'cog+1 flag/id
  long  params[4]               'for passing parameters

PUB start(inRENDEZVOUS, outRENDEZVOUS, serPins, baud) 

'' Start Serial driver - starts a cog if necessary
'' returns cog+1 if it had to start a cog, false if cog was already running.
'' serPins = rxPin << 8 | txPin
'' baud    = baud
''


  long[outRENDEZVOUS] := 4                              'Ping Ser Driver
  waitcnt(clkfreq>>2+cnt)
  if long[outRENDEZVOUS] == 0
    return False                                        'is alive

'else start driver

  long[inRENDEZVOUS]  := 0                              'clear the rendezvous locations
  long[outRENDEZVOUS] := ">"
  params[0] := inRENDEZVOUS                             'pass the parameters
  params[1] := outRENDEZVOUS
  params[2] := serPins
  params[3] := clkfreq / baud
  cog := cognew(@entry, @params) + 1                    'Start the cog

  repeat while long[outRENDEZVOUS]                      'wait until cleared (ok to go)

  return cog                                            'cog+1 (=0=false if failed)


DAT
                    ORG     0
'
' Initialisation. 
'

entry               mov     t1,par              'get the address of the parameter list
                    rdlong  rx1ptr,t1           'inRENDEZVOUS
                    add     t1,#4
                    rdlong  tx1ptr,t1           'outRENDEZVOUS
                    add     t1,#4
                    rdlong  t2,t1               'serPins
                    test    t2,#$80       wz    'if $FF, disable tx
        if_z        mov     tx1mask,#1          '=txmask 
        if_z        shl     tx1mask,t2
                    shr     t2,#8               'rxPin
                    test    t2,#$80       wz    'if $FF, disable rx
        if_z        mov     rx1mask,#1          '=rxmask 
        if_z        shl     rx1mask,t2
                    add     t1,#4
                    rdlong  baud1,t1            'baudticks

'                   wrlong  cNull,rx1ptr        'cleared by spin
                    wrlong  cNull,tx1ptr        'signal OK to go
                    
                    or      OUTA,tx1mask
                    or      DIRA,tx1mask
                    jmp     dRx1

rx1ptr              long    0    
tx1ptr              long    0 
rx1mask             long    0
tx1mask             long    0
baud1               long    0
'
' Receiver 
'
aRx1_Start          mov     rx1bits,#9          '# of bits to receive
                    mov     rx1cnt,baud1
                    shr     rx1cnt,#2           'quarter of a bit tick ...
                    add     rx1cnt,baud1        '... plus a full bit tick
                    mov     dRx1,#aRx1_Wait
                    
aRx1_Wait           test    rx1mask, INA  WC    'Wait for start bit
    if_c            jmp     #aRx1_Get                                 
                    add     rx1cnt, CNT
                    mov     dRx1,#aRx1_Bits
                    
aRx1_Bits           mov     t1,rx1cnt           'Check if bit receive period done
                    sub     t1,cnt
                    cmps    t1,#0         WC    'Time to sample yet?
    if_nc           jmp     #aRx1_Get
                    test    rx1mask, INA  WC    'Receive bit into carry
                    rcr     rx1data,#1          'Carry into Rx buffer
                    add     rx1cnt,baud1        'Go to next bit period
                    djnz    rx1bits,#aRx1_Bits  'Get next bit
    if_nc           jmp     #aRx1_Start
                    and     rx1data,cRxAnd      'Mask out the unwanted bits
                    rol     rx1data,rx1rol      'Put the data into the correct byte
                    mov     dRx1,#aRx1_Put
                    jmp     dTx1                'Go do some transmitter code

                    'General variables for the receiver
cRxAnd              long    %0111_1111_1000_0000_0000_0000_0000_0000
rx1len              long    0
rx1maxlen           long    (tx1fifo - rx1fifo - 1) * 4
dRx1                long    aRx1_Start
                    'Variables used to receive a byte into the FIFO
rx1data             long    0
rx1bits             long    0
rx1cnt              long    0
rx1rol              long    9
rx1out              long    0
rx1put              long    rx1fifo
rx1putb             long    $80
                    'Variables used to grab a byte from the FIFO
rx1ror              long    0                    
rx1get              long    rx1fifo
rx1getb             long    $80

aRx1_Put            cmp     rx1len,rx1maxlen  WZ
    if_z            jmp     #(aRx1_Get+2)
                    mov     dRx1,#aRx1_Start
                    add     rx1len,#1
                    or      rx1out,rx1data      'Merge in the new data byte
arp1                mov     rx1fifo,rx1out      'Write to the FIFO memory
                    add     rx1rol,#8           'Prapare for the next byte
                    rol     rx1putb,#8  WC      'C true every 4 cycles
    if_nc           jmp     dTx1
                    add     rx1put,#1
                    cmp     rx1put,#tx1fifo   WZ
    if_z            mov     rx1put,#rx1fifo
                    movd    arp1,rx1put         'Set the new FIFO destination 
                    mov     rx1out,#0
                    jmp     dTx1

aRx1_Get            cmp     rx1len,#0  WZ
    if_z            jmp     dTx1
                    rdlong  t1,rx1ptr  WZ       'Is the receive buffer empty?
    if_nz           jmp     dTx1                'Jump if not
                    sub     rx1len,#1
arg1                mov     t1,rx1fifo
                    ror     t1,rx1ror
                    and     t1,#$ff   WZ
    if_z            or      t1,#$100            '$00=$100
                    wrlong  t1,rx1ptr 
                    add     rx1ror,#8
                    rol     rx1getb,#8  WC      'C true every 4 cycles
    if_nc           jmp     dTx1
                    add     rx1get,#1
                    cmp     rx1get,#tx1fifo  WZ
    if_z            mov     rx1get,#rx1fifo
                    movs    arg1,rx1get
                    jmp     dTx1
'
' Transmitter 
'
aTx1_Start          cmp     tx1len,#0   WZ
    if_z            jmp     #aTx1_Put
                    mov     dTx1,#aTx1_Byte
                    sub     tx1len,#1
atg1                mov     tx1data,tx1fifo
                    ror     tx1data,tx1ror
                    and     tx1data,#$ff
                    add     tx1ror,#8
                    rol     tx1getb,#8  WC
    if_nc           jmp     dRx1     
                    add     tx1get,#1
                    cmp     tx1get,#fifo1end  WZ
    if_z            mov     tx1get,#tx1fifo
                    movs    atg1,tx1get
                    jmp     dRx1

aTx1_Byte           shl     tx1data,#2
                    or      tx1data,cFixedBits  'or in a idle line state and a start bit
                    mov     tx1bits,#11
                    mov     tx1cnt,cnt
aTx1_Bits           shr     tx1data,#1   WC
                    muxc    OUTA,tx1mask        
                    add     tx1cnt,baud1        'Bit period counter
                    mov     dTx1,#aTx1_Wait
                    jmp     dRx1

aTx1_Wait           mov     t1,tx1cnt           'Check bit period
                    sub     t1,CNT
                    cmps    t1,#0       WC      'Is bit period done yet?
    if_nc           jmp     #aTx1_Put
                    djnz    tx1bits,#aTx1_Bits  'Transmit next bit
                    mov     dTx1,#aTx1_Start
                    jmp     dRx1

aTx1_Put            cmp     tx1len,#(fifo1end-tx1fifo) WZ
    if_z            jmp     dRx1
                    rdlong  t1,tx1ptr  WZ
    if_z            jmp     dRx1
                    sub     t1,#$100
                    wrlong  cNull,tx1ptr
                    testn   t1,#$FF    wz
    if_nz           jmp     dRx1
                    add     tx1len,#1
                    'and     t1,#$ff
                    shl     t1,tx1rol
                    add     tx1rol,#8
                    or      tx1out,t1
atp1                mov     tx1fifo,tx1out    
                    rol     tx1putb,#8  WC
    if_nc           jmp     dRx1
                    add     tx1put,#1
                    cmp     tx1put,#fifo1end  WZ
    if_z            mov     tx1put,#tx1fifo
                    movd    atp1,tx1put
                    mov     tx1out,#0
                    jmp     dRx1
         
                    'General variables for the transmitter
tx1len              long    0
tx1maxlen           long    (fifo1end - tx1fifo - 1) * 4
dTx1                long    aTx1_Start
                    'Variables used to grab the transmit a byte from the FIFO
tx1data             long    0
tx1bits             long    0
tx1cnt              long    0
tx1ror              long    0
tx1get              long    tx1fifo
tx1getb             long    $80
                    'Variables used to receive a byte into the FIFO
tx1rol              long    0
tx1put              long    tx1fifo
tx1putb             long    $80
tx1out              long    0

'
' Data
'
cFixedBits          long    %1_0000_0000_0_1    'Stop + 8 x Data + Start + Idle bits
'cMinusOne           long    -1     
cNull               long    0     

t1                  long    0
t2                  long    0

'
' The buffer sizes may be freely adjusted. As long as the code still compiles
' into the cog. The sizes are currently at their maximum, but may be adjusted
' to favour either the receiver or tranmitter.
'
rx1fifo             res     RX1FIFO_SIZE
tx1fifo             res     TX1FIFO_SIZE
fifo1end

                    FIT     496
                    
{{
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    |
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}