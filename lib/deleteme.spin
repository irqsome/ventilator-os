CON
  _XINFREQ = 5_000_000
  _CLKMODE = pll16x + xtal1

  basepin = 12

OBJ

  tv : "_TVOUT.spin"
  _hub : "_HUBDEF.spin"

VAR

  word screen[1000]
  long thingy
  
PUB main | tv_pins,i

  wordfill(@screen,$242,1000)

  tv_pins := (basepin & $38) << 1 | (basepin & 4 == 4) & %0101
  tv.start(0,@thingy,@screen,0,tv_pins,1)

  repeat 3000                 
    i := (i+1)&255
    char(97)
  char(16)
  str(string("Henlo world!"))
  char(6)
  str(string("Henlo world!"))
  char(5)
  char(5)
  str(string("Henlo world!"))
  char(13)
  str(string("newline"))
  char(13)
  str(string("newline"))
  char(10)
  str(string("linefeed"))
  waitcnt(cnt+clkfreq)
  repeat 4000                 
    i := (i+1)&255
    char(255)
  char(1) 
  str(string("home"))
  char(11)
  waitcnt(cnt+clkfreq)
  char(7)
  char(12)
  str(string("clear below, ayy"))
  goto(20,20)
  str(string("goto, ayy"))
  waitcnt(cnt+clkfreq)
  char(0)
  str(string("color 0",13))
  waitcnt(cnt+clkfreq)
  char($101)
  str(string("color 1"))
  waitcnt(cnt+clkfreq)
  char(12)
  char($200+negx)
  char(13)
  char($102)
  str(string("color 2",13))
  char($103)
  str(string("color 3",13))
  char($104)
  str(string("color 4",13))
  char($105)
  str(string("color 5",13))
  char($106)
  str(string("color 6",13))
  char($107)
  str(string("color 7",13))
  char($108)
  str(string("color 8",13))
  char($109)
  str(string("color 9",13))
  char($10A)
  str(string("color 10",13))
  char($10B)
  str(string("color 11",13))
  char($10C)
  str(string("color 12",13))
  char($10D)
  str(string("color 13",13))
  char($10E)
  str(string("color 14",13))
  char($10F)
  str(string("color 15",13))
  char($110)
  str(string("color 16",13))
  char($111)
  str(string("color 17",13))
  char($112)
  str(string("color 18",13))
  char($113)
  str(string("color 19",13))
  char($114)
  str(string("color 20",13))
  
  

PRI char(c)
  repeat while thingy
  thingy := c+$100

PRI str(ptr) | c
  repeat
    c := byte[ptr++]
    if c == 0
      return
    char(c)

PRI goto(x,y)
  char(2)
  char(x)
  char(y)