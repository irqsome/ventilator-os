'' +--------------------------------------------------------------------------+
'' | ......OS   An Operating System for the Propeller Chip                    |
'' +--------------------------------------------------------------------------+
'' |                     HARDWARE DEFINITIONS              _HWDEF.SPIN        |
'' +--------------------------------------------------------------------------+
'' |  License            MIT License                                          |
'' +--------------------------------------------------------------------------+
'' RR20100216   Separate file: by Cluso99
'' RR20100217
'' RR20100221   TriBlade#2 change TV & Kbdata pins (SD conflict)
'' RR20120304   RamBlade/TriBlade#2/RamBlade3 hardware definitions
'' RR20120306
'' RR20120605   Document update only
'' RR20130719   correct _RB1 (was using _tb2D0)
'' RR20151210   add P8XBlade2 (12MHz, SD on P12..P15)
'' RR20151220   add CpuBlade7 (12MHz, SD on P8..P11)
'' RR20160121   add default xtal/pll modes to various hw
''              Note: Setting _clkmode = xtal1+pll16x <> $6F !!!
'' RR20160129                 _clkmode = xtal1+pll8x  <> $6E !!!
'' RR20160219   v1.00

CON

'' +--------------------------------------------------------------------------+
'' |            DEFAULT HARDWARE DEFINITIONS (change to suit your board)      |
'' |                                                                          |
'' |(note RamBlade/RamBlade3/TriBlade#2/P8XBlade2/CpuBlade7 set automatically)|
'' +--------------------------------------------------------------------------+
'' Note: 1. If you have a RamBlade/RamBlade3/TriBlade#2(second prop)/P8XBlade2/CpuBade7
''            the code will automatically detect the hardware defaults (further below).
''       2. For other boards, change the following clock setting appropriately,
''            and select appropriate pin settings.

'' Select one of the following for your hardware default
''   Note: _CLKMODE_ uses $6F/$6E, NOT xtal1 + pll16x/pll8x !!!

   _XINFREQ_     = 5_000_000     ' \ default setting
   _CLKFREQ_     = 80_000_000    ' |
   _CLKMODE_     = $6F           ' / xtal1 + pll16x
'  _XINFREQ_     = 6_000_000     ' \ set for 6.00MHz
'  _CLKFREQ_     = 96_000_000    ' |
'  _CLKMODE_     = $6F           ' / xtal1 + pll16x
'  _XINFREQ_     = 6_250_000     ' \ set for 6.25MHz
'  _CLKFREQ_     = 100_000_000   ' |
'  _CLKMODE_     = $6F           ' / xtal1 + pll16x
'  _XINFREQ_     = 6_500_000     ' \ set for 6.50MHz (below for RamBlade/RamBlade3/TriBlade#2)
'  _CLKFREQ_     = 108_000_000   ' |
'  _CLKMODE_     = $6F           ' / xtal1 + pll16x
'  _XINFREQ_     = 10_000_000    ' \ set for 10.00MHz
'  _CLKFREQ_     = 80_000_000    ' |
'  _CLKMODE_     = $6E           ' / xtal1 + pll8x
'  _XINFREQ_     = 12_000_000    ' \ set for 12.00MHz (below for P8XBlade2/CpuBlade7)
'  _CLKFREQ_     = 96_000_000    ' |
'  _CLKMODE_     = $6E           ' / xtal1 + pll8x


  _SIpin        = 31            ' \ usual serial input pin  (from TXD on pc)
  _SOpin        = 30            ' |              output pin (to   RXD on pc)
  _SIOmode      = 0             ' | usual mode
  _SIObaud      = 115200        ' / common baud (serial speed)
  _SIOpins      = _SIOmode << 16 | _SIpin << 8 | _SOpin

  _SDpin_CS     = 25            ' \ SD card pins: CS  (active low)
  _SDpin_DI     = 20            ' |               DI  (to SD)
  _SDpin_CLK    = 24            ' |               CLK (to SD)
  _SDpin_DO     = 21            ' |               DO  (fm SD)
' _SDpin_CD     = -1            ' | card detect   (can be DI)
' _SDpin_WP     = -1            ' / write protect (can be CLK) (not on microSD cards)
  _SDpins       = _SDpin_CS << 24 | _SDpin_DI << 16 | _SDpin_CLK << 8 | _SDpin_DO
  _RAMpin_CS    = 26

  _RTCpin_DAT   = -1            ' \ RTC (clock chip - often shared with EEPROM)
  _RTCpin_CLK   = -1            ' /
  

  _EEPROM_DAT   = 29            ' \ EEPROM
  _EEPROM_CLK   = 28            ' /
  _EEPROM_SIZE  = 64            ' 32/64/128KB
  _EEPROMpins   = _EEPROM_DAT << 8 | _EEPROM_CLK

  _KBDpin_CLK   = 9             ' \ PS2 Keyboard
  _KBDpin_DAT   = 8             ' /
  _KBDpins      = _KBDpin_CLK << 8 | _KBDpin_DAT

  _MOUSE_CLK    = 255           ' \ PS2 Mouse
  _MOUSE_DAT    = 255           ' /
  _MOUSEpins    = _MOUSE_CLK << 8 | _MOUSE_DAT

  _AUDIO_1      = 10            ' \ Audio out (chan 1 stereo or mono)
  _AUDIO_2      = 11            ' /           (chan 2 stereo)
  _AUDIOpins    = _AUDIO_2 << 8 | _AUDIO_1

  '_TVbasepin   = 12            ' \ TV (composite video)
 ' _TVpinmask   = $07           ' |   output pin mask
' _TVmode       = tbd           ' /   tbd: PAL/NTSC, timing?
   _TVpins      = %001_0101     ' TV pins in TV.spin format

  _VGAbasepin   = 0             ' \ VGA
  _VGApinmask   = $FF           ' |   output pin mask
' _VGAmode      = tbd           ' /   tbd: timing?

  '_SCREENROWS  = 25            ' \ Screen display size for text  (max 127)
  '_SCREENCOLS  = 40            ' /                               (max 255)

{
'' +--------------------------------------------------------------------------+
'' |            SPECIAL HARDWARE DEFINITIONS                                  |
'' |                                                                          |
'' |    Automatically detected for:                                           |
'' |     RamBlade/RamBlade3/TriBlade#2/P8XBlade2/CpuBlade7                    |
'' +--------------------------------------------------------------------------+

  _siRB1        = 23            'RamBlade SI/kbdData
  _soRB1        = 22            'RamBlade SO/tvPins
  _sioRB1       = _SIOmode << 16 | _siRB1 << 8 | _soRB1

  _tb2DO    =  9                '\ TriBlade2 microSD
  _tb2Clk   = 28                '|
  _tb2DI    =  8                '|
  _tb2CS    = 14                '|
  _tb2PU    = $3000_FF00        '/ pullups
  _tb2XTAL  = 6_500_000         '\ set for RamBlade/RamBlade3/TriBlade#2
  _tb2freq  = 104_000_000       '|
  _tb2PLL   = $6F               '/ xtal1 + pll16x

  _rb1DO    = 24                '\ RamBlade  microSD (aka RamBlade1)
  _rb1Clk   = 26                '|
  _rb1DI    = 25                '|
  _rb1CS    = 19                '|
  _rb1PU    = $3738_0000        '/ pullups
  _rb1XTAL  = 6_500_000         '\ set for RamBlade/RamBlade3/TriBlade#2
  _rb1freq  = 104_000_000       '|
  _rb1PLL   = $6F               '/ xtal1 + pll16x

  _rb3DO    = 20                '\ RamBlade3 microSD
  _rb3Clk   = 21                '|
  _rb3DI    = 22                '|
  _rb3CS    = 23                '|
  _rb3PU    = $3880_0000        '/ pullups
  _rb3XTAL  = 6_500_000         '\ set for RamBlade/RamBlade3/TriBlade#2
  _rb3freq  = 104_000_000       '|
  _rb3PLL   = $6F               '/ xtal1 + pll16x 

  _px2DO    = 12                '\ P8XBlade2 microSD
  _px2Clk   = 13                '|
  _px2DI    = 14                '|
  _px2CS    = 15                '|
  _px2PU    = $3000_8000        '/ pullups
  _px2XTAL  = 12_000_000        '\
  _px2freq  = 96_000_000        '|
  _px2PLL   = $6E               '/ xtal1 + pll8x  

  _cb7DO    =  8                '\ CpuBlade7 microSD
  _cb7Clk   =  9                '|
  _cb7DI    = 10                '|
  _cb7CS    = 11                '|
  _cb7PU    = $3000_0800        '/ pullups
  _cb7XTAL  = 12_000_000        '\
  _cb7freq  = 96_000_000        '|
  _cb7PLL   = $6E               '/ xtal1 + pll8x 

  'hardware value (3bits)
  _rb1val   = 1                 'RamBlade 
  _tb2val   = 2                 'TriBlade#2
  _rb3val   = 3                 'RamBlade3
  _px2val   = 4                 'P8XBlade2
  _cb7val   = 7                 'CpuBlade7
  _rb1 = _rb1val << 29 | _rb1CS << 24 | _rb1DI << 16 | _rb1Clk << 8 | _rb1DO
  _tb2 = _tb2val << 29 | _tb2CS << 24 | _tb2DI << 16 | _tb2Clk << 8 | _tb2DO
  _rb3 = _rb3val << 29 | _rb3CS << 24 | _rb3DI << 16 | _rb3Clk << 8 | _rb3DO
  _px2 = _px2val << 29 | _px2CS << 24 | _px2DI << 16 | _px2Clk << 8 | _px2DO
  _cb7 = _cb7val << 29 | _cb7CS << 24 | _cb7DI << 16 | _cb7Clk << 8 | _cb7DO }

  {_kdPin = 23                                          '1-pin PS2 keyboard
  _kcPin = -1 & $FF                                     'not used
  _ktime = 8200
  _kbdPar = _ktime << 16 | _kcPin << 8 | _kdpin}

  {_tvPina = -1 & $FF                                   'not used
  _tvPinb = -1 & $FF                                    'not used
  _tvPinc = 22                                          'best for 1-pin TV
  _tvPind = -1 & $FF
  _tvPins = _tvPind << 24 | _tvPinc << 16 | _tvPinb << 8 | _tvPina}

{==============================================================================}
{{  
'' Below are some other board settings (using older names)

'Demo/proto
'==========
  _xinfreq = 5_000_000
' _clkfreq = 80_000_000        
  _clkmode = xtal1 + pll16x

  rxPin  = 31                                           'serial
  txPin  = 30
  baud   = 115200

  kdPin  = 26                                           'PS2 keyboard 
  kcPin  = 27

  tvPina = 12                                           'TV
  tvPinb = 13
  tvPinc = 14
  tvPind = 15

  spiDO  = 0                                            'SD pins
  spiClk = 1
  spiDI  = 2
  spiCS  = 3

'Hybrid
'========
  _xinfreq = 6_000_000        
' _clkfreq = 96_000_000        
  _clkmode = xtal1 + pll16x

  rxPin  = 31                                           'serial
  txPin  = 30
  baud   = 115200

  kdPin  = 12                                           'PS2 keyboard 
  kcPin  = 13

  tvPina = 24                                           'TV
  tvPinb = 25
  tvPinc = 26
  tvPind = 27

  spiDO  = 8                                            'SD pins
  spiClk = 9
  spiDI  = 10
  spiCS  = 11

'Hydra
'========
  _xinfreq = 10_000_000        
' _clkfreq = 80_000_000        
  _clkmode = xtal1 + pll8x

  rxPin  = 31                                           'serial
  txPin  = 30
  baud   = 115200

  kdPin  = 13                                           'PS2 keyboard 
  kcPin  = 12

  tvPina = 24                                           'TV
  tvPinb = 25
  tvPinc = 26
  tvPind = 27

  spiDO  = 16                                           'SD pins
  spiClk = 17
  spiDI  = 18
  spiCS  = 19

}}

PUB dummy