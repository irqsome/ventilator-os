'' +--------------------------------------------------------------------------+
'' | Sphinx:  A Compiler for the Propeller Chip     LEX / CODEGEN / LINK.SPIN |
'' |                                  sub-module:   _STRINGX.SPIN             |
'' +--------------------------------------------------------------------------+
'' |  Authors:                                                                |
'' |  (c) 2009,2010      Michael Park (mpark)       (original Sphinx compiler)|
'' |  (c) 2015,2016      Ray Rodrick (Cluso99)      (include in PropOS)       |
'' |  License            MIT License - See end of file for terms of use       |
'' +--------------------------------------------------------------------------+
'' mp2009
'' RR20160217      rename _stringx.spin
'' RR20160228      works
''                 add tokenizeString & ignoreSpace (for _codegen)

pub stringCompare( p, q ) | lp, lq, d
{{
  Compares two strings. Returns a negative number if first string is "less than" the second,
  0 if they're identical, a positive number otherwise.
}}
  lp := strsize(p)
  lq := strsize(q)
  repeat lp <# lq 
    if (d := byte[p++] - (byte[q++] & $7f))
      return ~~d
  return lp-lq

PUB stringCopyUnsafe(whereToPut, whereToGet)

  return stringCopy(whereToPut, whereToGet,posx)

PUB stringCopy(whereToPut, whereToGet,bufferSize)

'' ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'' // Copies a string from one location to another. This method can corrupt memory.
'' //
'' // Returns a pointer to the new string.
'' //
'' // WhereToPut - Address of where to put the copied string.
'' // WhereToGet - Address of where to get the string to copy.
'' ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  result := strsize(whereToGet) <# (bufferSize-1)
  
  bytemove(whereToPut, whereToGet, result)
  byte[whereToPut+result]~

PUB stringAppend(whereToPut, whereToGet,bufferSize)

'' ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'' // Appends/Concatenates a string onto the end of another. This method can corrupt memory.
'' //
'' // Returns a pointer to the new string.
'' //
'' // WhereToPut - Address of the string to append/concatenate a string to.
'' // WhereToGet - Address of where to get the string to append/concatenate.
'' ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  result := strsize(whereToPut)
  return StringCopy(whereToPut+result,whereToGet,bufferSize-result)

pub stringToUpperCase( p )
  repeat while byte[p]
    if "a" =< byte[p] and byte[p] =< "z"
      byte[p] += constant( "A" - "a" )
    ++p

VAR
  word tokenStringPointer

PUB tokenizeString(characters) '' 8 Stack Longs

'' ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'' // Removes white space and new lines arround the inside of a string of characters.
'' //
'' // Returns a pointer to the tokenized string of characters, or an empty string when out of tokenized strings of characters.
'' //
'' // Characters - A pointer to a string of characters to be tokenized, or null to continue tokenizing a string of characters.
'' ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  if(characters)
    tokenStringPointer := characters

  result := tokenStringPointer := ignoreSpace(tokenStringPointer)

  repeat while(byte[tokenStringPointer])
    case byte[tokenStringPointer++]
      8 .. 13, 32, 127:
        byte[tokenStringPointer - 1] := 0
        quit

PRI ignoreSpace(characters) ' 4 Stack Longs

  result := characters
  repeat strsize(characters--)
    case byte[++characters]
      8 .. 13, 32, 127:
      other: return characters

{{
Copyright (c) 2009 Michael Park
+------------------------------------------------------------------------------------------------------------------------------+
|                                                   TERMS OF USE: MIT License                                                  |
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    |
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}
                 