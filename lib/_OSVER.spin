'' +--------------------------------------------------------------------------+
'' | VentilatorOS                       - OS Version info                     |
'' +--------------------------------------------------------------------------+
'' |  Authors:       (c)2011 by Kwabena W. Agyman (Kye) & J. Moxham (Dr_Acula)|
'' |                 (c)2012-2016 "Cluso99" Ray Rodrick                       |
'' |                 (c)2009-2010 "mpark"   Michael Park (Sphinx compiler)    |
'' |                 (c)2019      "Wuerfel_21" Ada Gottensträter              |
'' |  License:       MIT License - See end of file for terms of use           |
'' +--------------------------------------------------------------------------+


CON

major = 2
minor = 1
revision = 0

DAT
_distroname byte "VentilatorOS",0

PUB distro_name
  return @_distroname

PUB hw_vendor(hwid)
    case hwid >> 16  'hardware >> 29
      $0001 : return string("Cluso's")
      $0ADA : return string("IRQsome Software")
      other : return 0

PUB hw_name(hwid)
  case hwid  'hardware >> 29
      $0001_0001 : return(string("RamBlade"))
      $0001_0002 : return(string("TriBlade#2"))
      $0001_0003 : return(string("RamBlade3"))
      $0001_0004 : return(string("P8XBlade2"))
      $0001_0007 : return(string("CpuBlade7"))
      $0ADA_0001 : return(string("Ventilator"))
      other: return 0     